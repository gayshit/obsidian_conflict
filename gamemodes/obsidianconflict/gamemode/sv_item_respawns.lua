local sv_oc_weapon_respawn_time = CreateConVar("sv_oc_weapon_respawn_time", "20", bit.bor(FCVAR_NOTIFY, FCVAR_REPLICATED))
local sv_oc_item_respawn_time = CreateConVar("sv_oc_item_respawn_time", "30", bit.bor(FCVAR_NOTIFY, FCVAR_REPLICATED))

GM.RespawnQueue = {}
function GM:RespawnObject(ent, delay)
	if not self:IsDesignerPlacedEnt(ent) then return end

	delay = delay or 1
	for _, v in next, self.RespawnQueue do
		if v.item == ent then
			v.respawnTime = CurTime() + delay
			v.delay = delay

			return
		end
	end

	local data = {
		class = ent:GetClass(),
		pos = ent.InitialSpawnData and ent.InitialSpawnData.Pos or ent:GetPos(),
		ang = ent.InitialSpawnData and ent.InitialSpawnData.Ang or ent:GetAngles(),
		model = ent:GetModel(),
		name = ent:GetName(),
		respawnTime = CurTime() + delay,
		delay = delay,
		item = ent,
		kvs = {
			Model = ent:GetModel(),
			AmmoName = ent.AmmoType,
			Amount = ent.AmmoAmount,
		},
		creationID = ent:MapCreationID()
	}

	table.insert(self.RespawnQueue, data)
end

local function RespawnValid(data)
	local ent = data.item

	local bIsValid = IsValid(ent)
	if bIsValid and ent:IsWeapon() and ent:GetOwner():IsValid() then
		bIsValid = false
	end

	-- just despawned, update the respawn time
	if not bIsValid and data.m_bWasValid then
		data.respawnTime = CurTime() + data.delay
	end
	data.m_bWasValid = bIsValid

	return not bIsValid
end
function GM:UpdateItemRespawn()
	local ct = CurTime()

	for k, data in next, self.RespawnQueue do
		if not RespawnValid(data) then continue end

		if ct >= data.respawnTime then
			local e = ents.Create(data.class)
			e:SetPos(data.pos)
			e:SetAngles(data.ang)
			e:SetName(data.name)
			e:SetModel(data.model)

			for key, val in next, data.kvs do
				e:SetKeyValue(key, val)
			end

			e.oc_creationID = data.creationID
			e.IsDesignerPlaced = true

			e:Spawn()
			e:Activate()
			e.InitialSpawnData = data

			-- respawn effects
			if data.delay > 0.0 then
				e:EmitSound("AlyxEmp.Charge")

				local effectdata = EffectData()
				effectdata:SetOrigin(data.pos)
				effectdata:SetScale(1)
				effectdata:SetMagnitude(5)
				util.Effect("ElectricSpark", effectdata)
			end

			table.remove(self.RespawnQueue, k)
		end
	end
end

function GM:WeaponEquip(wep, owner)
	if not IsValid(owner) then return end

	if not wep.IsSpawnItem and wep.CreatedForPlayer ~= owner and not wep.DroppedByPlayer and not self.AMMO_LIKE_WEAPONS[wep:GetClass()] then
		self:RespawnObject(wep, sv_oc_weapon_respawn_time:GetFloat())
	end

	wep.CreatedForPlayer = nil
end

sk_max_pistol = GetConVar("sk_max_pistol")
sk_max_smg1 = GetConVar("sk_max_smg1")
sk_max_smg1_grenade = GetConVar("sk_max_smg1_grenade")
sk_max_357 = GetConVar("sk_max_357")
sk_max_ar2 = GetConVar("sk_max_ar2")
sk_max_ar2_altfire = GetConVar("sk_max_ar2_altfire")
sk_max_buckshot = GetConVar("sk_max_buckshot")
sk_max_crossbow = GetConVar("sk_max_crossbow")
sk_max_grenade = GetConVar("sk_max_grenade")
sk_max_rpg_round = GetConVar("sk_max_rpg_round")
sk_max_alyxgun = GetConVar("sk_max_alyxgun")
sk_max_sniper_round = GetConVar("sk_max_sniper_round")
sk_plr_dmg_crowbar = GetConVar("sk_plr_dmg_crowbar")
sk_npc_dmg_crowbar = GetConVar("sk_npc_dmg_crowbar")
sk_plr_dmg_stunstick = GetConVar("sk_plr_dmg_stunstick")
sk_npc_dmg_stunstick = GetConVar("sk_npc_dmg_stunstick")
sk_max_slam = GetConVar("sk_max_slam") -- creation moved to shared
-- For compatibility reasons we need those ConVars.


function GM:IsDesignerPlacedEnt(ent)
	if not ent:IsValid() then return false end

	if ent.IsDesignerPlaced then return true end
	if ent:MapCreationID() >= 1 then return true end

	return false
end

function GM:PlayerCanPickupAmmo(ply, ent)
	local class = ent:GetClass()
	local primaryFull = true
	local secondaryFull = true

	if ent.CreatedForPlayer == ply then
		return true
	end
	if ent.IsSpawnItem then
		return true
	end

	local CheckAmmoFull = function(ammoType, clipSize)
		if ammoType ~= -1 then
			local ammoName = game.GetAmmoName(ammoType)
			local cur = ply:GetAmmoCount(ammoType)
			local ammoMax = self.MAX_AMMO_DEF[ammoName]
			if clipSize == -1 then
				clipSize = 0
			end
			if ammoMax ~= nil then
				ammoMax = ammoMax:GetInt()
			else
				ammoMax = 9999
			end
			if cur >= ammoMax then
				return true
			end
		end

		return false
	end

	if ent:IsWeapon() then
		local clip1 = ent:Clip1()
		local clip2 = ent:Clip2()
		if clip1 > 0 and clip2 > 0 then
			primaryFull = CheckAmmoFull(ent:GetPrimaryAmmoType(), ent:Clip1())
			secondaryFull = CheckAmmoFull(ent:GetSecondaryAmmoType(), ent:Clip2())
			return primaryFull == false and secondaryFull == false
		elseif clip1 > 0 then
			primaryFull = CheckAmmoFull(ent:GetPrimaryAmmoType(), ent:Clip1())
			return primaryFull == false
		end
	end

	local ammo = self.ITEM_DEF[class]
	if ammo then
		local cur = ply:GetAmmoCount(ammo.Type)
		local ammoMax = ammo.Max:GetInt()
		if cur >= ammoMax then
			return false
		end
	end

	-- item_custom ammo
	if ent.AmmoType then
		local ammoMax = game.GetAmmoMax(game.GetAmmoID(ent.AmmoType))
		if ply:GetAmmoCount(ent.AmmoType) >= ammoMax then
			return false
		end
	end

	return true
end

function GM:PlayerCanPickupWeapon(ply, wep)
	if not IsValid(wep) then return end

	if wep.CreatedForPlayer then
		if wep.CreatedForPlayer == ply then
			if ply:HasWeapon(wep:GetClass()) then
				wep:Remove()
			end

			return true
		else
			return false
		end
	end
	if wep.IsSpawnItem then
		if ply:HasWeapon(wep:GetClass()) then
			wep:Remove()
		end

		return true
	end

	if self.AMMO_LIKE_WEAPONS[wep:GetClass()] then
		return self:PlayerCanPickupItem(ply, wep)
	end

	local flRespawnTime = sv_oc_weapon_respawn_time:GetFloat()

	if ply:HasWeapon(wep:GetClass()) then
		if wep:GetClass() == "weapon_healer" then return false end
		if not self:PlayerCanPickupAmmo(ply, wep) then
			return false
		end

		-- ammo stealing
		local clip1 = wep:Clip1()
		if clip1 > 0 then
			ply:GiveAmmo(clip1, wep:GetPrimaryAmmoType(), false)
			self:RespawnObject(wep, flRespawnTime)
			wep:Remove()
			return true
		end

		local clip2 = wep:Clip2()
		if clip2 > 0 then
			ply:GiveAmmo(clip2, wep:GetSecondaryAmmoType(), false)
			self:RespawnObject(wep, flRespawnTime)
			wep:Remove()
			return true
		end

		return false
	end

	self:RespawnObject(wep, flRespawnTime)
	return true
end

function GM:PlayerCanPickupItem(ply, item)
	if item.NotForPlayer then
		if item.NotForPlayer == ply then
			return false
		end
	elseif item.CreatedForPlayer then
		if item.CreatedForPlayer == ply then
			return true
		else
			return false
		end
	end

	if item.IsSpawnItem then
		return true
	end

	local class = item:GetClass()

	-- Dont pickup stuff if we dont need it.
	if class == "item_health" or class == "item_healthvial" or class == "item_healthkit" then
		if ply:Health() >= ply:GetMaxHealth() then
			return false
		end
	elseif class == "item_battery" then
		if ply:Armor() >= 100 then return false end
	elseif class == "item_suit" then
		return not ply:IsSuitEquipped()
	end

	if not self:PlayerCanPickupAmmo(ply, item) then
		return false
	end

	self:RespawnObject(item, sv_oc_item_respawn_time:GetFloat())
	return true
end
