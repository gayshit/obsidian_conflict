-- made by homonovus STEAM_0:0:41908082

local TAG = "LuaHudMenu"
util.AddNetworkString(TAG)

local MENU = {}
MENU.__index = MENU
MENU.__metatable = MENU

MENU.m_MenuString = ""
MENU.m_iAllowedMask = 0xffff
MENU.m_iDisplayTime = -1
MENU.m_Recipients = {}
MENU.m_RecipientsList = {}

function MENU:AddLiveRecipient(ply)
	if not (isentity(ply) and ply:IsValid() and ply:IsPlayer() and not ply:IsBot()) then return end

	if not self.m_Recipients[ply] then
		self.m_Recipients[ply] = true
		table.insert(self.m_RecipientsList, ply)

		--net.Start(TAG)
		--    net.WriteUInt(allowed, 16)          -- allowed input bitflag
		--    net.WriteUInt(display_time, 8)      -- display time (-1 means unlimited)
		--    net.WriteString(self.m_MenuString)  -- buffer
		--net.Send(ply)
	end
end

function MENU:RecalculateMenu(viewer)
	self.m_MenuString = ""
end

function MENU:Display(viewer, allowed, display_time)
	self:RecalculateMenu(viewer)

	display_time = display_time or self.m_iDisplayTime or -1
	allowed = allowed or self.m_iAllowedMask

	local curtime = CurTime()
	local viewerTable = viewer:GetTable()

	-- Only display if the menu's not identical to the one the player's already seeing
	if (viewerTable.m_MenuUpdateTime or 0) > curtime then
		if allowed == viewerTable.m_MenuSelectionBuffer and self.m_MenuString == viewerTable.m_MenuStringBuffer then
			return
		end
	end

	viewerTable.m_MenuSelectionBuffer = self.m_MenuString
	viewerTable.m_MenuUpdateTime = curtime + 10
	viewerTable.m_MenuSelectionBuffer = allowed

	net.Start(TAG)
	net.WriteUInt(allowed, 16)        -- allowed input bitflag
	net.WriteUInt(display_time, 8)    -- display time (-1 means unlimited)
	net.WriteString(self.m_MenuString) -- buffer
	net.Send(self.m_RecipientsList)
end

function MENU:Input(ply, iInput) end

function MENU:OnReset(viewer) end

concommand.Add("lua_menuselect", function(ply, cmd, args, argstr)
	if #args < 1 then return end

	local slot = tonumber(args[1])
	if not slot then return end

	local plyTable = ply:GetTable()

	local menu = plyTable.m_CurrentMenu
	if not menu then return end

	local curtime = CurTime()
	if menu:Input(ply, slot) then
		plyTable.m_MenuUpdateTime = curtime
		plyTable.m_MenuRefreshTime = curtime
	end
end)

local PLAYER = FindMetaTable("Player")

local MENU_UPDATETIME = 2.0

function PLAYER:HudMenuDisplay(selfTable)
	selfTable = selfTable or self:GetTable()

	local menu = selfTable.m_CurrentMenu
	if not menu then
		selfTable.m_MenuRefreshTime = 0
		return
	end

	local curtime = CurTime()
	if (selfTable.m_MenuRefreshTime or 0) > curtime then
		selfTable.m_MenuRefreshTime = math.min(selfTable.m_MenuRefreshTime or 0, curtime + MENU_UPDATETIME)
		return
	end

	selfTable.m_MenuRefreshTime = curtime + MENU_UPDATETIME

	if menu.OnOpen then
		menu:OnOpen(self)
	end

	menu:Display(self)
end

function PLAYER:HudMenuInput(iInput)
	local menu = self.m_CurrentMenu
	if menu then
		return menu:Input(self, iInput)
	end

	return false
end

function PLAYER:HudMenuReset(selfTable)
	selfTable = selfTable or self:GetTable()

	local menu = selfTable.m_CurrentMenu

	if menu and menu.OnReset then
		menu:OnReset(self)
	end

	net.Start(TAG)
	net.WriteUInt(0, 16) -- allowed input bitflag
	net.WriteUInt(0, 8) -- display time (-1 means unlimited)
	net.WriteString("") -- buffer
	net.Send(self)

	selfTable.m_MenuStringBuffer = ""
	selfTable.m_MenuRefreshTime = 0
	selfTable.m_MenuDisplayTime = 0

	selfTable.m_CurrentMenu = nil
end

_G.LuaHudMenus = {}
local MENUS = LuaHudMenus

function PLAYER:OpenHudMenu(strMenuName)
	local selfTable = self:GetTable()

	--local currentMenu = selfTable.m_CurrentMenu

	PLAYER.HudMenuReset(self, selfTable)

	local desiredMenu = MENUS[strMenuName]
	if desiredMenu then
		local newMenu = setmetatable({
			m_Recipients = {},
			m_RecipientsList = {},
		}, desiredMenu)
		selfTable.m_CurrentMenu = newMenu

		newMenu:AddLiveRecipient(self)
		PLAYER.HudMenuDisplay(self)

		return newMenu
	end
end

-- some random interaction prompt i made
local INTERACTION = {}
do
	function INTERACTION:Input(ply, iInput)
		local iPage = ply.HudMenu_page or 1

		local iQuitSelect = 2

		if iPage >= 3 then
			ply.HudMenu_page = 1
		else
			if iPage == 3 then
				iQuitSelect = 1
			end

			if iInput < iQuitSelect then
				ply.HudMenu_page = iPage + 1
				self:Display(ply)
			else
				ply.HudMenu_page = 1
			end
		end

		return true
	end

	function INTERACTION:RecalculateMenu(viewer)
		local viewer_stage = viewer.HudMenu_page or 1

		if viewer_stage == 1 then
			self.m_MenuString = string.format("Hello %s.\n->1.Hello.\n->2.Goodbye.", viewer:Nick())
		elseif viewer_stage == 2 then
			self.m_MenuString = "Welcome to page 2...\n->1.What happens now?\n->2.Goodbye."
		elseif viewer_stage == 3 then
			self.m_MenuString = "Nothing, goodbye.\n->1.Goodbye."
		end
	end

	function INTERACTION:OnReset(viewer)
		viewer.HudMenu_page = 1
	end

	MENUS["interactive_test"] = setmetatable(INTERACTION, MENU)
end

-- "Hello world" type
-- shows how to only allow certain keys on the client
local TEST = {}
do
	TEST.__index = TEST
	-- only allow 1, 2, 4, and 0 to be pressed
	TEST.m_iAllowedMask = bit.bor(1, bit.lshift(1, 1), bit.lshift(1, 3), bit.lshift(1, 9))

	function TEST:RecalculateMenu(viewer)
		self.m_MenuString = "Hello world!!!!!!\nI love CHudMenu, but this isn't it.\nThis is LuaHudMenu :D\n \n->1.Option 1...\n->2.Option 2...\n \nYou can put text in the middle!\n->And make it yellow, too!\n \n3.Option 3...\n->4.Option 4...\n \n0. Exit"
	end

	MENUS["test"] = setmetatable(TEST, MENU)
end

-- pagination
-- give it a list and it will generate pages if needed
local PAGINATION = {}
do
	PAGINATION.__index = PAGINATION
	PAGINATION.m_Options = {}
	PAGINATION.m_Page = 1

	PAGINATION.m_Title = "Super cool menu"
	PAGINATION.m_CustomCloseText = "Cancel"

	PAGINATION.m_ItemsPerPage = 7
	PAGINATION.m_bShowPages = true
	PAGINATION.m_bStaticPageTurners = true
	PAGINATION.m_bPadEmptyPages = true
	PAGINATION.m_bShowCancelButton = true
	PAGINATION.m_bSelectableOptions = true

	-- fill with random crap
	for i = 1, 30 do
		PAGINATION.m_Options[i] = string.format("Option %d...", i, i)
	end

	function PAGINATION:OptionSelected(viewer, iInput, iIndex, option)
		-- for override
	end

	--- @return boolean close whether the menu should close or not
	function PAGINATION:CloseSelected(viewer)
		-- for override
		return true
	end

	function PAGINATION:GetOption(i) -- for override
		return self.m_Options[i]
	end

	function PAGINATION:GetOptionTitle(i) -- for override
		return self.m_Options[i]
	end

	function PAGINATION:GetOptionsLength() -- for override
		return #self.m_Options
	end

	function PAGINATION:Input(ply, iInput)
		--if iInput == 10 then return true end

		local iCurPage = ply.HudMenu_page or 1

		local iItemsPerPage = math.Clamp(self.m_ItemsPerPage, 1, 7)
		local iTotalPages = math.ceil(self:GetOptionsLength() / iItemsPerPage)

		if iInput == 8 then -- previous page
			ply.HudMenu_page = math.max(iCurPage - 1, 1)
			self:Display(ply)
		elseif iInput == 9 then -- next page
			ply.HudMenu_page = math.min(iCurPage + 1, iTotalPages)
			self:Display(ply)
		elseif iInput == 10 then
			if self:CloseSelected(ply) then
				ply.m_CurrentMenu = nil
			end
		else
			local iIndex = ((iCurPage - 1) * iItemsPerPage) + iInput
			local selectedOption = self:GetOption(iIndex)

			--local title = self:GetOptionTitle(iIndex)
			--if #title:Trim() == 0 then
			--    repeat
			--        iIndex = iIndex + 1
			--    until iIndex >= (iCurPage * iItemsPerPage) or #self:GetOptionTitle(iIndex):Trim() > 0
			--end

			self:OptionSelected(ply, iInput, iIndex, selectedOption)
		end
	end

	function PAGINATION:RecalculateMenu(viewer)
		local iItemsPerPage = math.Clamp(self.m_ItemsPerPage, 1, 7)
		local iOptionsLength = self:GetOptionsLength()
		local iTotalPages = math.ceil(iOptionsLength / iItemsPerPage)

		local iCurPage = viewer.HudMenu_page or 1

		local str = {}

		if self.m_bShowPages then
			table.insert(str, string.format("%s (page %d/%d)", self.m_Title, iCurPage, iTotalPages))
		else
			table.insert(str, self.m_Title)
		end

		table.insert(str, " ") -- spacer

		local mask = self:ProcessItems(viewer, str)

		table.insert(str, " ") -- spacer

		if iCurPage > 1 then
			table.insert(str, "->8. Prev page")
			mask = bit.bor(mask, bit.lshift(1, 7))
		elseif self.m_bStaticPageTurners then
			table.insert(str, "8. Prev page")
		elseif self.m_bPadEmptyPages then
			table.insert(str, " ")
		end

		if iCurPage < iTotalPages then
			table.insert(str, "->9. Next page")
			mask = bit.bor(mask, bit.lshift(1, 8))
		elseif self.m_bStaticPageTurners then
			table.insert(str, "9. Next page")
		elseif self.m_bPadEmptyPages then
			table.insert(str, " ")
		end

		-- put a cancel on the end
		if self.m_bShowCancelButton then
			table.insert(str, string.format("->0. %s", self.m_CustomCloseText))
			mask = bit.bor(mask, bit.lshift(1, 9))
		end

		self.m_MenuString = table.concat(str, "\n")
		self.m_iAllowedMask = mask
	end

	function PAGINATION:ProcessItems(viewer, output)
		if not output then return end

		local iItemsPerPage = math.Clamp(self.m_ItemsPerPage, 1, 7)
		local iOptionsLength = self:GetOptionsLength()

		local iCurPage = viewer.HudMenu_page or 1

		local mask = 0

		local iItemsAdded = 0
		for i = 1, iItemsPerPage do
			local index = ((iCurPage - 1) * iItemsPerPage) + i
			if index <= iOptionsLength then
				local title = self:GetOptionTitle(index)

				if #title:Trim() == 0 then
					table.insert(output, " ")
				else
					if self.m_bSelectableOptions then
						mask = bit.bor(mask, bit.lshift(1, iItemsAdded))
						table.insert(output, string.format("->%d. %s", iItemsAdded + 1, title))
					else
						table.insert(output, string.format("%d. %s", iItemsAdded + 1, title))
					end

					iItemsAdded = iItemsAdded + 1
				end
			elseif self.m_bPadEmptyPages then
				table.insert(output, " ")
			else
				break
			end
		end

		return mask
	end

	function PAGINATION:OnReset(viewer)
		viewer.HudMenu_page = 1
	end

	function PAGINATION:OnOpen(viewer)
		viewer.HudMenu_page = 1
	end

	MENUS["pagination"] = setmetatable(PAGINATION, MENU)
end

local DEBUG = {}
do
	DEBUG.__index = DEBUG
	DEBUG.m_Options = {
		{title = "\"test\"",        open = "test"},
		{title = "\"interactive\"", open = "interactive_test"},
		{title = "pagination",      open = "pagination"},
	}

	DEBUG.m_Title = "LuaHudMenu debug"

	DEBUG.m_bShowPages = false
	DEBUG.m_bStaticPageTurners = false
	DEBUG.m_bPadEmptyPages = false

	-- the only modification needed to turn this into object oriented
	function DEBUG:GetOptionTitle(i)
		local opt = self:GetOption(i)

		return opt.title
	end

	function DEBUG:OptionSelected(viewer, iInput, iIndex, option)
		viewer:OpenHudMenu(option.open)
	end

	MENUS["debug"] = setmetatable(DEBUG, PAGINATION)
end

-- player selection list
local PLAYER_SELECT = {}
do
	PLAYER_SELECT.__index = PLAYER_SELECT
	PLAYER_SELECT.m_tPlayers = {}
	PLAYER_SELECT.m_Title = "Player select"
	PLAYER_SELECT.m_bRemoveViewer = false

	function PLAYER_SELECT:GetOptionsLength()
		return #self.m_tPlayers
	end

	function PLAYER_SELECT:GetOption(i)
		return self.m_tPlayers[i]
	end

	function PLAYER_SELECT:OnOpen(viewer)
		if table.IsEmpty(self.m_tPlayers) then
			self.m_tPlayers = player.GetAll()

			for i, v in ipairs(self.m_tPlayers) do
				if v == viewer then
					table.remove(self.m_tPlayers, i)
					break
				end
			end

			if not self.m_bRemoveViewer then
				table.insert(self.m_tPlayers, 1, viewer)
			end
		end
	end

	function PLAYER_SELECT:ProcessItems(viewer, output)
		if not output then return 0 end

		local iCurPage = viewer.HudMenu_page or 1
		local iItemsPerPage = math.Clamp(self.m_ItemsPerPage, 1, 7)

		local mask = 0
		local iItemsAdded = 0

		local iOptionsLength = self:GetOptionsLength()
		local iMaxNameLength = 0

		-- find longest name length
		for index = ((iCurPage - 1) * iItemsPerPage) + 1, (iCurPage * iItemsPerPage) do
			if index > iOptionsLength then break end

			local ply = self:GetOption(index)
			local name = ply:Nick()

			if #name > iMaxNameLength then
				iMaxNameLength = #name
			end
		end

		for index = ((iCurPage - 1) * iItemsPerPage) + 1, (iCurPage * iItemsPerPage) do
			if index <= iOptionsLength then
				local ply = self:GetOption(index)

				local timeStr = ""
				do
					local time = ply:TimeConnected()
					local seconds = time % 60
					local minutes = math.floor(time / 60)
					local hours = math.floor(minutes / 60)

					minutes = minutes % 60
					timeStr = string.format("%.2d:%.2d:%.2d", hours, minutes, seconds)
				end

				local name, tabs = ply:Nick(), " "
				if #name < iMaxNameLength then
					tabs = string.rep(" ", iMaxNameLength - #name)
				end

				local title = string.format("%s%s[%s](%s)", ply:Nick(), tabs, timeStr, ply:SteamID())

				mask = bit.bor(mask, bit.lshift(1, iItemsAdded))
				table.insert(output, string.format("->%d. %s", iItemsAdded + 1, title))

				iItemsAdded = iItemsAdded + 1
			elseif self.m_bPadEmptyPages then
				table.insert(output, " ")
			else
				break
			end
		end

		return mask
	end

	function PLAYER_SELECT:OptionSelected(viewer, iInput, iIndex, option)
		-- for override
	end

	MENUS["player_select"] = setmetatable(PLAYER_SELECT, MENUS["pagination"])
end

--local ply = Entity(1)
--ply:OpenHudMenu("test")
