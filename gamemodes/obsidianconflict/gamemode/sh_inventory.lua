AddCSLuaFile()

if SERVER then
	function Obsidian.PlayerGetHeldObject(ply)
		local obj = ply.oc_heldobj

		if IsValid(obj) then
			if obj:IsPlayerHolding() then
				if obj.oc_plyholding == ply then
					return obj
				end
			else
				obj.oc_plyholding = nil
			end
		end

		return NULL
	end

	function Obsidian.PlayerDropEntireInventory(ply)
		local obj = ply:GetNWEntity("oc_inv_prop")
		if obj:IsValid() then
			obj:SetParent(NULL)

			obj:SetSolid(obj.oc_orig_solid)
			obj:SetPos(ply:LocalToWorld(ply:OBBCenter()))
			obj:SetNoDraw(false)

			ply:SetNWEntity("oc_inv_prop", NULL)
		end

		if ply:GetHasCloak() then
			ply:SetHasCloak(false)

			--local ent = ents.Create("item_cloak")
			--ent:SetPos(ply:LocalToWorld(ply:OBBCenter()))
			--ent:Spawn()
		end
		if ply:GetHasShield() then
			ply:SetHasShield(false)

			--local ent = ents.Create("item_shield")
			--ent:SetPos(ply:LocalToWorld(ply:OBBCenter()))
			--ent:Spawn()
		end
		-- drop backpack of ammo/guns/etc
	end

	--local sv_maxinvobjectweight = GetConVar "sv_maxinvobjectweight"
	concommand.Add("inventory_store", function(ply)
		local obj = Obsidian.PlayerGetHeldObject(ply)
		if obj:IsValid() then
			if ply:GetNWEntity("oc_inv_prop"):IsValid() then
				ply:ChatPrint"you are already holding an object in your inventory"
				return
			end

			ply:SetNWEntity("oc_inv_prop", obj)

			obj.oc_orig_solid = obj:GetSolid()
			ply:DropObject()
			ply.oc_heldobj = nil
			ply:SetNW2Entity("oc_heldobj", NULL)

			obj:SetParent(ply)
			obj:SetLocalPos(vector_origin)
			obj:SetSolid(SOLID_NONE)
			obj:SetNoDraw(true)
		end
	end)

	concommand.Add("inventory_dropprop", function(ply)
		local obj = ply:GetNWEntity("oc_inv_prop")
		if obj:IsValid() then
			obj:SetParent(NULL)
			obj:SetSolid(obj.oc_orig_solid)
			obj:SetPos(ply:GetShootPos())
			obj:SetNoDraw(false)

			ply:DropObject()

			timer.Simple(0, function()
				if ply:IsValid() then
					ply:SetNWEntity("oc_inv_prop", NULL)

					if obj:IsValid() and hook.Run("AllowPlayerPickup", ply, obj) then
						obj:SetAbsVelocity(vector_origin)

						local objPhys = obj:GetPhysicsObject()
						if objPhys:IsValid() and not objPhys:IsCollisionEnabled() then
							objPhys:EnableCollisions(true)
						end

						ply:PickupObject(obj)
					end
				end
			end)
		end
	end)

	function GM:OnPlayerPhysicsPickup(ply, ent)
		ply.oc_heldobj = ent
		ent.oc_plyholding = ply

		ply:SetNW2Entity("oc_heldobj", ent)
	end

	function GM:OnPlayerPhysicsDrop(ply, ent, thrown)
		ply.oc_heldobj = nil
		ent.oc_plyholding = nil

		ply:SetNW2Entity("oc_heldobj", NULL)
	end
end
