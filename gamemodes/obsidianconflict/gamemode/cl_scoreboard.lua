local ScreenScaleH = ScreenScaleH
if not ScreenScaleH then
	ScreenScaleH = function(n)
		return n * (ScrH() / 480)
	end
end

local GM = GM or GAMEMODE

surface.CreateFont("oc_scoreboard_title", {
	font = "Obsidian",
	size = 32,
	weight = 600,
	antialias = true,
})
surface.CreateFont("oc_scoreboard_label", {
	font = "Obsidian",
	size = 18,
	weight = 600,
	antialias = true,
})
surface.CreateFont("oc_scoreboard_text", {
	font = "Obsidian",
	size = 18,
	weight = 500,
	antialias = true,
})

local mp_livesmode = GetConVar("mp_livesmode")

---@class Panel
local PLAYER_LINE = {}
do
	function PLAYER_LINE:Init()
		self:SetTall(28)

		self.avatar = vgui.Create("AvatarImage", self)
		self.avatar:SetSize(32, 32)
		self.avatar:SetPos(16, -2)
		self.avatar:SetCursor("hand")
		self.avatar.OnMousePressed = function(s, key)
			if self.player:IsBot() then return end
			if key == MOUSE_LEFT then
				self.player:ShowProfile()
			end
			if key == MOUSE_RIGHT then
				local m = DermaMenu()
				m:AddOption("Open Profile URL", function()
					self.player:ShowProfile()
				end):SetImage("icon16/book_go.png")
				m:AddOption("Copy Profile URL", function()
					SetClipboardText("http://steamcommunity.com/profiles/" .. tostring(self.player:SteamID64()))
				end):SetImage("icon16/book_link.png")

				m:AddSpacer()
				m:AddOption("Copy Steam ID", function()
					SetClipboardText(self.player:SteamID())
				end):SetImage("icon16/tag_blue.png")
				m:AddOption("Copy Community ID", function()
					SetClipboardText(tostring(self.player:SteamID64()))
				end):SetImage("icon16/tag_yellow.png")
				m:AddOption("Copy Account ID", function()
					SetClipboardText(tostring(self.player:AccountID()))
				end):SetImage("icon16/tag_red.png")

				m:Open()
			end
		end
	end

	function PLAYER_LINE:OnMousePressed(btn)
		local ply = self.player
		local lply = LocalPlayer()

		if btn == MOUSE_RIGHT then
			local menu = DermaMenu()

			local ulxTargetString = ""
			if ply == lply then
				ulxTargetString = "^"
			else
				ulxTargetString = ply:Name()

				local muted = ply:IsMuted()
				menu:AddOption(muted and "Unmute" or "Mute", function()
					ply:SetMuted(not muted)
				end):SetImage(muted and "icon16/sound_add.png" or "icon16/sound_delete.png")
			end

			if ulx then
				if ply ~= lply then
					menu:AddSpacer()
				end

				if CAMI.PlayerHasAccess(lply, "ulx forcerespawn", nil, ply) then
					menu:AddOption("Respawn", function()
						RunConsoleCommand("ulx", "forcerespawn", ulxTargetString)
					end):SetImage("icon16/arrow_refresh.png")
				end

				menu:AddSpacer()

				-- cheat commands
				if CAMI.PlayerHasAccess(lply, "ulx noclip", nil, ply) then
					local noclip = ply:GetMoveType() == MOVETYPE_NOCLIP
					menu:AddOption(noclip and "Noclip OFF" or "Noclip ON", function()
						RunConsoleCommand("ulx", "noclip", ulxTargetString)
					end):SetImage(noclip and "icon16/arrow_down.png" or "icon16/arrow_out.png")
				end
				if CAMI.PlayerHasAccess(lply, "ulx god", nil, ply) then
					local godmode = ply:HasGodMode()
					menu:AddOption(godmode and "Godmode OFF" or "Godmode ON", function()
						RunConsoleCommand("ulx", godmode and "ungod" or "god", ulxTargetString)
					end):SetImage(godmode and "icon16/shield_delete.png" or "icon16/shield_add.png")
				end

				if ply ~= lply then
					if CAMI.PlayerHasAccess(lply, "ulx goto", nil, ply) then
						menu:AddOption("Goto", function()
							RunConsoleCommand("ulx", "goto", ulxTargetString)
						end):SetImage("icon16/arrow_right.png")
					end
					if CAMI.PlayerHasAccess(lply, "ulx bring", nil, ply) then
						menu:AddOption("Bring", function()
							RunConsoleCommand("ulx", "bring", ulxTargetString)
						end):SetImage("icon16/arrow_left.png")
					end

					menu:AddSpacer()

					-- admin utility
					if CAMI.PlayerHasAccess(lply, "ulx slay", nil, ply) then
						menu:AddOption("Slay", function()
							RunConsoleCommand("ulx", "slay", ulxTargetString)
						end):SetImage("icon16/shield.png")
					end
					if CAMI.PlayerHasAccess(lply, "ulx gag", nil, ply) then
						local gagged = ply:GetNWBool("ulx_gagged", false)
						menu:AddOption(gagged and "Ungag" or "Gag", function()
							RunConsoleCommand("ulx", "gag", ulxTargetString, gagged)
						end):SetImage(gagged and "icon16/shield_delete.png" or "icon16/shield_add.png")
					end
					if CAMI.PlayerHasAccess(lply, "ulx mute", nil, ply) then
						local muted = ply:GetNWBool("ulx_muted", false)
						menu:AddOption(muted and "Unmute" or "Mute", function()
							RunConsoleCommand("ulx", "mute", ulxTargetString, muted)
						end):SetImage(muted and "icon16/shield_delete.png" or "icon16/shield_add.png")
					end
					if CAMI.PlayerHasAccess(lply, "ulx kick", nil, ply) then
						menu:AddOption("Kick", function()
							Derma_StringRequest("Kick " .. ulxTargetString, "Reason:", "", function(reason)
								RunConsoleCommand("ulx", "kick", ulxTargetString, reason)
							end)
						end):SetImage("icon16/door_out.png")
					end
				end
			end

			menu:Open()
		end
	end

	local icon_shield = Material("icon16/shield.png")

	-- sven styled health and ping color
	local function HealthColor(num)
		if num > 75 then
			return Color(0, 255, 0)
		elseif num > 50 then
			return Color(255, 255, 0)
		elseif num > 25 then
			return Color(255, 128, 0)
		else
			return Color(255, 0, 0)
		end
	end

	local function PingColor(num)
		if num > 350 then -- guessing
			return Color(255, 0, 0)
		elseif num > 250 then
			return Color(255, 128, 0)
		elseif num > 150 then
			return Color(255, 255, 0)
		elseif num > 75 then
			return Color(128, 255, 0)
		else
			return Color(0, 255, 0)
		end
	end

	function PLAYER_LINE:Paint(w, h)
		local ply = self.player
		if not IsValid(ply) then return end

		surface.SetFont("oc_scoreboard_label")
		if ply == LocalPlayer() then
			surface.SetDrawColor(192, 192, 192, 64)
			surface.DrawRect(0, 0, w, h)
		end

		local col = hook.Run("GetTeamColor", ply)
		surface.SetTextColor(col)
		local name = ply:Name()
		local nw, nh = surface.GetTextSize(name)
		surface.SetTextPos(64, h / 2 - nh / 2)
		surface.DrawText(name)

		local tcol = hook.Run("GetTeamNumColor", ply:Team())
		surface.SetTextColor(tcol)
		local x = w

		if ply:IsAdmin() then
			surface.SetMaterial(icon_shield)
			surface.SetDrawColor(255, 255, 255, 255)
			surface.DrawTexturedRect(x - 22, 6, 16, 16)
		end
		x = x - 28

		local ping = ply:IsBot() and "BOT" or ply:Ping()
		local pcol = ply:IsBot() and tcol or PingColor(ply:Ping())
		local pw, ph = surface.GetTextSize(ping)
		surface.SetTextColor(pcol)
		surface.SetTextPos(x - pw, h / 2 - ph / 2)
		surface.DrawText(ping)
		x = x - 100

		surface.SetTextColor(tcol)
		local deaths = mp_livesmode:GetInt() > 0 and ply:Lives() or ply:Deaths()
		local dw, dh = surface.GetTextSize(deaths)
		surface.SetTextPos(x - dw, h / 2 - dh / 2)
		surface.DrawText(deaths)
		x = x - 100

		local score = ply:Frags()
		local sw, sh = surface.GetTextSize(score)
		surface.SetTextPos(x - sw, h / 2 - sh / 2)
		surface.DrawText(score)
		x = x - 100

		if hook.Run("IsPlayerSpectator", LocalPlayer()) or LocalPlayer():Team() == ply:Team() then
			local armor = ply:Armor()
			local acol = HealthColor(armor)
			local aw, ah = surface.GetTextSize(armor)
			surface.SetTextColor(acol)
			surface.SetTextPos(x - aw, h / 2 - ah / 2)
			surface.DrawText(armor)
			x = x - 100

			local health = ply:Alive() and ply:Health() or "DEAD"
			local hcol = HealthColor(ply:Health())
			local hw, hh = surface.GetTextSize(health)
			surface.SetTextColor(hcol)
			surface.SetTextPos(x - hw, h / 2 - hh / 2)
			surface.DrawText(health)
			x = x - 100
		end
	end

	function PLAYER_LINE:Setup(ply)
		self.player = ply
		self.avatar:SetPlayer(ply, 32)
		self.last_team = ply:Team()
		self:SetZPos(ply:EntIndex())

		self:Think()
	end

	function PLAYER_LINE:Think()
		if not IsValid(self.player) then
			self:SetZPos(9999)
			self:Remove()
			return
		end
	end

	PLAYER_LINE = vgui.RegisterTable(PLAYER_LINE, "EditablePanel")
end

---@class Panel
local TEAM_PANEL = {}
do
	function TEAM_PANEL:Init()
		self:SetTall(20)
		self:DockPadding(0, 20, 0, 0)
	end

	local team_cache = {}
	function TEAM_PANEL:Setup(id)
		if not team_cache[id] then
			team_cache[id] = {
				name  = team.GetName(id),
				color = team.GetColor(id),
				count = #team.GetPlayers(id),
			}
		end

		self.team = id
		self:SetZPos(id)

		self:Think()
	end

	function TEAM_PANEL:Think()
		if self.team then
			local count = #team.GetPlayers(self.team)

			if team_cache[self.team].count ~= count then
				team_cache[self.team].count = count
			end

			if count == 0 then
				self:SetZPos(9999)
				self:Remove()
			end
		end
	end

	function TEAM_PANEL:Paint(w, h)
		if not self.team then return end
		local team = team_cache[self.team]

		surface.SetTextColor(team.color)
		surface.SetFont("oc_scoreboard_label")
		surface.SetTextPos(64, 1)

		local str = string.format("%s - %d player%s", team.name, team.count, team.count > 1 and "s" or "")
		surface.DrawText(str)

		surface.SetDrawColor(team.color)
		surface.DrawLine(1, 19, w - 2, 19)
	end

	function TEAM_PANEL:PerformLayout(w, h)
		self:SizeToChildren(false, true)
	end

	TEAM_PANEL = vgui.RegisterTable(TEAM_PANEL, "EditablePanel")
end

---@class Panel
local SCOREBOARD = {}
do
	local function FormatTime(num)
		local days = math.floor(num / 86400)
		num = num % 86400
		local hours = math.floor(num / 3600)
		num = num % 3600
		local minutes = math.floor(num / 60)
		num = num % 60
		num = math.floor(num)

		if days > 0 then
			return string.format("%.2d:%.2d:%.2d:%.2d", days, hours, minutes, num)
		elseif hours > 0 then
			return string.format("%.2d:%.2d:%.2d", hours, minutes, num)
		else
			return string.format("%.2d:%.2d", minutes, num)
		end
	end

	function SCOREBOARD:Init()
		self:SetSize(ScreenScaleH(444), ScreenScaleH(360))
		self:Center()
		self:DockPadding(8, 100, 8, 8)

		self.scroller = vgui.Create("DScrollPanel", self)
		self.scroller:Dock(FILL)

		self.teams = {}
		self.players = {}
	end

	function SCOREBOARD:Paint(w, h)
		surface.SetDrawColor(0, 0, 0, 128)
		surface.DrawRect(0, 0, w, h)
		surface.SetDrawColor(0, 128, 255)
		surface.DrawOutlinedRect(0, 0, w, h, 1)

		surface.SetTextColor(255, 180, 0)
		surface.SetFont("oc_scoreboard_title")
		surface.SetTextPos(26, 13)
		surface.DrawText(GetHostName())

		surface.SetFont("oc_scoreboard_label")
		surface.SetTextPos(26, 52)
		surface.DrawText("Current map: " .. game.GetMap())

		local time = "Time Elapsed: " .. FormatTime(CurTime())
		local tw = surface.GetTextSize(time)
		surface.SetTextPos(w - 26 - tw, 52)
		surface.DrawText(time)

		surface.SetFont("oc_scoreboard_text")
		local x = w - 8 - 28

		local pw = surface.GetTextSize("Latency")
		surface.SetTextPos(x - pw, 80)
		surface.DrawText("Latency")
		x = x - 100

		local deaths = mp_livesmode:GetInt() > 0 and "Lives" or "Deaths"
		local dw = surface.GetTextSize(deaths)
		surface.SetTextPos(x - dw, 80)
		surface.DrawText(deaths)
		x = x - 100

		local sw = surface.GetTextSize("Score")
		surface.SetTextPos(x - sw, 80)
		surface.DrawText("Score")
		x = x - 100

		local aw = surface.GetTextSize("Armor")
		surface.SetTextPos(x - aw, 80)
		surface.DrawText("Armor")
		x = x - 100

		local hw = surface.GetTextSize("Health")
		surface.SetTextPos(x - hw, 80)
		surface.DrawText("Health")
		x = x - 100
	end

	function SCOREBOARD:PerformLayout(w, h)
		self.scroller:SizeToChildren(false, true)
	end

	function SCOREBOARD:Think()
		self.teams = self.teams or {}
		self.players = self.players or {}

		if not IsValid(self.scroller) then
			self.scroller = vgui.Create("DScrollPanel", self)
			self.scroller:Dock(FILL)
		end

		for id in pairs(team.GetAllTeams()) do
			local plys = team.GetPlayers(id)
			if (not self.teams[id] or not IsValid(self.teams[id])) and #plys > 0 then
				local team_pnl = vgui.CreateFromTable(TEAM_PANEL, self.scroller)
				team_pnl:Setup(id)
				self.scroller:AddItem(team_pnl)
				team_pnl:Dock(TOP)
				self.teams[id] = team_pnl
			end

			for _, ply in ipairs(plys) do
				if not self.players[ply] or not IsValid(self.players[ply]) then
					local player_pnl = vgui.CreateFromTable(PLAYER_LINE, self.teams[id])
					player_pnl:Setup(ply)
					player_pnl:Dock(TOP)
					self.players[ply] = player_pnl
				end

				local player_pnl = self.players[ply]
				if ply:Team() ~= player_pnl.last_team then
					player_pnl:SetParent(self.teams[ply:Team()])
					self.teams[player_pnl.last_team]:InvalidateLayout()
					player_pnl.last_team = ply:Team()
				end
			end
		end
	end

	function SCOREBOARD:Show()
		if not self:IsVisible() then
			self:SetVisible(true)
			self:SetKeyboardInputEnabled(true)
			self:SetMouseInputEnabled(true)
		end
	end

	SCOREBOARD = vgui.RegisterTable(SCOREBOARD, "EditablePanel")
end

if IsValid(g_ScoreboardPanel) then
	g_ScoreboardPanel:Remove()
end
local scoreboard = vgui.CreateFromTable(SCOREBOARD)
scoreboard:SetVisible(false)
g_ScoreboardPanel = scoreboard

function GM:ScoreboardShow()
	if IsValid(scoreboard) then
		scoreboard:Show()
	else
		scoreboard = vgui.CreateFromTable(SCOREBOARD)
		g_ScoreboardPanel = scoreboard
	end

	return true
end

function GM:ScoreboardHide()
	if IsValid(scoreboard) then
		scoreboard:Hide()
	end

	CloseDermaMenus()
	gui.EnableScreenClicker(false)

	return true
end

hook.Add("PlayerBindPress", "oc_scoreboard", function(ply, bind, pressed)
	if pressed and bind == "+attack2" and IsValid(scoreboard) and scoreboard:IsVisible() then
		gui.EnableScreenClicker(true)
		return true
	end
end)
