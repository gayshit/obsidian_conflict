AddCSLuaFile()

local SNDLVL = {
	NONE = 0,
	IDLE = 60,
	STATIC = 66,
	NORM = 75,
	TALKING = 80,
	GUNFIRE = 140,
}

function GM:BulkRegisterSoundScripts(tab)
	for _, kv in ipairs(tab) do
		local SoundData = {
			name = kv.Key,
		}

		for _, kv2 in ipairs(kv.Value) do
			if kv2.Key == "rndwave" then
				SoundData.sound = {}

				for _, kv3 in ipairs(kv2.Value) do
					table.insert(SoundData.sound, kv3.Value)
				end
			elseif kv2.Key == "wave" then
				SoundData.sound = kv2.Value
			else
				SoundData[kv2.Key] = kv2.Value
			end
		end

		if not SoundData.volume then
			SoundData.volume = 1.0
		else
			SoundData.volume = tonumber(SoundData.volume)
		end

		if not SoundData.pitch then
			SoundData.pitch = 100 -- PITCH_NORM
		else
			if string.find(SoundData.pitch, ",") then
				SoundData.pitch = string.Split(SoundData.pitch, ",")

				SoundData.pitch[1] = tonumber(SoundData.pitch[1])
				SoundData.pitch[2] = tonumber(SoundData.pitch[2])
			else
				SoundData.pitch = tonumber(SoundData.pitch)
			end
		end

		-- parse SNDLVL_75dB to 75
		if not SoundData.soundlevel then
			SoundData.level = SNDLVL.NORM
		else
			local level = SoundData.soundlevel
			local match = string.match(level, "SNDLVL_(%d+)dB")

			if not match then
				-- parse SNDLVL_NORM to NORM
				match = SNDLVL[string.match(level, "SNDLVL_(%a+)")]
			end

			SoundData.soundlevel = nil
			SoundData.level = tonumber(match) or SNDLVL.NORM
		end

		sound.Add(SoundData)
		util.PrecacheSound(SoundData.name)
	end
end

local sounds = util.KeyValuesToTablePreserveOrder([["Sounds" {
    //Obsidian
    "Player.Cloak"
    {
        "channel"	"CHAN_STATIC"
        "volume"	"1.0"
        "soundlevel"	"SNDLVL_75dB"
        "pitch"	"95,105"
        "wave"	"player/player_cloak.wav"
    }

    "Player.ShieldOn"
    {
        "channel"	"CHAN_STATIC"
        "volume"	"1.0"
        "soundlevel"	"SNDLVL_75dB"
        "pitch"	"95,105"
        "wave"	"player/shield_start.wav"
    }

    "Player.ShieldOff"
    {
        "channel"	"CHAN_STATIC"
        "volume"	"1.0"
        "soundlevel"	"SNDLVL_75dB"
        "pitch"	"95,105"
        "wave"	"player/shield_stop.wav"
    }

    "Shield.BulletImpact"
    {
        "soundlevel"		"SNDLVL_75dB"
        "pitch"			"PITCH_NORM"
        "volume"			"0.7"

        "wave"		"player/shield_hit.wav"
    }

    "Weapon_Uzi.Single"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"0.7"
        "soundlevel"		"SNDLVL_90db"
        "soundlevel"	"SNDLVL_GUNFIRE"
        "pitch"			"95,105"

        "wave"			"^weapons/uzi/uzi_fire1.wav"
    }

    "Weapon_Uzi.clipout"
    {
        "channel"		"CHAN_ITEM"
        "volume"		"1.0"
        "soundlevel"	"SNDLVL_NORM"

        "wave"			"weapons/uzi/uzi_clipout.wav"
    }

    "Weapon_Uzi.clipin"
    {
        "channel"		"CHAN_ITEM"
        "volume"		"1.0"
        "soundlevel"	"SNDLVL_NORM"

        "wave"			"weapons/uzi/uzi_clipin.wav"
    }

    "Weapon_Uzi.boltpull"
    {
        "channel"		"CHAN_ITEM"
        "volume"		"1.0"
        "soundlevel"	"SNDLVL_NORM"

        "wave"			"weapons/uzi/uzi_boltpull.wav"
    }

    // Obsidian's weapon_alyxgun.txt (separate from ep1 sounds)
    "Weapon_AlyxGun.Reload"
    {
        "channel"		"CHAN_ITEM"
        "volume"		"0.5"
        "soundlevel"	"SNDLVL_NORM"
        "pitch"		"200"

        "wave"		"weapons/alyxgun/alyxgun_reload1.wav"
    }

    "Weapon_AlyxGun.Empty"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"0.7"
        "soundlevel"	"SNDLVL_NORM"

        "wave"		"weapons/pistol/pistol_empty.wav"
    }

    "Weapon_AlyxGun.Single"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"0.7"
        "soundlevel"	"SNDLVL_GUNFIRE"
        "pitch"			"98,102"
        "rndwave"
        {
            "wave"		")weapons/alyxgun/alyxgun_fire2.wav"
            "wave"		")weapons/alyxgun/alyxgun_fire2a.wav"
            "wave"		")weapons/alyxgun/alyxgun_fire2b.wav"
        }
    }

    "Weapon_AlyxGun.Special1"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"0.7"
        "soundlevel"	"SNDLVL_NORM"

        "wave"		"weapons/alyxgun/alyxgun_switch_burst.wav"
    }

    "Weapon_AlyxGun.Special2"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"0.7"
        "soundlevel"	"SNDLVL_NORM"

        "wave"			"weapons/alyxgun/alyxgun_switch_single.wav"
    }

    "Weapon_AlyxGun.Burst"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"0.7"
        "soundlevel"	"SNDLVL_GUNFIRE"
        "pitch"			"98,105"

        "wave"			"^weapons/alyxgun/alyxgun_fire3.wav"
    }

    // weapon_oicw.txt
    "Weapon_OICW.Empty"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"1.0"
        "soundlevel"	"SNDLVL_NORM"

        "wave"			"weapons/oicw/ar2_empty.wav"
    }

    "Weapon_OICW.Reload"
    {
        "channel"		"CHAN_ITEM"
        "volume"		"1.0"
        "soundlevel"	"SNDLVL_NORM"

        "wave"			"weapons/oicw/ar2_reload.wav"
    }

    "Weapon_OICW.Single"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"1.0"
        "soundlevel"	"SNDLVL_GUNFIRE"
        "pitch"			"95,105"

        "rndwave"
        {
            "wave"			")weapons/oicw/ar1_1.wav"
            "wave"			")weapons/oicw/ar1_2.wav"
            "wave"			")weapons/oicw/ar1_3.wav"
        }
    }

    "Weapon_OICW.ZoomedShot"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"1.0"
        "soundlevel"	"SNDLVL_GUNFIRE"
        "pitch"			"95,105"

        "rndwave"
        {
            "wave"			")weapons/oicw/ar2_fire1.wav"
            "wave"			")weapons/oicw/ar2_fire2.wav"
            "wave"			")weapons/oicw/ar2_fire3.wav"
        }
    }

    "Weapon_OICW.Special1"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"1.0"
        "soundlevel"	"SNDLVL_NORM"

        "wave"				"weapons/sniper/sniper_zoomin.wav"
    }

    "Weapon_OICW.Special2"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"1.0"
        "soundlevel"	"SNDLVL_NORM"

        "wave"			"weapons/sniper/sniper_zoomout.wav"
    }

    "Weapon_OICW.Double"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"1.0"
        "soundlevel"	"SNDLVL_GUNFIRE"

        "wave"			")weapons/oicw/ar2_altfire.wav"
    }

    "Weapon_OICW.NPC_Reload"
    {
        "channel"		"CHAN_ITEM"
        "volume"		"1.0"
        "soundlevel"	"SNDLVL_NORM"

        "wave"			"weapons/oicw/npc_ar2_reload.wav"
    }

    "Weapon_OICW.NPC_Double"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"1.0"
        "soundlevel"	"SNDLVL_GUNFIRE"

        "wave"			"weapons/oicw/npc_ar2_altfire.wav"
    }

    "Weapon_OICW.NPC_Single"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"1.0"
        "soundlevel"	"SNDLVL_GUNFIRE"
        "pitch"			"95,105"

        "wave"			"^weapons/oicw/npc_ar2_fire1.wav"
    }

    //custom_spas12.txt
    "Weapon_spas12.Single"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"0.86"
        "soundlevel"	"SNDLVL_GUNFIRE"
        "pitch"			"98,101"
        "rndwave"
        {
            "wave"			")weapons/spas12/shotgun_fire1.wav"
        //	"wave"			"weapons/shotgun/shotgun_fire6.wav"
        //	"wave"			"weapons/shotgun/shotgun_fire7.wav"
        }
    }


    "Weapon_spas12.NPC_Single"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"0.95"
        "soundlevel"	"SNDLVL_GUNFIRE"
        "pitch"			"98,101"

        "wave"			")weapons/spas12/shotgun_fire1.wav"
    }

    "Weapon_mp5k.Single"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"0.95"
        "soundlevel"	"SNDLVL_GUNFIRE"
        "pitch"			"98,101"

        "wave"			")weapons/mp5k/mp5k_fire1.wav"
    }

    "Weapon_mp5k.Burst"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"0.95"
        "soundlevel"	"SNDLVL_GUNFIRE"
        "pitch"			"98,101"

        "wave"			")weapons/mp5k/mp5k_fireburst1.wav"
    }

    "Weapon_cz52.Single"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"0.95"
        "soundlevel"	"SNDLVL_GUNFIRE"
        "pitch"			"118,121"

        "wave"			")weapons/cz52/fire.wav"
    }

    "Weapon_cz52.Reload"
    {
        "channel"		"CHAN_ITEM"
        "volume"		"1.0"
        "soundlevel"	"SNDLVL_NORM"

        "wave"			")weapons/cz52/reload.wav"
    }

    "Weapon_ak5.Single"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"0.9"
        "soundlevel"	"SNDLVL_GUNFIRE"
        "pitch"			"100,105"

        "wave"			")weapons/ak5/fire.wav"
    }

    "Weapon_ak5.Reload_Rotate"
    {
        "channel"		"CHAN_ITEM"
        "volume"		"0.9"
        "soundlevel"	"SNDLVL_NORM"

        "wave"			"weapons/ak5/ak5_reload_rotate.wav"
    }

    "Weapon_ak5.Reload_Push"
    {
        "channel"		"CHAN_ITEM"
        "volume"		"0.9"
        "soundlevel"	"SNDLVL_NORM"

        "wave"			"weapons/ak5/ak5_reload_push.wav"
    }

    "Weapon_remi870.Single"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"0.9"
        "soundlevel"		"SNDLVL_GUNFIRE"
        "pitch"			"98,101"
        "wave"			")weapons/remi870/fire.wav"

    }

    "Weapon_remi870.Special1"
    {
        "channel"		"CHAN_ITEM"
        "volume"		"0.8"
        "soundlevel"	"SNDLVL_NORM"

        "wave"		"weapons/remi870/cock.wav"
    }
    //g36c
    "Weapon_g36c.Single"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"1.0"
        "CompatibilityAttenuation"	"0.52"
        "pitch"		"PITCH_NORM"

        "wave"		")weapons/G36C/G36C-unsil.wav"
    }
}]], nil, true)
GM:BulkRegisterSoundScripts(sounds)

-- for some reason, gmod doesn't add stalker sound scripts
local sounds_stalker = util.KeyValuesToTablePreserveOrder([["Stalker" {
    "NPC_Stalker.BurnFlesh"
    {
        "channel"		"CHAN_STATIC"
        "volume"		"VOL_NORM"
        "pitch"			"PITCH_NORM"

        "soundlevel"	"SNDLVL_NORM"

        "wave"			"npc/stalker/laser_flesh.wav"
    }

    "NPC_Stalker.BurnWall"
    {
        "channel"		"CHAN_STATIC"
        "volume"		"VOL_NORM"
        "pitch"			"PITCH_NORM"

        "soundlevel"	"SNDLVL_NORM"

        "wave"			"npc/stalker/laser_burn.wav"
    }

    "NPC_Stalker.FootstepLeft"
    {
        "channel"		"CHAN_BODY"
        "volume"		"VOL_NORM"
        "pitch"			"PITCH_NORM"

        "soundlevel"	"SNDLVL_IDLE"

        "rndwave"
        {
            "wave"	"npc/stalker/stalker_footstep_left1.wav"
            "wave"	"npc/stalker/stalker_footstep_left2.wav"
        }
    }

    "NPC_Stalker.FootstepRight"
    {
        "channel"		"CHAN_BODY"
        "volume"		"VOL_NORM"
        "pitch"			"PITCH_NORM"

        "soundlevel"	"SNDLVL_IDLE"

        "rndwave"
        {
            "wave"	"npc/stalker/stalker_footstep_right1.wav"
            "wave"	"npc/stalker/stalker_footstep_right2.wav"
        }
    }

    "NPC_Stalker.Hit"
    {
        "channel"		"CHAN_WEAPON"
        "volume"		"VOL_NORM"
        "pitch"			"50, 150"

        "soundlevel"	"SNDLVL_NORM"

        "wave"			"npc/vort/foot_hit.wav"
    }

    "NPC_Stalker.AmbientLaserStart"
    {
        "channel"		"CHAN_AUTO"
        "volume"		"VOL_NORM"
        "pitch"			"PITCH_NORM"

        "soundlevel"	"SNDLVL_NORM"

        "wave"			"npc/stalker/laser_start.wav"
    }

    "NPC_Stalker.Ambient01"
    {
        "channel"		"CHAN_AUTO"
        "volume"		"VOL_NORM"
        "pitch"			"PITCH_NORM"

        "soundlevel"	"SNDLVL_NORM"

        "wave"			"npc/stalker/breathing3.wav"
    }

    "NPC_Stalker.Scream"
    {
        "channel"		"CHAN_AUTO"
        "volume"		"VOL_NORM"
        "pitch"			"PITCH_NORM"

        "soundlevel"	"SNDLVL_NORM"

        "wave"			"npc/stalker/go_alert2.wav"
    }
}]], nil, true)
GM:BulkRegisterSoundScripts(sounds_stalker)

local ObsidianPlayer = util.KeyValuesToTablePreserveOrder([["Obsidian Player" {
    "Obsidian_Female_Player.Medic"
    {
        "channel"		"CHAN_VOICE"
        "volume"		"VOL_NORM"
        "pitch"			"PITCH_NORM"

        "soundlevel"	"SNDLVL_NORM"

        "rndwave"
        {
            "wave"	"*vo/npc/female01/help01.wav"
            "wave"	"*vo/npc/female01/imhurt01.wav"
        }
    }

    "Obsidian_Female_Player.Attention"
    {
        "channel"		"CHAN_VOICE"
        "volume"		"VOL_NORM"
        "pitch"			"PITCH_NORM"

        "soundlevel"	"SNDLVL_NORM"

        "rndwave"
        {
            "wave"	"*vo/npc/female01/incoming02.wav"
            "wave"	"*vo/npc/female01/gethellout.wav"
        }
    }

    "Obsidian_Male_Player.Medic"
    {
        "channel"		"CHAN_VOICE"
        "volume"		"VOL_NORM"
        "pitch"			"PITCH_NORM"

        "soundlevel"	"SNDLVL_NORM"

        "rndwave"
        {
            "wave"	"*vo/npc/male01/help01.wav"
            "wave"	"*vo/npc/male01/imhurt01.wav"
        }
    }

    "Obsidian_Male_Player.Attention"
    {
        "channel"		"CHAN_VOICE"
        "volume"		"VOL_NORM"
        "pitch"			"PITCH_NORM"

        "soundlevel"	"SNDLVL_NORM"

        "rndwave"
        {
            "wave"	"*vo/npc/male01/incoming02.wav"
            "wave"	"*vo/npc/male01/gethellout.wav"
        }
    }

    "Obsidian_CombineS_Player.Medic"
    {
        "channel"		"CHAN_VOICE"
        "volume"		"VOL_NORM"
        "pitch"			"PITCH_NORM"

        "soundlevel"	"SNDLVL_NORM"

        "rndwave"
        {
            "wave"	"*npc/combine_soldier/vo/flatline.wav"
            "wave"	"*npc/combine_soldier/vo/coverhurt.wav"
        }
    }

    "Obsidian_CombineS_Player.Attention"
    {
        "channel"		"CHAN_VOICE"
        "volume"		"VOL_NORM"
        "pitch"			"PITCH_NORM"

        "soundlevel"	"SNDLVL_NORM"

        "rndwave"
        {
            "wave"	"*npc/combine_soldier/vo/displace.wav"
            "wave"	"*npc/combine_soldier/vo/displace2.wav"
            "wave"	"*npc/combine_soldier/vo/overwatchrequestreinforcement.wav"
        }
    }
}]], nil, true)
GM:BulkRegisterSoundScripts(ObsidianPlayer)

if CLIENT then
	local strings = util.KeyValuesToTable([["English" {
        "HL2_SNIPERRIFLE"	"SNIPER RIFLE"
        "HL2_TAUCANNON"		"TAU CANNON"
        "HL2_HOPWIRE"		"HOPWIRE GRENADE"
        "HL2_Healer"		"MEDPACK"
        "HL2_Manhackweapon"	"MANHACK"
        "HL2_UZI"		    "UZI"
        "HL2_AlyxGun"		"ALYX GUN"

        //"ScoreBoard_Player"		"%s1    -   %s2 player"
        //"ScoreBoard_Players"		"%s1    -   %s2 players"
        //"ScoreBoard_Deathmatch"		"Obsidian Conflict"
        //"ScoreBoard_TeamDeathmatch"	"Obsidian Conflict"
        //"Playerid_sameteam"		"Friend: %s1 Health: %s2"
        //"Playerid_diffteam"		"Enemy: %s1"
        //"Playerid_noteam"		"%s1 Health:%s2"
        //"Team"				"Team %s1"

        //"NPCid1"			"Name: %s1"
        //"NPCid2"			"Health: %s1"
        //"NPCid3"			"Points: %s1" //Frags
        //"NPCid4"			"%s1"

        //"Game_connected"			"%s1 has joined the game"
        //"Game_disconnected"			"%s1 has left the game"
        //"Obsidian_Spec_Blue_Score"		"Red:"
        //"Obsidian_Spec_Red_Score"		"Blue:"

        // Radio and chat strings can have control characters embedded to set colors.  For the control characters to be used, one must be at the start of the string.
        // The control characters can be copied and pasted in notepad.
        //  = 0x02 (STX) - Use team color up to the end of the player name.  This only works at the start of the string, and precludes using the other control characters.
        //  = 0x03 (ETX) - Use team color from this point forward
        //  = 0x04 (EOT) - Use location color from this point forward
        //  = 0x05 (ENQ) - Use achievement color from this point forward
        //  = 0x01 (SOH) - Use normal color from this point forward
        //"HL2MP_Chat_Team_Loc"		"(TEAM) %s1 @ %s3 :  %s2"
        //"HL2MP_Chat_Team"			"(TEAM) %s1 :  %s2"
        //"HL2MP_Chat_Team_Dead"		"*DEAD*(TEAM) %s1 :  %s2"
        //"HL2MP_Chat_Spec"			"(Spectator) %s1 :  %s2"
        //"HL2MP_Chat_All"			"%s1 :  %s2"
        //"HL2MP_Chat_AllDead"		"*DEAD* %s1 :  %s2"
        //"HL2MP_Chat_AllSpec"		"*SPEC* %s1 :  %s2"
        //"HL2MP_Name_Change"		"* %s1 changed name to %s2"

        "Obsidian_Hud_AUX_POWER"		"STAMINA"
        "Obsidian_Hud_Power_Module"		"POWER MODULE"
        "Obsidian_Hud_Cloak"			"CLOAKING DEVICE"
        "Obsidian_Hud_Shield"			"ENERGY SHIELD"

        //MERCHANT

        "info_weapon_357"			".357 MAGNUM\n\nThe defenition of bad ass. This revolver holds 6 shots and will send enemies flying from a single shot!"
        "info_weapon_ar2"			"OVERWATCH STANDARD ISSUE\n(PULSE-RIFLE)\n\nA deadly weapon in general, it will eat up it's 30 ammo clip fast, but will deal heavy damage. Secondary fire throws out a bouncing anti-matter orb."
        "info_weapon_crossbow"			"CROSSBOW\n\nShooting heat charged bolts, this weapon has a zoom function for sniping, but has a slight sink you need to acocunt for."
        "info_weapon_crowbar"			"CROWBAR\n\nA small crowbar for close combat, it's fairly fast attacking but mainly good for when you're out of ammo or don't want to use ammo."
        "info_weapon_frag"			"GRENADE\n\nA grenade that explodes with a semi-powerful force. Able to be picked up when tossed by use and gravitygun."
        "info_weapon_gauss"			"TAU CANNON\n\nA lightning cannon that can be charged for a rather large jump when shot at an angle. It holds 200 ammo and does not need to reload."
        "info_weapon_healer"			"MEDPACK\n\nA portable medpack, can be used ot heal allies in need but cannot be used on yourself."
        "info_weapon_manhack"			"MANHACK\n\nA manhack to be deployed whenever. Will attack your enemies on sight."
        "info_weapon_physcannon"		"ZERO-POINT ENERGY GUN\n(GRAVITY GUN)\n\nA Gravity Gun used to pick up items with secondary fire, and punt items with primary."
        "info_weapon_pistol"			"9MM PISTOL\n\nA rather simple pistol. Semi-Auto and holds 18 per clip."
        "info_weapon_rpg"			"RPG\n(ROCKET PROPELLED GRENADE)\n\nShoots a very powerful rocket, good for taking down striders and gunships."
        "info_weapon_shotgun"			"SHOTGUN\n\nPowerful at close range, this shotgun is perfect for close encounters. Holds 6 shells at a time."
        "info_weapon_slam"			"S.L.A.M\n(Selectable Lightweight Attack Munition)\n\nA weapon with 2 functions. Function 1, you can deploy it on a wall as a tripmine. Second, you can throw it and use secondary fire to detonate it as a remote mine."
        "info_weapon_smg1"			"SMG\n(SUBMACHINE GUN)\n\nA simple smg. Not too powerful but holds 45 per clip and has a secondary fire for grenades."
        "info_weapon_sniperrifle"		"SNIPER RIFLE\n\nVery powerful rifle for long range fighting, has a long range scope but does not keep steady very easly."
        "info_weapon_stunstick"			"STUNSTICK\n\nEver wanted to give those metrocops at city 17 a taste of their own medicine?"
        "info_weapon_uzi"			"UZI\n\nA weapon that can be double weilded. It holds 30 per gun and fires at a rapid pace. Eats out of your smg ammo."
        "info_weapon_alyxgun"			"ALYXGUN\n\nAlyx's trusty sidearm, now available on the open market! Includes full and burst fire modes."

        "info_item_battery"			"BATTERY\n\nRecharges 15 suit battery power."
        "info_item_healthkit"			"HEALTH KIT\n\nRestores 25 Health."
        "info_item_healthvial"			"HEALTH VIAL\n\nRestores 10 Health."
        "info_item_shield"			"PSG\n(PERSONAL SHIELD GENERATOR)\n\nUse while in contact with Hazardous Materials. Also protects against projectile fire for a short time. Nullifies Gas, Poison, Radiation, and Acid damage."
        "info_item_cloak"			"CLOAKING FIELD GENERATOR\n\nEver noticed how in the control settings there is a button for cloaking, but when you press it, it does nothing? Yeah, you need to buy this to cloak."
        "info_item_ammo_pistol"			"PISTOL AMMO\n\n"
        "info_item_ammo_smg1"			"SMG AMMO\n\n"
        "info_item_ammo_smg1_large"		"SMG AMMO (LARGER CACHE)\n\n"
        "info_item_ammo_ar2"			"COMBINE PULSE-RIFLE AMMO\n\n"
        "info_item_ammo_ar2_large"		"COMBINE PULSE-RIFLE AMMO (LARGER CACHE)\n\n"
        "info_item_ammo_357"			"357 AMMO\n\n"
        "info_item_ammo_357_large"		"357 AMMO (LARGER CACHE)\n\n"
        "info_item_ammo_crossbow"		"CROSSBOW AMMO\n\nWe will not help you pull your purchased bolts from crucified corpses."
        "info_item_rpg_round"			"ROCKET PROPELLED GRENADE\n\nA grenade that is rocket propelled. Scary isnt it."
        "info_item_ammo_smg1_grenade"		"SMG GRENADE\n\n"
        "info_item_box_sniper_rounds"		"SNIPER RIFLE ROUNDS\n\n"
        "info_item_box_buckshot"		"BUCKSHOT ROUNDS\n\n"
        "info_item_ammo_ar2_altfire"		"OVERWATCH DARK ENERGY GRENADE\n\n"
        "info_item_ammo_tau"			"TAU CANNON AMMO\n\n"
        "info_item_box_alyxrounds"		"ALYXGUN AMMO\n\n"

        //MAPS

        //Broken_escape_1
        "OC_be1_genroom"			"Looks like we need a code to open that door...\n> Search the code.\n The keypad needs energy...\n> Find a way to start the generators."

        //Broken_escape_2
        "OC_be2a_topdefense"			"The Combines' defense is really tough,\nlooks like there's something very important down there,\nget on the elevator and find it out!"
        "OC_be2b_coolants1"			"> Destroy Coolant 1."
        "OC_be2b_emitters1"			"> Destroy the spinning emitters to disable the coolant device."
        "OC_be2b_coolants2"			"> Destroy Coolant 2."
        "OC_be2b_problem"			"> Go back to the control room."
        "OC_be2c_energybridge1"			"> Keep disabling locked (red) consoles."
        "OC_be2c_aiconsoles"			"> Override both consoles at the top."
        "OC_be2c_aicoils"			"> Use AR-2 alt-fire to temporary disable shield of A.I. Coils. (x4)\n> Destroy exposed A.I. Coils while shield is disabled. (x4)"
        "OC_be2b_coolants3"			"> Destroy Coolant 3."
        "OC_be2b_coolants4"			"> Destroy Coolant 4."
        "OC_be2b_apcramp"			"> Go to the APC storage access ramp."
    }]], nil, true)

	for k, v in next, strings do
		language.Add(k, v)
	end
end

