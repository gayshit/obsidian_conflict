AddCSLuaFile()
local AUX = {}
_G.AUX = AUX

local sv_infinite_aux_power = GetConVar"oc_infinite_aux_power"
local sv_stickysprint = GetConVar"sv_stickysprint"
local oc_hud_mode = GetConVar("oc_hud_mode")

local SUITPOWER_CHARGE_RATE = 12.5
local SUITPOWER_BEGIN_RECHARGE_DELAY = 0.5

AUX_SUIT_DEVICE_SPRINT = bit.lshift(1, 0)
AUX_SUIT_DEVICE_FLASHLIGHT = bit.lshift(1, 1)
AUX_SUIT_DEVICE_BREATHER = bit.lshift(1, 2)
AUX_SUIT_DEVICE_SHIELD = bit.lshift(1, 3)
AUX_SUIT_DEVICE_CLOAK = bit.lshift(1, 4)

AUX.Devices = {
	SuitDeviceSprint = {
		id = AUX_SUIT_DEVICE_SPRINT,
		drainrate = 25,
		label = "SPRINT"
	},
	SuitDeviceFlashlight = {
		id = AUX_SUIT_DEVICE_FLASHLIGHT,
		drainrate = 2.222,
		label = "FLASHLIGHT"
	},
	SuitDeviceBreather = {
		id = AUX_SUIT_DEVICE_BREATHER,
		drainrate = 6.7,
		label = "OXYGEN"
	},

	SuitDeviceShield = {
		id = AUX_SUIT_DEVICE_SHIELD,
		drainrate = 10,
		label = "SHIELD"
	},
	SuitDeviceCloak = {
		id = AUX_SUIT_DEVICE_CLOAK,
		drainrate = 15,
		label = "CLOAK"
	}
}

function AUX.SuitPowerUpdate(ply, mv, cmd)
	if AUX.SuitPowerShouldRecharge(ply) then
		AUX.SuitPowerCharge(ply, SUITPOWER_CHARGE_RATE * FrameTime())
	elseif ply:GetNW2Int("ActiveDeviceBits", 0) ~= 0 then
		local powerLoad = ply:GetNW2Float("SuitPowerLoad", 0)

		if not sv_stickysprint:GetBool() and
				AUX.SuitPowerIsDeviceActive(ply, AUX.Devices.SuitDeviceSprint) and
				ply:GetVelocity():Length() < (ply:GetWalkSpeed() - 20) then
			powerLoad = powerLoad - AUX.Devices.SuitDeviceSprint.drainrate
		end

		if AUX.SuitPowerIsDeviceActive(ply, AUX.Devices.SuitDeviceFlashlight) then
			powerLoad = powerLoad + AUX.Devices.SuitDeviceFlashlight.drainrate
		end

		if not AUX.SuitPowerDrain(ply, powerLoad * FrameTime()) then
			if ply:IsSprinting() then
				if mv then
					mv:SetMaxSpeed(ply:GetWalkSpeed())
					mv:SetMaxClientSpeed(ply:GetWalkSpeed())
				end
				if cmd then
					cmd:RemoveKey(IN_SPEED)
				end
			end

			if SERVER and ply:FlashlightIsOn() then
				ply:Flashlight(false)
			end
		end
	end
end

function AUX.SuitPowerDrain(ply, power)
	if sv_infinite_aux_power:GetBool() then return true end

	ply:SetNW2Float("SuitPower", ply:GetNW2Float("SuitPower", 0) - power)

	if ply:GetNW2Float("SuitPower", 0) < 0 then
		ply:SetNW2Float("SuitPower", 0)
		return false
	end

	return true
end

function AUX.SuitPowerCharge(ply, power)
	ply:SetNW2Float("SuitPower", ply:GetNW2Float("SuitPower", 0) + power)

	if ply:GetNW2Float("SuitPower", 0) > 100 then
		ply:SetNW2Float("SuitPower", 100)
	end
end

function AUX.SuitPowerIsDeviceActive(ply, device)
	return bit.band(ply:GetNW2Int("ActiveDeviceBits", 0), device.id) ~= 0
end

function AUX.SuitPowerAddDevice(ply, device)
	-- Make sure this device is NOT active!!
	if bit.band(ply:GetNW2Int("ActiveDeviceBits", 0), device.id) ~= 0 then return false end

	--[[if( !IsSuitEquipped() )
        return false]]

	ply:SetNW2Int("ActiveDeviceBits", bit.bor(ply:GetNW2Int("ActiveDeviceBits", 0), device.id))
	ply:SetNW2Float("SuitPowerLoad", ply:GetNW2Float("SuitPowerLoad", 0) + device.drainrate)

	hook.Run("AUXDeviceAdded", ply, device)

	return true
end

function AUX.SuitPowerRemoveDevice(ply, device)
	-- Make sure this device is active!!
	if bit.band(ply:GetNW2Int("ActiveDeviceBits", 0), device.id) == 0 then return false end

	--[[if( !IsSuitEquipped() )
        return false]]

	-- Take a little bit of suit power when you disable a device. If the device is shutting off
	-- because the battery is drained, no harm done, the battery charge cannot go below 0.
	-- This code in combination with the delay before the suit can start recharging are a defense
	-- against exploits where the player could rapidly tap sprint and never run out of power.
	AUX.SuitPowerDrain(ply, device.drainrate * .1)

	ply:SetNW2Int("ActiveDeviceBits", bit.band(ply:GetNW2Int("ActiveDeviceBits", 0), bit.bnot(device.id)))
	ply:SetNW2Float("SuitPowerLoad", ply:GetNW2Float("SuitPowerLoad", 0) - device.drainrate)

	if ply:GetNW2Int("ActiveDeviceBits", 0) == 0 then
		ply:SetNW2Float("TimeAllSuitDevicesOff", CurTime())
	end

	hook.Run("AUXDeviceRemoved", ply, device)

	return true
end

function AUX.SuitPowerShouldRecharge(ply)
	if ply:IsSprinting() then return false end
	if ply:GetNW2Int("ActiveDeviceBits", 0) ~= 0 then return false end
	if ply:GetNW2Float("SuitPower", 0) >= 100 then return false end
	if CurTime() < ply:GetNW2Float("TimeAllSuitDevicesOff", 0) + SUITPOWER_BEGIN_RECHARGE_DELAY then return false end

	return true
end

local CHOKE_TIME = 1.5
local WATER_HEALTH_RECHARGE_TIME = 3

function GM:PlayerCheckDrowning(ply)
	if not ply:Alive() then return end

	local playerTable = ply:GetTable()

	playerTable.WaterDamage = playerTable.WaterDamage or 0
	local curTime = CurTime()

	if ply:WaterLevel() ~= 3 then
		if playerTable.IsDrowning then
			playerTable.IsDrowning = false
		end

		if playerTable.WaterDamage > 0 then
			playerTable.NextWaterHealthTime = playerTable.NextWaterHealthTime or curTime + WATER_HEALTH_RECHARGE_TIME

			if ply:Health() >= 100 then
				playerTable.WaterDamage = 0
			else
				if playerTable.NextWaterHealthTime < curTime then
					playerTable.WaterDamage = playerTable.WaterDamage - 10
					if ply:Health() + 10 > 100 then
						ply:SetHealth(100)
					else
						ply:SetHealth(ply:Health() + 10)
					end

					playerTable.NextWaterHealthTime = curTime + WATER_HEALTH_RECHARGE_TIME
				end
			end
		end
	else
		--playerTable.NextChokeTime = playerTable.NextChokeTime or curTime + CHOKE_TIME

		if ply:GetNW2Float("SuitPower", 0) == 0 and curTime > (playerTable.NextChokeTime or 0) then
			if not playerTable.IsDrowning then
				playerTable.IsDrowning = true
				playerTable.DrowningStartTime = CurTime()
				playerTable.WaterDamage = 0
				ply:EmitSound("Player.DrownStart")
			end

			if playerTable.DrowningStartTime + CHOKE_TIME < CurTime() then
				local dmgInfo = DamageInfo()
				dmgInfo:SetDamage(10)
				dmgInfo:SetDamageType(DMG_DROWN)
				dmgInfo:SetInflictor(game.GetWorld())
				dmgInfo:SetAttacker(game.GetWorld())

				ply:TakeDamageInfo(dmgInfo)

				playerTable.WaterDamage = playerTable.WaterDamage + 10
				playerTable.NextChokeTime = curTime + CHOKE_TIME
			end
		end
	end
end

function GM:VehicleMove(ply, vehicle, mv)
	-- restore thirdperson
	if mv:KeyPressed(IN_DUCK) and vehicle.SetThirdPersonMode then
		vehicle:SetThirdPersonMode(not vehicle:GetThirdPersonMode())
	end

	local iWheel = ply:GetCurrentCommand():GetMouseWheel()
	if iWheel ~= 0 and vehicle.SetCameraDistance then
		-- The distance is a multiplier
		-- Actual camera distance = ( renderradius + renderradius * dist )
		-- so -1 will be zero.. clamp it there.
		local newdist = math.Clamp(vehicle:GetCameraDistance() - iWheel * 0.03 * (1.1 + vehicle:GetCameraDistance()), -1, 10)
		vehicle:SetCameraDistance(newdist)
	end

	AUX.SuitPowerUpdate(ply, mv)
end

if CLIENT then
	local ScreenScaleH = ScreenScaleH
	if not ScreenScaleH then
		ScreenScaleH = function(n)
			return n * (ScrH() / 480)
		end
	end

	local AUXPOW = {}
	_G.AUXPOW = AUXPOW
	-- Parameters
	local WIDTH, HEIGHT = ScreenScaleH(102), ScreenScaleH(26)
	local EXPENSE_HEIGHT = ScreenScaleH(10)
	local ANIM_RATE = 0.04
	local FADE_SPEED = 0.04

	-- Variables
	local labels = {} -- Expenses to display
	local tick = 0   -- Animation tick
	local height = 0 -- Additional height
	local color = 0  -- Current color (red amount)
	local alpha = 0  -- Opacity

	---Returns the amount of power the client acknowledges the player has
	---@return number power
	function AUXPOW:GetPower()
		return LocalPlayer():GetNW2Float("SuitPower", 100) / 100
	end

	---Given a value, determines how much of each one is shown
	---@param a number
	---@param b number
	---@param value number
	---@return number result
	function AUXPOW:Intersect(a, b, value)
		return (a * value) + (b * (1 - value))
	end

	---Intersects two colors based on a value
	---@param a Color
	---@param b Color
	---@param value number
	---@return Color result
	function AUXPOW:IntersectColor(a, b, value)
		return Color(AUXPOW:Intersect(a.r, b.r, value), AUXPOW:Intersect(a.g, b.g, value), AUXPOW:Intersect(a.b, b.b, value), AUXPOW:Intersect(a.a, b.a, value))
	end

	-- Create scaled font
	surface.CreateFont("auxpow", {
		font = "Verdana",
		size = ScreenScaleH(9),
		weight = 900,
		antialias = true
	})

	---Returns the HUD element color
	---@return Color color
	local function GetColor()
		if oc_hud_mode:GetInt() == 0 then
			local col = AUXPOW:IntersectColor(AUXPOW:GetHUDColor(), AUXPOW:GetHUDCritColor(), color)
			return ColorAlpha(col, col.a * alpha)
		else
			local col = AUXPOW:GetHUDColor()
			return ColorAlpha(col, col.a * alpha)
		end
	end

	---Animates the HUD panel
	local function Animate()
		-- Color
		local col = 1
		if (AUXPOW:GetPower() < 0.2) then col = 0 end
		color = Lerp(RealFrameTime() * 4, color, col)

		-- Linear animations
		if (tick < CurTime()) then
			-- Label animation
			if (table.Count(labels) >= height) then
				height = math.min(height + ANIM_RATE, table.Count(labels))
			else
				height = math.max(height - ANIM_RATE, 0)
			end

			-- Fade-in/out
			if (AUXPOW:GetPower() < 1 or table.Count(labels) > 0) then
				alpha = math.min(alpha + FADE_SPEED, 1)
			else
				alpha = math.max(alpha - FADE_SPEED, 0)
			end

			tick = CurTime() + 0.01
		end
	end

	---Draws a segmented progress bar
	---@param x number
	---@param y number
	---@param w number segment width
	---@param h number segment height
	---@param m number segment margin
	---@param amount number number of segments
	---@param value number
	---@param col? Color color
	---@param a? number alpha
	---@param bgA? number background alpha
	function AUXPOW:DrawBar(x, y, w, h, m, amount, value, col, a, bgA)
		col = col or GetColor()
		a = a or alpha
		bgA = bgA or 0.4

		-- Background
		for i = 0, amount do
			draw.RoundedBox(0, x + (w + m) * i, y, w, h, Color(col.r * 0.8, col.g * 0.8, col.b * 0.8, col.a * bgA * a))
		end

		-- Foreground
		if (value <= 0) then return end
		for i = 0, math.Round(value * amount) do
			draw.RoundedBox(0, x + (w + m) * i, y, w, h, Color(col.r, col.g, col.b, col.a * a))
		end
	end

	---@return Color
	function AUXPOW:GetHUDColor()
		return GAMEMODE:GetHUDTextColor()
	end

	---@return Color
	function AUXPOW:GetHUDCritColor()
		return Color(255, 0, 0)
	end

	---Draws the auxiliary power HUD panel
	---@param x number
	---@param y number
	function AUXPOW:DrawHUDPanel(x, y)
		if (not LocalPlayer():Alive()) then
			labels = {}
			return
		end
		local w, h = WIDTH, HEIGHT + EXPENSE_HEIGHT * height
		y = y - (EXPENSE_HEIGHT * height)

		-- Animate
		Animate()

		-- Background
		draw.RoundedBox(6, x, y, w, h, ColorAlpha(GAMEMODE:GetHUDBGColor(), 80 * alpha))

		-- Title
		draw.SimpleText("AUX POW", "auxpow", x + ScreenScaleH(8), y + ScreenScaleH(4), GetColor())

		-- Bar
		self:DrawBar(x + ScreenScaleH(8), y + ScreenScaleH(16), ScreenScaleH(6), ScreenScaleH(4), ScreenScaleH(3), 9, self:GetPower())

		-- Expenses
		render.SetScissorRect(x, y, x + w, y + h, true)
		local i = 1
		for _, label in pairs(labels) do
			draw.SimpleText(label, "auxpow", x + ScreenScaleH(8), y + (ScreenScaleH(22) + (EXPENSE_HEIGHT * (i - 1))), GetColor())
			i = i + 1
		end
		render.SetScissorRect(0, 0, 0, 0, false)
	end

	---@param x number
	---@param y number
	function AUXPOW:DrawNewHUDPanel(x, y)
		if (not LocalPlayer():Alive()) then
			labels = {}
			return
		end
		local w, h = WIDTH, HEIGHT + EXPENSE_HEIGHT * height
		y = y - (EXPENSE_HEIGHT * height)

		-- Animate
		Animate()

		-- Title
		draw.SimpleText("AUX POWER", "oc_newhud_text", x + ScreenScaleH(8), y + ScreenScaleH(4), GetColor())

		-- Bar
		local barw = ScreenScaleH(92)
		local c = GetColor()
		c.r = c.r * 0.65
		c.g = c.g * 0.65
		c.b = c.b * 0.65
		surface.SetDrawColor(c)
		surface.DrawRect(x + ScreenScaleH(8), y + ScreenScaleH(16), barw, ScreenScaleH(4))

		surface.SetDrawColor(GetColor())
		surface.DrawRect(x + ScreenScaleH(8), y + ScreenScaleH(16), barw * self:GetPower(), ScreenScaleH(4))

		-- Expenses
		render.SetScissorRect(x, y, x + w, y + h, true)
		local i = 1
		for _, label in pairs(labels) do
			draw.SimpleText(label, "oc_newhud_text", x + ScreenScaleH(8), y + (ScreenScaleH(22) + (EXPENSE_HEIGHT * (i - 1))), GetColor())
			i = i + 1
		end
		render.SetScissorRect(0, 0, 0, 0, false)
	end

	local aux_x = ScreenScaleH(4)
	function GM:HUDDrawAUX()
		if sv_infinite_aux_power:GetBool() then return end

		for _, device in next, AUX.Devices do
			if AUX.SuitPowerIsDeviceActive(LocalPlayer(), device) then
				labels[device.id] = device.label
			else
				labels[device.id] = nil
			end
		end

		if not oc_hud_mode then oc_hud_mode = GetConVar"oc_hud_mode" end
		if oc_hud_mode:GetInt() == 0 then
			AUXPOW:DrawHUDPanel(ScreenScaleH(16), ScreenScaleH(390)) --hl2 stock: ScreenScaleH(16), ScreenScaleH(396)
		else
			aux_x = Lerp(RealFrameTime() * 4, aux_x, AUXPOW:GetPower() < 1 and ScreenScaleH(16) or ScreenScaleH(4))
			AUXPOW:DrawNewHUDPanel(aux_x, ScreenScaleH(384))
		end
	end
end
