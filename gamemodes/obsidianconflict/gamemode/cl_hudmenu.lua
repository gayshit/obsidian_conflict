if not CLIENT then return end

local TAG = "LuaHudMenu"

local PANEL = {}

PANEL.m_bMenuDisplayed = false
PANEL.m_bMenuTakesInput = false
PANEL.m_bitsValidSlots = 0
PANEL.m_flOpenCloseTime = 1
PANEL.m_flBlur = 0
PANEL.m_flTextScan = 0
PANEL.m_flSelectionAlphaOverride = 0

local ItemColor = Color(255, 167, 42, 200)
local MenuColor = Color(233, 208, 173, 255)
local BackgroundColor = Color(0, 0, 0, 100)

PANEL.m_nSelectedItem = 1

PANEL.m_Border = 20

function PANEL:Paint(w, h)
	if not self.m_bMenuDisplayed then return end

	local border = self.m_Border

	local wide = self.m_nMaxPixels + border
	local tall = self.m_nHeight + border

	local x, y = border / 2, border / 2

	BackgroundColor.a = 100 * (self.m_flSelectionAlphaOverride / 255)

	draw.RoundedBox(4, 0, 0, wide, tall, BackgroundColor)

	MenuColor.a = 255 * (self.m_flSelectionAlphaOverride / 255)
	ItemColor.a = 200 * (self.m_flSelectionAlphaOverride / 255)

	for i, line in ipairs(self.m_Processed) do
		local canblur = false
		if line.menuitem ~= 0 and self.m_nSelectedItem >= 0 and line.menuitem == self.m_nSelectedItem then
			canblur = true
		end

		local clr = line.menuitem ~= 0 and ItemColor or MenuColor
		surface.SetTextColor(clr)

		self.m_flTextScan = 1
		local strText = line.text
		local len = math.floor(#strText * self.m_flTextScan)
		if len ~= #strText then
			strText = string.sub(strText, 1, len)
		end

		surface.SetFont(line.menuitem ~= 0 and self.m_ItemFont or self.m_TextFont)
		surface.SetTextPos(x, y)
		surface.DrawText(strText)

		if canblur then
			-- draw the overbright blur
			surface.SetFont(self.m_ItemFontPulsing)

			for fl = self.m_flBlur, 0, -1 do
				if fl >= 1 then
					surface.SetTextPos(x, y)
					surface.DrawText(strText)
				else
					-- draw a percentage of the last one
					local col = table.Copy(clr)
					col.a = col.a * fl

					surface.SetTextColor(col)
					surface.SetTextPos(x, y)
					surface.DrawText(strText)
				end
			end
		end

		y = y + (line.height / 2)
	end
end

function PANEL:Init()
	self.m_nMaxPixels = ScreenScale(240)

	self:SetMouseInputEnabled(false)
	--self:ProcessText()
	--self:Resize()
end

local flSelectionTimeout = 5.0
function PANEL:Think()
	-- If we've been open for a while without input, hide
	--if self.m_bMenuDisplayed and CurTime() - self.m_flSelectionTime > flSelectionTimeout then
	--    self:Anim_MenuClose(function(anim, self)
	--        self.m_bMenuDisplayed = false
	--    end)
	--end

	-- check for if menu is set to disappear
	if self.m_flShutoffTime > 0 and self.m_flShutoffTime <= RealTime() then
		-- times up, shutoff
		self:Anim_MenuClose(function(anim, self)
			self.m_bMenuDisplayed = false
		end)
		--self.m_bMenuDisplayed = false
	end
end

function PANEL:Resize()
	local border = self.m_Border
	local x = border

	local wide = self.m_nMaxPixels + border
	local tall = self.m_nHeight + border

	local y = (ScrH() - tall) * 0.5

	self:SetPos(x, y)
	self:SetSize(wide, tall)
end

function PANEL:IsMenuOpen()
	return self.m_bMenuDisplayed and self.m_bMenuTakesInput
end

--[[
    // commands:
    //	Animate <panel name> <variable> <target value> <interpolator> <start time> <duration>
    //		variables:
    //			FgColor
    //			BgColor
    //			Position
    //			Size
    //			Blur		(hud panels only)
    //			TextColor	(hud panels only)
    //			Ammo2Color	(hud panels only)
    //			Alpha		(hud weapon selection only)
    //			SelectionAlpha  (hud weapon selection only)
    //			TextScan	(hud weapon selection only)
    //
    //		interpolator:
    //			Linear
    //			Accel - starts moving slow, ends fast
    //			Deaccel - starts moving fast, ends slow
    //
    //	RunEvent <event name> <start time>
    //		starts another even running at the specified time
    //
    //	StopEvent <event name> <start time>
    //		stops another event that is current running at the specified time
    //
    //	StopAnimation <panel name> <variable> <start time>
    //		stops all animations refering to the specified variable in the specified panel
    //
    //	StopPanelAnimations <panel name> <start time>
    //		stops all active animations operating on the specified panel
    //
    //
    // Useful game console commands:
    //	cl_Animationinfo <hudelement name> or <panelname>
    //		displays all the animatable variables for the hud element
]]

--[[
    event MenuOpen
    {
        StopEvent	MenuClose 0.0

        // fade in
        Animate HudMenu Alpha 		"255"		Linear 0.0 0.1
        Animate HudMenu SelectionAlpha 	"255"		Linear 0.0 0.1
        Animate HudMenu FgColor		"FgColor"		Linear 0.0 0.1
        Animate HudMenu MenuColor		"MenuColor"	Linear  0.0 0.1
        Animate HudMenu ItemColor		"ItemColor"	Linear 0.0 0.1
        Animate HudMenu TextScan		"1"		Linear 0.0 0.1

        // Undo any blur
        Animate HudMenu		Blur		"1"			Linear	0.0		0.01
    }
]]
local function MenuOpen(anim, panel, frac)
	panel.m_flSelectionAlphaOverride = Lerp(frac, anim.StartAlpha, 255)
	panel.m_flTextScan = Lerp(frac, anim.StartScan, 1)
	panel.m_flBlur = Lerp(frac, anim.StartBlur, 1)
end
function PANEL:Anim_MenuOpen()
	-- stop MenuClose
	local List = self.m_AnimList or {}
	for k, anim in next, List do
		if anim.m_bIsClosingAnimation then
			List[k] = nil
		end
	end

	local anim = self:NewAnimation(0.1, 0, 1)
	anim.Think = MenuOpen
	anim.StartAlpha = self.m_flSelectionAlphaOverride
	anim.StartScan = self.m_flTextScan
	anim.StartBlur = self.m_flBlur

	self.AnimOpen = anim
end

--[[
    event MenuClose
    {
        // Hide it
        Animate HudMenu Alpha 		"0" Linear 0.0 1
        Animate HudMenu SelectionAlpha 	"0" Linear 0.0 1
        Animate HudMenu FgColor		"0 0 0 0" Linear 0.0 1
        Animate HudMenu MenuColor		"0 0 0 0" Linear 0.0 1
        Animate HudMenu ItemColor		"0 0 0 0" Linear 0.0 1
    }
]]
local function MenuClose(anim, panel, frac)
	panel.m_flSelectionAlphaOverride = Lerp(frac, anim.StartAlpha, 0)
	panel.m_flTextScan = Lerp(frac, anim.StartScan, 0)
end
function PANEL:Anim_MenuClose(callback)
	local anim = self:NewAnimation(1, 0, 1)
	anim.m_bIsClosingAnimation = true
	anim.Think = MenuClose
	anim.OnEnd = callback
	anim.StartAlpha = self.m_flSelectionAlphaOverride
	anim.StartScan = self.m_flTextScan

	self.AnimClose = anim
end

--[[
    event MenuPulse
    {
        Animate HudMenu		Blur		"7"			Linear	0.0		0.1
        Animate HudMenu		Blur		"2"			Deaccel	0.1		0.1
        Animate HudMenu		Blur		"7"			Linear	0.2		0.1
        Animate HudMenu		Blur		"2"			Deaccel	0.3		0.1
        Animate HudMenu		Blur		"7"			Linear	0.4		0.1
        Animate HudMenu		Blur		"2"			Deaccel	0.5		0.1
        Animate	HudMenu		Blur		"1"			Deaccel	0.6		0.4
    }
]]

local function linear(f)
	return f
end
local deaccel = math.ease.OutExpo

local Stages = {
	-- {end value, function, start time, duration}
	{7, linear,  0.0, 0.1},
	{2, deaccel, 0.1, 0.1},
	{7, linear,  0.2, 0.1},
	{2, deaccel, 0.3, 0.1},
	{7, linear,  0.4, 0.1},
	{2, deaccel, 0.5, 0.1},
	{1, deaccel, 0.6, 0.4}
}

-- this animation coincidentally lasts 1 second, so we can just use the fraction as time
local function MenuPulse(anim, panel, frac)
	local curStage
	for i = 1, #Stages do
		local t = Stages[i]

		local time = t[3] + t[4]
		if time >= frac then
			curStage = t
			break
		end
	end

	local endValue = curStage[1]
	local func = curStage[2]

	panel.m_flBlur = Lerp(func(frac), anim.StartBlur, endValue)
end
function PANEL:Anim_MenuPulse()
	local anim = self:NewAnimation(1, 0, -1)
	anim.Think = MenuPulse
	anim.StartBlur = self.m_flBlur

	self.AnimBlur = anim
end

function PANEL:SelectMenuItem(menu_item)
	if menu_item > 0 and bit.band(self.m_bitsValidSlots, bit.lshift(1, menu_item - 1)) ~= 0 then
		if isfunction(self.CustomSelectMenuItem) then
			self:CustomSelectMenuItem(menu_item)
			self.CustomSelectMenuItem = nil
		else
			RunConsoleCommand("lua_menuselect", menu_item)
		end

		self.m_nSelectedItem = menu_item

		-- Pulse the selection
		self:Anim_MenuPulse()

		self.m_bMenuTakesInput = false
		self.m_flShutoffTime = RealTime() + self.m_flOpenCloseTime
		self:Anim_MenuClose()
	end
end

PANEL.m_TextFont = "HudDefault"
PANEL.m_ItemFont = "HudDefault"
PANEL.m_ItemFontPulsing = "HudDefault"

PANEL.m_strMenuString = "Pick a Team: \n\n\n->1. Humans\n\n->2. Aliens\n\n\n->3. Test\n\n\n->4. Test\n\n\n->5. Test\n\n\n->6. Test\n\n\n->7. Test\n\n\n->8. Test\n\n\n->9. Don't change team.\n\n\n->0. Test\n"
PANEL.m_Processed = {}
PANEL.m_nMaxPixels = 0
PANEL.m_nHeight = 0
function PANEL:ProcessText()
	table.Empty(self.m_Processed)
	self.m_nMaxPixels = 0
	self.m_nHeight = 0

	local str = self.m_strMenuString
	local iLength = #str

	local i, startpos, menuitem = 1, 1, 0
	while i < iLength do
		local char = string.sub(str, i, i)

		-- Special handling for menu item specifiers
		if i == startpos and char == "-" and string.sub(str, i + 1, i + 1) == ">" then
			menuitem = tonumber(string.match(str, "%d", i + 2))
			i = i + 2
			startpos = startpos + 2
			continue
		end

		-- Skip to end of line
		while i < iLength and string.sub(str, i, i) ~= "\n" do
			i = i + 1
		end

		-- Store off line
		if i - startpos >= 1 then
			self.m_Processed[#self.m_Processed + 1] = {
				text = string.sub(str, startpos, i),
				menuitem = menuitem,
				length = i - startpos,
				pixels = 0,
				height = 0,
			}
		end

		menuitem = 0

		-- Skip delimiter
		if string.sub(str, i, i) == "\n" then
			i = i + 1
		end

		startpos = i
	end

	-- Add final block
	if i - startpos >= 1 then
		self.m_Processed[#self.m_Processed + 1] = {
			text = string.sub(str, startpos, i),
			menuitem = menuitem,
			length = i - startpos,
			pixels = 0,
			height = 0,
		}
	end

	-- Now compute size
	for _, l in ipairs(self.m_Processed) do
		local font = l.menuitem ~= 0 and self.m_ItemFont or self.m_TextFont

		surface.SetFont(font)
		local w, h = surface.GetTextSize(l.text)

		l.pixels = w
		l.height = h

		if w > self.m_nMaxPixels then
			self.m_nMaxPixels = w
		end

		self.m_nHeight = self.m_nHeight + (l.height / 2)
	end

	self.m_nHeight = self.m_nHeight + 10
end

function PANEL:ShowMenu(menuName, validSlots)
	if isfunction(self.OnNewMenu) then
		self:OnNewMenu()
		self.OnNewMenu = nil
	end

	self.m_flShutoffTime = -1
	self.m_bitsValidSlots = validSlots

	if validSlots ~= 0 then
		self:Anim_MenuOpen()
	else
		self:Anim_MenuClose()
	end
	self.m_nSelectedItem = -1

	-- localize??
	--[[
	// we have the whole string, so we can localise it now
	char szMenuString[MAX_MENU_STRING];
	Q_strncpy( szMenuString, ConvertCRtoNL( hudtextmessage->BufferedLocaliseTextString( g_szPrelocalisedMenuString ) ), sizeof( szMenuString ) );
	g_pVGuiLocalize->ConvertANSIToUnicode( szMenuString, g_szMenuString, sizeof( g_szMenuString ) );
	]]

	self.m_strMenuString = menuName

	self:ProcessText()
	self:Resize()

	self.m_bMenuDisplayed = true
	self.m_bMenuTakesInput = true

	self.m_flSelectionTime = CurTime()
end

function PANEL:HideMenu()
	self.m_bMenuTakesInput = false
	self.m_flShutoffTime = RealTime() + self.m_flOpenCloseTime

	self:Anim_MenuClose()
end

local Factory = vgui.RegisterTable(PANEL, "Panel")

if IsValid(HUD_AMXMENU) then
	HUD_AMXMENU:Remove()
end

function CREATE_HUD_AMXMENU()
	if not IsValid(HUD_AMXMENU) then
		local panel = vgui.CreateFromTable(Factory)
		HUD_AMXMENU = panel

		return panel
	end
end

hook.Add("PlayerBindPress", "HudMenu", function(ply, bind, pressed, button)
	if button < KEY_0 or button > KEY_9 then return end
	if not pressed then return end

	local panel = HUD_AMXMENU
	if not IsValid(panel) then return end

	if panel:IsMenuOpen() then
		-- from [0-9] to [1-10]
		panel:SelectMenuItem((button - 2) % 10 + 1)

		return true
	end
end)

net.Receive(TAG, function(len)
	local allowed = net.ReadUInt(16)
	local display_time = net.ReadInt(8)
	local buffer = net.ReadString()

	local panel = HUD_AMXMENU
	if not IsValid(panel) then
		panel = CREATE_HUD_AMXMENU()
	end

	panel:ShowMenu(buffer, allowed)

	if display_time > 0 then
		panel.m_flShutoffTime = RealTime() + panel.m_flOpenCloseTime + display_time
	else
		panel.m_flShutoffTime = -1
	end
end)
