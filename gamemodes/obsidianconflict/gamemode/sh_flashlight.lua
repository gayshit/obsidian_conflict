AddCSLuaFile()

CreateClientConVar("oc_flashlight_col", "255 255 255", true, true)

if SERVER then
	GM.Flashlights = setmetatable({}, {__mode = "k"})
end

local PLY = FindMetaTable("Player")

local r_swingflashlight = GetConVar("r_swingflashlight")
local r_flashlightfar = GetConVar("r_flashlightfar")
local r_flashlightladderdist = GetConVar("r_flashlightladderdist")
local r_flashlightnear = GetConVar("r_flashlightnear")
local r_flashlightfov = GetConVar("r_flashlightfov")

local oc_flashlight_col

local flEpsilon = 0.1
local flDistCutoff = 128
local flDistDrag = 0.2

local FLASHLIGHT_MINS, FLASHLIGHT_MAXS = Vector(-4, -4, -4), Vector(4, 4, 4)

local function UpdateLight(ply, ent, vecPos, vecForward, vecRight, vecUp)
	local r_swingflashlight_val
	local r_flashlightfar_val
	local r_flashlightladderdist_val
	local r_flashlightnear_val
	local r_flashlightfov_val

	if SERVER then
		r_swingflashlight_val = true
		r_flashlightfar_val = 750
		r_flashlightladderdist_val = 40
		r_flashlightnear_val = 4
		r_flashlightfov_val = 60
	else
		r_swingflashlight_val = r_swingflashlight:GetBool()
		r_flashlightfar_val = r_flashlightfar:GetFloat()
		r_flashlightladderdist_val = r_flashlightladderdist:GetFloat()
		r_flashlightnear_val = r_flashlightnear:GetFloat()
		r_flashlightfov_val = r_flashlightfov:GetFloat()
	end

	local m_flDistMod = ent:GetNW2Float("m_flDistMod", 1)

	local bPlayerOnLadder = ply:GetMoveType() == MOVETYPE_LADDER

	local flOffsetY = 0

	if r_swingflashlight_val then
		local vecSwingLight = vecPos + vecForward * -12
		if vecSwingLight.z > vecPos.z then
			flOffsetY = flOffsetY + (vecSwingLight.z - vecPos.z)
		end
	end

	local vOrigin = vecPos + flOffsetY * vecUp

	local iMask = MASK_OPAQUE_AND_NPCS
	iMask = bit.band(iMask, bit.bnot(CONTENTS_HITBOX))
	iMask = bit.bor(iMask, CONTENTS_WINDOW)

	local vTarget = vecPos + vecForward * r_flashlightfar_val

	local vDir = vTarget - vOrigin
	local vRight = vecRight
	local vUp = vecUp
	vDir:Normalize()
	vRight:Normalize()
	vUp:Normalize()

	vUp = vUp - vDir:Dot(vUp) * vDir
	vUp:Normalize()
	vRight = vRight - vDir:Dot(vRight) * vDir
	vRight:Normalize()
	vRight = vRight - vUp:Dot(vRight) * vUp
	vRight:Normalize()

	local trace = util.TraceHull({
		start = vOrigin,
		endpos = vTarget,
		maxs = FLASHLIGHT_MINS,
		mins = FLASHLIGHT_MAXS,
		mask = iMask,
		filter = {ply, ply:GetViewModel(), ent}
	})

	local flDist = (trace.HitPos - vOrigin):Length()
	if flDist < flDistCutoff then
		local flPullBackDist = bPlayerOnLadder and r_flashlightladderdist_val or flDistCutoff - flDist
		m_flDistMod = Lerp(flDistDrag, m_flDistMod, flPullBackDist)

		if not bPlayerOnLadder then
			local backTrace = util.TraceHull({
				start = vOrigin,
				endpos = vOrigin - vDir * (flPullBackDist - flEpsilon),
				maxs = Vector(-4, -4, -4),
				mins = Vector(4, 4, 4),
				mask = iMask,
				filter = {ply, ply:GetViewModel(), ent}
			})

			if backTrace.Hit then
				local flMaxDist = (trace.HitPos - vOrigin):Length() - flEpsilon
				if m_flDistMod > flMaxDist then
					m_flDistMod = flMaxDist
				end
			end
		end

		ent:SetNW2Float("m_flDistMod", m_flDistMod)
	else
		m_flDistMod = Lerp(flDistDrag, m_flDistMod, 0)
		ent:SetNW2Float("m_flDistMod", m_flDistMod)
	end
	vOrigin = vOrigin - vDir * m_flDistMod

	ent:SetPos(vOrigin)

	local bFlicker = false

	--TODO: Episodic flashlight flicker?

	if bFlicker == false then
		ent:SetKeyValue("lightfov", r_flashlightfov_val)
	end

	local val
	if CLIENT then
		if not oc_flashlight_col then
			oc_flashlight_col = GetConVar("oc_flashlight_col")
		end
		val = oc_flashlight_col:GetString()
	else
		val = ply:GetInfo("oc_flashlight_col")
	end

	ent:SetKeyValue("lightcolor", val .. " 255")

	ent:SetKeyValue("nearz", r_flashlightnear_val + m_flDistMod)
	ent:SetKeyValue("farz", r_flashlightfar_val)
end

if SERVER then
	function GM:SetupFlashlight(ply)
		local bCreate = not IsValid(self.Flashlights[ply])

		local flash = bCreate and ents.Create("env_projectedtexture") or self.Flashlights[ply]
		flash:SetPos(ply:EyePos() + ply:EyeAngles():Forward() * 15)
		flash:SetAngles(ply:EyeAngles())
		flash:SetKeyValue("farz", ply:GetInfo("r_flashlightfar", "750.0"))
		flash:SetKeyValue("nearz", ply:GetInfo("r_flashlightnear", "4.0"))
		flash:SetKeyValue("lightfov", ply:GetInfo("r_flashlightfov", "60.0"))
		flash:SetKeyValue("lightcolor", ply:GetInfo("oc_flashlight_col", "255 255 255") .. " 255")
		flash:SetKeyValue("texturename", "effects/flashlight001")

		if bCreate then flash:Spawn() end

		flash:SetNW2Float("m_flDistMod", 1)
		ply:SetNW2Entity("flashlight_ent", flash)

		self.Flashlights[ply] = flash
	end

	function GM:DestroyFlashlight(ply)
		if IsValid(self.Flashlights[ply]) then
			ply:SetNW2Bool("flashlight_state", false)
			ply:SetNW2Entity("flashlight_ent", NULL)
			SafeRemoveEntity(self.Flashlights[ply])
			self.Flashlights[ply] = nil
		end
	end

	function GM:ToggleFlashlight(ply, state)
		if not ply:GetNW2Bool("flashlight_enabled", true) then return end
		ply:SetNW2Bool("flashlight_state", state)

		if not IsValid(self.Flashlights[ply]) then
			self:SetupFlashlight(ply)
		end

		if IsValid(self.Flashlights[ply]) then
			if state == true then
				self.Flashlights[ply]:Fire"TurnOn"
			else
				self.Flashlights[ply]:Fire"TurnOff"
			end
		end

		ply:EmitSound("Player.FlashlightOn")
	end

	local mp_flashlight = CreateConVar("oc_flashlight", "1", bit.bor(FCVAR_NOTIFY, FCVAR_REPLICATED))
	function GM:PlayerSwitchFlashlight(ply, on)
		if ply:GetNW2Float("SuitPower", 100) == 0 and on then return false end
		if not mp_flashlight:GetBool() then return false end

		self:ToggleFlashlight(ply, not ply:GetNW2Bool("flashlight_state", false))

		return false
	end

	function GM:UpdatePlayerFlashLight(ply, forward, right, up)
		UpdateLight(ply, self.Flashlights[ply], ply:EyePos(), forward, right, up)
	end

	--[[function GM:PlayerPostThink(ply)
		if not ply:Alive() and self.Flashlights[ply] then self:DestroyFlashlight(ply) end

		if IsValid(self.Flashlights[ply]) then
			local eyeVectors = ply:GetAimVector():Angle()
			local vecForward, vecRight, vecUp = eyeVectors:Forward(), eyeVectors:Right(), eyeVectors:Up()

			self:UpdatePlayerFlashLight(ply, vecForward, vecRight, vecUp)
			--UpdateLight(ply, self.Flashlights[ply], ply:EyePos(), vecForward, vecRight, vecUp)
			self.Flashlights[ply]:SetAngles(ply:EyeAngles())
		end
	end]]

	function PLY:Flashlight(on)
		GAMEMODE:ToggleFlashlight(self, on)
	end
else
	-- done to smooth out the flashlight on the client's end in first person, otherwise its jittery
	hook.Add("CalcView", "oc_flashlight", function(ply, pos, angles, fov, znear, zfar)
		local target = ply

		if ply:GetObserverMode() == OBS_MODE_IN_EYE then
			local obsTarget = ply:GetObserverTarget()
			if obsTarget:IsValid() and obsTarget:IsPlayer() then
				target = obsTarget
			end
		end

		local flashlight = target:GetNW2Entity("flashlight_ent")
		if IsValid(flashlight) and target:GetNW2Bool("flashlight_state", false) then
			local eyeVectors = target:GetAimVector():Angle()
			local vecForward, vecRight, vecUp = eyeVectors:Forward(), eyeVectors:Right(), eyeVectors:Up()

			UpdateLight(target, flashlight, target:EyePos(), vecForward, vecRight, vecUp)
			flashlight:SetAngles(target:EyeAngles())
		end
	end)
end

function PLY:FlashlightIsOn()
	return self:GetNW2Bool("flashlight_state", false)
end

function PLY:AllowFlashlight(state)
	self:SetNW2Bool("flashlight_enabled", state)
end
