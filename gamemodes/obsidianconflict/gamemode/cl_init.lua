include"thirdparty/cami.lua"

include"shared.lua"
include"cl_settings.lua"
include"cl_quickinfo.lua"
include"cl_hud.lua"
include"cl_weaponswitcher.lua"
include"cl_scoreboard.lua"
include"cl_hudmenu.lua"

local oc_chatsound = CreateClientConVar("oc_chatsound", "1")
local oc_voice = CreateClientConVar("oc_voice", "0", true, true, "Voice for callouts. 0 - auto, 1 - male, 2 - female, 3 - combine")
local oc_teamplay = GetConVar("oc_teamplay")

function GM:Initialize()
	self:RegisterOCWeapons()
	self:RegisterOCSoundScripts()
end

function GM:OnSpawnMenuOpen()
	RunConsoleCommand("lastinv")
end

local UNIT_TO_INCH = .75
local INCH_TO_CM = 2.54
function util.HUtoInch(units)
	return units * UNIT_TO_INCH
end

function util.HUtoCM(units)
	return util.HUtoInch(units) * INCH_TO_CM
end

function util.NiceMetric(cm)
	local m = cm / 100
	return string.format("%.1f%s", m >= 1 and m or cm, m >= 1 and "m" or "cm")
end

local function transferpoints()
	local amount = 1
	local who = -1

	local f = vgui.Create("DFrame")
	f:MakePopup()
	f:SetSize(360, 180)
	f:SetPos(85, 100)
	f:SetDraggable(false)
	f:SetTitle("Points Transfer")
	f:ShowCloseButton(false)

	local left = vgui.Create("DSizeToContents", f)
	left:SetSizeX(false)
	left:Dock(LEFT)
	left:DockPadding(0, 10, 0, 0)
	left:DockMargin(0, 0, 20, 0)
	left:SetWide(f:GetWide() / 2)
	left:InvalidateLayout()

	local right = vgui.Create("DSizeToContents", f)
	right:SetSizeX(false)
	right:Dock(FILL)
	right:DockPadding(20, 10, 0, 0)
	right:InvalidateLayout()

	local text = vgui.Create("RichText", left)
	text:Dock(FILL)
	text:SetText("How many points would you like to transfer?")
	text:SetVerticalScrollbarEnabled(false)
	function text:PerformLayout()
		self:SetFontInternal("Trebuchet24")
	end

	local entry = vgui.Create("DTextEntry", right)
	entry:Dock(TOP)
	entry:SetNumeric(true)
	entry:SetZPos(-100)
	function entry:OnChange()
		amount = tonumber(self:GetValue())
	end

	entry:SetValue(1)
	entry:OnTextChanged()

	local players = vgui.Create("DComboBox", right)
	players:Dock(TOP)
	function players:OnSelect(_, _, userid)
		who = userid
	end

	local accept = vgui.Create("DButton", right)
	accept:Dock(TOP)
	accept:SetText("Transfer Now")
	function accept:DoClick()
		net.Start("oc_cmd")
		net.WriteUInt(OC_CMD_TRANSFERPOINTS, 4)

		net.WriteUInt(who, 8)
		net.WriteUInt(amount, 16)
		net.SendToServer()

		f:Close()
	end

	local cancel = vgui.Create("DButton", right)
	cancel:Dock(TOP)
	cancel:SetText("Cancel")
	function cancel:DoClick()
		f:Close()
	end

	for _, p in player.Iterator() do
		if p == LocalPlayer() then continue end
		players:AddChoice(p:Name(), p:UserID())
	end
end
local function dropammo()
	--
end
local function toggle_cloak()
	net.Start("oc_cmd")
	net.WriteUInt(OC_CMD_TOGGLE_ITEM, 4)

	net.WriteUInt(0, 4)
	net.SendToServer()
end
local function toggle_shield()
	net.Start("oc_cmd")
	net.WriteUInt(OC_CMD_TOGGLE_ITEM, 4)

	net.WriteUInt(1, 4)
	net.SendToServer()
end

concommand.Add("oc_transferpoints", transferpoints)
concommand.Add("oc_cloak", toggle_cloak)
concommand.Add("oc_shield", toggle_shield)

function GM:PlayerBindPress(ply, bind, pressed)
	if ply ~= LocalPlayer() then return end

	if pressed then
		if (bind == "gmod_undo" or bind == "undo") then
			dropammo()
			return true
		elseif bind == "noclip" then
			transferpoints()
			return true
		elseif bind == "+attack3" then
			net.Start("oc_cmd")
			net.WriteUInt(OC_CMD_PING, 4)
			net.SendToServer()
			return true
		end
	end
end

-- networking NPC disposition
do
	net.Receive("oc_netdisp", function()
		local index = net.ReadUInt(16)
		local old = net.ReadUInt(8)
		local new = net.ReadUInt(8)
		local ent = Entity(index)

		if IsValid(ent) then
			hook.Run("EntityDispositionChanged", ent, old, new)
		else
			hook.Add("NetworkEntityCreated", "oc_" .. index, function(ent)
				if ent:EntIndex() == index then
					hook.Run("EntityDispositionChanged", ent, old, new)
					hook.Remove("NetworkEntityCreated", "oc_" .. index)
				end
			end)
		end
	end)
	hook.Add("EntityDispositionChanged", "oc", function(ent, old, new)
		ent.oc_disp = new
	end)

	local NPC = FindMetaTable("NPC")
	local localplayer = LocalPlayer() or NULL
	function NPC:Disposition(ent)
		if not localplayer:IsValid() then
			localplayer = LocalPlayer()
		end

		if ent == localplayer then
			return self.oc_disp
		end

		return D_NU
	end
end

hook.Add("OnPlayerChat", "oc_talk", function(ply, txt, team, dead)
	if IsValid(ply) and txt ~= "" and oc_chatsound:GetBool() then
		surface.PlaySound("common/oc_talk.wav")
	end
end)

function GM:InitPostEntity()
	player_manager.RunClass(LocalPlayer(), "Init")
end

function GM:GetTeamColor(ent)
	local team = TEAM_UNASSIGNED

	if not IsValid(ent) then
		return self:GetTeamNumColor(team)
	end

	if ent:IsPlayer() then
		if ent:IsBot() then
			team = ent:Team()
			return self:GetTeamNumColor(team)
		end

		if oc_teamplay:GetBool() then
			team = ent:Team()
			return self:GetTeamNumColor(team)
		else
			return ent:GetPlayerColor():ToColor()
		end
	else
		if ent.Team then
			team = ent:Team()
		end

		return self:GetTeamNumColor(team)
	end
end

concommand.Add("CallForAttention", function()
	net.Start("oc_cmd")
	net.WriteUInt(OC_CMD_PLAYERWAYPOINT, 4)

	net.WriteBool(false)
	net.SendToServer()
end)

concommand.Add("CallForMedic", function()
	net.Start("oc_cmd")
	net.WriteUInt(OC_CMD_PLAYERWAYPOINT, 4)

	net.WriteBool(true)
	net.SendToServer()
end)

concommand.Add("oc_ping", function()
	net.Start("oc_cmd")
	net.WriteUInt(OC_CMD_PING, 4)
	net.SendToServer()
end)

concommand.Add("drop", function()
	net.Start("oc_cmd")
	net.WriteUInt(OC_CMD_DROPWEAPON, 4)
	net.SendToServer()
end)

do
	local TAG = "oc_cmd"
	local SWITCH_NET_RECV = {
		[OC_CMD_PING] = function(len)
			local pos = net.ReadVector()
			local norm = net.ReadVector()

			-- TODO: (optional) ring particle effect, i personally think its kinda meh though, especially with it being
			--       shown off as just taking the trace hitnormal

			EmitSound(Sound("hl1/fvox/bell.wav"), pos, -2, CHAN_STATIC, 1, 75, 0, 100, 0)
		end,
	}

	net.Receive(TAG, function(len)
		local cmd = net.ReadUInt(4)
		len = len - 4

		local callback = SWITCH_NET_RECV[cmd]
		if isfunction(callback) then
			callback(len)
		else
			print(Format("Net \"%s\": unknown cmd %d, len: %d", TAG, cmd, len))
		end
	end)
end

-- networked PlayerSpawn to fix hull (and futureproofing)
net.Receive("oc_playerspawn", function(len)
	local ply = net.ReadEntity()
	hook.Call("ClientPlayerSpawn", GAMEMODE, ply)
end)

function GM:ClientPlayerSpawn(ply)
	if not IsValid(ply) then return end

	ply:ResetHull()
	ply:SetHullDuck(Vector(-16, -16, 0), Vector(16, 16, 32))
end

local oc_menu_commands = {
	"CallForMedic",
	"CallForAttention",
	"oc_ping",
	"locate_players",

	"oc_cloak",
	"oc_shield",

	"inventory_store",
	"inventory_dropprop",
	"drop",
}
local function CreateOCMenu()
	local me = LocalPlayer()

	local canWaypoint = CurTime() > me:GetNW2Float("NextPlayerWaypoint", 0)
	local waypointPrefix = canWaypoint and "->" or ""

	local hasCloak = me:GetHasCloak()
	local hasShield = me:GetHasShield()
	local cloak = hasCloak and "->" or ""
	local shield = hasShield and "->" or ""

	local hasProp = me:GetNWEntity("oc_inv_prop"):IsValid()
	local canStore = me:GetNW2Entity("oc_heldobj"):IsValid() and not hasProp

	local storePrefix = canStore and "->" or ""
	local dropPrefix = hasProp and "->" or ""

	local hasWeapon = me:GetActiveWeapon():IsValid()
	local weaponPrefix = hasWeapon and "->" or ""

	local mask = bit.bor(bit.lshift(1, 3), bit.lshift(1, 9))
	if canWaypoint then
		mask = bit.bor(mask, 1, bit.lshift(1, 1), bit.lshift(1, 2))
	end
	if hasCloak then
		mask = bit.bor(mask, bit.lshift(1, 4))
	end
	if hasShield then
		mask = bit.bor(mask, bit.lshift(1, 5))
	end
	if canStore then
		mask = bit.bor(mask, bit.lshift(1, 6))
	end
	if hasProp then
		mask = bit.bor(mask, bit.lshift(1, 7))
	end
	if hasWeapon then
		mask = bit.bor(mask, bit.lshift(1, 8))
	end

	local str = {
		waypointPrefix .. "1. Medic!",
		waypointPrefix .. "2. Attention!",
		waypointPrefix .. "3. Ping",
		"->4. Locate players",
		" ",
		cloak .. "5. Toggle Cloak",
		shield .. "6. Toggle Shield",
		" ",
		storePrefix .. "7. Store held prop",
		dropPrefix .. "8. Drop stored prop",
		weaponPrefix .. "9. Drop weapon",
		" ",
		"->0. Close menu",
	}

	return table.concat(str, "\n"), mask
end
local function UpdateOCMenu()
	local me = LocalPlayer()
	local buffer, allowed = CreateOCMenu()

	local panel = HUD_AMXMENU
	if not IsValid(panel) then return end
	if not panel:IsMenuOpen() or not panel.__oc_menu then return end

	if not me:Alive() then
		panel:SelectMenuItem(10)
		return
	end

	panel.m_bitsValidSlots = allowed
	panel.m_strMenuString = buffer

	panel:ProcessText()
	panel:Resize()
end

concommand.Add("oc_menu", function()
	local buffer, allowed = CreateOCMenu()

	local panel = HUD_AMXMENU
	if not IsValid(panel) then
		panel = CREATE_HUD_AMXMENU()
	end

	if panel:IsMenuOpen() and panel.__oc_menu then
		panel:SelectMenuItem(10)
	else
		panel.__oc_menu = true
		panel:ShowMenu(buffer, allowed)
		local function CleanupMenu(s)
			s.__oc_menu = nil
			timer.Remove("oc_menu")
		end
		panel.OnNewMenu = CleanupMenu
		panel.CustomSelectMenuItem = function(s, menu_item)
			timer.Remove("oc_menu")
			local cmd = oc_menu_commands[menu_item]
			if cmd then
				RunConsoleCommand(cmd)
			end
			CleanupMenu(s)
		end
		timer.Create("oc_menu", 0.1, 0, UpdateOCMenu)
	end
end)

function GM:OnContextMenuOpen()
	RunConsoleCommand("oc_menu")
end

local COLOR_WHITE = Color(255, 255, 255)
net.Receive("oc_mapdecals", function()
	--[[local len = net.ReadUInt(32)
	local data = net.ReadData(len)
	local json = util.Decompress(data, len)
	if not json then return end--]]
	local json = net.ReadString()

	local MapDecals = util.JSONToTable(json)
	local world = game.GetWorld()

	for _, decal in ipairs(MapDecals) do
		local mat = Material(decal.texture)
		if mat:IsError() then continue end

		util.DecalEx(mat, world, decal.pos, decal.norm, COLOR_WHITE, 1, 1)
	end
end)
