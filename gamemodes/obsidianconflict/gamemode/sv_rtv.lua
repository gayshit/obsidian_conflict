local oc_rtv_percent = CreateConVar("oc_rtv_percent", "0.6", FCVAR_ARCHIVE, "Percentage of players needed to successfully rock the vote")
local oc_rtv_minplayers = CreateConVar("oc_rtv_minplayers", "0", FCVAR_ARCHIVE, "Minimum players needed to start rocking the vote")
local oc_rtv_mintime = CreateConVar("oc_rtv_mintime", "2", FCVAR_ARCHIVE, "How many minutes before players are allowed to rock the vote")

Obsidian = Obsidian or {}
Obsidian.RTV = Obsidian.RTV or {}

Obsidian.RTV.Votes = setmetatable({}, {__mode = "k"})
Obsidian.RTV.Needed = math.floor(#player.GetHumans() * oc_rtv_percent:GetFloat())

local function BackToLobby()
	Obsidian.RTV.Changing = true
	PrintMessage(HUD_PRINTTALK, "Returning to lobby due to vote threshold being met.")

	local self = GAMEMODE

	self.LevelChangeTimer = self:CountdownDisplay("Returning to lobby", 10)
	self.BackToLobby = true
	self.BackToLobbyTimer = CurTime() + 10
end

hook.Add("PlayerDisconnected", "oc_rtv", function(ply)
	Obsidian.RTV.Votes[ply] = nil
	Obsidian.RTV.Needed = math.floor(#player.GetHumans() * oc_rtv_percent:GetFloat())

	if table.Count(Obsidian.RTV.Votes) >= Obsidian.RTV.Needed and not Obsidian.RTV.Changing then
		BackToLobby()
	end
end)

hook.Add("PlayerAuthed", "oc_rtv", function(ply)
	Obsidian.RTV.Needed = math.floor(#player.GetHumans() * oc_rtv_percent:GetFloat())
end)

hook.Add("PlayerSay", "oc_rtv", function(ply, str, isTeam)
	if not IsValid(ply) then return end
	if ply:IsBot() then return end

	local curtime = CurTime()

	local minplayers = oc_rtv_minplayers:GetInt()
	local mintime = oc_rtv_mintime:GetInt() * 60
	local votes = table.Count(Obsidian.RTV.Votes)
	local needed = Obsidian.RTV.Needed

	local rtv = str == "rtv" or str:match("[!/%.]rtv")
	local unrtv = str == "unrtv" or str:match("[!/%.]unrtv")

	if rtv or unrtv then
		if Obsidian.IsLobbyMap() then
			ply:ChatPrint("You're already at the lobby.")
			return ""
		elseif #player.GetHumans() < minplayers then
			ply:ChatPrint(Format("Not enough players to rock the vote. (%d needed)", minplayers))
			return ""
		elseif curtime < mintime then
			local timeleft = math.ceil(mintime - curtime)
			local minutes = timeleft >= 60

			local time = minutes and math.ceil(timeleft / 60) or timeleft
			local unit = minutes and "minute" or "second"
			local pluralunit = time > 1 and unit .. "s" or unit

			ply:ChatPrint(Format("Cannot rock the vote yet. (%d %s left)", time, pluralunit))
			return ""
		elseif Obsidian.RTV.Changing then
			ply:ChatPrint("The vote has already been rocked.")
			return ""
		end
	end

	if rtv then
		if Obsidian.RTV.Votes[ply] ~= nil then
			ply:ChatPrint(Format("You're already rocking the vote. (%d/%d needed)", votes, needed))
		else
			Obsidian.RTV.Votes[ply] = true
			votes = votes + 1
			if table.Count(Obsidian.RTV.Votes) >= Obsidian.RTV.Needed then
				BackToLobby()
			else
				PrintMessage(HUD_PRINTTALK, Format("%s wants to rock the vote. (%d/%d needed)", ply:Name(), votes, needed))
			end
		end

		return ""
	elseif unrtv then
		if Obsidian.RTV.Votes[ply] == nil then
			ply:ChatPrint("You're not currently rocking the vote.")
		else
			Obsidian.RTV.Votes[ply] = nil
			votes = votes - 1
			PrintMessage(HUD_PRINTTALK, Format("%s unrocks their vote. (%d/%d needed)", ply:Name(), votes, needed))
		end

		return ""
	end
end)
