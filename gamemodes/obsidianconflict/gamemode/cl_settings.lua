local oc_quickinfo = GetConVar("oc_quickinfo")
local oc_chatsound = GetConVar("oc_chatsound")
local oc_flashlight_col = GetConVar("oc_flashlight_col")

local cl_playerskin = GetConVar("cl_playerskin")
local cl_playerbodygroups = GetConVar("cl_playerbodygroups")
local cl_playermodel = GetConVar("cl_playermodel")

local function StrToCol(str)
	local col = Color(255, 255, 255)

	local r, g, b = str:match("(%d+) (%d+) (%d+)")

	col.r = tonumber(r) or 255
	col.g = tonumber(g) or 255
	col.b = tonumber(b) or 255

	return col
end

concommand.Add("oc_settings", function()
	local fr = vgui.Create("DFrame")
	fr:SetSize(ScrW() * (512 / 1600), ScrH() * (406 / 900))
	fr:ShowCloseButton(true)
	fr:SetTitle("Obsidian Conflict settings")
	fr:Center()
	fr:MakePopup()

	local pan_hud = vgui.Create("DPanel")
	local hud_leftside = vgui.Create("DPanel", pan_hud)
	local hud_rightside = vgui.Create("DPanel", pan_hud)

	local oPL = pan_hud.PerformLayout
	function pan_hud:PerformLayout(w, h)
		oPL(self, w, h)
		hud_leftside:SetSize(self:GetWide() / 2, self:GetTall())
		hud_rightside:SetSize(self:GetWide() / 2, self:GetTall())
		hud_rightside:SetPos(self:GetWide() / 2, 0)
	end

	local what = vgui.Create("DLabel", hud_leftside)
	what:Dock(TOP)
	what:SetText("Background Color")
	what:SetDark(true)
	local hud_bg_col = vgui.Create("DColorMixer", hud_leftside)
	hud_bg_col:Dock(TOP)
	hud_bg_col:SetAlphaBar(false)
	hud_bg_col:SetColor(GAMEMODE.HUD_COL_BG)
	function hud_bg_col:ValueChanged(col)
		GAMEMODE.HUD_COL_BG = Color(col.r, col.g, col.b)
		RunConsoleCommand("oc_hud_bg", ("%d %d %d"):format(col.r, col.g, col.b))
	end

	local reset_bg = vgui.Create("DButton", hud_leftside)
	reset_bg:Dock(TOP)
	reset_bg:SetText("Reset Background")
	function reset_bg:DoClick()
		hud_bg_col:SetColor(Color(0, 0, 0))
		GAMEMODE.HUD_COL_BG = Color(0, 0, 0)
		RunConsoleCommand("oc_hud_bg", "0 0 0")
	end

	what = vgui.Create("DLabel", hud_rightside)
	what:Dock(TOP)
	what:SetText("Text Color")
	what:SetDark(true)
	local hud_text_col = vgui.Create("DColorMixer", hud_rightside)
	hud_text_col:Dock(TOP)
	hud_text_col:SetAlphaBar(false)
	hud_text_col:SetColor(GAMEMODE.HUD_COL_TEXT)
	function hud_text_col:ValueChanged(col)
		GAMEMODE.HUD_COL_TEXT = Color(col.r, col.g, col.b)
		RunConsoleCommand("oc_hud_text", ("%d %d %d"):format(col.r, col.g, col.b))
	end

	local reset_text = vgui.Create("DButton", hud_rightside)
	reset_text:Dock(TOP)
	reset_text:SetText("Reset Text")
	function reset_text:DoClick()
		hud_text_col:SetColor(Color(0, 128, 255))
		GAMEMODE.HUD_COL_TEXT = Color(0, 128, 255)
		RunConsoleCommand("oc_hud_text", "0 128 255")
	end

	local pan_player = vgui.Create("DPanel")
	local player_leftside = vgui.Create("EditablePanel", pan_player)
	local player_rightside = vgui.Create("DPropertySheet", pan_player)

	local _oPL = pan_player.PerformLayout
	function pan_player:PerformLayout(w, h)
		_oPL(self, w, h)
		player_leftside:SetSize(self:GetWide() / 2, self:GetTall())
		player_rightside:SetSize(self:GetWide() / 2, self:GetTall())
		player_rightside:SetPos(self:GetWide() / 2, 0)
	end

	local pnl_plymdl = vgui.Create("DPropertySheet", player_leftside)
	pnl_plymdl:Dock(FILL)
	local pnl_model = vgui.Create("EditablePanel", pnl_plymdl)
	local pnl_body = vgui.Create("DScrollPanel", pnl_plymdl)
	pnl_plymdl:AddSheet("Model", pnl_model, "icon16/brick.png", false, false, "Model")
	pnl_plymdl:AddSheet("Bodygroups/Skins", pnl_body, "icon16/cog.png", false, false, "Bodygroups/Skins")

	local model_list = vgui.Create("DPanelSelect", pnl_model)
	model_list:Dock(FILL)

	local function UpdateBodyGroups(pnl, val)
		if pnl.type == "bgroup" then
			local str = string.Explode(" ", cl_playerbodygroups:GetString())
			if #str < pnl.typenum + 1 then
				for i = 1, pnl.typenum + 1 do
					str[i] = str[i] or 0
				end
			end
			str[pnl.typenum + 1] = math.Round(val)

			RunConsoleCommand("cl_playerbodygroups", table.concat(str, " "))
		elseif pnl.type == "skin" then
			RunConsoleCommand("cl_playerskin", math.Round(val))
		end
	end

	local function MakeNiceName(str)
		local newname = {}

		for _, s in pairs(string.Explode("_", str)) do
			if (string.len(s) == 1) then
				table.insert(newname, string.upper(s))
				continue
			end
			table.insert(newname, string.upper(string.Left(s, 1)) .. string.Right(s, string.len(s) - 1))           -- Ugly way to capitalize first letters.
		end

		return string.Implode(" ", newname)
	end

	local function RebuildBodygroupTab()
		pnl_body:Clear()

		local mdl = player_manager.TranslatePlayerModel(cl_playermodel:GetString())

		local info1 = util.GetModelInfo(mdl)
		local nskins = info1.SkinCount - 1
		if (nskins > 0) then
			local skins = vgui.Create("DNumSlider")
			skins:Dock(TOP)
			skins:SetText("Skin")
			skins:SetDark(true)
			skins:SetTall(32)
			skins:SetDecimals(0)
			skins:SetMax(nskins)
			skins:SetValue(cl_playerskin:GetInt())
			skins.type = "skin"
			skins.OnValueChanged = UpdateBodyGroups

			pnl_body:AddItem(skins)
		end

		-- taken from outfitter
		if mdlinspect then
			local info2 = mdlinspect.Open(mdl)
			info2:ParseHeader()
			local parts = info2:BodyPartsEx()

			local skip = 0
			local groups = string.Explode(" ", cl_playerbodygroups:GetString())
			for k, part in next, parts do
				if #part.models < 2 then
					print("skip", part.name)
					skip = skip + 1
					continue
				end

				local name = part.name:gsub("%.smd$", "")
					:gsub("([a-z0-9])([A-Z])([a-z])",
						function(q, a, b)
							return q .. " " .. a:lower() .. b
						end)
					:gsub("[_%.%-]", " ")
					:gsub("(%s)%s*", "%1")

				local bgroup = vgui.Create("DNumSlider")
				bgroup:Dock(TOP)
				bgroup:SetText(MakeNiceName(name))
				bgroup:SetDark(true)
				bgroup:SetTall(32)
				bgroup:SetDecimals(0)
				bgroup.type = "bgroup"
				bgroup.typenum = k - skip + 1
				bgroup:SetMax(#part.models - 1)
				bgroup:SetValue(groups[k - skip] or 0)
				bgroup.OnValueChanged = UpdateBodyGroups

				pnl_body:AddItem(bgroup)
			end
		end
	end

	for name, model in SortedPairs(player_manager.AllValidModels()) do
		local icon = vgui.Create("SpawnIcon")
		icon:SetModel(model)
		icon:SetSize(64, 64)
		icon:SetTooltip(name)
		icon.playermodel = name

		model_list:AddPanel(icon, {cl_playermodel = name})
	end

	function model_list:OnActivePanelChanged(old, new)
		if old ~= new then
			RunConsoleCommand("cl_playerbodygroups", "0")
			RunConsoleCommand("cl_playerskin", "0")
			timer.Simple(0, function()
				RebuildBodygroupTab()
			end)
		end
	end

	timer.Simple(0, function()
		RebuildBodygroupTab()
	end)

	if outfitter then
		local open_outfitter = vgui.Create("DButton", pnl_model)
		open_outfitter:Dock(BOTTOM)
		open_outfitter:SetText("Open Outfitter")
		function open_outfitter:DoClick()
			RunConsoleCommand("outfitter")
			fr:Close()
		end
	end

	local update_model = vgui.Create("DButton", player_leftside)
	update_model:Dock(BOTTOM)
	update_model:DockPadding(0, 16, 0, 0)
	update_model:SetText("Update Player Model")
	update_model:SetTooltip("Only use this if you don't want to wait for a respawn.")
	function update_model:DoClick()
		net.Start("oc_cmd")
		net.WriteUInt(OC_CMD_UPDATE_PLMODEL, 4)

		net.WriteString(GetConVar("cl_playermodel"):GetString())
		net.SendToServer()
	end

	local pnl_pcol = vgui.Create("EditablePanel", player_rightside)
	local pnl_wcol = vgui.Create("EditablePanel", player_rightside)
	local pnl_flash = vgui.Create("EditablePanel", player_rightside)
	player_rightside:AddSheet("Player Color", pnl_pcol, "icon16/user.png", false, false, "Player Color")
	player_rightside:AddSheet("Weapon Color", pnl_wcol, "icon16/gun.png", false, false, "Weapon Color")
	player_rightside:AddSheet("Effects Color", pnl_flash, "icon16/lightbulb.png", false, false, "Flashlight Color")

	local pl_col = vgui.Create("DColorMixer", pnl_pcol)
	pl_col:Dock(TOP)
	pl_col:SetAlphaBar(false)
	pl_col:SetVector(Vector(GetConVar("cl_playercolor"):GetString()))
	function pl_col:ValueChanged(col)
		RunConsoleCommand("cl_playercolor", tostring(pl_col:GetVector()))
	end

	local update_color = vgui.Create("DButton", pnl_pcol)
	update_color:Dock(TOP)
	update_color:SetText("Update Player Color")
	update_color:SetTooltip("Only use this if you don't want to wait for a respawn.")
	function update_color:DoClick()
		net.Start("oc_cmd")
		net.WriteUInt(OC_CMD_UPDATE_PLCOL, 4)

		net.WriteVector(pl_col:GetVector())
		net.SendToServer()
	end

	local wep_col = vgui.Create("DColorMixer", pnl_wcol)
	wep_col:Dock(TOP)
	wep_col:SetAlphaBar(false)
	wep_col:SetVector(Vector(GetConVar("cl_weaponcolor"):GetString()))
	function wep_col:ValueChanged(col)
		RunConsoleCommand("cl_weaponcolor", tostring(wep_col:GetVector()))
	end

	update_color = vgui.Create("DButton", pnl_wcol)
	update_color:Dock(TOP)
	update_color:SetText("Update Weapon Color")
	update_color:SetTooltip("Only use this if you don't want to wait for a respawn.")
	function update_color:DoClick()
		net.Start("oc_cmd")
		net.WriteUInt(OC_CMD_UPDATE_WEPCOL, 4)

		net.WriteVector(wep_col:GetVector())
		net.SendToServer()
	end

	local flash_col = vgui.Create("DColorMixer", pnl_flash)
	flash_col:Dock(TOP)
	flash_col:SetAlphaBar(false)
	flash_col:SetColor(StrToCol(oc_flashlight_col:GetString()))
	function flash_col:ValueChanged(col)
		RunConsoleCommand("oc_flashlight_col", ("%d %d %d"):format(col.r, col.g, col.b))
	end

	local pan_misc = vgui.Create("DPanel")

	local function add_button(label, cvar)
		local check = vgui.Create("DCheckBoxLabel", pan_misc)
		check:SetText(label)
		check:SetConVar(cvar)
		check:Dock(TOP)
		check:SetDark(true)
		return check
	end
	add_button("Play Chat Sound", "oc_chatsound")
	add_button("Show Quick Info", "oc_quickinfo")
	add_button("TargetID: Show Percentage", "oc_targetid_usepercent")

	local prop = vgui.Create("DPropertySheet", fr)
	prop:Dock(FILL)
	prop:AddSheet("HUD", pan_hud, "icon16/vcard.png", false, false, "HUD Settings")
	prop:AddSheet("Player", pan_player, "icon16/user.png", false, false, "Player Settings")
	prop:AddSheet("Misc.", pan_misc, nil, false, false, "Miscellaneous Settings")
end)
