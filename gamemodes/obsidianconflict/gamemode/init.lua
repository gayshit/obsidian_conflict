include"sv_item_respawns.lua"
include"sv_transition.lua"
include"sv_hudmenu.lua"
include"sv_rtv.lua"

include"shared.lua"
AddCSLuaFile"cl_init.lua"
AddCSLuaFile"cl_hud.lua"
AddCSLuaFile"cl_quickinfo.lua"
AddCSLuaFile"cl_scoreboard.lua"
AddCSLuaFile"cl_settings.lua"
AddCSLuaFile"cl_weaponswitcher.lua"
AddCSLuaFile"cl_hudmenu.lua"
AddCSLuaFile"thirdparty/mdlinspect.lua"
AddCSLuaFile"thirdparty/cami.lua"
DEFINE_BASECLASS"gamemode_base"


SF_NORESPAWN = bit.lshift(1, 30)

SF_TRIGGER_ALLOW_CLIENTS = 0x01                 -- Players can fire this trigger
SF_TRIGGER_ALLOW_NPCS = 0x02                    -- NPCS can fire this trigger
SF_TRIGGER_ALLOW_PUSHABLES = 0x04               -- Pushables can fire this trigger
SF_TRIGGER_ALLOW_PHYSICS = 0x08                 -- Physics objects can fire this trigger
SF_TRIGGER_ONLY_PLAYER_ALLY_NPCS = 0x10         -- *if* NPCs can fire this trigger, this flag means only player allies do so
SF_TRIGGER_ONLY_CLIENTS_IN_VEHICLES = 0x20      -- *if* Players can fire this trigger, this flag means only players inside vehicles can
SF_TRIGGER_ALLOW_ALL = 0x40                     -- Everything can fire this trigger EXCEPT DEBRIS!
SF_TRIG_PUSH_ONCE = 0x80                        -- trigger_push removes itself after firing once
SF_TRIG_PUSH_AFFECT_PLAYER_ON_LADDER = 0x100    -- if pushed object is player on a ladder, then this disengages them from the ladder (HL2only)
SF_TRIGGER_ONLY_CLIENTS_OUT_OF_VEHICLES = 0x200 -- *if* Players can fire this trigger, this flag means only players outside vehicles can
SF_TRIG_TOUCH_DEBRIS = 0x400                    -- Will touch physics debris objects
SF_TRIGGER_ONLY_NPCS_IN_VEHICLES = 0x800        -- *if* NPCs can fire this trigger, only NPCs in vehicles do so (respects player ally flag too)
SF_TRIGGER_DISALLOW_BOTS = 0x1000               -- Bots are not allowed to fire this trigger

SF_ZOMBIE_NO_HEADCRAB = bit.lshift(1, 16)

SF_BREAK_TRIGGER_ONLY = 0x001              -- may only be broken by trigger
SF_BREAK_TOUCH = 0x002                     -- can be 'crashed through' by running player (plate glass)
SF_BREAK_PRESSURE = 0x004                  -- can be broken by a player standing on it
SF_BREAK_PHYSICS_BREAK_IMMEDIATELY = 0x200 -- the first physics collision this breakable has will immediately break it
SF_BREAK_DONT_TAKE_PHYSICS_DAMAGE = 0x400  -- this breakable doesn't take damage from physics collisions
SF_BREAK_NO_BULLET_PENETRATION = 0x800     -- don't allow bullets to penetrate

local mp_teamplay = GetConVar"oc_teamplay"

--[[
mp_barnacle_swallow    =    1
sk_defender_health    =    0
sk_dmg_energy_grenade    =    0
sk_energy_grenade_radius    =    0
sk_fastzombie_dmg_claw    =    3
sk_fastzombie_dmg_leap    =    5
sk_fastzombie_health    =    50
sk_hassassin_dmg    =    5
sk_helicopter_blast_push    =    1
sk_mortarsynth_beamdmg    =    0
sk_stalker_aggressive    =    0
sv_allowgaussjump    =    1
]]

local sv_allownegativescore = CreateConVar("sv_allownegativescore", "1", bit.bor(FCVAR_NOTIFY, FCVAR_REPLICATED), "Allow a player's score to drop below zero?")
local sv_deathpenalty = CreateConVar("sv_deathpenalty", "1", bit.bor(FCVAR_NOTIFY, FCVAR_REPLICATED), "How much to subtract from players score on death")
local oc_lobby_map = CreateConVar("oc_lobby_map", "oc_lobby", FCVAR_ARCHIVE, "Map to use for return to lobby")

function GM:Initialize()
	local map = game.GetMap()

	-- send map over FastDL? :D
	local strMapPath = Format("maps/%s.bsp", map)
	resource.AddFile(strMapPath)

	local f = file.Read(string.format("maps/cfg/%s_modify.txt", map), "GAME")
	if f and f ~= "" then
		local data = util.KeyValuesToTablePreserveOrder(f, false, true)

		if data then
			resource.AddSingleFile(string.format("maps/cfg/%s_modify.txt", map))
			self.MapAddScript = data
		end
	end

	f = file.Read(string.format("maps/cfg/%s_briefing.txt", map), "GAME")
	if f and f ~= "" then
		resource.AddSingleFile(string.format("maps/cfg/%s_briefing.txt", map))
	end

	f = file.Read(string.format("maps/cfg/%s_cfg.txt", map), "GAME")
	if f and f ~= "" then
		local data = util.KeyValuesToTablePreserveOrder(f, false, true)

		if data then
			self.MapCfgScript = data
		end
	end

	self.MapAddScript = self.MapAddScript or {}
	self.MapCfgScript = self.MapCfgScript or {}

	if self.MapAddScript[1] then
		for _, v in ipairs(self.MapAddScript) do
			if v.Key:lower() == "spawnitems" then
				self.MapAddSpawnItems = v.Value
			end
		end
	end

	f = file.Read(string.format("maps/cfg/%s_soundscripts.txt", map), "GAME")
	if f and f ~= "" then
		local data = util.KeyValuesToTable("\"Sounds\" {" .. f .. "}", false, true)

		if data then
			resource.AddSingleFile(string.format("maps/cfg/%s_soundscripts.txt", map))
			self.MapSoundScripts = data
		end
	end

	self.MapAddSpawnItems = self.MapAddSpawnItems or {}

	self:RegisterOCWeapons()
	self:RegisterOCSoundScripts()

	if map == "oc_neolobby" then
		concommand.Add("oc_neolobby_open", function(ply)
			if #player.GetHumans() > 1 then
				ply:PrintMessage(HUD_PRINTTALK, "Just use the buttons...")
				return
			end

			local _mainc = ents.FindByName("mainc")
			if #_mainc == 0 or not IsValid(_mainc[1]) then
				ply:PrintMessage(HUD_PRINTTALK, "Map's broken, can't find main math entity")
				return
			end
			local mainc = _mainc[1]

			local _safety_button_math = ents.FindByName("safety_button_math")
			if #_safety_button_math == 0 or not IsValid(_safety_button_math[1]) then
				ply:PrintMessage(HUD_PRINTTALK, "Map's broken, can't find second math entity")
				return
			end
			local safety_button_math = _safety_button_math[1]

			mainc:Fire("SetValue", 5)
			timer.Simple(1, function()
				safety_button_math:Fire("Add", 2)
				timer.Simple(1, function()
					safety_button_math:Fire("Add", 2)
				end)
			end)
		end, nil, "Force opens the map teleporter in neolobby when only one person")
	end
end

function GM:GiveMapSpawnItems(ply)
	if not (ply:IsValid() and ply:IsPlayer()) then return false end

	if self.MapAddSpawnItems[1] then
		local flAmmoQuantityScale = self:GetAmmoQuanityScale()
		local pos = ply:GetPos()

		for _, v in ipairs(self.MapAddSpawnItems) do
			local key = self.LoadoutReplacementWeapons[v.Key] or v.Key
			local val = math.max(tonumber(v.Value) or 1, 1)

			-- check if spawned entity is an ammo item, if so, give the player ammo
			local itemDef = self.ITEM_DEF[key]
			if itemDef and itemDef.Type and not self.AMMO_LIKE_WEAPONS[key] then
				ply:GiveAmmo(itemDef.Amount * val * flAmmoQuantityScale, itemDef.Type, true)

				continue
			end

			for i = 1, val do
				local e = Obsidian.CreateEntNoRespawn(key)
				if not IsValid(e) then
					print("[OC] failed to create spawnitem", key)
					continue
				end

				e.CreatedForPlayer = ply
				e:SetPos(pos)
				e:Spawn()
			end
		end

		return true
	end

	return false
end

GM.LoadoutReplacementWeapons = {}
function GM:PlayerLoadout(ply)
	if ply.TransitionData then
		local Weapons = ply.TransitionData.Weapons

		local prevWep, activeWep

		for i, data in ipairs(Weapons) do
			local ent = ply:Give(data.Class)
			if not ent:IsValid() then continue end

			if data.Active then
				activeWep = ent
			end
			if data.Previous then
				prevWep = ent
			end

			ent:SetClip1(data.Clip1)
			ent:SetClip2(data.Clip2)

			-- set player ammo from data.Ammo, data.Ammo.Count is reserve ammo, data.Ammo.Id is the ammo ID
			if data.Ammo then
				ply:SetAmmo(data.Ammo.Count, data.Ammo.Id)
			end
		end

		if activeWep then
			ply:SelectWeapon(activeWep:GetClass())
		end
		if prevWep then
			ply:SetSaveValue("m_hLastWeapon", prevWep)
		end

		return
	end

	--Obsidian.GivePlayerWeapon(ply, "weapon_physcannon")
	--Obsidian.GivePlayerWeapon(ply, "weapon_crowbar")

	if not ply._just_spawned then
		self:GiveMapSpawnItems(ply)
	end

	-- backwards compat
	--for _,ent in pairs(ents.FindByClass("game_player_equip")) do
	--    ent:Touch(ply)
	--end
end

-- adapted from gmod-coop
local npc_replace = {
	["npc_hgrunt"] = "npc_combine_s",
	["npc_houndeye"] = "monster_houndeye",
	["npc_bullsquid"] = "monster_bullchicken",
	["npc_antlionworker"] = "npc_antlion_worker",
}

for name in pairs(npc_replace) do
	scripted_ents.Register({Type = "point"}, name)
end

local convar_overrides = {
	["sv_infinite_aux_power"] = function(val)
		RunConsoleCommand("oc_infinite_aux_power", tostring(val))
	end,
	["mp_teamplay"] = function(val)
		RunConsoleCommand("oc_teamplay", tostring(val))
	end,
	["mp_flashlight"] = function(val)
		RunConsoleCommand("oc_flashlight", tostring(val))
	end
}
GM.MapDecals = GM.MapDecals or {}
GM.MapDecalEnts = GM.MapDecalEnts or setmetatable({}, {__mode = "k"})
function GM:InitPostEntity()
	self:InitializeCurrentLevel()
	self:TransferPlayers()

	timer.Simple(0, function()
		self:PostLoadTransitionData()
		--self:InitializeResources()

		RunConsoleCommand("sk_max_gauss_round", "150")
		RunConsoleCommand("gmod_maxammo", "0")
		RunConsoleCommand("sv_defaultdeployspeed", "1")
		game.SetGlobalState("gordon_precriminal", GLOBAL_OFF)

		if self.MapCfgScript[1] then
			for _, v in ipairs(self.MapCfgScript) do
				print("[OC] map cfg:", v.Key, v.Value)

				if isfunction(convar_overrides[v.Key]) then
					convar_overrides[v.Key](v.Value)
					continue
				end

				RunConsoleCommand(v.Key, tostring(v.Value))
			end
		end

		if not self.MapAddScript then return end
		for _, v2 in ipairs(self.MapAddScript) do
			if v2.Key:lower() == "remove" then
				for _, v3 in ipairs(v2.Value) do
					if v3.Key:lower() == "origin" then
						for _, v4 in ipairs(v3.Value) do
							local es = ents.FindInSphere(Vector(v4.Key), 50)
							print("origin remove", v4.Key)
							PrintTable(es)
						end
					else
						for _, v4 in ipairs(v3.Value) do
							for _, v in ipairs(ents.FindByClass(v4.Key)) do
								SafeRemoveEntity(v)
							end
						end
					end
				end
			elseif v2.Key:lower() == "add" then
				for _, v3 in ipairs(v2.Value) do
					local class = npc_replace[v3.Key] or v3.Key

					local e = ents.Create(class)
					if not IsValid(e) then
						print(string.format("[OC] failed to create mapadd ent %s", class))
						continue
					end

					if v3.Key == "npc_hgrunt" then
						e:SetModel("models/hgrunt/hgrunt" .. math.random(1, 5) .. ".mdl")
					end

					for _, v4 in ipairs(v3.Value) do
						if v4.Key == "npchealth" then
							e.oc_npchealth = tonumber(v4.Value)
						elseif v4.Key == "model" and v4.Value == "models/combine_elite_soldier.mdl" then
							v4.Value = "models/combine_super_soldier.mdl"
						end
						e:SetKeyValue(v4.Key, v4.Value)
					end

					if e:GetClass() ~= "info_node" then -- "// DO NOT CALL SPAWN ON info_node ENTITIES!"
						e:Spawn()
					end

					e.IsDesignerPlaced = true
					print("[OC] add ent", e, e:GetClass(), e:GetPos())
				end
			elseif v2.Key:lower() == "modify" then
				for _, v3 in ipairs(v2.Value) do
					if v3.Key:lower() == "classname" then
						for _, v4 in ipairs(v3.Value) do
							for _, ent in ipairs(ents.FindByClass(v4.Key)) do
								for _, v5 in ipairs(v4.Value) do
									if v5.Key:lower() == "classname" then
										local newEnt = ents.Create(v5.Value)
										newEnt:SetPos(ent:GetPos())
										newEnt:SetAngles(ent:GetAngles())
										newEnt:SetName(ent:GetName())

										for k, v in pairs(ent:GetKeyValues()) do
											if k == "classname" or k == "origin" or k:lower():sub(1, 2) == "on" or not isstring(v) then continue end
											newEnt:SetKeyValue(k, v)
										end

										newEnt:SetOwner(ent:GetOwner())

										newEnt.__outputs = ent.__outputs
										newEnt.__kv = ent.__kv

										if ent.__outputs and istable(ent.__outputs) then
											for k, tbl in pairs(ent.__outputs) do
												for _, v in pairs(tbl) do
													newEnt:Fire("AddOutput", k .. " " .. v, 0)
												end
											end
										end

										if ent.__kv and istable(ent.__kv) then
											for k, v in pairs(ent.__kv) do
												if k == "classname" or k == "origin" or k:lower():sub(1, 2) == "on" or not isstring(v) then continue end
												newEnt:SetKeyValue(k, v)
											end
										end

										newEnt:Spawn()

										SafeRemoveEntity(ent)
									else
										ent:SetKeyValue(v5.Key, v5.Value)
									end
								end
							end
						end
					elseif v3.Key:lower() == "origin" then
						for _, v4 in ipairs(v3.Value) do
							for _, e in ipairs(ents.FindInSphere(Vector(v4.Key), 2)) do
								for _, v5 in ipairs(v4.Value) do
									e:SetKeyValue(v5.Key, v5.Value)
								end
							end
						end
					elseif v3.Key:lower() == "targetname" then
						for _, v4 in ipairs(v3.Value) do
							for _, e in ipairs(ents.FindByName(v4.Key)) do
								for _, v5 in ipairs(v4.Value) do
									if v5.Key == "texture" then
										e.__kv.texture = v5.Value
									end

									e:SetKeyValue(v5.Key, v5.Value)
								end
							end
						end
					end
				end
			end
		end

		for ent in next, self.MapDecalEnts do
			if not IsValid(ent) then continue end

			local name = ent:GetName()
			local ang = ent:GetAngles()

			-- we love hardcoding fixes
			if name == "Map_Posterboard_5" or name == "Map_Posterboard_6" then
				ang.y = 180
			end

			local norm = ang:Forward() * -1

			self.MapDecals[#self.MapDecals + 1] = {
				texture = ent.__kv.texture,
				pos = ent:GetPos(),
				norm = norm,
			}

			SafeRemoveEntity(ent)
		end

		self:CreateTransitionObjects()
	end)
end

GM.SkeetShotNPCs = {
	npc_pigeon = true,
	npc_crow = true,
	npc_seagull = true,
	npc_manhack = true
}
function GM:OnNPCKilled(npc, atk, inf)
	if atk.AddFrags then
		if npc:Disposition(atk) == D_LI then
			atk:AddFrags(-3)
		else
			local amt = 3

			if self.SkeetShotNPCs[npc:GetClass()] and
					not npc:IsOnGround() and
					inf:GetClass() == "crossbow_bolt" and
					npc:GetPos():Distance(atk:GetPos()) > 450
			then
				amt = 10
			end

			atk:AddFrags(amt)
		end
	end

	if npc.DummyEnt and IsValid(npc.DummyEnt) then
		npc.DummyEnt:TriggerOutput("OnDeath", atk)
		npc.DummyEnt:Remove()
	end

	-- send killfeed for npcs with display names
	if npc:GetNW2String("displayname", "") ~= "" then
		BaseClass.OnNPCKilled(self, npc, atk, inf)
	end
end

function GM:GetDeathNoticeEntityName(ent)
	local displayname = ent:GetNW2String("displayname", "")

	if displayname ~= "" then
		return displayname
	end

	-- doesn't call hook.Call/hook.Run, cringe
	return BaseClass.GetDeathNoticeEntityName(self, ent)
end

GM.RandomNPCWeps = {
	"weapon_ar2",
	"weapon_smg1",
	"weapon_shotgun"
}
GM.RandomCopWeps = {
	"weapon_smg1",
	"weapon_pistol"
}
function GM:EntityKeyValue(ent, key, val)
	local k = key:lower()
	local classname = ent:GetClass()

	ent.__kv = ent.__kv or {}
	ent.__kv[k] = val

	if key:sub(1, 2) == "On" then
		ent.__outputs = ent.__outputs or {}
		ent.__outputs[key] = ent.__outputs[key] or {}
		table.insert(ent.__outputs[key], val)
	end

	if ent:IsNPC() then
		if k == "teamnumber" then
			local old = ent.oc_teamnum
			ent.oc_teamnum = tonumber(val)
			hook.Call("EntityTeamChanged", self, ent, old, ent.oc_teamnum)
		elseif k == "additionalequipment" then
			if val == "random" then
				ent:Give(table.Random(classname ~= "npc_metropolice" and self.RandomNPCWeps or self.RandomCopWeps))
			else
				ent:Give(val)
			end
			return ""
		elseif k == "npcname" then
			ent:SetNW2String("displayname", val and val ~= "" and val or ent:GetName() == "" and classname or ent:GetName())
		elseif k == "npchealth" then
			ent.oc_npchealth = tonumber(val)
		else
			--print(ent, k, val)
		end
	elseif not ent:IsPlayer() then
		if classname:find("info_player_") then
			if k == "startdisabled" then
				ent.oc_disabled = tobool(val)
			end
		elseif ent:IsWorld() then
			if k == "numlives" and (tonumber(val) and tonumber(val) > 0) then
				RunConsoleCommand("mp_livesmode", "1")
				RunConsoleCommand("mp_numlives", tonumber(val))
			end
		elseif classname == "ambient_generic" and k == "message" then
			ent.oc_ag_message = val
		elseif classname == "infodecal" and k == "texture" then
			self.MapDecalEnts[ent] = true
		elseif k == "spawnflags" then
			if classname == "func_breakable" then
				if ent:HasSpawnFlags(SF_BREAK_TRIGGER_ONLY) then
					ent:SetNW2Bool("oc_breakable_trigger", true)
				end
			end
		end
	end
end

function GM:EntityTeamChanged(ent, old, new)
	if not mp_teamplay:GetBool() then return end

	timer.Simple(0, function()
		if not ent:IsValid() then return end

		for _, e in ents.Iterator() do
			if e == ent then continue end

			if e:IsPlayer() then
				if e:Team() == new then
					ent:AddEntityRelationship(e, D_LI, 999)
				else
					ent:AddEntityRelationship(e, D_HT, 999)
				end
			elseif e:IsNPC() then
				if e.oc_teamnum == new or ent:GetClass() == "npc_merchant" then
					ent:AddEntityRelationship(e, D_LI, 999)
					e:AddEntityRelationship(ent, D_LI, 999)
				else
					ent:AddEntityRelationship(e, D_HT, 999)
					e:AddEntityRelationship(ent, D_HT, 999)
				end
			end
		end
	end)
end

local function GetLogging(title)
	return function(...)
		MsgC(Color(0, 255, 255), Format("[OC-%s] ", title))
		print(...)
	end
end

local DbgPrint = GetLogging("Transition")
local DbgError = function(...)
	MsgC(Color(255, 0, 0), Format("[OC-%s] ", "Transition"))
	print(...)
end

function GM:PlayerSpawn(ply, transition)
	player_manager.SetPlayerClass(ply, "player_oc")

	if mp_teamplay:GetBool() then
		for _, ent in next, ents.FindByClass("npc_*") do
			if ent.oc_teamnum == ply:Team() then
				--print(ply, v, ply:Team(), v.oc_teamnum)
				ent:AddEntityRelationship(ply, D_LI, 999)
			end
		end
	end

	ply:UnSpectate()
	ply:SetNW2Float("SuitPower", 100)

	if self.IsChangeLevel then
		ply.TransitionData = self:GetPlayerTransitionData(ply)
	end

	player_manager.OnPlayerSpawn(ply, transition or self.IsChangeLevel)
	player_manager.RunClass(ply, "Spawn")
	net.Start("oc_playerspawn")
	net.WriteEntity(ply)
	net.Broadcast()

	ply:SetCanZoom(true)

	hook.Call("PlayerLoadout", self, ply)
	hook.Call("PlayerSetModel", self, ply)

	ply:SetupHands()

	ply:SetNoCollideWithTeammates(false)

	local useSpawnpoint = true

	local transitionData = ply.TransitionData
	if transitionData ~= nil and transitionData.Include == true then
		DbgPrint("Player " .. tostring(ply) .. " has transition data.")

		-- We keep those.
		ply:SetFrags(transitionData.Frags)
		ply:SetDeaths(transitionData.Deaths)
		ply:SetLives(transitionData.Lives)

		if transitionData.Vehicle ~= nil then
			local vehicle = self:FindEntityByTransitionReference(transitionData.Vehicle)
			if IsValid(vehicle) then
				--DbgPrint("Putting player " .. tostring(ply) .. " back in vehicle: " .. tostring(vehicle))
				-- Sometimes does crazy things to the view angles, this only helps to a certain amount.
				local eyeAng = vehicle:WorldToLocalAngles(transitionData.EyeAng)

				vehicle:SetVehicleEntryAnim(false)
				vehicle.ResetVehicleEntryAnim = true
				ply:EnterVehicle(vehicle)
				ply:SetEyeAngles(eyeAng) -- We call it again because the vehicle sets it to how you entered.

				useSpawnpoint = false
			else
				--DbgPrint("Unable to find player " .. tostring(ply) .. " vehicle: " .. tostring(transitionData.Vehicle))
			end
		elseif transitionData.Ground ~= nil then
			local groundEnt = self:FindEntityByTransitionReference(transitionData.Ground)
			if IsValid(groundEnt) then
				local newPos = groundEnt:LocalToWorld(transitionData.GroundPos)
				--DbgPrint("Using func_tracktrain as spawn position reference.", newPos, groundEnt)
				ply:TeleportPlayer(newPos, transitionData.Ang)
				useSpawnpoint = false
			else
				--DbgPrint("Ground set but not found")
			end
		else
			--DbgPrint("Player " .. tostring(ply) .. " uses normal position")
			ply:TeleportPlayer(transitionData.Pos, transitionData.Ang)
			ply:SetAngles(transitionData.Ang)
			ply:SetEyeAngles(transitionData.EyeAng)
			useSpawnpoint = false
		end
	end

	ply.UseSpawnPoint = useSpawnpoint
end

function GM:PlayerInitialSpawn(ply, map_trans)
	ply._just_spawned = true

	if mp_teamplay:GetBool() then
		if #team.GetPlayers(TEAM_BLUE) > #team.GetPlayers(TEAM_RED) then
			ply:SetTeam(TEAM_RED)
		else
			ply:SetTeam(TEAM_BLUE)
		end
	else
		ply:SetTeam(TEAM_COOP)
	end
end

function GM:SetupMove(ply, mv, ucmd)
	if ply._just_spawned and not ucmd:IsForced() then
		ply._just_spawned = nil

		timer.Simple(0, function()
			self:GiveMapSpawnItems(ply)
		end)
		hook.Run("PlayerFullyConnected", ply)
	end

	if player_manager.RunClass(ply, "StartMove", mv, ucmd) then return true end
end

function GM:FinishMove(ply, mv)
	if player_manager.RunClass(ply, "FinishMove", mv) then return true end

	if SERVER then
		-- Network velocity to every client to properly sync animations.
		--ply:SetNetworkAbsVelocity(mv:GetVelocity())

		-- Teleport queue.
		local modifiedPlayer = false

		if ply.TeleportQueue ~= nil and #ply.TeleportQueue > 0 then
			local data = ply.TeleportQueue[1]
			ply:SetPos(data.pos)
			ply:SetAngles(data.ang)
			ply:SetVelocity(data.vel)
			ply:SetEyeAngles(data.ang)
			table.remove(ply.TeleportQueue, 1)
			modifiedPlayer = true
			--DbgPrint("Teleported player: " .. tostring(ply) .. " to " .. tostring(data.pos))
		end

		local curPos = mv:GetOrigin()
		if ply.LastPlayerPos ~= nil then
			local distance = ply.LastPlayerPos:Distance(curPos)
			if distance >= 100 then
				-- Player probably teleported
				--DbgPrint("Teleport detected, disabling player collisions temporarily.")
				--ply:DisablePlayerCollide(true)
			end
		end
		ply.LastPlayerPos = curPos

		if modifiedPlayer == true then
			return modifiedPlayer
		end
	end
end

GM.FirstPlayerHasConnected = false
function GM:PlayerFullyConnected(ply)
	if not self.FirstPlayerHasConnected then
		self.FirstPlayerHasConnected = true
		--[[timer.Simple(1, function()
			for _, ent in pairs(ents.FindByClass("infodecal")) do
				ent:Fire("Activate")
			end
		end)--]]
	end

	if #self.MapDecals > 0 then
		local json = util.TableToJSON(self.MapDecals, false)
		-- fails to decompress on cl idk why
		--[[if not self.MapDecalsCompressed then
			self.MapDecalsCompressed = util.Compress(json)
		end

		local data = self.MapDecalsCompressed
		local len = #data--]]
		net.Start("oc_mapdecals")
		--net.WriteUInt(len, 32)
		--net.WriteData(data, len)
		net.WriteString(json)
		net.Send(ply)
	end

	-- idk if this one is needed but sure
	net.Start("oc_playerspawn")
	net.WriteEntity(ply)
	net.Send(ply)
end

local mp_livesmode = GetConVar("mp_livesmode")
function GM:DoPlayerDeath(ply, atk, info)
	ply:CreateRagdoll()
	ply:AddDeaths(1)

	if mp_livesmode:GetInt() > 0 then
		ply:AddLives(-1)
	end

	local frags = ply:Frags()
	ply:SetFrags(math.max(
		sv_allownegativescore:GetBool() and frags or 0,
		frags - sv_deathpenalty:GetInt()
	))

	Obsidian.PlayerDropEntireInventory(ply)
end

function GM:PlayerSilentDeath(ply)
	Obsidian.PlayerDropEntireInventory(ply)
end

function GM:AUXDeviceAdded(ply, device)
	if device.id == AUX_SUIT_DEVICE_CLOAK then
		ply:SetMaterial("models/shadertest/shader3", true)
		ply:GetHands():SetMaterial("models/shadertest/shader3", true)
		ply:GetViewModel():SetMaterial("models/shadertest/shader3", true)

		local wep = ply:GetActiveWeapon()
		if wep:IsValid() then
			wep:SetMaterial("models/shadertest/shader3", true)
		end

		ply:EmitSound("Player.Cloak")
		ply:AddFlags(FL_NOTARGET)
	elseif device.id == AUX_SUIT_DEVICE_SHIELD then
		ply:EmitSound("Player.ShieldOn")
	end
end

function GM:AUXDeviceRemoved(ply, device)
	if device.id == AUX_SUIT_DEVICE_CLOAK then
		ply:SetMaterial("")
		ply:GetHands():SetMaterial("")
		ply:GetViewModel():SetMaterial("")

		local wep = ply:GetActiveWeapon()
		if wep:IsValid() then
			wep:SetMaterial("")
		end

		ply:EmitSound("Player.Cloak")
		ply:RemoveFlags(FL_NOTARGET)
	elseif device.id == AUX_SUIT_DEVICE_SHIELD then
		ply:EmitSound("Player.ShieldOff")
	end
end

function GM:PlayerTick(ply, mv)
	if ply:GetObserverMode() ~= OBS_MODE_NONE and ply:GetObserverMode() ~= OBS_MODE_ROAMING and not ply:GetObserverTarget():IsValid() then
		ply:SetObserverMode(OBS_MODE_ROAMING)
	end

	self:PlayerCheckDrowning(ply)

	for name, convar in next, self.MAX_AMMO_DEF do
		if ply:GetAmmoCount(game.GetAmmoID(name)) > convar:GetInt() then
			ply:SetAmmo(convar:GetInt(), game.GetAmmoID(name))
		end
	end

	if ply:_Frags() ~= 0 then
		ply:AddFrags(ply:_Frags())
		ply:_SetFrags(0)
	end
end

local GARG_DAMAGE = bit.bor(DMG_ENERGYBEAM, DMG_CRUSH, DMG_MISSILEDEFENSE, DMG_BLAST)
local SHIELD_DAMAGE = bit.bor(DMG_BULLET, DMG_CLUB, DMG_SLASH, DMG_BUCKSHOT, DMG_ACID, DMG_RADIATION, DMG_POISON, DMG_NERVEGAS)
function GM:EntityTakeDamage(victim, dmg)
	local atk = dmg:GetAttacker()

	-- no friendly fire
	if (victim:IsNPC() and atk:IsPlayer() and victim:Disposition(atk) == D_LI) or
			(atk:IsNPC() and victim:IsPlayer() and atk:Disposition(victim) == D_LI) then
		return true
	end

	if victim:IsPlayer() then
		-- check shield active
		if AUX.SuitPowerIsDeviceActive(victim, AUX.Devices.SuitDeviceShield) then
			if dmg:IsDamageType(SHIELD_DAMAGE) then
				victim:EmitSound("Shield.BulletImpact")
			end

			if dmg:IsDamageType(DMG_BULLET) then
				local startPos = dmg:GetReportedPosition()
				local endPos = dmg:GetDamagePosition()

				local tr = util.TraceLine({
					start = startPos,
					endpos = endPos,
					filter = dmg:GetAttacker(),
					mask = MASK_SHOT
				})

				local data = EffectData()
				data:SetOrigin(dmg:GetDamagePosition())
				data:SetNormal(tr.HitNormal)

				-- TODO: ShieldImpact effect
				util.Effect("AR2Impact", data, false)
			end

			return true
		end
	end

	-- HACK!!!!
	-- stop headcrabs spawning on no-headcrab zombies
	if victim:IsNPC() and victim:Classify() == CLASS_ZOMBIE and victim:HasSpawnFlags(SF_ZOMBIE_NO_HEADCRAB) and dmg:IsDamageType(DMG_BULLET) and victim:Health() - dmg:GetDamage() <= 0 then
		dmg:SetDamageType(bit.band(dmg:GetDamageType(), bit.bnot(DMG_BULLET)))
	end
end

function GM:FireBulletsCallback(ent, bullet, tr, dmg)
	if tr.Entity:GetClass() == "monster_gargantua" and not dmg:IsDamageType(GARG_DAMAGE) then
		dmg:SetDamageType(bit.bor(dmg:GetDamageType(), DMG_BLAST))
	end
end

function GM:PostEntityTakeDamage(victim, dmg, took)
	-- points for damage :)
	if took then
		local atk = dmg:GetAttacker()
		math.randomseed(CurTime())

		if atk.AddFrags and
				dmg:GetDamage() > 0 and math.random() < .1 and
				(victim:IsNPC() or (victim:IsPlayer() and hook.Run("PlayerShouldTakeDamage", victim, atk))) and
				victim:GetClass() ~= "npc_merchant"
		then
			atk:AddFrags(1)
		end
	end
end

-- see: weapon_healer
function GM:PlayerOnHealEntity(ply, ent)
	math.randomseed(CurTime())
	if math.random() < .5 then
		ply:AddFrags(1)
	end
end

function GM:PlayerSelectSpawn(ply, transiton)
	if transiton then return end
	if ply.UseSpawnPoint == false then
		ply.UseSpawnPoint = nil
		return
	end

	-- Save information about all of the spawn points
	-- in a team based game you'd split up the spawns
	--if not IsTableOfEntitiesValid(self.SpawnPoints) then
	self.LastSpawnPoint = 0
	self.SpawnPoints = ents.FindByClass("info_player_start")
	self.SpawnPoints = table.Add(self.SpawnPoints, ents.FindByClass("info_player_deathmatch"))

	-- CS Maps
	--self.SpawnPoints = table.Add(self.SpawnPoints, ents.FindByClass("info_player_counterterrorist"))
	--self.SpawnPoints = table.Add(self.SpawnPoints, ents.FindByClass("info_player_terrorist"))

	-- OC maps
	self.SpawnPoints = table.Add(self.SpawnPoints, ents.FindByClass("info_player_red"))
	self.SpawnPoints = table.Add(self.SpawnPoints, ents.FindByClass("info_player_blue"))
	--end

	local i = 1
	while i <= #self.SpawnPoints do
		local ent = self.SpawnPoints[i]

		if ent and not ent:IsValid() or ent.oc_disabled then
			table.remove(self.SpawnPoints, i)
		else
			i = i + 1
		end
	end

	local Count = i - 1

	if Count == 0 then
		-- Msg("[PlayerSelectSpawn] Error! No spawn points!\n")
		return nil
	end

	-- If any of the spawnpoints have a MASTER flag then only use that one.
	-- This is needed for single player maps.
	for _, spawn in next, self.SpawnPoints do
		if spawn:HasSpawnFlags(1) and hook.Call("IsSpawnpointSuitable", self, ply, spawn, true) then
			return spawn
		end
	end

	local ChosenSpawnPoint = nil

	-- Try to work out the best, random spawnpoint
	for i = 1, Count do
		ChosenSpawnPoint = table.Random(self.SpawnPoints)

		if IsValid(ChosenSpawnPoint) and ChosenSpawnPoint:IsInWorld() then
			if (ChosenSpawnPoint == ply:GetVar("LastSpawnpoint") or ChosenSpawnPoint == self.LastSpawnPoint) and Count > 1 then continue end

			if hook.Call("IsSpawnpointSuitable", self, ply, ChosenSpawnPoint, i == Count) then
				self.LastSpawnPoint = ChosenSpawnPoint
				ply:SetVar("LastSpawnpoint", ChosenSpawnPoint)
				return ChosenSpawnPoint
			end
		end
	end

	return ChosenSpawnPoint
end

local function ReadSound(FileName)
	if not FileName then return end

	local filter = RecipientFilter()
	filter:AddAllPlayers()

	-- The sound is always re-created serverside because of the RecipientFilter.
	local sound = CreateSound(game.GetWorld(), FileName, filter) -- create the new sound, parented to the worldspawn (which always exists)
	if sound then
		sound:SetSoundLevel(0)                                    -- play everywhere
		sound:Play()
	end

	return sound -- useful if you want to stop the sound yourself
end

function GM:CountdownDisplay(label, length)
	if not isnumber(length) then return end

	local lct = ents.Create("game_countdown_timer")
	lct:Spawn()
	lct:Activate()
	lct:Fire("SetTimerLabel", label)
	lct:Fire("StartTimer", tostring(length))

	return lct
end

SF_AMBIENT_GENERIC_GLOBAL_SOUND = 0x1

function GM:AcceptInput(ent, inp, actor, caller, data)
	local classname = ent:GetClass()

	if classname:find"info_player_" then
		if inp == "Enable" or inp == "Disable" then
			ent.oc_disabled = inp == "Disable"
			return true
		end
	elseif ent:IsNPC() then
		if inp == "SetTeam" then
			local old = ent.oc_teamnum
			ent.oc_teamnum = tonumber(data)
			hook.Call("EntityTeamChanged", self, ent, old, ent.oc_teamnum)
			return true
		end
	elseif classname == "point_servercommand" and inp == "Command" then
		data = data:gsub("sv_hl2mp", "sv_oc")
		local cmd_data = string.Split(data, " ")
		if cmd_data[1] == "echo" then
			print("[OC] ECHO: ", unpack(cmd_data, 2))
		elseif cmd_data[1] == "ent_fire" then
			-- so far this isn't needed
			--PrintTable(cmd_data)
		else
			if IsConCommandBlocked(cmd_data[1]) then
				print("[OC] point_servercommand attempted to run GMod blocked command: ", data)
			elseif cmd_data[1] == "say" then
				table.remove(cmd_data, 1)
				PrintMessage(HUD_PRINTTALK, table.concat(cmd_data, " "))
			else
				RunConsoleCommand(unpack(cmd_data))
			end
		end
		return true
	elseif classname == "game_end" and inp == "EndGame" then
		self.LevelChangeTimer = self:CountdownDisplay("Level Change", 10)
		self.BackToLobby = true
		self.BackToLobbyTimer = CurTime() + 10

		return true
	elseif classname == "point_viewcontrol" then
		local should_freeze = ent:HasSpawnFlags(4)

		if inp == "Enable" then
			local m_hPlayer = ent:GetInternalVariable("m_hPlayer")
			timer.Simple(0, function()
				if m_hPlayer:IsValid() then
					m_hPlayer:SetViewEntity(ent)
					if should_freeze then m_hPlayer:AddFlags(FL_FROZEN) end
				else
					for _, ply in player.Iterator() do
						ply:SetViewEntity(ent)
						if should_freeze then ply:AddFlags(FL_FROZEN) end
					end
				end
			end)
		elseif inp == "EnableForActivatorOnly" then
			ent:SetSaveValue("m_hPlayer", actor)
			ent:Fire"Enable"
		elseif inp == "Disable" then
			local m_hPlayer = ent:GetInternalVariable("m_hPlayer")
			timer.Simple(0, function()
				if m_hPlayer:IsValid() then
					m_hPlayer:SetViewEntity(m_hPlayer)
					if should_freeze then m_hPlayer:RemoveFlags(FL_FROZEN) end

					if IsValid(ent) then
						ent:SetSaveValue("m_hPlayer", NULL)
					end
				else
					for _, ply in player.Iterator() do
						ply:SetViewEntity(ply)
						if should_freeze then ply:RemoveFlags(FL_FROZEN) end
					end
				end

				-- oc_nerv :)
				if ent.oc_KILL_NOW then
					SafeRemoveEntity(ent)
				end
			end)
		elseif inp == "DisableForActivatorOnly" then
			ent:SetSaveValue("m_hPlayer", actor)
			ent:Fire"Disable"
		elseif inp == "Kill" then
			ent.oc_KILL_NOW = true
			ent:Fire("Disable")
			return true
		end
	elseif classname == "ambient_generic" then
		-- if playing sound and flag is set to play globally
		if ent:HasSpawnFlags(SF_AMBIENT_GENERIC_GLOBAL_SOUND) then
			if inp == "PlaySound" then
				local sound = ReadSound(ent.oc_ag_message)
				ent.oc_g_sound = sound
				ent.oc_playing_sound = true

				sound:Stop()
				timer.Simple(0, function()
					sound:Play()
				end)

				return true
			elseif inp == "StopSound" then
				if ent.oc_g_sound then
					ent.oc_playing_sound = false
					ent.oc_g_sound:Stop()
				end

				return true
			elseif inp == "ToggleSound" then
				if ent.oc_g_sound then
					if ent.oc_playing_sound then
						ent.oc_g_sound:Stop()
					else
						ent.oc_g_sound:Play()
					end
				end

				ent.oc_playing_sound = not ent.oc_playing_sound
				return true
			elseif inp == "Volume" and tonumber(data) then
				local val = tonumber(data)
				if ent.oc_g_sound then
					ent.oc_g_sound:ChangeVolume(val)
				end

				return true
			end
		end
	end
end

function GM:OnEntityCreated(ent)
	if not IsValid(ent) then return end

	local classname = ent:GetClass()
	local name = ent:GetName()

	-- Run this next frame so we can safely remove entities and have their actual names assigned.
	timer.Simple(0, function()
		if not IsValid(ent) then return end

		-- Required information for respawning some things.
		ent.InitialSpawnData = {
			Pos = ent:GetPos(),
			Ang = ent:GetAngles(),
			Mins = ent:OBBMins(),
			Maxs = ent:OBBMaxs(),
		}

		--[[if ent:IsWeapon() == true then
			self:TrackWeapon(ent)
			if ent:CreatedByMap() == true then
				DbgPrint("Level designer created weapon: " .. tostring(ent))
				self:InsertLevelDesignerPlacedObject(ent)
			end
		elseif ent:IsItem() == true then
			if ent:CreatedByMap() == true then
				DbgPrint("Level designer created item: " .. tostring(ent))
				self:InsertLevelDesignerPlacedObject(ent)
			end
		end]]

		if ent:IsNPC() then
			local owner = ent:GetOwner()
			if owner:IsValid() and owner:IsNPC() then
				local enemies = owner:GetKnownEnemies()

				for _, enemy in ipairs(enemies) do
					ent:AddEntityRelationship(enemy, owner:Disposition(enemy), 2)
				end
			end

			if ent:Classify() == CLASS_ZOMBIE and ent:HasSpawnFlags(SF_ZOMBIE_NO_HEADCRAB) then
				ent:SetBodygroup(1, 0)
			end

			if ent.oc_npchealth then
				ent:SetMaxHealth(math.max(ent.oc_npchealth, ent:Health()))
				ent:SetHealth(math.max(ent.oc_npchealth, ent:Health()))
			end

			ent:SetLagCompensated(true)
		end

		if classname == "env_rockettrail" or classname == "env_smoketrail" then
			local parent = ent:GetParent()
			if IsValid(parent) then
				local owner = parent:GetInternalVariable("m_hOwnerEntity")
				if IsValid(owner) and owner:IsPlayer() then
					ent:SetSaveValue("m_StartColor", owner:GetWeaponColor())
				end
			end
		elseif classname == "env_spritetrail" and not ent.OurOwn then
			local parent = ent:GetInternalVariable("m_hAttachedToEntity")
			if IsValid(parent) and parent:GetClass() == "npc_grenade_frag" then
				local owner = parent:GetInternalVariable("m_hOwnerEntity")
				if IsValid(owner) and owner:IsPlayer() then
					SafeRemoveEntity(ent)
					local e = util.SpriteTrail(parent, parent:LookupAttachment("fuse") or 0, owner:GetWeaponColor():ToColor(), true, 8, 1, 0.5, 1, "sprites/physbeam.vmt")
					e.OurOwn = true
					e:SetSaveValue("m_hAttachedToEntity", parent)
					parent.spriteTrail = e
				end
			end
		elseif classname == "beam" then
			local parent = ent:GetInternalVariable("m_hEndEntity")
			if IsValid(parent) and parent:GetClass() == "npc_tripmine" then
				local owner = parent:GetInternalVariable("m_hOwner")
				if IsValid(owner) and owner:IsPlayer() then
					ent:SetColor(owner:GetWeaponColor():ToColor())
				end
			end
		elseif classname == "env_sprite" and not ent.OurOwn then
			local parent = ent:GetParent()
			if IsValid(parent) then
				if parent:GetClass() == "npc_grenade_frag" then
					local owner = parent:GetInternalVariable("m_hOwnerEntity")
					if IsValid(owner) and owner:IsPlayer() then
						SafeRemoveEntity(ent)
						local e = ents.Create("env_sprite")
						e.OurOwn = true
						e:SetKeyValue("model", "sprites/light_glow02.vmt")
						e:SetSaveValue("m_ModelName", "sprites/light_glow02.vmt")
						e:SetKeyValue("scale", "0.2")
						e:SetKeyValue("rendermode", "3")
						e:SetKeyValue("renderfx", "14")
						e:SetKeyValue("GlowProxySize", "4")
						e:SetKeyValue("effects", "81")
						e:SetSaveValue("m_nBrightness", 200)
						if parent.spriteTrail then
							e:SetSaveValue("m_hMovePeer", parent.spriteTrail)
						end

						e:SetParent(parent)
						e:SetAttachment(parent, parent:LookupAttachment("fuse") or 0)
						e:SetColor(owner:GetWeaponColor():ToColor())
						e:Fire("ShowSprite")
						e:Spawn()
					end
				elseif parent:GetClass() == "npc_satchel" then
					local owner = parent:GetInternalVariable("m_hOwnerEntity")
					if IsValid(owner) and owner:IsPlayer() then
						SafeRemoveEntity(ent)
						local e = ents.Create("env_sprite")
						e.OurOwn = true
						e:SetKeyValue("model", "sprites/light_glow02.vmt")
						e:SetSaveValue("m_ModelName", "sprites/light_glow02.vmt")
						e:SetKeyValue("scale", "0.3")
						e:SetKeyValue("rendermode", "3")
						e:SetKeyValue("renderfx", "10")
						e:SetKeyValue("GlowProxySize", "2")
						e:SetKeyValue("effects", "81")
						e:SetSaveValue("m_nBrightness", 255)

						e:SetParent(parent)
						e:SetLocalPos(Vector(0, 0, 0))
						e:SetColor(owner:GetWeaponColor():ToColor())
						e:Fire("ShowSprite")
						e:Spawn()
					end
				end
			end
		elseif classname == "env_laserdot" then
			local owner = ent:GetInternalVariable("m_hOwnerEntity")
			if IsValid(owner) and owner:IsPlayer() then
				ent:SetSaveValue("rendermode", RENDERMODE_NONE)
				ent:SetColor(Color(0, 0, 0, 0))

				local e = ents.Create("env_sprite")
				e.OurOwn = true
				e:SetKeyValue("model", "sprites/light_glow02.vmt")
				e:SetSaveValue("m_ModelName", "sprites/light_glow02.vmt")
				e:SetKeyValue("scale", "0.2")
				e:SetKeyValue("rendermode", "3")
				e:SetKeyValue("renderfx", "14")
				e:SetKeyValue("GlowProxySize", "4")
				e:SetKeyValue("effects", "16")

				e:SetParent(ent)
				e:SetLocalPos(Vector(0, 0, 0))
				e:SetColor(owner:GetWeaponColor():ToColor())
				e:Fire("ShowSprite")
				e:Spawn()
			end
		elseif classname == "grenade_hand" then
			ent:SetNoDraw(true)

			-- handle human assassin
			timer.Simple(0, function()
				if ent:IsValid() then
					local hThrower = ent:GetInternalVariable("m_hThrower")

					if hThrower:GetClass() == "monster_human_assassin" then
						local proj = ents.Create("npc_grenade_frag")
						proj:SetPos(ent:GetPos())
						proj:SetAngles(ent:GetAngles())
						proj:SetOwner(hThrower)
						proj:SetSaveValue("m_hThrower", hThrower)
						proj:Spawn()
						proj:Fire("SetTimer", "2")
						local vel = ent:GetVelocity()
						vel.z = vel.z + 100
						ent:Remove()

						local phys = proj:GetPhysicsObject()
						if phys:IsValid() then
							phys:SetVelocity(vel)
							phys:SetAngleVelocity(vel)
						end
					else
						ent:SetNoDraw(false)
					end
				end
			end)
		end

		if classname == "monster_babycrab" and ent:GetOwner():IsNPC() then
			ent:SetSolid(SOLID_NONE)

			timer.Simple(0, function()
				local newEnt = ents.Create("npc_headcrab")
				newEnt:SetPos(ent:GetPos())
				newEnt:SetAngles(ent:GetAngles())
				newEnt:SetOwner(ent:GetOwner())

				newEnt:Spawn()
				ent:Remove()
			end)

			return
		end

		-- replacement stuff
		if npc_replace[classname] and IsValid(ent) then
			ent:SetSolid(SOLID_NONE)

			timer.Simple(0, function()
				local newEnt = ents.Create(npc_replace[classname])
				newEnt:SetPos(ent:GetPos())
				newEnt:SetAngles(ent:GetAngles())
				newEnt:SetName(name)

				if classname == "npc_hgrunt" then
					newEnt:CapabilitiesAdd(CAP_USE_WEAPONS)
					newEnt:SetModel("models/hgrunt/hgrunt" .. math.random(1, 5) .. ".mdl")
				end

				for k, v in pairs(ent:GetKeyValues()) do
					if k == "classname" or k:lower():sub(1, 2) == "on" or not isstring(v) then continue end
					newEnt:SetKeyValue(k, v)
				end

				newEnt:SetOwner(ent:GetOwner())

				newEnt.__outputs = ent.__outputs
				newEnt.__kv = ent.__kv

				if ent.__outputs and istable(ent.__outputs) then
					for k, tbl in pairs(ent.__outputs) do
						for _, v in pairs(tbl) do
							newEnt:Fire("AddOutput", k .. " " .. v, 0)
						end
					end
				end

				if ent.__kv and istable(ent.__kv) then
					for k, v in pairs(ent.__kv) do
						if k == "classname" or k == "origin" or k:sub(1, 2) == "on" or not isstring(v) then continue end
						newEnt:SetKeyValue(k, v)
					end
				end

				newEnt:Spawn()

				SafeRemoveEntity(ent)
			end)
		end

		if classname == "func_breakable" and name ~= "" then
			ent:SetNW2String("displayname", name)
		end

		if ent:GetModel() == "models/combine_elite_soldier.mdl" then
			ent:SetModel("models/combine_super_soldier.mdl")
		end
	end)

	-- Deal with vehicles at the same frame, sometimes it wouldn't show the gun.
	if ent:IsVehicle() and not game.GetMap():match("^[d1|d2|d3]") then
		if not IsValid(ent) then return end
		ent:Fire("enablegun", "1")

		timer.Simple(0, function()
			if not IsValid(ent) then return end
			ent:SetBodygroup(1, 1)
		end)
	end
end

function GM:PlayerSpray(ply)
	timer.Simple(0, function()
		ply:AllowImmediateDecalPainting(true)
	end)

	return false
end

function GM:PlayerShouldTakeDamage(ply, atk)
	if atk:IsPlayer() then
		if mp_teamplay:GetBool() then
			if ply:Team() == atk:Team() then
				return false
			else
				return true
			end
		end

		if atk ~= ply then return false end
	end

	return true
end

GM.LivesModeRestart = false
GM.WaitingForPlayers = true
function GM:Think()
	if self.WaitingForPlayers and player.GetCount() > 0 then
		self.WaitingForPlayers = false
	else
		if mp_livesmode:GetInt() > 0 then
			local bNoneAlive = true

			for _, ply in player.Iterator() do
				--if ply:IsBot() then continue end
				if ply:Team() == TEAM_SPECTATOR then continue end

				if ply:Lives() > 0 then
					bNoneAlive = false
					break
				end
			end

			if not self.WaitingForPlayers and bNoneAlive and not self.LivesModeRestart then
				self.LivesModeRestart = true
				-- no players alive! restarting level...
				PrintMessage(HUD_PRINTCENTER, "No players alive! Restarting level...")
				self:CountdownDisplay("Restarting Level", 10)
				timer.Simple(10, function()
					RunConsoleCommand("changelevel", game.GetMap())
				end)
			end
		end
	end

	if self.BackToLobby and CurTime() > self.BackToLobbyTimer then
		RunConsoleCommand("changelevel", oc_lobby_map:GetString())
	end
end

local NPC = FindMetaTable("NPC")
function NPC:Frags()
	return self:GetNW2Int("oc_frags", 0)
end

function NPC:SetFrags(val)
	self:SetNW2Int("oc_frags", val)
end

function NPC:AddFrags(val)
	self:SetFrags(self:Frags() + val)
end

local PLY = FindMetaTable"Player"
PLY._SetFrags = PLY._SetFrags or PLY.SetFrags
PLY._AddFrags = PLY._AddFrags or PLY.AddFrags
function PLY:SetFrags(num)
	return self:SetNW2Int("oc_frags", num)
end

function PLY:AddFrags(num)
	self:SetNW2Int("oc_frags", self:Frags() + num)
end

function PLY:AddLives(num)
	self:SetNW2Int("oc_lives", self:Lives() + num)
end

function PLY:SetLives(num)
	self:SetNW2Int("oc_lives", num)
end

function PLY:TeleportPlayer(pos, ang, vel)
	local data = {}
	data.vel = vel or self:GetVelocity()
	data.ang = ang or self:GetAngles()
	data.pos = pos or Vector(0, 0, 0)
	self.TeleportQueue = self.TeleportQueue or {}
	table.insert(self.TeleportQueue, data)
	-- Make sure players won't get stuck in each other.
	--self:DisablePlayerCollide(true)
end

--[[function PLY:DisablePlayerCollide(state, temporarily)
	if state == true then
		self.NextPlayerCollideTest = CurTime() + 2
	end
	if temporarily == nil then
		temporarily = true
	end
	self:SetNWBool("DisablePlayerCollide", state)
	self.DisablePlayerCollisionTemporarily = temporarily
	self:CollisionRulesChanged()
	--DbgPrint(self, "DisablePlayerCollide", tostring(state))
end]]

local ENTITY = FindMetaTable"Entity"
ENTITY._MapCreationID = ENTITY._MapCreationID or ENTITY.MapCreationID
function ENTITY:MapCreationID()
	return self.oc_creationID or self:_MapCreationID()
end

function GM:ShowHelp(ply)
	ply:ConCommand("oc_settings")
end

function GM:ShowTeam(ply)
	ply:ConCommand("oc_briefing")
end

function GM:ShowSpare1(ply)
end

function GM:ShowSpare2(ply)
end

do
	local TAG = "oc_cmd"
	util.AddNetworkString(TAG)

	local VOICE_TABLE = {
		male = "Obsidian_Male_Player",
		female = "Obsidian_Female_Player",
		combine = "Obsidian_CombineS_Player",
	}

	local PREFERED_VOICE = {
		"male",
		"female",
		"combine",
	}

	local PING_BLACKLIST = {
		func_brush = true,
		func_lod = true,
	}

	local SWITCH_NET_RECV = {
		[OC_CMD_UPDATE_PLCOL] = function(len, pl)
			pl:SetPlayerColor(net.ReadVector())
		end,
		[OC_CMD_UPDATE_PLMODEL] = function(len, pl)
			local mdl = net.ReadString()

			pl:SetModel(player_manager.TranslatePlayerModel(mdl))

			local skin = pl:GetInfoNum("cl_playerskin", 0)
			pl:SetSkin(skin)

			local groups = pl:GetInfo("cl_playerbodygroups")
			if (groups == nil) then groups = "" end
			local groups = string.Explode(" ", groups)
			for k = 0, pl:GetNumBodyGroups() - 1 do
				pl:SetBodygroup(k, tonumber(groups[k + 1]) or 0)
			end
		end,
		[OC_CMD_UPDATE_WEPCOL] = function(len, pl)
			pl:SetWeaponColor(net.ReadVector())
		end,
		[OC_CMD_CHANGETEAM] = function(len, pl)
			local blue = net.ReadBool()
			if blue == nil then return end
			if not mp_teamplay:GetBool() then return end

			if pl.lastteamttime and pl.lastteamttime > CurTime() then
				pl:ChatPrint("Cannot change teams that fast.")
				return
			end

			if pl:Team() == (blue and TEAM_BLUE or TEAM_RED) then
				pl:ChatPrint("You're already on that team.")
				return
			end

			if blue and #team.GetPlayers(TEAM_BLUE) > #team.GetPlayers(TEAM_RED) then
				pl:PrintMessage(HUD_PRINTCENTER, "Too many players on Blue.")
				return
			elseif not blue and #team.GetPlayers(TEAM_RED) > #team.GetPlayers(TEAM_BLUE) then
				pl:PrintMessage(HUD_PRINTCENTER, "Too many players on Red.")
				return
			end

			pl:Kill()
			pl:SetTeam(blue and TEAM_BLUE or TEAM_RED)

			pl.lastteamttime = CurTime() + 5
		end,
		[OC_CMD_TRANSFERPOINTS] = function(len, ply)
			local receiver = Player(net.ReadUInt(8))
			if not IsValid(receiver) then return end -- receiving player doesnt exist

			local amount = net.ReadUInt(16)
			if ply:Frags() < amount then return end -- not enough points to transfer

			ply:AddFrags(-amount)
			receiver:AddFrags(amount)
		end,
		[OC_CMD_TOGGLE_ITEM] = function(len, ply)
			local item = net.ReadUInt(4)

			if item == 0 then -- cloak
				if AUX.SuitPowerIsDeviceActive(ply, AUX.Devices.SuitDeviceCloak) then
					AUX.SuitPowerRemoveDevice(ply, AUX.Devices.SuitDeviceCloak)
				elseif ply:GetHasCloak() then
					AUX.SuitPowerAddDevice(ply, AUX.Devices.SuitDeviceCloak)
				end
			elseif item == 1 then -- shield
				if AUX.SuitPowerIsDeviceActive(ply, AUX.Devices.SuitDeviceShield) then
					AUX.SuitPowerRemoveDevice(ply, AUX.Devices.SuitDeviceShield)
				elseif ply:GetHasShield() then
					AUX.SuitPowerAddDevice(ply, AUX.Devices.SuitDeviceShield)
				end
			end
		end,
		[OC_CMD_PLAYERWAYPOINT] = function(len, ply)
			if not ply:Alive() then return end
			if ply:Team() == TEAM_SPECTATOR then return end
			if CurTime() < (ply.NextPlayerWaypoint or 0) then return end

			local bMedic = net.ReadBool()

			local mdl = ply:GetModel()
			local voice = "male"
			for key in pairs(VOICE_TABLE) do
				if key == "male" and mdl:find("female") then continue end
				if mdl:find(key) then
					voice = key
					break
				end
			end

			if mdlinspect and file.Exists(mdl, "GAME") then
				local info = mdlinspect.Open(mdl)
				info:ParseHeader()
				local submodels = info:IncludedModels()

				for _, model in ipairs(submodels) do
					for _, path in ipairs(model) do
						if path == "models/f_anm.mdl" then
							voice = "female"
							break
						elseif path == "models/m_anm.mdl" then
							voice = "male"
							break
						end
					end
				end
			end

			local prefered = ply:GetInfoNum("oc_voice", 0)
			local mapped = PREFERED_VOICE[prefered]
			if mapped then
				voice = mapped
			end

			local prefix = VOICE_TABLE[voice]

			ply:EmitSound(Format("%s.%s", prefix, bMedic and "Medic" or "Attention"))

			local filter = RecipientFilter()
			filter:AddRecipientsByTeam(ply:Team())
			filter:RemovePlayer(ply)

			net.Start("info_waypoint")
			net.WriteUInt(1, 8)
			net.WriteUInt(ply:EntIndex(), 16)
			net.WriteString(bMedic and "sprites/medic" or "sprites/attention")
			net.WriteString(ply:Nick())
			net.WriteEntity(ply)
			net.WriteVector(vector_origin)
			net.WriteFloat(10)
			net.Send(filter)

			ply.NextPlayerWaypoint = CurTime() + 5
			ply:SetNW2Float("NextPlayerWaypoint", ply.NextPlayerWaypoint)
		end,
		[OC_CMD_PING] = function(len, ply)
			if not ply:Alive() then return end
			if ply:Team() == TEAM_SPECTATOR then return end
			if CurTime() < (ply.NextPlayerWaypoint or 0) then return end

			local tr = ply:GetEyeTrace()
			local ent = tr.Entity
			if ent:IsValid() and PING_BLACKLIST[ent:GetClass()] then
				ent = NULL
			end

			local filter = RecipientFilter()
			filter:AddRecipientsByTeam(ply:Team())

			net.Start("info_waypoint")
			net.WriteUInt(1, 8)
			net.WriteUInt(ply:EntIndex(), 16)
			net.WriteString("sprites/attention")
			net.WriteString(ply:Nick() .. " pinged at this!")
			net.WriteEntity(ent)
			net.WriteVector(tr.HitPos)
			net.WriteFloat(10)
			net.Send(filter)

			net.Start(TAG)
			net.WriteUInt(OC_CMD_PING, 4)
			net.WriteVector(tr.HitPos)
			net.WriteVector(tr.HitNormal)
			net.Send(filter)

			ply.NextPlayerWaypoint = CurTime() + 5
		end,
		[OC_CMD_DROPWEAPON] = function(len, ply)
			ply:DropWeapon()
		end,
	}

	net.Receive(TAG, function(len, ply)
		local cmd = net.ReadUInt(4)
		len = len - 4

		local callback = SWITCH_NET_RECV[cmd]
		if isfunction(callback) then
			callback(len, ply)
		else
			print(Format("Net \"%s\": unknown cmd %d, len: %d", TAG, cmd, len))
		end
	end)
end

util.AddNetworkString("info_waypoint")
util.AddNetworkString("oc_netdisp")
util.AddNetworkString("oc_playerspawn")
util.AddNetworkString("oc_mapdecals")

function GM:PlayerPostThink(ply)
	-- network ent:Disposition()
	ply.oc_entdisp = ply.oc_entdisp or {}
	for _, npc in ipairs(ents.FindByClass("npc_*")) do
		if not (IsValid(npc) and npc:IsNPC()) then continue end -- wtf?

		if not ply.oc_entdisp[npc] or ply.oc_entdisp[npc] ~= npc:Disposition(ply) then
			hook.Run("PlayerDispositionChanged", ply, npc, ply.oc_entdisp[npc], npc:Disposition(ply))
			ply.oc_entdisp[npc] = npc:Disposition(ply)
		end
	end

	-- flashlight stuff
	if not ply:Alive() and IsValid(self.Flashlights[ply]) then self:DestroyFlashlight(ply) end

	if IsValid(self.Flashlights[ply]) and ply:GetNW2Bool("flashlight_state", false) then
		local eyeVectors = ply:GetAimVector():Angle()
		local vecForward, vecRight, vecUp = eyeVectors:Forward(), eyeVectors:Right(), eyeVectors:Up()

		--UpdateLight(ply, self.Flashlights[ply], ply:EyePos(), vecForward, vecRight, vecUp)
		self:UpdatePlayerFlashLight(ply, vecForward, vecRight, vecUp)
		self.Flashlights[ply]:SetAngles(ply:EyeAngles())
	end
end

function GM:PlayerDispositionChanged(ply, ent, old, new)
	net.Start("oc_netdisp")
	net.WriteUInt(ent:EntIndex(), 16)
	net.WriteUInt(old or 0, 8)
	net.WriteUInt(new or 0, 8)
	net.Send(ply)
end

function GM:PlayerDisconnected(ply)
	-- wtf?
	if not (ply:IsValid() and ply:IsPlayer()) then return end

	Obsidian.PlayerDropEntireInventory(ply)

	if self.Flashlights[ply] and IsValid(self.Flashlights[ply]) then
		self:DestroyFlashlight(ply)
	end

	-- final player disconnects
	if #player.GetHumans() == 1 then
		self.WaitingForPlayers = true
	end
end

local NO_NOISE = {
	func_lod = true
}
function GM:KeyPress(ply, key)
	if not IsValid(ply) then return end

	if key == IN_USE then
		local ent = self:PlayerUseTrace(ply)

		if IsValid(ent) and not (ent:IsNPC() or ent:IsPlayer()) and not NO_NOISE[ent:GetClass()] then
			ply:EmitSound"HL2Player.Use"
		else
			ply:EmitSound"HL2Player.UseDeny"
		end
	end

	if self:IsPlayerSpectator(ply) then
		local iObsMode = ply:GetObserverMode()
		if key == IN_ATTACK or key == IN_ATTACK2 then
			-- when roaming, try to target entity we're aiming at
			if iObsMode == OBS_MODE_ROAMING then
				local tr = ply:GetEyeTrace()
				local ent = tr.Entity

				if ent:IsValid() and (ent:IsPlayer() or ent:IsNPC()) then
					ply:SetObserverMode(OBS_MODE_IN_EYE)
					ply:SpectateEntity(ent)
				end

				return true
			end

			local iStep = key == IN_ATTACK and 1 or -1

			local specList = self:GetPlayerSpectateList(ply)
			local len = #specList

			-- select next target from spectating list
			local currentEnt = ply:GetObserverTarget()

			if currentEnt:IsValid() then
				for k, ent in ipairs(specList) do
					if ent == currentEnt then
						local nexti = (k + iStep - 1) % len + 1

						local new = specList[nexti]
						ply:SpectateEntity(new)
						break
					end
				end
			end

			if currentEnt ~= ply:GetObserverTarget() and len > 0 then
				local new = specList[math.random(1, len)]
				ply:SpectateEntity(new)
			end

			return true
		elseif key == IN_JUMP then
			-- go from roaming -> chase -> in eye
			iObsMode = iObsMode + 1
			if iObsMode > OBS_MODE_ROAMING then
				iObsMode = OBS_MODE_IN_EYE
			end

			ply:Spectate(iObsMode)

			return true
		end
	end
end

function GM:ShutDown()
	-- cleanup :)
end
