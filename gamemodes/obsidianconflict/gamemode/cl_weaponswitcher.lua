local sndMove = Sound("Player.WeaponSelectionMoveSlot")
local sndSelect = Sound("Player.WeaponSelectionClose")

local lply = LocalPlayer()

local function ScreenScaleH(n)
	return n * (ScrH() / 480)
end

local curSlot = 0
local curPos = 1
local selTime = 0
local wepCount = 0

local tCache = {}
local tCacheLength = {}

local WeaponNames = {
	weapon_smg1        = language.GetPhrase("#HL2_SMG1"),
	weapon_shotgun     = language.GetPhrase("#HL2_Shotgun"),
	weapon_crowbar     = language.GetPhrase("#HL2_Crowbar"),
	weapon_pistol      = language.GetPhrase("#HL2_Pistol"),
	weapon_357         = language.GetPhrase("#HL2_357Handgun"),
	weapon_crossbow    = language.GetPhrase("#HL2_Crossbow"),
	weapon_physgun     = language.GetPhrase("#GMOD_Physgun"),
	weapon_rpg         = language.GetPhrase("#HL2_RPG"),
	weapon_bugbait     = language.GetPhrase("#HL2_Bugbait"),
	weapon_frag        = language.GetPhrase("#HL2_Grenade"),
	weapon_ar2         = language.GetPhrase("#HL2_Pulse_Rifle"),
	weapon_physcannon  = language.GetPhrase("#HL2_GravityGun"),
	weapon_stunstick   = language.GetPhrase("#HL2_StunBaton"),
	weapon_slam        = language.GetPhrase("#HL2_SLAM"),
	weapon_annabelle   = language.GetPhrase("#HL2_Annabelle"),
	weapon_handgrenade = language.GetPhrase("#HL1_HandGrenade"),
	gmod_camera        = language.GetPhrase("#GMOD_Camera"),
	weapon_cubemap     = "Cubemap",
	none               = "Hands",
}

local WeaponIconsHL2 = {
	weapon_smg1            = "a",
	weapon_shotgun         = "b",
	weapon_crowbar         = "c",
	weapon_pistol          = "d",
	weapon_357             = "e",
	weapon_crossbow        = "g",
	weapon_physgun         = "h",
	weapon_rpg             = "i",
	weapon_bugbait         = "j",
	weapon_frag            = "k",
	weapon_ar2             = "l",
	weapon_physcannon      = "m",
	weapon_stunstick       = "n",
	weapon_slam            = "o",
	weapon_medkit          = "+",
	weapon_healer          = "+",
	hands                  = "C",
	none                   = "C",
	weapon_annabelle       = "(",

	--unobtanable stuff
	weapon_cubemap         = "",
	weapon_citizenpackage  = "",
	weapon_citizensuitcase = "",
	weapon_oldmanharpoon   = "",

	--hl1
	weapon_357_hl1         = "",
	weapon_crossbow_hl1    = "",
	weapon_crowbar_hl1     = "",
	weapon_egon            = "",
	weapon_gauss           = "h",
	weapon_glock_hl1       = "",
	weapon_handgrenade     = "",
	weapon_hornetgun       = "",
	weapon_mp5_hl1         = "",
	weapon_rpg_hl1         = "",
	weapon_satchel         = "",
	weapon_shotgun_hl1     = "",
	weapon_snark           = "",
	weapon_tripmine        = "",
}

local WIOffsets = {
	hands              = 10,
	none               = 10,
	weapon_medkit      = 5,
	weapon_healer      = 5,
	weapon_annabelle   = 0,
	weapon_handgrenade = 10,
}

local HL1Weps = {
	weapon_hl1_357         = true,
	weapon_hl1_glock       = true,
	weapon_hl1_crossbow    = true,
	weapon_hl1_crowbar     = true,
	weapon_hl1_egon        = true,
	weapon_hl1_handgrenade = true,
	weapon_hl1_hornetgun   = true,
	weapon_hl1_mp5         = true,
	weapon_hl1_rpg         = true,
	weapon_hl1_satchel     = true,
	weapon_hl1_shotgun     = true,
	weapon_hl1_snark       = true,
	weapon_hl1_gauss       = true,
	weapon_hl1_tripmine    = true,
}

local function DrawFontIcon(wep, x, y, w, h, tbl, font1, font2, offset)
	font1 = font1:gsub("^WeaponIcons", "HL2WeaponIcons")
	font2 = font2:gsub("^WeaponIconsSelected", "HL2WeaponIconsSelected")

	y = y + (WIOffsets[wep:GetClass()] and WIOffsets[wep:GetClass()] or offset)
	x = x + 10
	w = w - 20

	draw.DrawText(tbl[wep:GetClass()], font2, x + w / 2, y, GAMEMODE:GetHUDTextColor(), TEXT_ALIGN_CENTER)
	draw.DrawText(tbl[wep:GetClass()], font1, x + w / 2, y, GAMEMODE:GetHUDTextColor(), TEXT_ALIGN_CENTER)
end

local function DrawCharIcon(wep, x, y, w, h, char1, char2, font1, font2, offset)
	font1 = font1:gsub("^WeaponIcons", "HL2WeaponIcons")
	font2 = font2:gsub("^WeaponIconsSelected", "HL2WeaponIconsSelected")

	y = y + (WIOffsets[wep:GetClass()] and WIOffsets[wep:GetClass()] or offset)
	x = x + 10
	w = w - 20

	draw.DrawText(char2, font2, x + w / 2, y, GAMEMODE:GetHUDTextColor(), TEXT_ALIGN_CENTER)
	draw.DrawText(char1, font1, x + w / 2, y, GAMEMODE:GetHUDTextColor(), TEXT_ALIGN_CENTER)
end

local function DWSHL1(wep, x, y, wide, tall, alpha)
	local col = GAMEMODE:GetHUDTextColor()
	surface.SetDrawColor(col.r, col.g, col.b, alpha)
	surface.SetTexture(wep.WepSelectIcon)

	y = y + 30
	x = x + 10
	wide = wide - 20

	surface.DrawTexturedRect(x, y, wide, wide / 2)
	--if isfunction(wep.PrintWeaponInfo) then wep:PrintWeaponInfo( x + wide + 20, y + tall * 0.95, alpha ) end
end

local iconCache = {}
local defaultIcon = surface.GetTextureID("weapons/swep")

local function DWSFallback(wep, x, y, wide, tall, alpha)
	if not iconCache[wep:GetClass()] then
		if Material("vgui/entities/" .. wep:GetClass()):IsError() then
			iconCache[wep:GetClass()] = defaultIcon
		else
			iconCache[wep:GetClass()] = surface.GetTextureID("vgui/entities/" .. wep:GetClass())
		end
	end

	surface.SetDrawColor(255, 255, 255, alpha)
	if tostring(wep.WepSelectIcon):find("Material") then
		surface.SetTexture(defaultIcon)
	else
		surface.SetTexture(wep.WepSelectIcon == defaultIcon and iconCache[wep:GetClass()] or wep.WepSelectIcon or defaultIcon)
	end

	y = y + 10
	x = x + 10
	wide = wide - 20

	surface.DrawTexturedRect(x, y, wide, wide / 2)
	if isfunction(wep.PrintWeaponInfo) and wep.DrawWeaponInfoBox == true then wep:PrintWeaponInfo(x + wide + 20, y + tall * 0.95, alpha) end
end

local DWSCustom = {}

local function DrawWeaponBox(x, y, wep, selected)
	if not IsValid(wep) then return end
	local w, h = ScreenScaleH(100), selected and ScreenScaleH(60) or ScreenScaleH(25)
	draw.RoundedBox(10, x, y, w, h, ColorAlpha(GAMEMODE:GetHUDBGColor(), selected and 80 or 40))

	surface.SetFont("oc_HUDSelectionText")
	draw.DrawText(WeaponNames[wep:GetClass()] or (wep.PrintName or language.GetPhrase(wep:GetClass())), "oc_HUDSelectionText", x + w / 2, y + ScreenScaleH(4), GAMEMODE:GetHUDTextColor(), TEXT_ALIGN_CENTER)

	if wep.BounceWeaponIcon then wep.BounceWeaponIcon = false end

	if selected then
		local _y = y - ScreenScaleH(8)
		if wep.Char and wep.CharFont and wep.SelectedChar and wep.SelectedCharFont then
			DrawCharIcon(wep, x, _y, w, h, wep.Char, wep.SelectedChar, wep.CharFont, wep.SelectedCharFont, 18)
		elseif WeaponIconsHL2[wep:GetClass()] then
			DrawFontIcon(wep, x, _y, w, h, WeaponIconsHL2, "HL2WeaponIcons", "HL2WeaponIconsSelected", 24)
		elseif HL1Weps[wep:GetClass()] then
			DWSHL1(wep, x, _y, w, h, 255)
		elseif DWSCustom[wep:GetClass()] then
			DWSCustom[wep:GetClass()](x, _y, w, h, 255)
		else
			if wep.DrawWeaponSelection and isfunction(wep.DrawWeaponSelection) then
				wep:DrawWeaponSelection(x, _y, w, h, 255)
			else
				DWSFallback(wep, x, _y, w, h, 255)
			end
		end
	end

	return h
end

local boxSize = ScreenScaleH(25)
local function DrawSlot(x, y, slot)
	if tCacheLength and tCacheLength[slot] ~= 0 then
		draw.RoundedBox(10, x, y, boxSize, boxSize, ColorAlpha(GAMEMODE:GetHUDBGColor(), 40))
		draw.DrawText(slot, "oc_HUDSelectionNumbers", x + ScreenScaleH(4), y + ScreenScaleH(4), GAMEMODE:GetHUDTextColor())
	end
end

local spacing = ScreenScaleH(2)
local w = ScreenScaleH(100)
local wbw = boxSize + spacing
local slots = {
	[1] = function(x, y)
		for i = 0, 4 do
			DrawSlot(x + w + spacing + ((spacing + boxSize) * i), y, i + 2)
		end
	end,
	[2] = function(x, y)
		DrawSlot(x, y, 1)
		for i = 0, 3 do
			DrawSlot(x + spacing + boxSize + w + spacing + ((spacing + boxSize) * i), y, i + 3)
		end
	end,
	[3] = function(x, y)
		DrawSlot(x, y, 1)
		DrawSlot(x + (spacing + boxSize), y, 2)
		for i = 0, 2 do
			DrawSlot(x + (spacing + boxSize) * 2 + w + spacing + ((spacing + boxSize) * i), y, i + 4)
		end
	end,
	[4] = function(x, y)
		for i = 0, 2 do
			DrawSlot(x + (spacing + boxSize) * i, y, i + 1)
		end
		for i = 0, 1 do
			DrawSlot(x + (spacing + boxSize) * 3 + w + spacing + ((spacing + boxSize) * i), y, i + 5)
		end
	end,
	[5] = function(x, y)
		for i = 0, 3 do
			DrawSlot(x + (spacing + boxSize) * i, y, i + 1)
		end
		DrawSlot(x + (spacing + boxSize) * 4 + w + spacing, y, 6)
	end,
	[6] = function(x, y)
		for i = 0, 4 do
			DrawSlot(x + (spacing + boxSize) * i, y, i + 1)
		end
	end
}

local function DrawWeaponSelector(x, y, slot, weps, pos)
	for n, wslot in pairs(weps) do
		if n == slot - 1 and pos > 0 then
			local sx, sy = x, y
			for c, wep in pairs(wslot) do
				local h = DrawWeaponBox(sx + wbw * (slot - 2), sy, wep, c == pos) or 0
				if c == 1 then
					draw.DrawText(slot - 1, "oc_HUDSelectionNumbers", sx + wbw * (slot - 2) + ScreenScaleH(4), sy + ScreenScaleH(4), GAMEMODE:GetHUDTextColor())
				end
				sy = sy + h + spacing
			end
		end
	end

	slots[slot - 1](x, y)
end

local function DrawSelector()
	DrawWeaponSelector(GAMEMODE.CurResW / 2 - ScreenScaleH(235) / 2, ScreenScaleH(16), curSlot + 1, tCache, curPos)
end

for i = 1, 6 do
	tCache[i] = {}
	tCacheLength[i] = 0
end

local function PrecacheWeapons()
	for i = 1, 6 do
		for j = 1, tCacheLength[i] do
			tCache[i][j] = nil
		end
		tCacheLength[i] = 0
	end

	wepCount = 0
	for _, wep in pairs(lply:GetWeapons()) do
		wepCount = wepCount + 1

		local slot = wep:GetSlot() + 1

		if slot <= 6 then
			local len = tCacheLength[slot] + 1
			tCacheLength[slot] = len
			tCache[slot][len] = wep
		end
	end

	for i = 1, 6 do
		table.sort(tCache[i], function(a, b) return a:GetSlotPos() < b:GetSlotPos() end)
	end

	if curSlot ~= 0 then
		local len = tCacheLength[curSlot]
		if len < curSlot then
			curSlot = len == 0 and 0 or len
		end
	end
end

local w_alpha = 1
local wpnFading = false
function GM:DrawOverlay()
	if not IsValid(lply) then lply = LocalPlayer() end

	if IsValid(lply)
			and lply:Alive()
			and (wepCount < table.Count(lply:GetWeapons()) or not IsValid(tCache) or not IsValid(tCacheLength) or table.Count(tCache) == 0 or table.Count(tCacheLength) == 0)
			and curSlot == 0
	then
		PrecacheWeapons()
	end

	if curSlot == 0 then return end

	local show
	show = show or RealTime() < selTime + 2
	w_alpha = Lerp(RealFrameTime() * 3.5, w_alpha, show and 1 or 0)
	if w_alpha <= 0.005 then
		curSlot = 0

		selTime = RealTime()
		w_alpha = 1
		wpnFading = false
		return
	end

	if w_alpha < 1 then
		wpnFading = true
	end

	surface.SetAlphaMultiplier(w_alpha)
	if IsValid(lply) and lply:Alive() and (not lply:InVehicle() or lply:GetAllowWeaponsInVehicle()) then
		DrawSelector()
	else
		curSlot = 0
	end
	surface.SetAlphaMultiplier(1)
end

hook.Add("PlayerBindPress", "oc_weaponswitcher", function(ply, bind, pressed)
	if not IsValid(lply) then lply = LocalPlayer() end
	if not ply:Alive()
			or ply:InVehicle() and not ply:GetAllowWeaponsInVehicle()
			or GetConVar("hud_fastswitch"):GetBool()
			or ply ~= lply
	then
		return
	end

	if input.IsKeyDown(KEY_LALT) then return end

	bind = bind:lower()

	if bind == "cancelselect" then
		if pressed then curSlot = 0 end
		return true
	end

	if bind == "invprev" then
		if not pressed then return true end

		if wepCount == 0 then return true end

		if wpnFading == true then wpnFading = false end

		local loop = curSlot == 0

		if loop then
			local wep = ply:GetActiveWeapon()
			local slot = 1
			local slotCache = tCache[slot]
			if IsValid(wep) then
				slot = wep:GetSlot() + 1
				slotCache = tCache[slot]
			end

			if slotCache[1] ~= wep then
				curSlot = slot
				curPos = 1

				for i = 2, tCacheLength[slot] do
					if slotCache[i] == wep then
						curPos = i - 1
						break
					end
				end

				selTime = RealTime()
				ply:EmitSound(sndMove)
				w_alpha = 1

				return true
			end

			curSlot = slot
		end

		if loop or curPos == 1 then
			for i = 1, 6 do
				if curSlot <= 1 then
					curSlot = 6
				else
					curSlot = curSlot - 1
				end
				if tCacheLength[curSlot] ~= 0 then break end
			end

			curPos = tCacheLength[curSlot]
		else
			curPos = curPos - 1
		end

		selTime = RealTime()
		ply:EmitSound(sndMove)
		w_alpha = 1

		return true
	end

	if bind == "invnext" then
		if not pressed then return true end

		if wepCount == 0 then return true end

		if wpnFading == true then wpnFading = false end

		local loop = curSlot == 0

		if loop then
			local wep = ply:GetActiveWeapon()

			local slot = 1
			local len = tCacheLength[slot]
			local slotCache = tCache[slot]
			if IsValid(wep) then
				slot = wep:GetSlot() + 1
				len = tCacheLength[slot]
				slotCache = tCache[slot]
			end

			if IsValid(wep) and slotCache[len] ~= wep then
				curSlot = slot
				curPos = 1

				for i = 1, len - 1 do
					if slotCache[i] == wep then
						curPos = i + 1
						break
					end
				end

				selTime = RealTime()
				ply:EmitSound(sndMove)
				w_alpha = 1

				return true
			end

			curSlot = slot
		end

		if loop or curPos == tCacheLength[curSlot] then
			for i = 1, 6 do
				if curSlot == 6 then
					curSlot = 1
				else
					curSlot = curSlot + 1
				end
				if tCacheLength[curSlot] ~= 0 then break end
			end

			curPos = 1
		else
			curPos = curPos + 1
		end

		selTime = RealTime()
		ply:EmitSound(sndMove)
		w_alpha = 1

		return true
	end

	if bind:sub(1, 4) == "slot" then
		local menu = HUD_AMXMENU
		if IsValid(menu) and menu:IsMenuOpen() then return end

		if wpnFading == true then wpnFading = false end
		local slot = tonumber(bind:sub(5))

		if slot == nil or slot <= 0 or slot > 6 then return end
		if not pressed then return true end

		ply:EmitSound(sndMove)

		if tCacheLength[slot] <= 0 then return true end

		if wepCount == 0 then
			ply:EmitSound(sndMove)
			return true
		end

		if slot <= 6 then
			if curSlot > 0 then ply:EmitSound(sndMove) end

			if slot == curSlot then
				if curPos == tCacheLength[curSlot] then
					curPos = 1
				else
					curPos = curPos + 1
				end
			elseif tCacheLength[slot] ~= 0 then
				curSlot = slot
				curPos = 1
			end

			selTime = RealTime()
			w_alpha = 1
		end

		return true
	end

	if curSlot ~= 0 then
		if bind == "+attack" then
			if wpnFading == true then return false end
			local wep = NULL
			if curPos > 0 then wep = tCache[curSlot][curPos] end

			curSlot = 0

			if IsValid(wep) and wep ~= ply:GetActiveWeapon() then
				input.SelectWeapon(wep)
			end

			selTime = RealTime()
			ply:EmitSound(sndSelect)

			return true
		end

		if bind == "+attack2" then
			selTime = RealTime()
			curSlot = 0

			return true
		end
	end
end)
