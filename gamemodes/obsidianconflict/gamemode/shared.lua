AddCSLuaFile()
DeriveGamemode"base"

if CLIENT then
	D_ER = 0
	D_HT = 1
	D_FR = 2
	D_LI = 3
	D_NU = 4
end

local mdlinspect = _G.mdlinspect
if not mdlinspect then
	-- pcall isnt sufficient enough to shut up not found
	if file.Exists("includes/modules/mdlinspect.lua", "LUA") then
		local ok = pcall(require, "mdlinspect")
		if not ok then
			local ok2 = pcall(include, "thirdparty/mdlinspect.lua")
			if not ok2 then
				error("cannot find mdlinspect, why")
			end
		end
	else
		local ok = pcall(include, "thirdparty/mdlinspect.lua")
		if not ok then
			error("cannot find mdlinspect, why")
		end
	end
end
if not FindMetaTable("File").ReadString then
	-- pcall isnt sufficient enough to shut up not found
	if file.Exists("includes/modules/fileextras.lua", "LUA") then
		local ok = pcall(require, "fileextras")
		if not ok then
			local ok2 = pcall(include, "thirdparty/fileextras.lua")
			if not ok2 then
				error("cannot find fileextras, why")
			end
		end
	else
		local ok = pcall(include, "thirdparty/fileextras.lua")
		if not ok then
			error("cannot find fileextras, why")
		end
	end
end

Obsidian = Obsidian or {}
function Obsidian.GetKeyValueFile(path)
	local t = util.KeyValuesToTablePreserveOrder(file.Read(path, "GAME"))
	--t = table.CollapseKeyValue(table input)
	return t
end

-- this kinda sucks oh well
local LOBBY_MAPS = {
	oc_lobby = true,
	oc_neolobby = true,
	oc_archive = true,
	keyfox_lobby_winter = true,
	["3dlobby"] = true,
	mapselect_v2 = true,
}
function Obsidian.IsLobbyMap()
	return LOBBY_MAPS[game.GetMap()] ~= nil
end

if SERVER then
	local known_replace = {}
	function Obsidian.CreateEntNoRespawn(class, bNoRemove)
		local _class = known_replace[class] or class

		local ent = ents.Create(_class)
		if not ent:IsValid() then
			_class = _class:gsub("\\", "/")
			ent = ents.Create(_class)
		end

		if not ent:IsValid() then
			ErrorNoHalt("unknown entity class: " .. class .. "!")
			return NULL
		end

		if class ~= _class then
			known_replace[class] = _class
		end
		ent.IsSpawnItem = true

		if bNoRemove == nil or bNoRemove == false then
			timer.Simple(5, function()
				if ent:IsValid() and not (ent:IsWeapon() and ent:GetOwner():IsValid()) then
					ent:Remove()
				end
			end)
		end

		return ent
	end

	-- gives weapon to plaeyr if they dont have item
	-- if they do have it, give the ammo instead :)
	-- returns the wep_class weapon given/already existing
	function Obsidian.GivePlayerWeapon(ply, wep_class, bNoAmmo)
		if not (IsValid(ply) and isentity(ply) and ply:IsPlayer()) then return NULL end

		local bHasWeapon = ply:HasWeapon(wep_class)
		local hWep = NULL

		if not bHasWeapon then
			hWep = ply:Give(wep_class, bNoAmmo)
			hWep.IsSpawnItem = true
			hWep.CreateForPlayer = ply

			timer.Simple(engine.TickInterval() * 2, function()
				if hWep:IsValid() and not (hWep:IsWeapon() and hWep:GetOwner():IsValid()) then
					hWep:Remove()
				end
			end)
		else
			hWep = ply:GetWeapon(wep_class)
			local iNumAmmoToGive = hWep:GetMaxClip1()

			ply:GiveAmmo(iNumAmmoToGive, hWep:GetPrimaryAmmoType())
		end

		return hWep
	end

	function Obsidian.StoreOutputSafe(ent, key, val)
		ent:StoreOutput(key, val:gsub("!Activator", "!activator"):gsub("!Self", "!self"):gsub("!Player", "!player"))
	end

	local tab_resource_add = {
		["resource/fonts/cs.ttf"] = true,
		["resource/fonts/custom.ttf"] = true,
		["resource/fonts/obsidian.ttf"] = true,
		["resource/fonts/obsidianconflictnewhud.ttf"] = true,
		["resource/fonts/obsidianweaps.ttf"] = true,

		["materials/oc_newhud/ammo.png"] = true,
		["materials/oc_newhud/health.png"] = true,
		["materials/oc_newhud/score.png"] = true,
		["materials/oc_newhud/timer.png"] = true,

		["materials/sprites/attention.vmt"] = true,
		["materials/sprites/attention_waypoint.vmt"] = true,
		["materials/sprites/cloak_icon.vmt"] = true,
		["materials/sprites/energy.vmt"] = true,
		["materials/sprites/health.vmt"] = true,
		["materials/sprites/hud_tab1.vmt"] = true,
		["materials/sprites/hud_tab2.vmt"] = true,
		["materials/sprites/hud_tab3.vmt"] = true,

		["materials/sprites/levelchange.vmt"] = true,

		["materials/sprites/medic.vmt"] = true,
		["materials/sprites/medic_waypont.vmt"] = true,

		["materials/sprites/merchant_buy.vmt"] = true,
		["materials/sprites/merchant_buy_waypoint.vmt"] = true,

		["materials/sprites/waypoint_ammo.vmt"] = true,
		["materials/sprites/waypoint_attack.vmt"] = true,
		["materials/sprites/waypoint_climb.vmt"] = true,
		["materials/sprites/waypoint_defend.vmt"] = true,
		["materials/sprites/waypoint_destroy.vmt"] = true,
		["materials/sprites/waypoint_move.vmt"] = true,
		["materials/sprites/waypoint_move2.vmt"] = true,
		["materials/sprites/waypoint_store.vmt"] = true,

		-- HUD ammo icons
		["materials/sprites/weapons/weapon_357.vmt"] = true,
		["materials/sprites/weapons/weapon_alyxgun.vmt"] = true,
		["materials/sprites/weapons/weapon_ar2.vmt"] = true,
		["materials/sprites/weapons/weapon_crossbow.vmt"] = true,
		["materials/sprites/weapons/weapon_frag.vmt"] = true,
		["materials/sprites/weapons/weapon_healer.vmt"] = true,
		["materials/sprites/weapons/weapon_manhack.vmt"] = true,
		["materials/sprites/weapons/weapon_pistol.vmt"] = true,
		["materials/sprites/weapons/weapon_rpg.vmt"] = true,
		["materials/sprites/weapons/weapon_shotgun.vmt"] = true,
		["materials/sprites/weapons/weapon_slam.vmt"] = true,
		["materials/sprites/weapons/weapon_smg1.vmt"] = true,
		["materials/sprites/weapons/weapon_sniperrifle.vmt"] = true,
		["materials/sprites/weapons/weapon_gauss.vmt"] = true,

		["materials/sprites/sniper_scope.vmt"] = true,

		-- OICW
		["models/weapons/v_oicw.mdl"] = true,
		["models/weapons/w_oicw.mdl"] = true,
		["materials/models/weapons/v_oicw/v_oicw_sheet.vmt"] = true,
		["materials/models/weapons/w_oicw/w_oicw.vmt"] = true,

		-- AlyxGun
		["models/weapons/c_alyxgun.mdl"] = true,
		["models/weapons/w_alyxgun.mdl"] = true,
		["materials/models/weapons/w_alyxgun/alyxgun.vmt"] = true,

		-- UZI
		["models/weapons/v_uzi.mdl"] = true,
		["models/weapons/w_uzi_r.mdl"] = true,
		["models/weapons/w_uzi_l.mdl"] = true,
		["materials/models/weapons/v_uzi/uzi.vmt"] = true,

		-- sniper rifle
		["models/weapons/v_sniper.mdl"] = true,
		["models/weapons/w_sniper.mdl"] = true,

		-- gauss gun/tau cannon
		["models/weapons/taummod/c_gauss.mdl"] = true,
		["materials/models/weapons/v_gauss/back.vmt"] = true,
		["materials/models/weapons/v_gauss/capacitor.vmt"] = true,
		["materials/models/weapons/v_gauss/coils.vmt"] = true,
		["materials/models/weapons/v_gauss/details1.vmt"] = true,
		["materials/models/weapons/v_gauss/fan.vmt"] = true,
		["materials/models/weapons/v_gauss/fanback.vmt"] = true,
		["materials/models/weapons/v_gauss/generator.vmt"] = true,
		["materials/models/weapons/v_gauss/glowchrome.vmt"] = true,
		["materials/models/weapons/v_gauss/glowtube.vmt"] = true,
		["materials/models/weapons/v_gauss/hand.vmt"] = true,
		["materials/models/weapons/v_gauss/spindle.vmt"] = true,
		["materials/models/weapons/v_gauss/stock.vmt"] = true,
		["materials/models/weapons/v_gauss/supportarm.vmt"] = true,
		["models/weapons/taummod/w_gauss.mdl"] = true,
	}

	for str in next, tab_resource_add do
		resource.AddFile(str)
	end
end

-- networking
OC_CMD_UPDATE_PLCOL = 0
OC_CMD_UPDATE_PLMODEL = 1
OC_CMD_UPDATE_WEPCOL = 2
OC_CMD_CHANGETEAM = 3
OC_CMD_TRANSFERPOINTS = 4
OC_CMD_TOGGLE_ITEM = 5
OC_CMD_PLAYERWAYPOINT = 6
OC_CMD_PING = 7
OC_CMD_DROPWEAPON = 8

GM.Name = "Obsidian Conflict"
GM.Author = "homonovus"
GM.TeamBased = true

CreateConVar("oc_infinite_aux_power", "0", bit.bor(FCVAR_NOTIFY, FCVAR_REPLICATED))
CreateConVar("oc_teamplay", "0", bit.bor(FCVAR_NOTIFY, FCVAR_REPLICATED))

CreateConVar("mp_bunnyhop", "2", bit.bor(FCVAR_NOTIFY, FCVAR_REPLICATED), "Bunnyhop mode. 0 = off, 1 = ep2 style (max speed enforced), 2 = unlimited hop speed.")
CreateConVar("mp_livesoverride", "0", bit.bor(FCVAR_NOTIFY, FCVAR_REPLICATED))
local mp_numlives = CreateConVar("mp_numlives", "10", bit.bor(FCVAR_NOTIFY, FCVAR_REPLICATED))
CreateConVar("mp_playercollide", "1", bit.bor(FCVAR_NOTIFY, FCVAR_REPLICATED), "Should players collide with each other?")
CreateConVar("mp_livesmode", "0", bit.bor(FCVAR_NOTIFY, FCVAR_REPLICATED))
CreateConVar("sv_maxinvobjectweight", "1", bit.bor(FCVAR_NOTIFY, FCVAR_REPLICATED), "Maximum allowed inventory object weight")

CreateConVar("sk_max_slam", "3", bit.bor(FCVAR_ARCHIVE, FCVAR_REPLICATED), "")

include"sh_sounds.lua"
include"sh_aux.lua"
include"sh_flashlight.lua"
include"sh_code_im_ashamed_of.lua"
include"sh_spectate.lua"
include"sh_inventory.lua"
include"player_class/player_oc.lua"
include"sh_compat.lua"

--stuff for physcannon2 from lambda gamemode
include"thirdparty/lambda_entity_extend.lua"
include"thirdparty/lambda_ents_extend.lua"
include"thirdparty/lambda_interpvalue.lua"
include"thirdparty/lambda_npc_extend.lua"
include"thirdparty/lambda_surfaceproperties.lua"
include"thirdparty/lambda_utils.lua"
if SERVER then
	AddCSLuaFile"thirdparty/lambda_entity_extend.lua"
	AddCSLuaFile"thirdparty/lambda_ents_extend.lua"
	AddCSLuaFile"thirdparty/lambda_interpvalue.lua"
	AddCSLuaFile"thirdparty/lambda_npc_extend.lua"
	AddCSLuaFile"thirdparty/lambda_surfaceproperties.lua"
	AddCSLuaFile"thirdparty/lambda_utils.lua"
end

local function REGISTER_WEP(name, data)
	local SWEP = {
		Primary = {},
		Secondary = {},
		ViewModelFlip = false
	}
	SWEP.Base = "oc_base"
	SWEP.ClassName = name
	SWEP.Spawnable = true

	SWEP.Primary.Ammo = tostring(data.primary_ammo)
	SWEP.Primary.DefaultClip = tonumber(data.default_clip)
	SWEP.Primary.ClipSize = tonumber(data.clip_size)

	SWEP.ViewModelFOV = 54

	SWEP.Secondary.Ammo = tostring(data.secondary_ammo)
	SWEP.Secondary.DefaultClip = tonumber(data.default_clip2)
	SWEP.Secondary.ClipSize = tonumber(data.clip2_size)

	SWEP.PrintName = tostring(data.printname)
	SWEP.ViewModel = data.viewmodel
	SWEP.WorldModel = data.playermodel
	SWEP.Weight = tonumber(data.weight)
	SWEP.Slot = tonumber(data.bucket)
	SWEP.SlotPos = tonumber(data.bucket_position)

	SWEP.IronSightOffset = data.ironsightoffset

	SWEP.SoundData = data.sounddata

	if tobool(data.csviewmodel) then
		SWEP.CSMuzzleFlashes = true

		local filename = string.StripExtension(string.GetFileFromFilename(data.viewmodel))
		local hopeful = string.format("models/weapons/cstrike/%s.mdl", filename:gsub("v_", "c_"))
		if util.IsValidModel(hopeful) then
			SWEP.ViewModel = hopeful
			SWEP.UseHands = true
		else
			SWEP.ViewModelFlip = not tobool(data.builtrighthanded)
			SWEP.ViewModelFOV = 70
		end

		SWEP.SoundData.reload = ""
	end

	-- got damn it oc_nerv
	if data.advanced then
		SWEP.Primary.FastFire = tobool(data.advanced.fastfire1)
		SWEP.Primary.Type = tonumber(data.advanced.firetype1)
		SWEP.Primary.Spread = tonumber(data.advanced.firecone1)
		SWEP.Primary.Delay = tonumber(data.advanced.firerate1)

		SWEP.Primary.FireUnderWater = tobool(data.advanced.fireunderwater1)

		SWEP.Primary.BurstAmount = tonumber(data.advanced.burstamount1)
		SWEP.Primary.BetweenBurstTime = tonumber(data.advanced.betweenbursttime1)

		SWEP.Primary.FireConeLerp = tobool(data.advanced.fireconelerp1)
		SWEP.Primary.FireConeLerpto = tonumber(data.advanced.fireconelerpto1)

		SWEP.Secondary.FastFire = tobool(data.advanced.fastfire2)
		SWEP.Secondary.Type = tonumber(data.advanced.firetype2)
		SWEP.Secondary.Delay = tonumber(data.advanced.firerate2) or 0
		SWEP.Secondary.UseAmmo = tobool(data.advanced.secondaryammoused)

		SWEP.Secondary.FireUnderWater = tobool(data.advanced.fireunderwater2)

		SWEP.Secondary.BurstAmount = tonumber(data.advanced.burstamount2)
		SWEP.Secondary.BetweenBurstTime = tonumber(data.advanced.betweenbursttime2)

		SWEP.AnimSet = tonumber(data.advanced.playeranimationset)

		SWEP.NumberOfRecoilAnims = tonumber(data.advanced.numberofrecoilanims)
		SWEP.RecoilIncrementSpeed = tonumber(data.advanced.recoilincrementspeed)

		SWEP.TracerType = tonumber(data.advanced.tracertype)
		SWEP.TracerFrequency = tonumber(data.advanced.tracerfrequency)

		SWEP.ImpactEffect = tonumber(data.advanced.impacteffect)

		SWEP.UseScopedFireCone = tobool(data.advanced.usescopedfirecone)
		SWEP.ScopedFireCone = tonumber(data.advanced.scopedfirecone)

		if data.advanced.scopedcolorr then
			SWEP.ScopeOverlayCol = Color(
				data.advanced.scopedcolorr,
				data.advanced.scopedcolorg,
				data.advanced.scopedcolorb
			)
		end

		SWEP.NPCBurstMin = tonumber(data.advanced.npcminbursts)
		SWEP.NPCBurstMax = tonumber(data.advanced.npcmaxbursts)
		SWEP.NPCBurstDelay = tonumber(data.advanced.npcrateoffire)

		SWEP.NPCRestMin = tonumber(data.advanced.npcminrest)
		SWEP.NPCRestMax = tonumber(data.advanced.npcmaxrest)
	else
		SWEP.Primary.FastFire = false
		SWEP.Primary.Type = 1
		SWEP.Primary.Spread = 3
		SWEP.Primary.Delay = .075

		SWEP.Primary.FireUnderWater = true

		SWEP.Secondary.FastFire = false
		SWEP.Secondary.Type = 0
		SWEP.Secondary.Delay = 0
		SWEP.Secondary.FireUnderWater = false
		SWEP.Secondary.BurstAmount = 0
		SWEP.Secondary.BetweenBurstTime = 0
	end

	if data.texturedata then
		SWEP.CharFont = data.texturedata.weapon.font
		SWEP.Char = data.texturedata.weapon.character

		SWEP.SelectedCharFont = data.texturedata.weapon_s.font
		SWEP.SelectedChar = data.texturedata.weapon_s.character
	end

	SWEP.Spawnable = true
	SWEP.Category = "Obsidian Conflict"

	weapons.Register(SWEP, SWEP.ClassName)
end

GM.__GsubSpellingMistakes = {
	["Granade"] = "Grenade",
	["granade"] = "grenade",
}

SKILL_EASY = 1
SKILL_MEDIUM = 2
SKILL_HARD = 3

local skill = GetConVar("skill")
function GM:GetSkillLevel()
	return skill:GetInt()
end

-- +custom per-map ammos
GM.MAX_AMMO_DEF = {
	["Pistol"] = sk_max_pistol,
	["SMG1"] = sk_max_smg1,
	["SMG1_Grenade"] = sk_max_smg1_grenade,
	["357"] = sk_max_357,
	["AR2"] = sk_max_ar2,
	["Buckshot"] = sk_max_buckshot,
	["AR2AltFire"] = sk_max_ar2_altfire,
	["XBowBolt"] = sk_max_crossbow,
	["Grenade"] = sk_max_grenade,
	["RPG_Round"] = sk_max_rpg_round,
	["slam"] = sk_max_slam,
	["AlyxGun"] = sk_max_alyxgun,
	["SniperRound"] = sk_max_sniper_round
}
GM.AMMO_LIKE_WEAPONS = {
	["weapon_frag"] = true,
	["weapon_slam"] = true,
}
GM.ITEM_DEF = {
	["item_ammo_pistol"] = {Amount = 20, Type = "Pistol", Max = sk_max_pistol},
	["item_ammo_pistol_large"] = {Amount = 100, Type = "Pistol", Max = sk_max_pistol},
	["item_ammo_smg1"] = {Amount = 45, Type = "SMG1", Max = sk_max_smg1},
	["item_ammo_smg1_large"] = {Amount = 225, Type = "SMG1", Max = sk_max_smg1},
	["item_ammo_smg1_grenade"] = {Amount = 1, Type = "SMG1_Grenade", Max = sk_max_smg1_grenade},
	["item_ammo_357"] = {Amount = 6, Type = "357", Max = sk_max_357},
	["item_ammo_357_large"] = {Amount = 20, Type = "357", Max = sk_max_357},
	["item_ammo_ar2"] = {Amount = 20, Type = "AR2", Max = sk_max_ar2},
	["item_ammo_ar2_large"] = {Amount = 100, Type = "AR2", Max = sk_max_ar2},
	["item_ammo_ar2_altfire"] = {Amount = 1, Type = "AR2AltFire", Max = sk_max_ar2_altfire},
	["item_box_buckshot"] = {Amount = 10, Type = "Buckshot", Max = sk_max_buckshot},
	["item_ammo_crossbow"] = {Amount = 6, Type = "XBowBolt", Max = sk_max_crossbow},
	["item_rpg_round"] = {Amount = 1, Type = "RPG_Round", Max = sk_max_rpg_round},
	["item_box_alyxrounds"] = {Amount = 15, Type = "AlyxGun", Max = sk_max_alyxgun},
	["weapon_frag"] = {Amount = 1, Type = "Grenade", Max = sk_max_grenade},
	["weapon_slam"] = {Amount = 1, Type = "slam", Max = sk_max_slam},
	["item_box_sniper_rounds"] = {Amount = 10, Type = "SniperRound", Max = sk_max_sniper_round}
}

local sk_ammo_qty_scale1 = GetConVar("sk_ammo_qty_scale1")
local sk_ammo_qty_scale2 = GetConVar("sk_ammo_qty_scale2")
local sk_ammo_qty_scale3 = GetConVar("sk_ammo_qty_scale3")
function GM:GetAmmoQuanityScale()
	local iSkill = self:GetSkillLevel()

	if iSkill == SKILL_EASY then
		return sk_ammo_qty_scale1:GetFloat()
	elseif iSkill == SKILL_MEDIUM then
		return sk_ammo_qty_scale2:GetFloat()
	elseif iSkill == SKILL_HARD then
		return sk_ammo_qty_scale3:GetFloat()
	end

	return 1
end

function GM:RegisterOCAmmo(modify_file)
	local data = modify_file or self.MapAddScript or Obsidian.GetKeyValueFile(string.format("maps/cfg/%s_modify.txt", game.GetMap()))
	data = table.CollapseKeyValue(data)
	data = data.CustomAmmo or data.customammo

	if data then
		for name, t in next, data do
			local a = {
				name = name,
				plydmg = t.plrdmg,
				npcdmg = t.npcdmg,
				force = t.ftpersec,
				dmgtype = t.dmgtype,
				maxcarry = t.maxcarry,
				tracer = t.tracer
			}

			local maxCarry = t.maxcarry
			self.MAX_AMMO_DEF[name] = {GetInt = function() return maxCarry end}

			game.AddAmmoType(a)
			print("[OC] registered custom ammo type", a.name)
		end
	end
end

function GM:RegisterOCWeapons()
	-- if the server sends the file, it passed all checks; we know it can keyvalue it correctly
	if file.Exists(string.format("maps/cfg/%s_modify.txt", game.GetMap()), "GAME") then
		local data = self.MapAddScript or Obsidian.GetKeyValueFile(string.format("maps/cfg/%s_modify.txt", game.GetMap()))

		-- attempt to register ammo
		self:RegisterOCAmmo(data)
	end

	-- recursively register weapons
	local dir_base = "scripts/customweapons/"
	local function go_thru_dir(dir)
		print("[OC] registering weapon dir: ", dir_base .. dir)
		local files, dirs = file.Find(dir_base .. dir .. "/*", "GAME")

		for _, filename in ipairs(files) do
			if filename == "readme.txt" or filename == "bucket positions.xls" then continue end

			if SERVER then
				--resource.AddSingleFile(dir_base .. (dir ~= "" and dir .. "/" or "") .. v)
			end

			local data = util.KeyValuesToTable(file.Read(dir_base .. dir .. "/" .. filename, "GAME"), true, false)
			local name = (dir ~= "" and (dir .. "/") or "") .. string.StripExtension(filename)
			local ok, why = pcall(REGISTER_WEP, name, data)
			if not ok then
				print(string.format("[OC] failed to register weapon script %s; %s", name, why))
			else
				print("[OC] registered custom weapon script", name)
			end
		end

		for _, subdir in ipairs(dirs) do
			go_thru_dir(dir .. subdir)
		end
	end

	go_thru_dir("")
end

function GM:RegisterOCSoundScripts()
	if not self.MapSoundScripts and file.Exists(string.format("maps/cfg/%s_soundscripts.txt", game.GetMap()), "GAME") then
		self.MapSoundScripts = util.KeyValuesToTable("\"Sounds\" {" .. file.Read(string.format("maps/cfg/%s_soundscripts.txt", game.GetMap()), "GAME") .. "}", false, true)
	end

	--print("[OC] self:RegisterOCSoundScripts()")
	--PrintTable(self.MapSoundScripts)
	if self.MapSoundScripts then
		self:BulkRegisterSoundScripts(self.MapSoundScripts)
	end
end

function GM:OnReloaded()
	self:RegisterOCWeapons()
	self:RegisterOCSoundScripts()

	local ammo_slam = game.GetAmmoData(game.GetAmmoID("slam"))
	ammo_slam.maxcarry = "sk_max_slam"
	game.AddAmmoType(ammo_slam)

	if CLIENT then
		self:HUDInit()
	end
end

function GM:CreateTeams()
	TEAM_COOP = 1
	TEAM_BLUE = 2
	TEAM_RED = 3

	team.SetUp(TEAM_COOP, "Player", Color(255, 180, 0), false)
	team.SetUp(TEAM_BLUE, "Team Blue", Color(154, 205, 255), false)
	team.SetUp(TEAM_RED, "Team Red", Color(255, 62, 62), false)
	team.SetUp(TEAM_SPECTATOR, "Spectator", Color(192, 192, 192), false)

	team.SetSpawnPoint(TEAM_RED, {
		"info_player_red"
	})
	team.SetSpawnPoint(TEAM_BLUE, {
		"info_player_blue"
	})
	team.SetSpawnPoint(TEAM_COOP, {
		"info_player_deathmatch"
	})
end

function GM:PlayerNoclip(ply, on)
	if not on or game.SinglePlayer() then return true end

	return ply:Alive() and ply:IsAdmin()
end

function GM:Tick()
	if SERVER then
		self:UpdateItemRespawn()

		--[[for ply, light in next, self.Flashlights do
		if not IsValid(ply) then
			SafeRemoveEntity(light)
			self.Flashlights[ply] = nil
		end
	end]]
	end
end

local PLY = FindMetaTable"Player"
PLY._Frags = PLY._Frags or PLY.Frags
function PLY:Frags()
	return self:GetNW2Int("oc_frags", 0)
end

function PLY:Lives()
	return self:GetNW2Int("oc_lives", mp_numlives:GetInt())
end

-- FIXME: if gmod adds ply:SetSlowWalkSpeed() to player_manager.OnPlayerSpawn
--[[player_manager._OnPlayerSpawn = player_manager._OnPlayerSpawn or player_manager.OnPlayerSpawn
local _, LookupPlayerClass = debug.getupvalue(player_manager._OnPlayerSpawn, 1)
function player_manager.OnPlayerSpawn(ply, transition)
	local class = LookupPlayerClass(ply)
	if not class then return end

	player_manager._OnPlayerSpawn(ply, transition)
	ply:SetSlowWalkSpeed(class.SlowWalkSpeed)
end--]]

function GM:EntityFireBullets(ent, bullet)
	local oldCallback = bullet.Callback

	hook.Run("FireBulletsModifyBullet", ent, bullet)

	bullet.Callback = function(atk, tr, dmg, ...)
		local ret
		if isfunction(oldCallback) then
			ret = oldCallback(atk, tr, dmg, ...)
		end

		hook.Run("FireBulletsCallback", ent, bullet, tr, dmg)

		return ret
	end

	return true
end
