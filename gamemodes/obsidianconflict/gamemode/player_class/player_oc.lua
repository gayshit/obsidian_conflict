AddCSLuaFile()

if CLIENT then
	CreateConVar("cl_playercolor", "0.24 0.34 0.41", {FCVAR_ARCHIVE, FCVAR_USERINFO, FCVAR_DONTRECORD}, "The value is a Vector - so between 0-1 - not between 0-255")
	CreateConVar("cl_weaponcolor", "0.30 1.80 2.10", {FCVAR_ARCHIVE, FCVAR_USERINFO, FCVAR_DONTRECORD}, "The value is a Vector - so between 0-1 - not between 0-255")
	CreateConVar("cl_playerskin", "0", {FCVAR_ARCHIVE, FCVAR_USERINFO, FCVAR_DONTRECORD}, "The skin to use, if the model has any")
	CreateConVar("cl_playerbodygroups", "0", {FCVAR_ARCHIVE, FCVAR_USERINFO, FCVAR_DONTRECORD}, "The bodygroups to use, if the model has any")
end

local PLAYER = {}

PLAYER.DisplayName = "OC Player"

PLAYER.WalkSpeed = 250           -- How fast to move when not running
PLAYER.RunSpeed = 320            -- How fast to move when running
PLAYER.SlowWalkSpeed = 150       -- How fast to move when slow walking
PLAYER.CrouchedWalkSpeed = 1 / 3 -- Multiply move speed by this when crouching
PLAYER.DuckSpeed = 1 / 3         -- How fast to go from not ducking, to ducking
PLAYER.UnDuckSpeed = 1 / 3       -- How fast to go from ducking, to not ducking
PLAYER.JumpPower = 150           -- How powerful our jump should be
PLAYER.CanUseFlashlight = true   -- Can we use the flashlight
PLAYER.MaxHealth = 100           -- Max health we can have
PLAYER.MaxArmor = 100            -- Max armor we can have
PLAYER.StartHealth = 100         -- How much health we start with
PLAYER.StartArmor = 0            -- How much armour we start with
PLAYER.DropWeaponOnDie = false   -- Do we drop our weapon when we die
PLAYER.TeammateNoCollide = true  -- Do we collide with teammates or run straight through them
PLAYER.AvoidPlayers = false      -- Automatically swerves around other players
PLAYER.UseVMHands = true         -- Uses viewmodel hands

-- we check if these items are active in the AUX bits
function PLAYER:SetupDataTables()
	self.Player:NetworkVar("Bool", 0, "HasShield")
	self.Player:NetworkVar("Bool", 1, "HasCloak")
end

-- SERVER calls it automatically
-- CLIENT calls it from GM:InitPostEntity()
function PLAYER:Init()
	self.Player:SetHullDuck(Vector(-16, -16, 0), Vector(16, 16, 32))
end

function PLAYER:Spawn()
	local ply = self.Player
	ply:ResetHull()
	ply:SetHullDuck(Vector(-16, -16, 0), Vector(16, 16, 32))

	local plcol = ply:GetInfo("cl_playercolor")
	ply:SetPlayerColor(Vector(plcol))

	local wcol = Vector(ply:GetInfo("cl_weaponcolor"))
	if wcol:Length() == 0 then
		wcol = Vector(0.001, 0.001, 0.001)
	end
	ply:SetWeaponColor(wcol)
end

function PLAYER:Loadout() end

function PLAYER:Death() end

function PLAYER:SetModel()
	local ply = self.Player

	local cl_playermodel = ply:GetInfo("cl_playermodel")
	local modelname = player_manager.TranslatePlayerModel(cl_playermodel)
	util.PrecacheModel(modelname)
	ply:SetModel(modelname)

	local skin = ply:GetInfoNum("cl_playerskin", 0)
	ply:SetSkin(skin)

	local groups = ply:GetInfo("cl_playerbodygroups")
	if (groups == nil) then groups = "" end
	groups = string.Explode(" ", groups)

	for k = 0, ply:GetNumBodyGroups() - 1 do
		ply:SetBodygroup(k, tonumber(groups[k + 1]) or 0)
	end
end

-- Clientside only
function PLAYER:CalcView(view) end -- Setup the player's view

-- Creates the user command on the client
function PLAYER:CreateMove(cmd)
	AUX.SuitPowerUpdate(self.Player, nil, cmd)
end

function PLAYER:ShouldDrawLocal() end -- Return true if we should draw the local player

PLAYER.m_bIsJumping = false
function PLAYER:StartMove(move, cmd)
	if bit.band(move:GetButtons(), IN_JUMP) ~= 0 and bit.band(move:GetOldButtons(), IN_JUMP) == 0 and self.Player:OnGround() then
		self.m_bIsJumping = true
	end
end

function PLAYER:IsSprinting()
	local ply = self.Player
	if not IsValid(ply) then return false end

	local flWalkSpeed = ply:GetWalkSpeed()
	local MoveType = ply:GetMoveType()
	local bCanSprint = (MoveType ~= MOVETYPE_WALK or (ply:WaterLevel() > 1 or ply:OnGround())) and not ply:Crouching() and ply:GetVelocity():LengthSqr() > flWalkSpeed * flWalkSpeed and ply:GetNW2Float("SuitPower") > 0
	return bCanSprint and ply:KeyDown(IN_SPEED)
end

-- Runs the move (can run multiple times for the same client)
function PLAYER:Move(mv)
	local ply = self.Player
	local plyTable = ply:GetTable()

	local bIsSprinting = self:IsSprinting()

	if bIsSprinting then
		if not plyTable.m_bWasSprinting and CLIENT and IsFirstTimePredicted() then
			ply:EmitSound("HL2Player.SprintStart")
		end

		AUX.SuitPowerAddDevice(ply, AUX.Devices.SuitDeviceSprint)
	else
		AUX.SuitPowerRemoveDevice(ply, AUX.Devices.SuitDeviceSprint)
	end

	plyTable.m_bWasSprinting = bIsSprinting

	if ply:FlashlightIsOn() then
		AUX.SuitPowerAddDevice(ply, AUX.Devices.SuitDeviceFlashlight)
	else
		AUX.SuitPowerRemoveDevice(ply, AUX.Devices.SuitDeviceFlashlight)
	end
	if ply:WaterLevel() == 3 then
		AUX.SuitPowerAddDevice(ply, AUX.Devices.SuitDeviceBreather)
	else
		AUX.SuitPowerRemoveDevice(ply, AUX.Devices.SuitDeviceBreather)
	end

	if ply:GetNW2Float("SuitPower") <= 0 then
		-- engine says player is sprinting, slow them down
		if ply:IsSprinting() then
			local flWalkSpeed = ply:GetWalkSpeed()
			mv:SetMaxSpeed(flWalkSpeed)
			mv:SetMaxClientSpeed(flWalkSpeed)
		end

		AUX.SuitPowerRemoveDevice(ply, AUX.Devices.SuitDeviceShield)
		AUX.SuitPowerRemoveDevice(ply, AUX.Devices.SuitDeviceCloak)
	end

	AUX.SuitPowerUpdate(ply, mv)
end

-- Copy the results of the move back to the Player
function PLAYER:FinishMove(move)
	if self.m_bIsJumping then
		-- Compute the speed boost

		local velDirXY = move:GetVelocity():GetNormalized()
		velDirXY.z = 0

		-- HL2 normally provides a much weaker jump boost when sprinting
		local speedBoostPerc = ((not self.Player:Crouching()) and 0.045) or 0.015

		local speedAddition = math.abs(move:GetVelocity():Length() * speedBoostPerc)

		-- Clamp it to make sure they can't bunnyhop to ludicrous speed
		--[[if newSpeed > maxSpeed then
            speedAddition = speedAddition - (newSpeed - maxSpeed)
        end]]

		-- Apply the speed boost
		move:SetVelocity(velDirXY * speedAddition + move:GetVelocity())
	end

	self.m_bIsJumping = false
end

function PLAYER:ViewModelChanged(vm, old, new) end

function PLAYER:PreDrawViewModel(vm, weapon) end

function PLAYER:PostDrawViewModel(vm, weapon) end

function PLAYER:GetHandsModel()
	local playermodel = player_manager.TranslateToPlayerModelName(self.Player:GetModel())
	return player_manager.TranslatePlayerHands(playermodel)
end

player_manager.RegisterClass("player_oc", PLAYER, nil)
