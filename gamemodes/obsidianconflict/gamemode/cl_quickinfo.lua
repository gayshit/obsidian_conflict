local draw = draw

local surface = surface
local render = render
local math = math
local me

local qi_ammoFade = 0
local qi_healthFade = 0
local qi_lastAmmo = 0
local qi_lastHealth = 100
local qi_warnAmmo = false
local qi_warnHealth = false
local qi_fadedOut = false
local qi_dimmed = false
local qi_lastEventTime = 0
local qi_alpha = 255

local QUICKINFO_EVENT_DURATION = 1.0
local QUICKINFO_BRIGHTNESS_FULL = 255
local QUICKINFO_BRIGHTNESS_DIM = 64
local QUICKINFO_FADE_IN_TIME = 0.5
local QUICKINFO_FADE_OUT_TIME = 2

local function GetTextSize(fnt, str)
	fnt = fnt or "DermaDefault"
	str = str or ""

	surface.SetFont(fnt)
	local w, h = surface.GetTextSize(str)

	return w, h
end

local function DrawWarning(x, y, char, time)
	local scale = math.abs(math.sin(SysTime() * 8)) * 128

	if time <= (RealFrameTime() * 200) then
		if scale < 40 then
			time = 0
			return time
		else
			time = time + (RealFrameTime() * 200)
		end
	end

	time = time - (RealFrameTime() * 200)
	local caution = Color(255, 0, 0)
	caution.a = scale

	draw.DrawText(char, "QuickInfoBars", x, y, caution)

	return time
end

local function DrawIconProgressBar(x, y, char, char2, percentage, color)
	percentage = math.min(1, percentage)
	percentage = math.max(0, percentage)

	local w, h = GetTextSize("QuickInfoBars", char)
	h = h + 1

	local barOfs = h * percentage

	render.SetScissorRect(x, y, x + w, y + barOfs, true)
	draw.DrawText(char2, "QuickInfoBars", x, y, color)
	render.SetScissorRect(0, 0, 0, 0, false)

	render.SetScissorRect(x, y + barOfs, x + w, y + h, true)
	draw.DrawText(char, "QuickInfoBars", x, y, color)
	render.SetScissorRect(0, 0, 0, 0, false)
end

function GM:DrawQuickinfo(x, y)
	if not IsValid(me) then
		me = LocalPlayer()
		return
	end
	local wep = me:GetActiveWeapon()
	if not IsValid(me) or not IsValid(wep) then return end
	if not me:Alive() then return end

	local fadeOut = me:KeyDown(IN_ZOOM)

	if qi_fadedOut ~= fadeOut then
		qi_dimmed = false

		if fadeOut then
			qi_alpha = Lerp(RealFrameTime() / 0.25, qi_alpha, 0)
			if math.floor(qi_alpha + 0.5) == 0 then
				qi_fadedOut = fadeOut
			end
		else
			qi_alpha = Lerp(RealFrameTime() / QUICKINFO_FADE_IN_TIME, qi_alpha, QUICKINFO_BRIGHTNESS_FULL)
			if math.floor(qi_alpha + 0.5) == QUICKINFO_BRIGHTNESS_FULL then
				qi_fadedOut = fadeOut
			end
		end
	elseif not qi_fadedOut then
		if (SysTime() - qi_lastEventTime) > QUICKINFO_EVENT_DURATION then
			if not qi_dimmed then
				qi_alpha = Lerp(RealFrameTime() / QUICKINFO_FADE_OUT_TIME, qi_alpha, QUICKINFO_BRIGHTNESS_DIM)
				if math.floor(qi_alpha + 0.5) == QUICKINFO_BRIGHTNESS_DIM then
					qi_dimmed = true
				end
			end
		elseif qi_dimmed then
			qi_alpha = Lerp(RealFrameTime() / QUICKINFO_FADE_IN_TIME, qi_alpha, QUICKINFO_BRIGHTNESS_FULL)
			if math.floor(qi_alpha + 0.5) == QUICKINFO_BRIGHTNESS_FULL then
				qi_dimmed = false
			end
		end
	end

	local scalar = 138 / 255

	surface.SetAlphaMultiplier(qi_alpha / 255)

	local health = me:Health()
	if health ~= qi_lastHealth then
		qi_alpha = 255
		qi_lastEventTime = SysTime()
		qi_lastHealth = health

		local healthPerc = health / me:GetMaxHealth()

		if healthPerc <= 0.25 then
			if qi_warnHealth == false then
				qi_healthFade = 255
				qi_warnHealth = true

				surface.PlaySound("common/warning.wav")
			end
		else
			qi_warnHealth = false
		end
	end

	local ammo = wep:Clip1()
	if ammo ~= qi_lastAmmo then
		qi_alpha = 255
		qi_lastEventTime = SysTime()
		qi_lastAmmo = ammo

		local ammoPerc = ammo / wep:GetMaxClip1()

		if wep:GetMaxClip1() > 1 and ammoPerc <= 0.25 then
			if qi_warnAmmo == false then
				qi_ammoFade = 255
				qi_warnAmmo = true

				surface.PlaySound("common/warning.wav")
			end
		else
			qi_warnAmmo = false
		end
	end

	local sinScale = math.abs(math.sin(SysTime() * 8)) * 128

	if qi_healthFade > 0 then
		local w, h = GetTextSize("QuickInfoBars", "[")
		qi_healthFade = DrawWarning(x - (w * 2), y - h / 2, "[", qi_healthFade)
	else
		local healthPerc = math.Clamp(health / me:GetMaxHealth(), 0, 1)
		local healthColor = qi_warnHealth and Color(255, 0, 0) or table.Copy(self:GetHUDTextColor())

		if qi_warnHealth then
			healthColor.a = sinScale
		else
			healthColor.a = scalar * 255
		end

		local w, h = GetTextSize("QuickInfoBars", "[")
		DrawIconProgressBar(x - (w * 2), y - h / 2, "[", "{", 1 - healthPerc, healthColor)
	end

	if qi_ammoFade > 0 then
		local w, h = GetTextSize("QuickInfoBars", "]")
		qi_ammoFade = DrawWarning(x + w, y - h / 2, "]", qi_ammoFade)
	else
		local ammoPerc

		if wep:GetMaxClip1() <= 0 then
			ammoPerc = 0
		else
			ammoPerc = math.Clamp(1 - (ammo / wep:GetMaxClip1()), 0, 1)
		end

		local ammoColor = qi_warnAmmo and Color(255, 0, 0) or table.Copy(self:GetHUDTextColor())

		if qi_warnAmmo then
			ammoColor.a = sinScale
		else
			ammoColor.a = scalar * 255
		end

		local w, h = GetTextSize("QuickInfoBars", "]")
		DrawIconProgressBar(x + w, y - h / 2, "]", "}", ammoPerc, ammoColor)
	end

	surface.SetAlphaMultiplier(1)
end
