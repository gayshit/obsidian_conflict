AddCSLuaFile()

if SERVER then
	do
		local hooks = hook.GetTable()
		if hooks.PlayerSpawn["DynamicCamera:PlayerSpawn"] then
			GM.__DynamicCameraFixed = GAMEMODE and GAMEMODE.__DynamicCameraFixed or false

			if not GM.__DynamicCameraFixed then
				local oldhook = hooks.PlayerSpawn["DynamicCamera:PlayerSpawn"]
				hook.Add("PlayerSpawn", "DynamicCamera:PlayerSpawn", function(ply)
					timer.Simple(0, function()
						oldhook(ply)
					end)
				end)

				GM.__DynamicCameraFixed = true
			end
		end
	end
elseif CLIENT then
	if outfitter then
		local PLAYER = FindMetaTable("Player")

		PLAYER.oldResetHull = PLAYER.oldResetHull or PLAYER.ResetHull
		function PLAYER:ResetHull(...)
			local who = debug.getinfo(2, "S")
			if who.short_src == "lua/outfitter/cl_util.lua" then return end

			return self:oldResetHull(...)
		end
	end
end
