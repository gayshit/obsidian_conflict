AddCSLuaFile()
local capsUsable = bit.bor(0x00000010, 0x00000020, 0x00000040, 0x00000080)
local function IsUseableEntity(ply, entity, requiredCaps)
	if CLIENT then return false end
	if isentity(entity) and IsValid(entity) then
		local caps = entity:ObjectCaps()
		if bit.band(caps, capsUsable) ~= 0 and bit.band(caps, requiredCaps) == requiredCaps then
			return true
		end
	end

	return false
end

local sv_debug_player_use = GetConVar"sv_debug_player_use"

local PLAYER_USE_RADIUS = 80
local useableContents = bit.bor(MASK_SOLID, CONTENTS_DEBRIS, CONTENTS_PLAYERCLIP)
local NUM_TANGENTS = 8
local tangents = {
	0,
	1,
	0.57735026919,
	0.3639702342, 0.267949192431,
	0.1763269807,
	-0.1763269807,
	-0.267949192431
}
local FLT_MAX = 3.40282347E+38

local function IntervalDistance(x, x0, x1)
	-- swap so x0 < x1
	if (x0 > x1) then
		local tmp = x0
		x0 = x1
		x1 = tmp
	end

	if (x < x0) then
		return x0 - x
	elseif (x > x1) then
		return x - x1
	end

	return 0
end

local function CalcClosestPointToLineT(P, vLineA, vLineB, vDir)
	vDir:Set(vLineB - vLineA)

	local div = vDir:Dot(vDir)
	if div < 0.00001 then
		return 0
	else
		return (vDir:Dot(P) - vDir:Dot(vLineA)) / div
	end
end

local function Vector__MulAdd(self, a, b, scalar)
	self.x = a.x + b.x * scalar
	self.y = a.y + b.y * scalar
	self.z = a.z + b.z * scalar
end

local function CalcClosestPointOnLine(P, vLineA, vLineB, vClosest)
	local vDir = Vector()
	local t = CalcClosestPointToLineT(P, vLineA, vLineB, vDir)
	Vector__MulAdd(vClosest, vLineA, vDir, t)
	return t
end

local function CalcDistanceToLine(P, vLineA, vLineB)
	local vClosest = Vector()
	local t = CalcClosestPointOnLine(P, vLineA, vLineB, vClosest)
	return P:Distance(vClosest), t
end

function GM:PlayerUseTrace(ply)
	local forward, up = ply:GetAimVector(), ply:GetAimVector():Angle():Up()

	local tr
	local searchCenter = ply:EyePos()

	local pFoundByTrace = NULL
	local pObject = NULL
	local pNearest = NULL
	local nearestDist = FLT_MAX


	for i = 0, NUM_TANGENTS do
		if i == 0 then
			tr = util.TraceLine{
				start = searchCenter,
				endpos = searchCenter + forward * 1024,
				mask = useableContents,
				filter = ply,
				collisiongroup = COLLISION_GROUP_NONE
			}
		else
			local down = forward - tangents[i] * up
			down:Normalize()
			tr = util.TraceHull{
				start = searchCenter,
				endpos = searchCenter + down * 72,
				mins = -Vector(16, 16, 16),
				maxs = Vector(16, 16, 16),
				mask = useableContents,
				filter = ply,
				collisiongroup = COLLISION_GROUP_NONE
			}
		end
		pObject = tr.Entity

		if SERVER then
			pFoundByTrace = pObject
		end

		local bUsable = IsUseableEntity(ply, pObject, 0)
		while IsValid(pObject) and not bUsable and IsValid(pObject:GetMoveParent()) do
			pObject = pObject:GetMoveParent()
			bUsable = IsUseableEntity(ply, pObject, 0)
		end

		--print(dist, PLAYER_USE_RADIUS)
		if bUsable then
			local delta = tr.HitPos - tr.StartPos
			local centerZ = ply:WorldSpaceCenter().z
			delta.z = IntervalDistance(tr.HitPos.z, centerZ + ply:OBBMins().z, centerZ + ply:OBBMaxs().z)
			local dist = delta:Length()
			if dist < PLAYER_USE_RADIUS then
				if SERVER and sv_debug_player_use:GetBool() then
					debugoverlay.Line(searchCenter, tr.HitPos, 30, Color(0, 255, 0, 255), true)
				end

				if sv_debug_player_use:GetBool() then
					Msg(string.format("Trace using: %s\n", IsValid(pObject) and pObject:GetClass() or "no usable entity found"))
				end

				pNearest = pObject

				if i == 0 then return pObject end
			end
		end
	end

	if IsValid(ply:GetGroundEntity()) and IsUseableEntity(ply:GetGroundEntity(), 0x00000100) then
		pNearest = ply:GetGroundEntity()
	end

	if IsValid(pNearest) then
		local point = ply:NearestPoint(searchCenter)
		nearestDist = CalcDistanceToLine(point, searchCenter, forward)
		if sv_debug_player_use:GetBool() then
			Msg(string.format("Trace found %s, dist %.2f\n", pNearest:GetClass(), nearestDist))
		end
	end

	for _, e in ipairs(ents.FindInSphere(searchCenter, PLAYER_USE_RADIUS)) do
		if not IsValid(e) then continue end
		if not IsUseableEntity(ply, e, 0x00000200) then continue end

		local point = e:NearestPoint(searchCenter)
		local dir = point - searchCenter
		dir:Normalize()
		local dot = dir:Dot(forward)

		if dot < .8 then continue end

		local dist = CalcDistanceToLine(point, searchCenter, forward)

		if sv_debug_player_use:GetBool() then
			Msg(string.format("Radius found %s, dist %.2f\n", e:GetClass(), dist))
		end

		if dist < nearestDist then
			local trOccluded = util.TraceLine{
				start = searchCenter,
				endpos = point,
				mask = useableContents,
				filter = ply,
				collisiongroup = COLLISION_GROUP_NONE
			}

			if trOccluded.Fraction == 1 or trOccluded.Entity == pObject then
				pNearest = pObject
				nearestDist = dist
			end
		end
	end

	if SERVER and sv_debug_player_use:GetBool() then
		if not IsValid(pNearest) then
			debugoverlay.Line(searchCenter, tr.HitPos, 30, Color(255, 0, 0), true)
			debugoverlay.Cross(tr.HitPos, 16, 30, Color(255, 0, 0), true)
		elseif pNearest == pFoundByTrace then
			debugoverlay.Line(searchCenter, tr.HitPos, 30, Color(0, 255, 0), true)
			debugoverlay.Cross(tr.HitPos, 16, 30, Color(0, 255, 0), true)
		else
			debugoverlay.Box(pNearest:WorldSpaceCenter(), -Vector(8, 8, 8), Vector(8, 8, 8), 30, Color(0, 255, 0), true)
		end
	end

	if sv_debug_player_use:GetBool() then
		Msg(string.format("Radial using: %s\n", IsValid(pNearest) and pNearest:GetClass() or "no usable entity found"))
	end

	return pNearest
end
