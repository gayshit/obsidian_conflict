local util = util
local draw = draw
local surface = surface
local hook = hook
local me
local TAG_TIMER = "game_countdown_timer"
local TAG_TEXT = "game_text_quick"
local TAG_WAYPOINT = "info_waypoint"
local TAG_MSGMP = "point_message_multiplayer"
local mp_teamplay = GetConVar("oc_teamplay")

GM.CurResW, GM.CurResH = 0, 0

local ScreenScaleH = ScreenScaleH
if not ScreenScaleH then
	ScreenScaleH = function(n)
		return n * (ScrH() / 480)
	end
end

local function StrToCol(str)
	local col = Color(255, 255, 255)

	local r, g, b = str:match("(%d+) (%d+) (%d+)")

	col.r = tonumber(r) or 255
	col.g = tonumber(g) or 255
	col.b = tonumber(b) or 255

	return col
end

local oc_hud_text = CreateClientConVar("oc_hud_text", "0 128 255")
local oc_hud_bg = CreateClientConVar("oc_hud_bg", "0 0 0")
local oc_hud_mode = CreateClientConVar("oc_hud_mode", "0")
local oc_quickinfo = CreateClientConVar("oc_quickinfo", "1", true)
local oc_targetid_usepercent = CreateClientConVar("oc_targetid_usepercent", "1")

GM.HUD_COL_TEXT = StrToCol(oc_hud_text:GetString())
GM.HUD_COL_BG = StrToCol(oc_hud_bg:GetString())

function GM:GetHUDBGColor()
	return self.HUD_COL_BG
end

function GM:GetHUDTextColor()
	return self.HUD_COL_TEXT
end

GM.HUDTexts = GM.HUDTexts or {}
GM.HUDWaypoints = GM.HUDWaypoints or {}
GM.HUDQuickTexts = GM.HUDQuickTexts or {}

local MSGMP_FULLUPDATE = 0
local MSGMP_SETMSG = 1
local MSGMP_TOGGLE = 2
local MSGMP_REMOVE = 3
net.Receive(TAG_MSGMP, function()
	local ent_index = net.ReadUInt(16)
	local type = net.ReadUInt(4)

	GAMEMODE.HUDTexts[ent_index] = GAMEMODE.HUDTexts[ent_index] or {
		p_idx = -1,
		col = Color(255, 255, 255),
		radius = 64
	}

	if type == MSGMP_FULLUPDATE then
		local enabled = net.ReadBool()
		local radius = net.ReadFloat()
		local text = net.ReadString()
		local pos = net.ReadVector()
		local col = net.ReadColor()
		local p_idx = net.ReadUInt(16)

		GAMEMODE.HUDTexts[ent_index] = {
			enabled = enabled,
			radius = radius,
			text = text,
			pos = pos,
			col = col,
			p_idx = p_idx
		}
	elseif type == MSGMP_SETMSG then
		GAMEMODE.HUDTexts[ent_index].text = net.ReadString()
		--GAMEMODE.HUDTexts[ent_index].enabled = true
	elseif type == MSGMP_TOGGLE then
		GAMEMODE.HUDTexts[ent_index].enabled = net.ReadBool()
	elseif type == MSGMP_REMOVE then
		GAMEMODE.HUDTexts[ent_index] = nil
	else
		print(TAG_MSGMP, "unhandled type", type)
	end
end)

net.Receive(TAG_WAYPOINT, function()
	local type = net.ReadUInt(8)
	local index = net.ReadUInt(16)
	local image = net.ReadString()
	local text = net.ReadString()
	local parent = net.ReadEntity()
	local pos = net.ReadVector()

	if type == 0 then -- permanent on
		local on = net.ReadBool()
		GAMEMODE.HUDWaypoints[index] = {
			image = Material(image),
			text = text,
			enabled = on,
			parent = parent:IsValid() and parent or nil,
			pos = pos
		}
	elseif type == 1 then -- time based
		local time = net.ReadFloat()
		GAMEMODE.HUDWaypoints[index] = {
			image = Material(image),
			text = text,
			endtime = CurTime() + time,
			parent = parent:IsValid() and parent or nil,
			pos = pos
		}
	elseif type == 2 then -- kill
		if GAMEMODE.HUDWaypoints[index] then
			GAMEMODE.HUDWaypoints[index] = nil
		end
	end
end)

local function keep_inside_circle(x, y, r)
	local A = {
		x = ScrW() / 2,
		y = ScrH() / 2
	}
	local B = {
		x = x,
		y = y
	}
	local C = {}

	C.x = A.x + (r / 2 * ((B.x - A.x) / math.sqrt(math.pow(B.x - A.x, 2) + math.pow(B.y - A.y, 2))))
	C.y = A.y + (r / 2 * ((B.y - A.y) / math.sqrt(math.pow(B.x - A.x, 2) + math.pow(B.y - A.y, 2))))

	return C
end

local function is_outside_circle(x, y, r)
	return x ^ 2 + y ^ 2 > (r / 2) ^ 2
end

net.Receive(TAG_TEXT, function()
	local channel = net.ReadUInt(8)
	local t = {
		text = net.ReadString(),
		x = net.ReadFloat(),
		y = net.ReadFloat(),
		holdtime = net.ReadFloat(),
		color = net.ReadColor(),
		color2 = net.ReadColor(),
		start = CurTime(),
		fxnum = net.ReadInt(4),
		fadeinTime = net.ReadFloat(),
		fadeoutTime = net.ReadFloat(),
		fxTime = net.ReadFloat()
	}

	if t.x == -1 then
		t.x = .5
	end
	if t.y == -1 then
		t.y = .5
	end

	GAMEMODE.HUDQuickTexts[channel] = t
end)

local MAX_EDICT_BITS = 13 -- https://github.com/Facepunch/garrysmod/blob/master/garrysmod/lua/includes/extensions/net.lua#L5
GM.ActiveTimer = GAMEMODE and GAMEMODE.ActiveTimer or NULL
net.Receive(TAG_TIMER, function()
	local index = net.ReadUInt(MAX_EDICT_BITS)
	GAMEMODE.ActiveTimer = Entity(index)

	if index ~= 0 and not GAMEMODE.ActiveTimer:IsValid() then
		hook.Add("NetworkEntityCreated", Format("oc.ent_wait_%d", index), function(ent)
			if ent:EntIndex() == index and ent:GetClass() == TAG_TIMER then
				GAMEMODE.ActiveTimer = ent
				hook.Remove("NetworkEntityCreated", "oc_timer")
			end
		end)
	end
end)

local tab1 = Material"sprites/hud_tab1"
local tab2 = Material"sprites/hud_tab2"
local tab3 = Material"sprites/hud_tab3"

local new_hp = Material("oc_newhud/health.png", "mips smooth")
local new_ammo = Material("oc_newhud/ammo.png", "mips smooth")
local new_score = Material("oc_newhud/score.png", "mips smooth")
local new_timer = Material("oc_newhud/timer.png", "mips smooth")

local health = Material"sprites/health"
local energy = Material"sprites/energy"

local h_glowa = 255
local h_old = 0
local a_glowa = 255
local a_old = 0
local s_glowa = 255
local s_old = 0
local s_delta = 0
local l_glowa = 255
local l_old = 0
local l_delta = 0

local am_glowa = 255
local am_old = 0
local am_pos = ScrW() - ScreenScaleH(150)
local am2_glowa = 255
local am2_old = 0

local ammo_mats = {
	SMG1 = Material"sprites/weapons/weapon_smg1",
	AR2 = Material"sprites/weapons/weapon_ar2",
	Buckshot = Material"sprites/weapons/weapon_shotgun",
	["357"] = Material"sprites/weapons/weapon_357",
	Pistol = Material"sprites/weapons/weapon_pistol",
	XBowBolt = Material"sprites/weapons/weapon_crossbow",
	AlyxGun = Material"sprites/weapons/weapon_alyxgun",
	Grenade = Material"sprites/weapons/weapon_frag",
	RPG_Round = Material"sprites/weapons/weapon_rpg",
	SniperRound = Material"sprites/weapons/weapon_sniperrifle",
	GaussEnergy = Material"sprites/weapons/weapon_gauss"
}
local wep_mats = {
	weapon_medkit = Material"sprites/weapons/weapon_healer",
	weapon_healer = Material"sprites/weapons/weapon_healer"
}

local ammoChars = {
	["357"] = "q",
	["alyxgun"] = "p",
	["ar2"] = "u",
	["ar2altfire"] = "z",
	["xbowbolt"] = "w",
	["grenade"] = "v",
	["pistol"] = "p",
	["buckshot"] = "s",
	["rpg_round"] = "x",
	["smg1"] = "r",
	["smg1_grenade"] = "t",
}

local ammoCharsWeps = {
	["weapon_annabelle"] = "s",
	["weapon_annabeller"] = "s"
}

local function CreateFonts()
	surface.CreateFont("oc_scorefont", {
		font = "Verdana",
		size = ScreenScaleH(9),
		weight = 700,
		shadow = true,
		antialias = false
	})
	surface.CreateFont("oc_tinyfont", {
		font = "Obsidian",
		size = ScreenScaleH(9),
		weight = 600,
		shadow = true,
		antialias = true
	})
	surface.CreateFont("oc_bigfont", {
		font = "Obsidian",
		size = ScreenScaleH(18),
		weight = 600,
		shadow = true,
		antialias = true
	})
	surface.CreateFont("oc_hudnumbers", {
		font = "Obsidian",
		size = ScreenScaleH(32),
		antialias = true,
		blursize = 0,
		scanlines = 0,
		additive = true,
	})
	surface.CreateFont("oc_hudnumbers_glow", {
		font = "Obsidian",
		size = ScreenScaleH(32),
		antialias = true,
		blursize = ScreenScaleH(4),
		scanlines = ScreenScaleH(2),
		additive = true,
	})
	surface.CreateFont("oc_hudnumbers_small", {
		font = "Obsidian",
		size = ScreenScaleH(16),
		antialias = true,
		blursize = 0,
		scanlines = 0,
		weight = 600,
		additive = true,
	})
	surface.CreateFont("oc_hudnumbers_small_glow", {
		font = "Obsidian",
		size = ScreenScaleH(16),
		antialias = true,
		blursize = ScreenScaleH(4),
		scanlines = ScreenScaleH(2),
		weight = 600,
		additive = true,
	})
	surface.CreateFont("oc_hudnumbers_new", {
		font = "ObsidianConflictNewHUD",
		size = ScreenScaleH(24),
		antialias = true,
		blursize = 0,
		scanlines = 0,
		additive = true,
	})
	surface.CreateFont("oc_hudnumbers_new_glow", {
		font = "ObsidianConflictNewHUD",
		size = ScreenScaleH(24),
		antialias = true,
		blursize = ScreenScaleH(4),
		scanlines = ScreenScaleH(2),
		additive = true,
	})
	surface.CreateFont("oc_hudnumbers_new_small", {
		font = "ObsidianConflictNewHUD",
		size = ScreenScaleH(10),
		antialias = true,
		blursize = 0,
		scanlines = 0,
		weight = 600,
		additive = true,
	})
	surface.CreateFont("oc_hudnumbers_new_small_glow", {
		font = "ObsidianConflictNewHUD",
		size = ScreenScaleH(10),
		antialias = true,
		blursize = ScreenScaleH(4),
		scanlines = ScreenScaleH(2),
		weight = 600,
		additive = true,
	})
	surface.CreateFont("oc_targetid", {
		font = "Verdana",
		size = 24,
		antialias = true,
		weight = 600,
		shadow = true
	})
	surface.CreateFont("oc_HUDSelectionNumbers", {
		font = "Verdana",
		size = ScreenScaleH(11),
		weight = 700,
		antialias = true,
		additive = true,
	})
	surface.CreateFont("oc_HUDSelectionText", {
		font = "Obsidian",
		size = ScreenScaleH(8),
		weight = 700,
		antialias = true,
	})

	surface.CreateFont("CustomWeaponIcons", {
		font = "Custom",
		size = ScreenScaleH(64),
		weight = 0,
		antialias = true,
		additive = true,
		blursize = 0,
		scanlines = 0,
	})
	surface.CreateFont("CustomWeaponIconsSelected", {
		font = "Custom",
		size = ScreenScaleH(64),
		weight = 0,
		antialias = true,
		additive = true,
		blursize = ScreenScaleH(4),
		scanlines = ScreenScaleH(2),
	})

	surface.CreateFont("ObsidianWeaponIcons", {
		font = "Obsidianweaps",
		size = ScreenScaleH(64),
		weight = 0,
		antialias = true,
		additive = true,
		blursize = 0,
		scanlines = 0,
	})
	surface.CreateFont("ObsidianWeaponIconsSelected", {
		font = "Obsidianweaps",
		size = ScreenScaleH(64),
		weight = 0,
		antialias = true,
		additive = true,
		blursize = ScreenScaleH(4),
		scanlines = ScreenScaleH(2),
	})

	surface.CreateFont("CSWeaponIcons", {
		font = "cs",
		size = ScreenScaleH(64),
		weight = 0,
		antialias = true,
		additive = true,
		blursize = 0,
		scanlines = 0,
	})
	surface.CreateFont("CSWeaponIconsSelected", {
		font = "cs",
		size = ScreenScaleH(64),
		weight = 0,
		antialias = true,
		additive = true,
		blursize = ScreenScaleH(4),
		scanlines = ScreenScaleH(2),
	})
	surface.CreateFont("HL2WeaponIcons", {
		font = "HalfLife2",
		size = ScreenScaleH(64),
		weight = 0,
		antialias = true,
		additive = true,
		blursize = 0,
		scanlines = 0,
	})
	surface.CreateFont("HL2WeaponIconsSelected", {
		font = "HalfLife2",
		size = ScreenScaleH(64),
		weight = 0,
		antialias = true,
		additive = true,
		blursize = ScreenScaleH(4),
		scanlines = ScreenScaleH(2),
	})

	surface.CreateFont("HL2Crosshairs", {
		font = "HalfLife2",
		antialias = false,
		size = 40,
		weight = 0,
		additive = true,
	})
	surface.CreateFont("QuickInfoBars", {
		font = "HL2cross",
		size = 63,
		weight = 0,
		antialias = true,
		additive = true,
	})

	surface.CreateFont("oc_newhud_icons", {
		font = "HalfLife2",
		antialias = false,
		size = ScreenScaleH(64),
		weight = 0,
		additive = true,
	})
	surface.CreateFont("oc_newhud_text", {
		font = "Obsidian",
		size = ScreenScaleH(9.5),
		weight = 700,
		--additive = true,
		antialias = true,
	})

	surface.CreateFont("oc_WeaponIconsSmall", {
		font = "HalfLife2",
		size = ScreenScaleH(32),
		weight = 0,
		blursize = 0,
		scanlines = 0,
		additive = true,
		antialias = true,
	})
end

CreateFonts()
function GM:HUDInit(reloaded)
	CreateFonts()
end

function GM:OnResolutionChanged(oldW, oldH, newW, newH)
	self:HUDInit()
end

--needs to be called in think for accuracy
--due to HUDPaint not being called when in menu
--which is where res changes will happen
function GM:Think()
	local scrW = ScrW()
	local scrH = ScrH()

	self.CurResW = self.CurResW or scrW
	self.CurResH = self.CurResH or scrH

	local resChanged = false

	if self.CurResW ~= scrW or self.CurResH ~= scrH then
		resChanged = true
	end

	if resChanged then
		local curW = self.CurResW
		local curH = self.CurResH

		hook.Call("OnResolutionChanged", self, curW, curH, scrW, scrH)

		self.CurResH = resH
		self.CurResW = resW
	end
end

concommand.Add("oc_briefing", function()
	GAMEMODE:ShowBriefing()
end)

local function empty() end
function GM:ShowBriefing()
	if not IsValid(me) then me = LocalPlayer() end
	local brief = file.Read(string.format("maps/cfg/%s_briefing.txt", game.GetMap()), "GAME")
	local lines = brief and brief:Split"\n" or {}

	local fr = vgui.Create("DFrame")
	fr:SetTitle("")
	fr:SetSize(800, 600)
	fr:Center()
	fr:MakePopup()
	fr:ShowCloseButton(false)

	local left = vgui.Create("DPanel", fr)
	local right = vgui.Create("DPanel", fr)
	left.Paint = empty
	right.Paint = empty

	local close = vgui.Create("DButton", left)
	close:Dock(BOTTOM)
	close:SetText"Cancel"
	close:SetTall(64)
	close:DockMargin(15, 15, 15, 15)
	close:SetFont("oc_tinyfont")
	close.UpdateColours = empty
	close:SetTextStyleColor(Color(255, 180, 0))
	function close:DoClick()
		fr:Close()
	end

	close.Paint = function(s, w, h)
		if s.Hovered then
			surface.SetDrawColor(255, 180, 0, 64)
			surface.DrawRect(0, 0, w, h)
		end
		surface.SetDrawColor(255, 180, 0, 255)
		surface.DrawOutlinedRect(0, 0, w, h)
	end

	local spec = vgui.Create("DButton", left)
	spec:Dock(BOTTOM)
	spec:SetText(me:Team() == TEAM_SPECTATOR and (mp_teamplay:GetBool() and "Auto Join Team" or "Join Game") or "Spectate")
	spec:SetTall(64)
	spec:DockMargin(15, 15, 15, 15)
	spec:SetFont("oc_tinyfont")
	spec.UpdateColours = empty
	spec:SetTextStyleColor(Color(255, 180, 0))
	function spec:DoClick()
		fr:Close()
		net.Start("oc_spectate")
		net.SendToServer()
	end

	spec.Paint = function(s, w, h)
		if s.Hovered then
			surface.SetDrawColor(255, 180, 0, 64)
			surface.DrawRect(0, 0, w, h)
		end
		surface.SetDrawColor(255, 180, 0, 255)
		surface.DrawOutlinedRect(0, 0, w, h)
	end

	function fr:PerformLayout(w, h)
		left:SetSize(self:GetWide() / 2, self:GetTall())
		right:SetSize(self:GetWide() / 2, self:GetTall())
		right:SetPos(self:GetWide() / 2, 0)
	end

	local right_header = vgui.Create("DPanel", right)
	right_header.Paint = empty
	right_header:Dock(TOP)
	right_header:SetTall(64)
	right_header:DockMargin(15, 15, 15, 15)

	local text = vgui.Create("RichText", right)
	text:Dock(FILL)
	text.Paint = empty
	text:SetText("No Briefing Available")
	function text:PerformLayout(w, h)
		self:SetFontInternal("oc_tinyfont")
		self:SetFGColor(255, 180, 0, 255)
	end

	if not table.IsEmpty(lines) then
		text:SetText("")

		for _, t in ipairs(lines) do
			text:AppendText(t)
			text:AppendText"\n"
		end
	end

	if mp_teamplay:GetBool() then
		local left_header = vgui.Create("DLabel", left)
		left_header.Paint = empty
		left_header:Dock(TOP)
		left_header:SetTall(64)
		left_header:DockMargin(15, 15, 15, 15)
		left_header:SetText"Join a Team"

		function left_header:Paint(w, h)
			draw.SimpleText(self:GetText(), "oc_bigfont", w / 2, h / 2, Color(255, 180, 0), TEXT_ALIGN_CENTER, nil)

			return true
		end

		local red = vgui.Create("DButton", left)
		red:Dock(TOP)
		red:SetText("Team Red")
		red:SetTall(64)
		red:DockMargin(15, 15, 15, 15)
		red:SetFont("oc_tinyfont")
		red.UpdateColours = empty
		red:SetTextStyleColor(Color(255, 62, 62))

		function red:DoClick()
			fr:Close()
			net.Start("oc_cmd")
			net.WriteUInt(OC_CMD_CHANGETEAM, 4)
			net.WriteBool(false)
			net.SendToServer()
		end

		red.Paint = function(s, w, h)
			if s.Hovered then
				surface.SetDrawColor(255, 62, 62, 64)
				surface.DrawRect(0, 0, w, h)
			end
			surface.SetDrawColor(255, 62, 62, 255)
			surface.DrawOutlinedRect(0, 0, w, h)
		end

		local blue = vgui.Create("DButton", left)
		blue:Dock(TOP)
		blue:SetText("Team Blue")
		blue:SetTall(64)
		blue:DockMargin(15, 15, 15, 15)
		blue:SetFont("oc_tinyfont")
		blue.UpdateColours = empty
		blue:SetTextStyleColor(Color(154, 205, 255))

		function blue:DoClick()
			fr:Close()
			net.Start("oc_cmd")
			net.WriteUInt(OC_CMD_CHANGETEAM, 4)
			net.WriteBool(true)
			net.SendToServer()
		end

		blue.Paint = function(s, w, h)
			if s.Hovered then
				surface.SetDrawColor(154, 205, 255, 64)
				surface.DrawRect(0, 0, w, h)
			end
			surface.SetDrawColor(154, 205, 255, 255)
			surface.DrawOutlinedRect(0, 0, w, h)
		end
	end

	function fr:Paint(w, h)
		surface.SetDrawColor(0, 0, 0, 128)
		surface.DrawRect(0, 0, w, h)
	end
end

local mp_livesmode = GetConVar("mp_livesmode")

local green = Color(0, 255, 0)
local red = Color(255, 0, 0)
local white = Color(255, 255, 255)
local black = Color(0, 0, 0)

local drawCrosshair = true
local format = string.FormattedTime

GM.__ShownBriefing = GAMEMODE and GAMEMODE.__ShownBriefing or false
function GM:HUDPaint()
	if not self.__ShownBriefing then
		-- FIXME: the server should really send this to clients
		if file.Exists(string.format("maps/cfg/%s_briefing.txt", game.GetMap()), "GAME") then
			self:ShowBriefing()
		end

		self.__ShownBriefing = true
	end

	self.CurResW = self.CurResW or ScrW()
	self.CurResH = self.CurResH or ScrH()

	if hook.Run("HUDShouldDraw", "OCTargetID") then
		hook.Run("HUDDrawTargetID")
	end
	if hook.Run("HUDShouldDraw", "OCPickupHistory") then
		hook.Run("HUDDrawPickupHistory")
	end
	if hook.Run("HUDShouldDraw", "OCDeathNotice") then
		hook.Run("DrawDeathNotice", 0.85, 0.04)
	end

	if hook.Run("HUDShouldDraw", "OCHudHealth") then
		hook.Run("HUDDrawHealth")
	end
	if hook.Run("HUDShouldDraw", "OCHudBattery") then
		hook.Run("HUDDrawBattery")
	end

	if hook.Run("HUDShouldDraw", "OCHudScore") then
		hook.Run("HUDDrawScore")
	end
	if mp_livesmode:GetInt() > 0 and hook.Run("HUDShouldDraw", "OCHudLives") then
		hook.Run("HUDDrawLives")
	end

	if hook.Run("HUDShouldDraw", "OCHudAmmo") then
		hook.Run("HUDDrawAmmo")
	end
	if hook.Run("HUDShouldDraw", "OCHudSecondaryAmmo") then
		hook.Run("HUDDrawSecondaryAmmo")
	end

	if mp_teamplay:GetBool() and hook.Run("HUDShouldDraw", "OCHudTeam") then
		if oc_hud_mode:GetInt() == 0 then
			local x, y = ScreenScaleH(2), ScreenScaleH(24)
			local w = ScreenScaleH(55)
			local h = ScreenScaleH(20)

			surface.SetFont("oc_scorefont")
			local wide, high = surface.GetTextSize(team.GetName(me:Team()))
			w = math.max(wide, w)
			h = math.max(high, h)

			render.SetScissorRect(x, y, x + w, y + h, true)
			draw.RoundedBox(6, x, y, w, h, ColorAlpha(self:GetHUDBGColor(), 80))
			draw.SimpleText(team.GetName(me:Team()), "oc_scorefont", x + ScreenScaleH(4), y + ScreenScaleH(2), team.GetColor(me:Team()))
			render.SetScissorRect(0, 0, 0, 0, false)
		else
			local x, y = ScreenScaleH(2), ScreenScaleH(2)
			draw.SimpleText(team.GetName(me:Team()), "oc_newhud_text", x + ScreenScaleH(110), y + ScreenScaleH(25), team.GetColor(me:Team()))
		end
	end

	local ActiveTimer = self.ActiveTimer
	if ActiveTimer:IsValid() then
		local timerElapsed = ActiveTimer:GetPaused() and ActiveTimer:GetTimeLeft() or ActiveTimer:GetEndTime() - CurTime()
		local timerName = ActiveTimer:GetLabel()
		local timerRemaining = math.max(timerElapsed, 0)

		local TOTAL_PULSE_DURATION = 3
		local FADEOUT_DURATION = 1
		local SMALL_PULSE_DURATION = 0.25
		local TOTAL_ENDING_ANIMATION_DURATION = TOTAL_PULSE_DURATION + FADEOUT_DURATION

		local textBaseAlpha = 196
		local textBaseCol = ColorAlpha(self:GetHUDTextColor(), textBaseAlpha)
		local RED = ColorAlpha(red, textBaseAlpha)
		local textCol = textBaseCol

		local flAlphaPerc = 1
		local flPulsePerc = 0

		if timerRemaining <= 0 then
			local flEndingTimeElapsed = math.min(-timerElapsed, TOTAL_ENDING_ANIMATION_DURATION)
			local flEndingTimeLeft = math.max(TOTAL_ENDING_ANIMATION_DURATION + timerElapsed, 0)
			--local flEndingFrac = math.min(flEndingTimeElapsed / TOTAL_ENDING_ANIMATION_DURATION, 1)

			if flEndingTimeElapsed <= TOTAL_PULSE_DURATION then
				flPulsePerc = math.max(math.sin((flEndingTimeElapsed % 1) * 12), 0)

				--print(timerElapsed, flPulsePerc, flEndingTimeElapsed, flEndingTimeElapsed % 1)

				--flAlphaPerc = flPulsePerc
				textCol = AUXPOW:IntersectColor(RED, textBaseCol, flPulsePerc)
			elseif flEndingTimeLeft <= FADEOUT_DURATION then
				flAlphaPerc = flEndingTimeLeft / FADEOUT_DURATION
				textCol.a = Lerp(flAlphaPerc, 0, textBaseAlpha)
			end
		end

		if timerElapsed > -TOTAL_ENDING_ANIMATION_DURATION and hook.Run("HUDShouldDraw", "OCHudTimer") then
			if oc_hud_mode:GetInt() == 0 then
				local w, h = ScreenScaleH(192), ScreenScaleH(48)
				local x, y = (ScrW() / 2) - (w / 2), ScreenScaleH(2)
				local t_x, t_y = ScreenScaleH(4), ScreenScaleH(2)
				local d_x, d_y = ScreenScaleH(4), ScreenScaleH(18)

				draw.RoundedBox(6, x, y, w, h, ColorAlpha(self:GetHUDBGColor(), 80 * flAlphaPerc))

				surface.SetFont("oc_hudnumbers")
				local txt = format(timerRemaining, "%02i:%02i:%02i")
				local _w = surface.GetTextSize(txt)

				render.SetScissorRect(x, y, x + w, y + h, true)
				draw.SimpleText(timerName, "oc_hudnumbers_small", (x + w / 2) + t_x, y + t_y, textCol, TEXT_ALIGN_CENTER)
				draw.SimpleText(txt, "oc_hudnumbers", (x + w / 4) + d_x, y + d_y, textCol)

				local glowCol = ColorAlpha(textCol, 255 * flPulsePerc)
				draw.SimpleText(timerName, "oc_hudnumbers_small_glow", (x + w / 2) + t_x, y + t_y, glowCol, TEXT_ALIGN_CENTER)
				draw.SimpleText(txt, "oc_hudnumbers_glow", (x + w / 4) + d_x, y + d_y, glowCol)
				render.SetScissorRect(0, 0, 0, 0, false)
			else
				local w, h = ScreenScaleH(268), ScreenScaleH(48)
				local x, y = (ScrW() / 2) - (w / 2), ScreenScaleH(2)

				surface.SetAlphaMultiplier(flAlphaPerc)
				surface.SetMaterial(new_timer)
				surface.SetDrawColor(self:GetHUDBGColor())
				surface.DrawTexturedRect(x, y, w, h)
				surface.SetAlphaMultiplier(1)

				surface.SetFont("oc_hudnumbers_new")
				local txt = format(timerRemaining, "%02i:%02i:%02i")

				draw.SimpleText(timerName, "oc_hudnumbers_small", x + w / 2, y + ScreenScaleH(2), textCol, TEXT_ALIGN_CENTER)
				draw.SimpleText(txt, "oc_hudnumbers_new", x + w / 2, y + ScreenScaleH(20), textCol, TEXT_ALIGN_CENTER)

				local glowCol = ColorAlpha(textCol, 255 * flPulsePerc)
				draw.SimpleText(timerName, "oc_hudnumbers_new_small_glow", x + w / 2, y + ScreenScaleH(2), glowCol, TEXT_ALIGN_CENTER)
				draw.SimpleText(txt, "oc_hudnumbers_new_glow", x + w / 2, y + ScreenScaleH(20), glowCol, TEXT_ALIGN_CENTER)
			end
		elseif timerElapsed <= -TOTAL_ENDING_ANIMATION_DURATION then
			self.ActiveTimer = NULL
		end
	end

	if hook.Run("HUDShouldDraw", "OCHudGameTextQuick") then
		for _, v in next, self.HUDQuickTexts do
			if v.fxnum == 0 or v.fxnum == 1 then
				local vars = v.vars or {}
				v.vars = vars
				vars.a = vars.a or 0

				if v.start + v.holdtime > CurTime() then
					vars.a = Lerp(RealFrameTime() * (1 / v.fadeinTime), vars.a, 255)
				else
					vars.a = Lerp(RealFrameTime() * (1 / v.fadeoutTime), vars.a, 0)
				end

				if not vars.started and vars.a >= 250 then
					vars.started = true
					v.start = CurTime()
				end

				if vars.a > 1 then
					draw.SimpleText(v.text, "CloseCaption_Bold", ScrW() * v.x, ScrH() * v.y, ColorAlpha(v.color, vars.a), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
				end
			else
				draw.SimpleText(v.text, "CloseCaption_Bold", ScrW() * v.x, ScrH() * v.y, v.color, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
			end
		end
	end
	if hook.Run("HUDShouldDraw", "OCHudWaypoint") then
		for _, waypoint in next, self.HUDWaypoints do
			if (isbool(waypoint.enabled) and not waypoint.enabled) or (waypoint.endtime and waypoint.endtime <= CurTime()) then continue end

			-- find pos in world
			local pos
			if waypoint.parent then
				local e = waypoint.parent
				if not e:IsValid() then continue end

				pos = e:LocalToWorld(e:OBBCenter())
			else
				pos = waypoint.pos
			end

			local spos = pos:ToScreen()

			-- clip screen pos to screen
			if is_outside_circle((ScrW() / 2) - spos.x, (ScrH() / 2) - spos.y, ScrH() - (64 + 48 + 8)) then
				spos = keep_inside_circle(spos.x, spos.y, ScrH() - (64 + 48 + 8))
			end

			-- draw icon
			render.OverrideAlphaWriteEnable(true, true)
			render.OverrideBlend(true, BLEND_ONE, BLEND_ONE, BLENDFUNC_ADD)

			surface.SetMaterial(waypoint.image)
			surface.SetDrawColor(255, 255, 255)
			surface.DrawTexturedRect(spos.x - 16, spos.y - 16, 32, 32)

			render.OverrideAlphaWriteEnable(false, false)
			render.OverrideBlend(false)

			local m_dist = util.HUtoCM(pos:Distance(me:GetPos()))
			draw.SimpleTextOutlined(util.NiceMetric(m_dist), nil, spos.x - 16, spos.y + 16, white, nil, nil, 1, black)
			draw.SimpleTextOutlined(waypoint.text, nil, spos.x - 16, spos.y + 32, white, nil, nil, 1, black)
		end
	end
	if hook.Run("HUDShouldDraw", "OCHudMessageMultiplayer") then
		for k, v in next, self.HUDTexts do
			if not v.enabled then continue end
			local pos = v.pos
			local parent = Entity(v.p_idx)

			if v._has_parent and not parent:IsValid() then
				self.HUDTexts[k] = nil
				continue
			end

			if IsValid(parent) then
				pos:Set(parent:GetPos())

				-- if parent is valid, v.pos acts as local position
				pos:Add(v.pos)
				v._has_parent = true
			end

			if pos:Distance(me:GetPos()) > v.radius then continue end

			local spos = pos:ToScreen()
			draw.SimpleTextOutlined(v.text, nil, spos.x, spos.y, v.col, TEXT_ALIGN_CENTER, nil, 1, Color(0, 0, 0))
		end
	end
	if hook.Run("HUDShouldDraw", "OCHudAux") then
		hook.Run("HUDDrawAUX")
	end

	if hook.Run("HUDShouldDraw", "OCHudSpectate") and GAMEMODE:IsPlayerSpectator(me) then
		local mode = me:GetObserverMode()
		local ent = me:GetObserverTarget()

		if ent:IsValid() and mode ~= OBS_MODE_ROAMING then
			if ent:IsPlayer() then
				local str = ("%s (%s)"):format(ent:Name(), oc_targetid_usepercent:GetBool() and ("%d%%"):format(math.Clamp(ent:Health() / ent:GetMaxHealth() * 100, 0, 100)) or ("%d/%d"):format(math.max(0, ent:Health()), ent:GetMaxHealth()))
				surface.SetFont("oc_HUDSelectionText")
				local tw, th = surface.GetTextSize(str)

				local x, y = (self.CurResW / 2) - (tw + 32), self.CurResH - 128
				draw.RoundedBox(4, x, y, tw + 32, 64, ColorAlpha(self:GetHUDBGColor(), 80))
				draw.SimpleText(str, "oc_HUDSelectionText", x + ((tw + 32) / 2), y + 32, self:GetHUDTextColor(), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
			elseif ent:IsNPC() then
				local str = ("%s (%s)"):format(ent:GetNW2String("displayname", ent:GetClass()) or ent:GetClass(), oc_targetid_usepercent:GetBool() and ("%d%%"):format(math.Clamp(ent:Health() / ent:GetMaxHealth() * 100, 0, 100)) or ("%d/%d"):format(math.max(0, ent:Health()), ent:GetMaxHealth()))
				surface.SetFont("oc_HUDSelectionText")
				local tw, th = surface.GetTextSize(str)

				local x, y = (self.CurResW / 2) - ((tw + 32) / 2), self.CurResH - 128
				draw.RoundedBox(4, x, y, tw + 32, 64, ColorAlpha(self:GetHUDBGColor(), 80))
				draw.SimpleText(str, "oc_HUDSelectionText", x + ((tw + 32) / 2), y + 32, self:GetHUDTextColor(), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
			end
		end
	end

	if drawCrosshair then
		surface.SetFont("HL2Crosshairs")
		local _, h = surface.GetTextSize("Q")
		draw.DrawText("Q", "HL2Crosshairs", self.CurResW / 2, (self.CurResH / 2) - h / 2, Color(255, 255, 255), TEXT_ALIGN_CENTER)

		if oc_quickinfo:GetBool() then
			self:DrawQuickinfo(self.CurResW / 2, self.CurResH / 2)
		end
	end
end

function GM:HUDDrawHealth()
	if oc_hud_mode:GetInt() == 0 then
		local x, y = ScreenScaleH(16), ScreenScaleH(430)
		local w, h = ScreenScaleH(158), ScreenScaleH(41)

		if me:Health() ~= h_old then
			h_old = me:Health()
			h_glowa = 255
		end

		h_glowa = Lerp(RealFrameTime(), h_glowa, 0)

		render.SetScissorRect(x, y, x + w, y + h, true)
		surface.SetMaterial(tab1)
		surface.SetDrawColor(self:GetHUDBGColor())
		surface.DrawTexturedRect(x, y, w, h)

		surface.SetMaterial(health)
		surface.SetDrawColor(255, 255, 255, 255)
		surface.DrawTexturedRect(x + ScreenScaleH(4), y + ScreenScaleH(4), ScreenScaleH(32), ScreenScaleH(32))

		draw.SimpleText(math.max(me:Health(), 0), "oc_hudnumbers_glow", x + ScreenScaleH(46), y + ScreenScaleH(6), ColorAlpha(self:GetHUDTextColor(), h_glowa))
		draw.SimpleText(math.max(me:Health(), 0), "oc_hudnumbers", x + ScreenScaleH(46), y + ScreenScaleH(6), ColorAlpha(self:GetHUDTextColor(), 196))
		render.SetScissorRect(0, 0, 0, 0, false)
	else
		local x, y = ScreenScaleH(16), ScreenScaleH(433)
		local w, h = ScreenScaleH(130), ScreenScaleH(38)

		if me:Health() ~= h_old then
			h_old = me:Health()
			h_glowa = 255
		end

		h_glowa = Lerp(RealFrameTime(), h_glowa, 0)

		render.SetScissorRect(x, y - ScreenScaleH(24), x + w, y + h, true)
		surface.SetMaterial(new_hp)
		surface.SetDrawColor(self:GetHUDBGColor())
		surface.DrawTexturedRect(x, y, w, h)

		draw.SimpleText(math.max(me:Health(), 0), "oc_hudnumbers_new_glow", x + ScreenScaleH(38), y + ScreenScaleH(4), ColorAlpha(self:GetHUDTextColor(), h_glowa))
		draw.SimpleText(math.max(me:Health(), 0), "oc_hudnumbers_new", x + ScreenScaleH(38), y + ScreenScaleH(4), ColorAlpha(self:GetHUDTextColor(), 196))
		draw.SimpleText("+", "oc_newhud_icons", x + ScreenScaleH(0), y - ScreenScaleH(42), ColorAlpha(self:GetHUDTextColor(), 196))
		draw.SimpleText("HEALTH", "oc_newhud_text", x + ScreenScaleH(0.5), y - ScreenScaleH(24.5), ColorAlpha(self:GetHUDTextColor(), 196))

		render.SetScissorRect(0, 0, 0, 0, false)
	end
end

function GM:HUDDrawBattery()
	if oc_hud_mode:GetInt() == 0 then
		local x, y = ScreenScaleH(148), ScreenScaleH(430)
		local w, h = ScreenScaleH(158), ScreenScaleH(41)

		if me:Armor() ~= a_old then
			a_old = me:Armor()
			a_glowa = 255
		end

		a_glowa = Lerp(RealFrameTime(), a_glowa, 0)

		render.SetScissorRect(x, y, x + w, y + h, true)
		surface.SetMaterial(tab1)
		surface.SetDrawColor(self:GetHUDBGColor())
		surface.DrawTexturedRect(x, y, w, h)

		surface.SetMaterial(energy)
		surface.SetDrawColor(255, 255, 255, 255)
		surface.DrawTexturedRect(x + ScreenScaleH(4), y + ScreenScaleH(4), ScreenScaleH(32), ScreenScaleH(32))

		draw.SimpleText(me:Armor(), "oc_hudnumbers_glow", x + ScreenScaleH(46), y + ScreenScaleH(6), ColorAlpha(self:GetHUDTextColor(), a_glowa))
		draw.SimpleText(me:Armor(), "oc_hudnumbers", x + ScreenScaleH(46), y + ScreenScaleH(6), ColorAlpha(self:GetHUDTextColor(), 196))
		render.SetScissorRect(0, 0, 0, 0, false)
	else
		local x, y = ScreenScaleH(135), ScreenScaleH(433)
		local w, h = ScreenScaleH(130), ScreenScaleH(38)

		if me:Armor() ~= a_old then
			a_old = me:Armor()
			a_glowa = 255
		end

		a_glowa = Lerp(RealFrameTime(), a_glowa, 0)

		render.SetScissorRect(x, y - ScreenScaleH(24), x + w, y + h, true)

		surface.SetMaterial(new_hp)
		surface.SetDrawColor(self:GetHUDBGColor())
		surface.DrawTexturedRect(x, y, w, h)

		draw.SimpleText(me:Armor(), "oc_hudnumbers_new_glow", x + ScreenScaleH(38), y + ScreenScaleH(4), ColorAlpha(self:GetHUDTextColor(), a_glowa))
		draw.SimpleText(me:Armor(), "oc_hudnumbers_new", x + ScreenScaleH(38), y + ScreenScaleH(4), ColorAlpha(self:GetHUDTextColor(), 196))
		draw.SimpleText("*", "oc_newhud_icons", x, y - ScreenScaleH(42), ColorAlpha(self:GetHUDTextColor(), 196))
		draw.SimpleText("SUIT", "oc_newhud_text", x + ScreenScaleH(0.5), y - ScreenScaleH(24.5), ColorAlpha(self:GetHUDTextColor(), 196))

		render.SetScissorRect(0, 0, 0, 0, false)
	end
end

local am_height = ScreenScaleH(34)
function GM:HUDDrawAmmo()
	if oc_hud_mode:GetInt() == 0 then
		local w, h = ScreenScaleH(163), ScreenScaleH(41)

		local veh = me:GetVehicle()
		if IsValid(veh) then
			local x = ScrW() - ScreenScaleH(150)
			local y = ScreenScaleH(430)

			local atype, clip, ammo = veh:GetAmmo()

			if ammo ~= -1 then
				local amt = clip == -1 and ammo or clip

				if amt ~= am_old then
					am_old = clip == -1 and ammo or clip
					am_glowa = 255
				end
				am_glowa = Lerp(RealFrameTime(), am_glowa, 0)

				render.SetScissorRect(x, y, x + w, y + h, true)
				draw.RoundedBox(4, x, y, w, h, ColorAlpha(self:GetHUDBGColor(), 80))
				draw.SimpleText(amt, "oc_hudnumbers_glow", x + ScreenScaleH(44), y + ScreenScaleH(5), ColorAlpha(self:GetHUDTextColor(), am_glowa))
				draw.SimpleText(amt, "oc_hudnumbers", x + ScreenScaleH(44), y + ScreenScaleH(5), ColorAlpha(self:GetHUDTextColor(), 196))
				render.SetScissorRect(0, 0, 0, 0, false)
			end
		else
			local wep = me:GetActiveWeapon()
			if IsValid(wep) then
				local x = am_pos
				local y = ScreenScaleH(430)
				local amt

				if wep.CustomAmmoDisplay and wep:CustomAmmoDisplay() then
					amt = wep:CustomAmmoDisplay().PrimaryClip
				elseif wep:GetPrimaryAmmoType() ~= -1 then
					if wep:Clip1() == -1 then
						amt = me:GetAmmoCount(wep:GetPrimaryAmmoType())
					else
						amt = wep:Clip1()
					end
				end

				if wep:GetPrimaryAmmoType() == -1 or wep:Clip1() == -1 then
					am_pos = Lerp(RealFrameTime() / 0.5, am_pos, self.CurResW - ScreenScaleH(118))
				end

				if wep:GetSecondaryAmmoType() ~= -1 then
					am_pos = Lerp(RealFrameTime() / 0.5, am_pos, self.CurResW - ScreenScaleH(222))
				else
					am_pos = Lerp(RealFrameTime() / 0.5, am_pos, self.CurResW - ScreenScaleH(150))
				end

				render.SetScissorRect(x, y, x + w, y + h, true)

				if amt then
					surface.SetMaterial(wep:GetPrimaryAmmoType() ~= -1 and (wep:Clip1() == -1 and tab1 or tab3) or tab1)
					surface.SetDrawColor(self:GetHUDBGColor())
					surface.DrawTexturedRect(x, y, w, h)

					if ammo_mats[game.GetAmmoName(wep:GetPrimaryAmmoType())] or wep_mats[wep:GetClass()] then
						surface.SetMaterial(ammo_mats[game.GetAmmoName(wep:GetPrimaryAmmoType())] or wep_mats[wep:GetClass()])
						surface.SetDrawColor(255, 255, 255, 255)
						surface.DrawTexturedRect(x + ScreenScaleH(4), y + ScreenScaleH(4), ScreenScaleH(32), ScreenScaleH(32))
					end

					if amt ~= am_old then
						am_old = amt
						am_glowa = 255
					end

					am_glowa = Lerp(RealFrameTime(), am_glowa, 0)

					draw.SimpleText(amt, "oc_hudnumbers_glow", x + ScreenScaleH(44), y + ScreenScaleH(5), ColorAlpha(self:GetHUDTextColor(), am_glowa))
					draw.SimpleText(amt, "oc_hudnumbers", x + ScreenScaleH(44), y + ScreenScaleH(5), ColorAlpha(self:GetHUDTextColor(), 196))
				end

				if wep:GetPrimaryAmmoType() ~= -1 and wep:Clip1() ~= -1 then
					draw.SimpleText(me:GetAmmoCount(wep:GetPrimaryAmmoType()), "oc_hudnumbers_small", x + ScreenScaleH(110), y + ScreenScaleH(7), ColorAlpha(self:GetHUDTextColor(), 196))
				end

				render.SetScissorRect(0, 0, 0, 0, false)
			end
		end
	else
		local w, h = ScreenScaleH(132), am_height
		local x, y = self.CurResW - w - ScreenScaleH(18), self.CurResH - h - ScreenScaleH(10)
		local wep = me:GetActiveWeapon()
		if wep:IsValid() then
			am_height = Lerp(RealFrameTime() * 4, am_height, wep:GetSecondaryAmmoType() ~= -1 and ScreenScaleH(60) or ScreenScaleH(34))
			local amt
			local amt2 = -1


			if wep.CustomAmmoDisplay and wep:CustomAmmoDisplay() then
				amt = wep:CustomAmmoDisplay().PrimaryClip
			elseif wep:GetPrimaryAmmoType() ~= -1 then
				if wep:Clip1() == -1 then
					amt = me:GetAmmoCount(wep:GetPrimaryAmmoType())
				else
					amt = wep:Clip1()
				end
			end

			if wep.CustomAmmoDisplay and wep:CustomAmmoDisplay() then
				amt2 = wep:CustomAmmoDisplay().SecondaryAmmo
			elseif wep:GetSecondaryAmmoType() ~= -1 then
				amt2 = me:GetAmmoCount(wep:GetSecondaryAmmoType())
			end

			if amt2 ~= am2_old then
				am2_old = amt2
				am2_glowa = 255
			end

			am2_glowa = Lerp(RealFrameTime(), am2_glowa, 0)

			render.SetScissorRect(x, y, x + w, y + h, true)
			if amt then
				surface.SetMaterial(new_ammo)
				surface.SetDrawColor(self:GetHUDBGColor())
				surface.DrawTexturedRect(x, y, w, h)

				--[[if ammo_mats[game.GetAmmoName(wep:GetPrimaryAmmoType())] or wep_mats[wep:GetClass()] then
				surface.SetMaterial(ammo_mats[game.GetAmmoName(wep:GetPrimaryAmmoType())] or wep_mats[wep:GetClass()])
				surface.SetDrawColor(255,255,255,255)
				surface.DrawTexturedRect(x + ScreenScaleH(4), y + ScreenScaleH(4), ScreenScaleH(32), ScreenScaleH(32))
			end--]]

				if amt ~= am_old then
					am_old = amt
					am_glowa = 255
				end

				am_glowa = Lerp(RealFrameTime(), am_glowa, 0)

				draw.SimpleText(amt, "oc_hudnumbers_new_glow", x + (string.len(amt) >= 3 and ScreenScaleH(34) or ScreenScaleH(52)), y + ScreenScaleH(2), ColorAlpha(self:GetHUDTextColor(), am_glowa))
				draw.SimpleText(amt, "oc_hudnumbers_new", x + (string.len(amt) >= 3 and ScreenScaleH(34) or ScreenScaleH(52)), y + ScreenScaleH(2), ColorAlpha(self:GetHUDTextColor(), 196))
				draw.SimpleText("AMMO", "oc_newhud_text", x + ScreenScaleH(14), y + ScreenScaleH(18), ColorAlpha(self:GetHUDTextColor(), 196))

				if wep:GetPrimaryAmmoType() ~= -1 and (ammoCharsWeps[wep:GetClass()] or ammoChars[game.GetAmmoName(wep:GetPrimaryAmmoType()):lower()]) then
					surface.SetFont("oc_newhud_text")
					local nLabelWidth, nLabelHeight = surface.GetTextSize("AMMO")
					surface.SetFont("oc_WeaponIconsSmall")
					local iw, ih = surface.GetTextSize(ammoCharsWeps[wep:GetClass()] or ammoChars[game.GetAmmoName(wep:GetPrimaryAmmoType()):lower()] or "")
					local ix = ScreenScaleH(14) + (nLabelWidth - iw) / 2
					local iy = ScreenScaleH(18) - (nLabelHeight + (ih / 2))

					draw.SimpleText(ammoCharsWeps[wep:GetClass()] or ammoChars[game.GetAmmoName(wep:GetPrimaryAmmoType()):lower()] or "", "oc_WeaponIconsSmall", x + ix, y + iy, ColorAlpha(self:GetHUDTextColor(), 196), TEXT_ALIGN_LEFT)
				end
			end


			if wep:GetPrimaryAmmoType() ~= -1 and wep:Clip1() ~= -1 then
				draw.SimpleText(me:GetAmmoCount(wep:GetPrimaryAmmoType()), "oc_hudnumbers_new_small", x + ScreenScaleH(92), y + ScreenScaleH(2), ColorAlpha(self:GetHUDTextColor(), 196))
			end

			if wep:GetSecondaryAmmoType() ~= -1 then
				draw.SimpleText(amt2, "oc_hudnumbers_new_glow", x + ScreenScaleH(60), y + ScreenScaleH(28), ColorAlpha(self:GetHUDTextColor(), am2_glowa))
				draw.SimpleText(amt2, "oc_hudnumbers_new", x + ScreenScaleH(60), y + ScreenScaleH(28), ColorAlpha(self:GetHUDTextColor(), 196))
				draw.SimpleText("ALT", "oc_newhud_text", x + ScreenScaleH(25), y + ScreenScaleH(44), ColorAlpha(self:GetHUDTextColor(), 196))

				if (ammoCharsWeps[wep:GetClass()] or ammoChars[game.GetAmmoName(wep:GetSecondaryAmmoType()):lower()]) then
					surface.SetFont("oc_newhud_text")
					local nLabelWidth, nLabelHeight = surface.GetTextSize("ALT")
					surface.SetFont("oc_WeaponIconsSmall")
					local iw, ih = surface.GetTextSize(ammoCharsWeps[wep:GetClass()] or ammoChars[game.GetAmmoName(wep:GetSecondaryAmmoType()):lower()] or "")
					local ix = ScreenScaleH(25) + (nLabelWidth - iw) / 2
					local iy = ScreenScaleH(44) - (nLabelHeight + (ih / 2))

					draw.SimpleText(ammoCharsWeps[wep:GetClass()] or ammoChars[game.GetAmmoName(wep:GetSecondaryAmmoType()):lower()] or "", "oc_WeaponIconsSmall", x + ix, y + iy, ColorAlpha(self:GetHUDTextColor(), 196), TEXT_ALIGN_LEFT)
				end
			end
			render.SetScissorRect(0, 0, 0, 0, false)
		end
	end
end

function GM:HUDDrawSecondaryAmmo()
	if oc_hud_mode:GetInt() == 0 then
		local veh = me:GetVehicle()
		if not IsValid(veh) then
			local wep = me:GetActiveWeapon()
			if wep:IsValid() and wep:GetSecondaryAmmoType() ~= -1 then
				local x, y = ScrW() - ScreenScaleH(76), ScreenScaleH(430)
				local w = ScreenScaleH(60)
				local h = ScreenScaleH(38)

				draw.RoundedBox(6, x, y, w, h, ColorAlpha(self:GetHUDBGColor(), 80))

				if me:GetAmmoCount(wep:GetSecondaryAmmoType()) ~= am2_old then
					am2_old = me:GetAmmoCount(wep:GetSecondaryAmmoType())
					am2_glowa = 255
				end

				am2_glowa = Lerp(RealFrameTime(), am2_glowa, 0)

				render.SetScissorRect(x, y, x + w, y + h, true)
				draw.SimpleText(me:GetAmmoCount(wep:GetSecondaryAmmoType()), "oc_hudnumbers_glow", x + ScreenScaleH(10), y + ScreenScaleH(1), ColorAlpha(self:GetHUDTextColor(), am2_glowa))
				draw.SimpleText(me:GetAmmoCount(wep:GetSecondaryAmmoType()), "oc_hudnumbers", x + ScreenScaleH(10), y + ScreenScaleH(1), ColorAlpha(self:GetHUDTextColor(), 196))
				render.SetScissorRect(0, 0, 0, 0, false)
			end
		end
	end
	-- new hud note: we have have to most likely handle in primary ammo for animations :/
end

function GM:HUDDrawScore()
	if oc_hud_mode:GetInt() == 0 then
		local x, y = ScreenScaleH(2), ScreenScaleH(2)
		local w = ScreenScaleH(55)
		local h = ScreenScaleH(20)

		if me:Frags() ~= s_old then
			s_delta = me:Frags() - s_old
			s_old = me:Frags()
			s_glowa = 255
		end

		s_glowa = Lerp(RealFrameTime(), s_glowa, 0)

		render.SetScissorRect(x, y, x + w, y + h, true)
		draw.RoundedBox(6, x, y, w, h, ColorAlpha(self:GetHUDBGColor(), 80))
		draw.SimpleText("Score:", "oc_tinyfont", x + ScreenScaleH(4), y + ScreenScaleH(2), self:GetHUDTextColor())
		draw.SimpleText((s_delta > 0 and "+" or "") .. s_delta, "oc_scorefont", x + ScreenScaleH(28), y + ScreenScaleH(2), ColorAlpha(s_delta > 0 and green or red, s_glowa))
		draw.SimpleText(string.format("%.8d", me:Frags()), "oc_scorefont", x + ScreenScaleH(4), y + ScreenScaleH(10), self:GetHUDTextColor())
		render.SetScissorRect(0, 0, 0, 0, false)
	else
		local x, y = ScreenScaleH(2), ScreenScaleH(2)
		local w = (mp_livesmode:GetInt() > 0 or mp_teamplay:GetBool()) and ScreenScaleH(192) or ScreenScaleH(128)
		local h = ScreenScaleH(55)

		if me:Frags() ~= s_old then
			s_delta = me:Frags() - s_old
			s_old = me:Frags()
			s_glowa = 255
		end

		s_glowa = Lerp(RealFrameTime(), s_glowa, 0)

		render.SetScissorRect(x, y, x + w, y + h, true)
		surface.SetMaterial(new_score)
		surface.SetDrawColor(self:GetHUDBGColor())
		surface.DrawTexturedRect(x, y, w, h)

		draw.SimpleText(me:Frags(), "oc_hudnumbers_new_small", x + ScreenScaleH(28), y + ScreenScaleH(25), ColorAlpha(self:GetHUDTextColor(), 196))
		draw.SimpleText("SCORE", "oc_newhud_text", x + ScreenScaleH(28), y + ScreenScaleH(40), ColorAlpha(self:GetHUDTextColor(), 196))
		render.SetScissorRect(0, 0, 0, 0, false)

		draw.SimpleText((s_delta > 0 and "+" or "") .. s_delta, "oc_hudnumbers_new_small", x + ScreenScaleH(28), y + ScreenScaleH(56), ColorAlpha(s_delta > 0 and green or red, s_glowa))
	end
end

function GM:HUDDrawLives()
	local curLives = math.max(me:Lives(), 0)

	if oc_hud_mode:GetInt() == 0 then
		local x, y = ScreenScaleH(108), ScreenScaleH(431)
		local w, h = ScreenScaleH(40), ScreenScaleH(39)

		render.SetScissorRect(x, y, x + w, y + h, true)
		draw.RoundedBox(6, x, y, w, h, ColorAlpha(self:GetHUDBGColor(), 80))
		draw.SimpleText("LIVES", "oc_tinyfont", x + ScreenScaleH(11), y + ScreenScaleH(2), self:GetHUDTextColor())
		draw.SimpleText(curLives, "oc_hudnumbers", x + ScreenScaleH(6), y + ScreenScaleH(8), ColorAlpha(self:GetHUDTextColor(), 196))
		render.SetScissorRect(0, 0, 0, 0, false)
	else
		local x, y = ScreenScaleH(2), ScreenScaleH(2)

		if curLives ~= l_old then
			l_delta = curLives - l_old
			l_old = curLives
			l_glowa = 255
		end

		l_glowa = Lerp(RealFrameTime(), l_glowa, 0)

		draw.SimpleText(curLives, "oc_hudnumbers_new_small", x + ScreenScaleH(100), y + ScreenScaleH(25), ColorAlpha(self:GetHUDTextColor(), 196))
		draw.SimpleText("LIVES", "oc_newhud_text", x + ScreenScaleH(100), y + ScreenScaleH(40), ColorAlpha(self:GetHUDTextColor(), 196))

		draw.SimpleText((l_delta > 0 and "+" or "") .. l_delta, "oc_hudnumbers_new_small", x + ScreenScaleH(100), y + ScreenScaleH(56), ColorAlpha(l_delta > 0 and green or red, l_glowa))
	end
end

local HUD_HIDE = {
	CHudHealth        = true,
	CHudBattery       = true,
	CHudAmmo          = true,
	CHudSecondaryAmmo = true,
	CHudSuitPower     = true
}
local HUD_HIDE_DEAD = {
	OCHudHealth = true,
	OCHudBattery = true,
	OCHudAmmo = true,
	OCHudSecondaryAmmo = true,
	OCHudTeam = true,
	OCHudLives = true,
}
function GM:HUDShouldDraw(el)
	if HUD_HIDE[el] then return false end

	if not IsValid(me) then
		me = LocalPlayer()
		return
	end

	local wep = me:GetActiveWeapon()
	if el == "CHUDQuickInfo" and (IsValid(wep) and wep.IsOCWeapon and wep:GetZoomLevel() > 0) then return false end

	if el == "CHudCrosshair" or el == "CHUDQuickInfo" then
		if me:IsFlagSet(FL_FROZEN) then
			drawCrosshair = false
			return
		end

		if IsValid(wep) and wep:IsWeapon() then
			if wep.IsOCWeapon and wep:GetZoomLevel() > 0 then
				drawCrosshair = false
				return
			end

			if not wep.DoDrawCrosshair and wep.ShouldDrawCrosshair then
				drawCrosshair = wep:ShouldDrawCrosshair() or true
				return
			end
		end

		if IsValid(me:GetVehicle()) then
			drawCrosshair = false
			return
		end

		drawCrosshair = true
		return false
	end

	if not me:Alive() then
		drawCrosshair = false
		if HUD_HIDE_DEAD[el] then return false end
	end

	return true
end

local COLOR_FRIENDLY = Color(128, 255, 128)
local COLOR_ENEMY = Color(255, 128, 128)
local COLOR_UNKNOWN = Color(156, 156, 156)

local DISPOSITION_MAP = {
	[D_ER] = {COLOR_UNKNOWN, "Unknown"},
	[D_HT] = {COLOR_ENEMY, "Enemy"},
	[D_FR] = {COLOR_ENEMY, "Enemy"},
	[D_LI] = {COLOR_FRIENDLY, "Friendly"},
	[D_NU] = {COLOR_ENEMY, "Enemy"},
}

local ALPHA_FADE_DURATION = 1
local t_a = 0
local t_alphaFadeStart = 0
local t_lastEnt = NULL
local function do_we_care_about_this(ent)
	if ent:IsValid() then
		if ent:IsNPC() or ent:IsPlayer() then
			return true
		end
		if ent:IsVehicle() and IsValid(ent:GetDriver()) then
			return true
		end
		if ent:GetClass() == "func_breakable" then
			return true
		end
	end

	return false
end

local hud_showtargetid = GetConVar("hud_showtargetid")
function GM:HUDDrawTargetID()
	if not hud_showtargetid:GetBool() then return end

	if not IsValid(me) then
		me = LocalPlayer()
	end

	local trace = util.TraceLine({
		filter = me,
		start = me:GetShootPos(),
		endpos = me:GetShootPos() + (me:GetAimVector() * 10000),
		mask = MASK_SHOT
	})

	local curtime = CurTime()

	if IsValid(trace.Entity) and do_we_care_about_this(trace.Entity) then
		t_lastEnt = trace.Entity

		if trace.Entity:IsVehicle() and IsValid(trace.Entity:GetDriver()) then
			t_lastEnt = trace.Entity:GetDriver()
		end

		if t_lastEnt ~= me then
			t_a = 255
		end

		t_alphaFadeStart = curtime
	elseif t_a > 0 then
		local timeLeft = t_alphaFadeStart + ALPHA_FADE_DURATION - curtime
		local frac = math.Clamp(timeLeft / ALPHA_FADE_DURATION, 0, 1)
		local eased = math.ease.OutQuad(1 - frac)
		t_a = Lerp(eased, 255, 0)
	end

	surface.SetFont("oc_targetid")
	local _, fh = surface.GetTextSize("W")
	local x, y = 20, ScrH() / 2
	if IsValid(t_lastEnt) and t_lastEnt ~= me then
		local ent = t_lastEnt
		local disp = DISPOSITION_MAP[D_ER]
		local color = disp[1]
		local name
		local points = -1

		if ent:IsNPC() then
			points = ent:GetNW2Int("oc_frags", 0)

			if ent:GetClass() == "npc_merchant" then
				name = ent:GetMerchantName()
				disp = DISPOSITION_MAP[D_LI]
			else
				name = ent:GetNW2String("displayname", language.GetPhrase(ent:GetClass()))
			end

			name = name or language.GetPhrase(ent:GetClass())
			disp = DISPOSITION_MAP[ent:Disposition(me)] or DISPOSITION_MAP[D_ER]
			color = disp[1]
		elseif ent:IsPlayer() then
			name = ent:Nick()
			points = ent:Frags()
			disp = me:Team() == ent:Team() and DISPOSITION_MAP[D_LI] or DISPOSITION_MAP[D_HT]
			color = self:GetTeamColor(ent)
		else
			name = ent:GetNW2String("displayname", "Breakable")
			points = 0
			disp = {COLOR_UNKNOWN, ent:GetNW2Bool("oc_breakable_trigger", false) and "Unbreakable" or "Breakable"}
			color = disp[1]
		end

		local healthStr = string.format("%d/%d", math.max(0, ent:Health()), ent:GetMaxHealth())
		if oc_targetid_usepercent:GetBool() then
			healthStr = string.format("%d%%", math.Clamp(ent:Health() / ent:GetMaxHealth() * 100, 0, 100))
		end

		draw.SimpleText(string.format("Name: %s", name), "oc_targetid", x, y - (fh / 2) - fh, ColorAlpha(color, t_a))
		draw.SimpleText(string.format("Health: %s", healthStr), "oc_targetid", x, y - (fh / 2), ColorAlpha(color, t_a))
		draw.SimpleText(string.format("Points: %d", points), "oc_targetid", x, y + (fh / 2), ColorAlpha(color, t_a))
		draw.SimpleText(disp[2], "oc_targetid", x, y + (fh / 2) + fh, ColorAlpha(color, t_a))
	end
end

local VEH = FindMetaTable"Vehicle"

function VEH:GetDriver()
	for _, e in ipairs(self:GetChildren()) do
		if e:IsPlayer() then return e end
	end
end

local UndecorateNick = UndecorateNick or function(s) return s end

function GM:AddEntityWaypoint(ent, text, icon, duration)
	self.HUDWaypoints[ent:EntIndex()] = {
		image = Material("sprites/attention"),
		text = text or ent:GetClass(),
		endtime = CurTime() + math.min(duration or 5, 10),
		parent = ent,
	}
end

concommand.Add("locate_players", function(ply)
	local iMyTeam = ply:Team()
	for _, other in player.Iterator() do
		if other == ply then continue end
		if not other:Alive() then continue end
		if other:Team() ~= iMyTeam then continue end

		GAMEMODE:AddEntityWaypoint(other, UndecorateNick(other:Nick()), "sprites/attention", 5)
	end
end)
