AddCSLuaFile()

function GM:IsPlayerSpectator(ply)
	if not IsValid(ply) then return false end

	if not ply:Alive() or ply:Team() == TEAM_SPECTATOR then
		if ply:GetObserverMode() ~= OBS_MODE_NONE then
			return true
		end
	end

	return false
end

if CLIENT then
	local disable = {
		--OCTargetID = true,
		OCHudHealth = true,
		OCHudBattery = true,
		OCHudAmmo = true,
		OCHudSecondaryAmmo = true,
		OCHudScore = true,
		OCHudTeam = true,
		OCHudLives = true,
	}

	hook.Add("HUDShouldDraw", "oc_spectate", function(name)
		local lply = LocalPlayer()

		if GAMEMODE:IsPlayerSpectator(lply) and disable[name] then
			return false
		end
	end)
else
	util.AddNetworkString("oc_spectate")

	local mp_teamplay = GetConVar("oc_teamplay")
	local mp_livesmode = GetConVar("mp_livesmode")
	net.Receive("oc_spectate", function(len, ply)
		if not ply or not IsValid(ply) then return end

		if ply.lastteamttime and ply.lastteamttime > CurTime() then
			--ply:ChatPrint("Cannot change teams that fast.")
			return
		end

		if ply:Team() == TEAM_SPECTATOR then
			-- nice try
			if GAMEMODE:PlayerIsRespawnSpectator(ply) then return end

			if mp_teamplay:GetBool() then
				if #team.GetPlayers(TEAM_BLUE) > #team.GetPlayers(TEAM_RED) then
					ply:SetTeam(TEAM_RED)
				else
					ply:SetTeam(TEAM_BLUE)
				end
			else
				ply:SetTeam(TEAM_COOP)
			end

			ply:UnSpectate()
			ply:Spawn()
			ply.manual_spectate = false
		else
			if ply:Alive() then
				ply:Kill()
			end

			GAMEMODE:PlayerSpawnAsSpectator(ply)
			local specList = GAMEMODE:GetPlayerSpectateList(ply)

			ply:Spectate(OBS_MODE_CHASE)
			ply:SpectateEntity(specList[math.random(1, #specList)])
			ply.manual_spectate = true
		end

		ply.lastteamttime = CurTime() + 0.1
	end)

	function GM:PlayerIsRespawnSpectator(ply)
		local bRespawnSpectator = false
		if mp_livesmode:GetInt() > 0 then
			bRespawnSpectator = ply:Lives() <= 0
		end

		if not bRespawnSpectator then
			bRespawnSpectator = self:PlayerSelectSpawn(ply, false) == nil
		end

		return bRespawnSpectator
	end

	function GM:PlayerDeathThink(pl)
		if pl.NextSpawnTime and pl.NextSpawnTime > CurTime() then return end
		if pl:Team() == TEAM_SPECTATOR and pl.manual_spectate then return end

		local bRespawnSpectator = self:PlayerIsRespawnSpectator(pl)

		--print(pl, "bRespawnSpectator", bRespawnSpectator)

		if pl:IsBot() or pl:KeyPressed(bit.bor(IN_ATTACK, IN_ATTACK2, IN_JUMP)) then
			if not bRespawnSpectator then
				pl:Spawn()
			else
				pl:Spectate(OBS_MODE_CHASE)
			end
		end
	end

	local function GetParent(ent)
		local parent = ent:GetParent()

		if parent and parent:IsValid() then
			return GetParent(parent)
		end

		return ent
	end
	local ClassHandler = {
		npc_bullseye = false,
		npc_enemyfinder = false,
		--npc_furniture = false,
		--npc_grenade_frag = false,
		npc_merchant = false,
		npc_apcdriver = false,
	}
	function GM:GetPlayerSpectateList(ply)
		local tospec = player.GetAll()
		table.RemoveByValue(tospec, ply)

		for _, e in ents.Iterator() do
			local classname = e:GetClass()

			if e:IsNPC() and (ClassHandler[classname] == nil or ClassHandler[classname] ~= false) then
				table.insert(tospec, e)
			elseif classname == "point_camera" then
				table.insert(tospec, e)
			end
		end

		return tospec
	end

	local PLAYER = FindMetaTable("Player")

	PLAYER.oldSpectate = PLAYER.oldSpectate or PLAYER.Spectate
	function PLAYER:Spectate(mode)
		self:oldSpectate(mode)

		-- NPCs should never see spectators. A workaround for the fact that gmod NPCs
		-- do not ignore them by default.
		self:SetNoTarget(true)

		if mode == OBS_MODE_ROAMING then
			self:SetMoveType(MOVETYPE_NOCLIP)
		end
	end

	PLAYER.oldSpectateEntity = PLAYER.oldSpectateEntity or PLAYER.SpectateEntity
	function PLAYER:SpectateEntity(ent)
		self:oldSpectateEntity(ent)

		if IsValid(ent) and ent:IsPlayer() then
			self:SetupHands(ent)
		end
	end

	PLAYER.oldUnSpectate = PLAYER.oldUnSpectate or PLAYER.UnSpectate
	function PLAYER:UnSpectate()
		self:oldUnSpectate()
		self:SetNoTarget(false)
	end
end
