AddCSLuaFile()

if CLIENT then
	language.Add("GaussEnergy_ammo", "Uranium")
end

sound.Add({
	name = "Weapon_Gauss.Draw",
	channel = CHAN_ITEM,
	volume = 0.5,
	level = 65,
	sound = "weapons/gauss/gauss_deploy.wav"
})
sound.Add({
	name = "Weapon_Gauss.FidgetSpin",
	channel = CHAN_ITEM,
	volume = 0.5,
	level = 65,
	sound = "weapons/gauss/gauss_fidget.wav"
})

--[[sound.Add({
    name = "Weapon_Generic.Movement2",
    channel = CHAN_AUTO,
    volume = {0.05, 0.1},
    level = 65,
    pitch = {98, 101},
    sound = "weapons/movement/weapon_movement2.wav"
})
sound.Add({
    name = "Weapon_Generic.Movement4",
    channel = CHAN_AUTO,
    volume = {0.05, 0.1},
    level = 65,
    pitch = {98, 101},
    sound = "weapons/movement/weapon_movement4.wav"
})
sound.Add({
    name = "Weapon_Generic.Movement5",
    channel = CHAN_AUTO,
    volume = {0.05, 0.1},
    level = 65,
    pitch = {98, 101},
    sound = "weapons/movement/weapon_movement5.wav"
})]]

SWEP.Base = "oc_base"
SWEP.PrintName = "#HL2_TAUCANNON"
SWEP.Category = "Obsidian Conflict"

DEFINE_BASECLASS(SWEP.Base)

SWEP.Spawnable = true
SWEP.AdminOnly = false
SWEP.UseHands = true
SWEP.ViewModelFOV = 54

SWEP.Slot = 2
SWEP.SlotPos = 2

SWEP.AnimSet = 3

SWEP.ViewModel = Model"models/weapons/taummod/c_gauss.mdl"
SWEP.WorldModel = Model"models/weapons/taummod/w_gauss.mdl"

SWEP.Primary.Ammo = "GaussEnergy"
SWEP.Primary.DefaultClip = 150
SWEP.Primary.ClipSize = -1
SWEP.Primary.Automatic = true

SWEP.Secondary.Ammo = "none"
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.ClipSize = -1
SWEP.Secondary.Automatic = false

local MAX_GAUSS_CHARGE_TIME = 3
local GAUSS_CHARGE_AMMO_BURN_INTERVAL = 0.15
local MAX_TRACE_LENGTH = 1.732050807569 * (2 * 16384)

SWEP.Char = "h"
SWEP.CharFont = "WeaponIcons"
SWEP.SelectedChar = "h"
SWEP.SelectedCharFont = "WeaponIconsSelected"

-- gauss gun performs its own attack handling
function SWEP:PrimaryAttack() end

function SWEP:SecondaryAttack() end

function SWEP:SetupDataTables()
	BaseClass.SetupDataTables(self)

	self:NetworkVar("Float", 3, "CannonTime")
	self:NetworkVar("Float", 4, "CannonChargeStartTime")
	self:NetworkVar("Bool", 2, "CannonCharging")
end

function SWEP:PerformIdleAnimation()
	if self:GetCannonCharging() then
		self:SetWeaponAnim(ACT_VM_PULLBACK)
	else
		local randVal = math.Round(util.SharedRandom("GaussIdle", 0, 7, self:EntIndex()))

		if randVal == 0 then
			self:SetWeaponSequence("inspect1")
		else
			self:SetWeaponAnim(ACT_VM_IDLE)
		end
	end
end

function SWEP:FireCannon()
	if self:GetCannonTime() > CurTime() then return end
	if self:Ammo1() == 0 then return end

	self:EmitSound("PropJeep.FireCannon")

	local iShotsFired = (self:GetShotsFired() % 3) + 1
	self:SetShotsFired(iShotsFired)

	if iShotsFired == 1 then
		self:SetWeaponAnim(ACT_VM_PRIMARYATTACK)
	elseif iShotsFired == 2 then
		self:SetWeaponAnim(ACT_VM_RECOIL1)
	elseif iShotsFired == 3 then
		self:SetWeaponAnim(ACT_VM_RECOIL2)
	end

	local owner = self:GetOwner()

	owner:SetAnimation(PLAYER_ATTACK1)

	self:SetCannonTime(CurTime() + 0.2)
	self:SetCannonCharging(false)

	local vecSrc = owner:GetShootPos()
	local vecDir = (owner:GetAimVector():Angle() + owner:GetViewPunchAngles()):Forward()

	local damage = 15

	owner:FireBullets({
		Src = vecSrc,
		Dir = vecDir,
		Num = 1,
		TracerName = "",
		Damage = damage,
		AmmoType = self.Primary.Ammo,
		Attacker = owner,
		Spread = Vector(0.00873, 0.00873, 0.00873),   --VECTOR_CONE_1DEGREES
		Distance = MAX_TRACE_LENGTH,
		Callback = function(attacker, tr, dmg)
			self:DrawBeam(owner:GetShootPos(), tr.HitPos, 2.4, 1)

			local hitEnt = tr.Entity
			if hitEnt:IsWorld() or hitEnt:IsValid() and not (hitEnt:IsPlayer() or hitEnt:IsNPC() or hitEnt:IsNextBot()) then
				local n = math.abs(vecDir:Dot(tr.HitNormal))

				if (n < 0.5) then -- 60 degrees
					local vecReflect = vecDir + (tr.HitNormal * 2.0 * n)

					vecDir:Set(vecReflect)
					vecSrc:Set(tr.HitPos) -- + vecDir * 8

					-- explode a bit
					local dmginfo = DamageInfo()
					dmginfo:SetInflictor(self)
					dmginfo:SetAttacker(owner)
					dmginfo:SetDamage(damage * n)
					dmginfo:SetDamageType(DMG_GENERIC)
					util.BlastDamageInfo(dmginfo, tr.HitPos, damage * n * 2.5)

					-- lose energy
					if (n == 0) then
						n = 0.1
					end
					damage = damage * (1 - n)

					owner:FireBullets({
						Src = vecSrc,
						Dir = vecDir,
						Num = 1,
						TracerName = "GaussTracer",
						Damage = damage,
						AmmoType = self.Primary.Ammo,
						Attacker = owner,
						Spread = Vector(0.00873, 0.00873, 0.00873),   --VECTOR_CONE_1DEGREES
						Distance = MAX_TRACE_LENGTH,
						Callback = function(attacker, tr, dmg)
							self:DrawBeam(tr.StartPos, tr.HitPos, 2.4, -1)
						end
					})

					return {effects = false, damage = true}
				end
			end
		end
	})

	self:TakePrimaryAmmo(1)
end

function SWEP:ChargeCannon()
	if self:GetCannonTime() > CurTime() then return end
	if self:Ammo1() == 0 then
		if self:GetCannonCharging() then
			self:FireChargedCannon()
		end

		return
	end

	if not self:GetCannonCharging() then
		self:SetCannonChargeStartTime(CurTime())
		self:SetCannonCharging(true)

		self:SetWeaponAnim(ACT_VM_PULLBACK)

		if not self.ChargeSound then
			self.ChargeSound = CreateSound(self:GetOwner(), Sound("Jeep.GaussCharge"))
		end
		if IsFirstTimePredicted() then
			self.ChargeSound:PlayEx(.7, 110)
		end
	else
		local chargeAmount = (CurTime() - self:GetCannonChargeStartTime()) / MAX_GAUSS_CHARGE_TIME
		local pitch = (CurTime() - self:GetCannonChargeStartTime()) * (100 / MAX_GAUSS_CHARGE_TIME) + 100
		if chargeAmount > 1 then
			chargeAmount = 1
		end
		if pitch > 250 then
			pitch = 250
		end

		self.ChargeSound:ChangePitch(pitch)
	end
end

function SWEP:FireChargedCannon()
	local owner = self:GetOwner()

	self:SetWeaponAnim(ACT_VM_SECONDARYATTACK)
	self:GetOwner():SetAnimation(PLAYER_ATTACK1)

	self:SetCannonCharging(false)
	self:SetCannonTime(CurTime() + 0.5)

	self:StopChargeSound()

	self:EmitSound("PropJeep.FireChargedCannon")

	local vecSrc = owner:GetShootPos()
	local vecDir = owner:GetAimVector()

	local chargeAmount = (CurTime() - self:GetCannonChargeStartTime()) / MAX_GAUSS_CHARGE_TIME
	if chargeAmount > 1 then chargeAmount = 1 end

	-- original gauss gun would remove ammo on an interval, and fire automatically when reaching 0 ammo
	-- we remove ammo on fire instead, and use the charge amount to determine damage
	-- this causes a difference in the damage calculated, so let's account for that change here
	local iRemoveAmmo = math.floor((chargeAmount * MAX_GAUSS_CHARGE_TIME) / GAUSS_CHARGE_AMMO_BURN_INTERVAL)
	local iMaxRemove = math.floor(MAX_GAUSS_CHARGE_TIME / GAUSS_CHARGE_AMMO_BURN_INTERVAL)
	iRemoveAmmo = math.Clamp(iRemoveAmmo, 1, owner:GetAmmoCount(self:GetPrimaryAmmoType()))

	self:TakePrimaryAmmo(iRemoveAmmo)
	chargeAmount = iRemoveAmmo / iMaxRemove

	local damage = 15 + ((250 - 15) * chargeAmount)

	owner:FireBullets({
		Src = vecSrc,
		Dir = vecDir,
		Num = 1,
		TracerName = "",
		Damage = damage,
		AmmoType = self.Primary.Ammo,
		Attacker = owner,
		Spread = vector_origin,
		Distance = MAX_TRACE_LENGTH,
		Callback = function(attacker, tr, dmg)
			self:DrawBeam(owner:GetShootPos(), tr.HitPos, 9.6, 1)

			local hitEnt = tr.Entity
			if hitEnt:IsWorld() or hitEnt:IsValid() and not (hitEnt:IsPlayer() or hitEnt:IsNPC() or hitEnt:IsNextBot()) then
				local n = math.abs(vecDir:Dot(tr.HitNormal))

				if n < 0.5 then -- reflect if hit surface at least than 60 degrees
					local vecReflect = vecDir + (tr.HitNormal * 2.0 * n)

					-- lose energy
					damage = damage * (1 - n)

					owner:FireBullets({
						Src = tr.HitPos,
						Dir = vecReflect,
						Num = 1,
						TracerName = "",
						Damage = damage,
						AmmoType = self.Primary.Ammo,
						Attacker = owner,
						Spread = vector_origin,
						Distance = MAX_TRACE_LENGTH * (1 - tr.Fraction),
						Callback = function(_, tr2, dmg2)
							self:DrawBeam(tr2.StartPos, tr2.HitPos, 9.6, -1)
						end
					})

					-- explode a bit
					local dmginfo = DamageInfo()
					dmginfo:SetInflictor(self)
					dmginfo:SetAttacker(owner)
					dmginfo:SetDamage(damage * n)
					dmginfo:SetDamageType(DMG_GENERIC)
					dmginfo:SetReportedPosition(tr.StartPos)
					dmginfo:SetDamagePosition(tr.HitPos)
					util.BlastDamageInfo(dmginfo, tr.HitPos, damage * n * 2.5)

					return {effects = false, damage = true}
				elseif tr.HitWorld and not tr.HitSky then -- attempt to wall penetrate
					local impactfx = EffectData()
					impactfx:SetOrigin(tr.HitPos)
					impactfx:SetNormal(tr.HitNormal)

					local testPos = tr.HitPos + (tr.Normal * 48)
					local trPenetrate = util.TraceLine({
						start = testPos,
						endpos = tr.HitPos,
						filter = owner,
						mask = MASK_SHOT
					})

					if not trPenetrate.AllSolid then
						local flDistPenetrated = trPenetrate.HitPos:Distance(trPenetrate.StartPos)

						-- lose energy
						damage = math.max(damage - flDistPenetrated, 0)

						if damage > 0 then
							-- effect
							if IsFirstTimePredicted() then
								util.Decal("RedGlowFade", trPenetrate.StartPos, trPenetrate.HitPos + (trPenetrate.Normal * 1.1), {self, owner})

								local data = EffectData()
								data:SetOrigin(trPenetrate.HitPos)
								data:SetStart(trPenetrate.StartPos)
								data:SetSurfaceProp(tr.SurfaceProps)
								data:SetNormal(trPenetrate.HitNormal)
								data:SetDamageType(DMG_GENERIC)
								data:SetHitBox(trPenetrate.HitBox)
								if CLIENT then
									data:SetEntity(trPenetrate.Entity)
								else
									data:SetEntIndex(trPenetrate.Entity:EntIndex())
								end

								local filter
								if SERVER then
									filter = RecipientFilter()
									filter:AddPVS(trPenetrate.HitPos)

									if not game.SinglePlayer() and owner:IsPlayer() then
										filter:RemovePlayer(owner)
									end
								end

								util.Effect("Impact", data, nil, filter)
							end

							owner:FireBullets({
								Src = trPenetrate.HitPos,
								Dir = vecDir,
								Num = 1,
								TracerName = "",
								Damage = damage,
								AmmoType = self.Primary.Ammo,
								Attacker = owner,
								Spread = vector_origin,
								Distance = MAX_TRACE_LENGTH * (1 - tr.Fraction),
								Callback = function(_, tr2, dmg2)
									self:DrawBeam(tr2.StartPos, tr2.HitPos, 9.6, -1)
								end
							})
						end
					end
				end
			end
		end
	})

	if IsFirstTimePredicted() then
		local vel = -owner:GetAimVector() * (damage * 5)
		owner:SetVelocity(vel)
	end
end

function SWEP:DrawBeam(startPos, endPos, width, att)
	local fFirstBeam = true

	if IsFirstTimePredicted() then
		local beam = EffectData()
		beam:SetEntity(self)

		if fFirstBeam then
			beam:SetFlags(1) --from viewmodel
			fFirstBeam = false
		else
			beam:SetFlags(0) --in world
		end

		beam:SetStart(startPos)
		beam:SetOrigin(endPos)
		beam:SetAttachment(att)
		beam:SetScale(width)

		local filter

		if SERVER then
			filter = RecipientFilter()
			filter:AddPVS(startPos)
			filter:AddPVS(endPos)

			local owner = self:GetOwner()

			if not game.SinglePlayer() and owner:IsPlayer() then
				filter:RemovePlayer(owner)
			end
		end

		util.Effect("oc_gaussbeam", beam, nil, filter)
	end
end

function SWEP:Think()
	if CLIENT and game.SinglePlayer() then return end

	local owner = self:GetOwner()

	if owner:KeyDown(IN_ATTACK) then
		if self:GetCannonCharging() then
			self:FireChargedCannon()
		else
			self:FireCannon()
		end
	elseif owner:KeyDown(IN_ATTACK2) then
		self:ChargeCannon()
	elseif not owner:KeyDown(IN_ATTACK2) and self:GetCannonCharging() then
		self:FireChargedCannon()
	else
		self:WeaponIdle()
	end
end

function SWEP:OnDrop()
	self:StopChargeSound()
end

function SWEP:Holster()
	self:StopChargeSound()

	self:SetCannonCharging(false)

	local owner = self:GetOwner()
	if not owner:IsValid() then return true end

	local vm = owner:GetViewModel()
	if not IsValid(vm) then return true end

	vm:ManipulateBoneAngles(5, Angle(0, 0, 0))
	vm:ManipulateBoneAngles(6, Angle(0, 0, 0))

	return true
end

function SWEP:StopChargeSound()
	if self.ChargeSound then
		self.ChargeSound:Stop()
	end
end

function SWEP:OnRemove()
	self:StopChargeSound()
end
