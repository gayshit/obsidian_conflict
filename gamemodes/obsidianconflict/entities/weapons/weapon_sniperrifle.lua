-- v_sniper.mdl

SWEP.PrintName = "#HL2_SNIPERRIFLE"
SWEP.Base = "oc_base"
SWEP.Spawnable = true
SWEP.ViewModel = "models/weapons/v_sniper.mdl"
SWEP.WorldModel = "models/weapons/w_sniper.mdl"
SWEP.Category = "Obsidian Conflict"

DEFINE_BASECLASS"oc_base"

SWEP.Char = "C"
SWEP.CharFont = "ObsidianWeaponIcons"
SWEP.SelectedChar = "C"
SWEP.SelectedCharFont = "ObsidianWeaponIconsSelected"

SWEP.Slot = 3

SWEP.ViewModelFOV = 54

SWEP.Primary.ClipSize = 1
SWEP.Primary.DefaultClip = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "SniperRound"
SWEP.Primary.Damage = 32

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Ammo = "none"

SWEP.SoundData = {
	reload = "Weapon_SniperRifle.Reload"
}

function SWEP:DoImpactEffect(tr, dmg)
	local data = EffectData()
	data:SetOrigin(tr.HitPos + (tr.HitNormal * 1))
	data:SetNormal(tr.HitNormal)

	util.Effect("AR2Impact", data)
end

function SWEP:Initialize()
	self:SetHoldType("ar2")
	self:SetDeploySpeed(1)
end

function SWEP:PrimaryAttack()
	if not self:CanPrimaryAttack() then return end
	---@class Player
	local owner = self:GetOwner()

	owner:FireBullets({
		Src = owner:GetShootPos(),
		Dir = (owner:GetAimVector():Angle() + owner:GetViewPunchAngles()):Forward(),
		Spread = Vector(0, 0, 0),
		Num = 1,
		Damage = self.Primary.Damage,
		TracerName = "AR2Tracer",
		Callback = function(ent, tr, dmg)
			-- this fixes oc_nerv's flamethrow turret
			dmg:SetDamageType(DMG_BULLET)
		end
	})

	if owner:IsPlayer() then
		owner:ViewPunchReset(2)

		local angles = owner:EyeAngles()
		angles.x = angles.x + util.SharedRandom("KickBackX", -4, 4)
		angles.y = angles.y + util.SharedRandom("KickBackY", -4, 4)
		angles.z = 0

		if (game.SinglePlayer() and SERVER) or (not game.SinglePlayer() and CLIENT and IsFirstTimePredicted()) then
			owner:SetEyeAngles(angles)
		end

		owner:ViewPunch(Angle(util.SharedRandom("sniperpax", -1, -3), util.SharedRandom("sniperpay", -.2, .2), 0) * .5)
	end

	self:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
	self:EmitSound"Weapon_SniperRifle.Single"

	owner:SetAnimation(PLAYER_ATTACK1)
	owner:MuzzleFlash()

	self:SetShotsFired(self:GetShotsFired() + 1)

	self:TakePrimaryAmmo(1)
	self:SetNextPrimaryFire(CurTime() + .075)
end

function SWEP:SecondaryAttack()
	---@class Player
	local owner = self:GetOwner()
	self:SetZoomLevel(self:GetZoomLevel() + 1)
	self:SetHoldType("rpg")

	if self:GetZoomLevel() > 2 then
		self:SetZoomLevel(0)
		self:SetHoldType("ar2")
		owner:SetCanZoom(true)
	else
		owner:SetCanZoom(false)
	end

	owner:SetFOV(self.ZoomLevels[self:GetZoomLevel()], 0)
	self:SetNextSecondaryFire(CurTime() + .5)
end

if CLIENT then
	local mat_LaserTrail = CreateMaterial("oc_sniperbeam", "Sprite", util.KeyValuesToTable([[
    "Sprite"
    {
        "$basetexture" "sprites/laserbeam"
        "$vertexalpha" "1"
        "$translucent" "1"
        "$vertexcolor" "1"
        "$nocull" "1"
        "$spriterendermode" "5"
    }
    ]]))
	local mat_LaserDot = CreateMaterial("oc_laserhit", "Sprite", util.KeyValuesToTable([[
    "Sprite"
    {
        "$basetexture" "sprites/physgun_glow"

        "$spriteorientation" "vp_parallel"
        "$spriteorigin" "[ 0.50 0.50 ]"
        "$vertexalpha" 1
        "$vertexcolor" 1
        "$spriterendermode" 5
        "$alpha" "0.9"
        "$nocull" "1"
    }
    ]]))

	function SWEP:PostDrawViewModel(vm, _, owner)
		self.BaseClass.PostDrawViewModel(self, vm, _, owner)

		if self:GetActivity() == ACT_VM_DRAW then return end
		if self:GetInReload() then return end

		local att = vm:LookupAttachment("laser")
		if not att then return end

		local attData = vm:GetAttachment(att)
		if not attData then return end

		local iZoomLevel = self:GetZoomLevel()
		local flLaserScale
		if iZoomLevel > 0 then
			flLaserScale = 0.01 * self:GetZoomLevel()
		else
			flLaserScale = 1
		end

		local attPos = attData.Pos
		local hitPos = util.TraceLine({
			start = owner:GetShootPos(),
			endpos = owner:GetShootPos() + (owner:GetAimVector():Angle() + owner:GetViewPunchAngles()):Forward() * 1024,
			filter = owner,
			mask = MASK_SHOT
		}).HitPos

		local col = owner:GetWeaponColor():ToColor()

		render.SetMaterial(mat_LaserTrail)
		render.DrawBeam(attPos, hitPos, 0.25 * flLaserScale, 0, 0, ColorAlpha(col, 128))
		render.DrawBeam(attPos, hitPos, 1 * flLaserScale, 0, 0, ColorAlpha(col, 32))

		render.SetMaterial(mat_LaserDot)
		render.DrawSprite(hitPos, 4 * flLaserScale, 4 * flLaserScale, col)
	end

	function SWEP:DrawWorldModel()
		self:DrawModel()

		---@class Player
		local owner = self:GetOwner()
		if not owner:IsValid() then return end

		local att = self:LookupAttachment("1")
		if not att then return end

		local attData = self:GetAttachment(att)
		if not attData then return end

		local attPos = attData.Pos
		local hitPos = util.TraceLine({
			start = owner:GetShootPos(),
			endpos = owner:GetShootPos() + (owner:GetAimVector():Angle() + owner:GetViewPunchAngles()):Forward() * 1024,
			filter = owner,
			mask = MASK_SHOT
		}).HitPos

		local col = owner:GetWeaponColor():ToColor()

		render.SetMaterial(mat_LaserTrail)
		render.DrawBeam(attPos, hitPos, 0.25, 0, 0, ColorAlpha(col, 128))
		render.DrawBeam(attPos, hitPos, 1, 0, 0, ColorAlpha(col, 32))

		render.SetMaterial(mat_LaserDot)
		render.DrawSprite(hitPos, 8, 8, col)
	end
end
