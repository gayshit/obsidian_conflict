AddCSLuaFile()

SWEP.Base = "weapon_base"
SWEP.Category = "Obsidian Conflict"
SWEP.Spawnable = false
SWEP.IsOCWeapon = true
SWEP.UseHands = false
SWEP.BounceWeaponIcon = false
SWEP.DrawWeaponInfoBox = false

DEFINE_BASECLASS("weapon_base")

SWEP.Slot = 1
SWEP.SlotPos = 8
SWEP.Weight = 2
SWEP.ViewModelFOV = 80

SWEP.AnimSet = 1
SWEP.Damage = 8

SWEP.Primary.ClipSize = 18
SWEP.Primary.DefaultClip = 18
SWEP.Primary.Type = 1
SWEP.Primary.Ammo = "pistol"
SWEP.Primary.Spread = 10
SWEP.Primary.Automatic = true

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Type = 0
SWEP.Secondary.Ammo = "None"
SWEP.Secondary.Automatic = true

SWEP.SoundData = {
	reload = "Weapon_Pistol.Reload",
	reload_npc = "Weapon_Pistol.NPC_Reload",
	empty = "Weapon_Pistol.Empty",
	single_shot = "Weapon_Pistol.Single",
	single_shot_npc = "Weapon_Pistol.NPC_Single",
	special1 = "Weapon_Pistol.Special1",
	special2 = "Weapon_Pistol.Special2",
	burst = "Weapon_Pistol.Burst"
}

SWEP.NumberOfRecoilAnims = 0

-- 1 = Pistol, 2 = AR2, 3 = crossbow, 4 = physgun, 5 = shotgun, 6 = smg1
local holdtypes = {
	"pistol",
	"ar2",
	"crossbow",
	"physgun",
	"shotgun",
	"smg"
}

-- 0 = none, 1 = normal, 2 = strider, 3 = ar2, 4 = helicopter, 5 = Gunship, 6 = Gauss, 7 = Airboat
local tracers = {
	[0] = "Tracer",
	"Tracer",
	"StriderTracer",
	"AR2Tracer",
	"HelicopterTracer",
	"GunshipTracer",
	"GaussTracer",
	"AirboatGunTracer"
}

-- 0 = none, 1 = normal, 2 = AR2, 3 = jeep, 4 = Gauss, 5 = airboat, 6 = helicopter
local impacteffects = {
	[0] = "",
	"Impact",
	"AR2Impact",
	"ImpactJeep",
	"ImpactGauss",
	"AirboatGunImpact",
	"HelicopterImpact"
}
function SWEP:DoImpactEffect(tr, dmg)
	local effect = impacteffects[self.ImpactEffect]
	if not effect or effect == "" or effect == "Impact" then return false end

	if self.ImpactEffect ~= 1 and self.ImpactEffect ~= 0 then
		local data = EffectData()
		data:SetOrigin(tr.HitPos + (tr.HitNormal * 1))
		data:SetNormal(tr.HitNormal)

		util.Effect(impacteffects[self.ImpactEffect or 0], data)
	end
end

function SWEP:SetupDataTables()
	self:NetworkVar("Int", 0, "BurstNum")
	self:NetworkVar("Int", 1, "ZoomLevel")
	self:NetworkVar("Int", 2, "ShotsFired")
	self:NetworkVar("Bool", 0, "InReload")
	self:NetworkVar("Bool", 1, "NeedPump")
	self:NetworkVar("Float", 0, "FinishReloadTime")
	self:NetworkVar("Float", 1, "BurstTime")
	self:NetworkVar("Float", 2, "WeaponIdleTime")
end

function SWEP:SetWeaponSequence(idealSequence, flPlaybackRate)
	if isstring(idealSequence) then
		idealSequence = self:LookupSequence(idealSequence)
	end

	if not idealSequence or idealSequence == -1 then return false end
	flPlaybackRate = isnumber(flPlaybackRate) and flPlaybackRate or 1

	self:SendViewModelMatchingSequence(idealSequence)
	self:SetSequence(idealSequence)

	local owner = self:GetOwner()
	if owner:IsValid() and owner:IsPlayer() then
		local vm = owner:GetViewModel()
		if vm:IsValid() then
			vm:SendViewModelMatchingSequence(idealSequence)
			vm:SetPlaybackRate(flPlaybackRate)
		end
	end

	-- Set the next time the weapon will idle
	self:SetWeaponIdleTime(CurTime() + (self:SequenceDuration(idealSequence) * flPlaybackRate))
	return true
end

function SWEP:SetWeaponAnim(idealAct, flPlaybackRate)
	local idealSequence = self:SelectWeightedSequence(idealAct)
	if not idealSequence or idealSequence == -1 then return false end
	flPlaybackRate = isnumber(flPlaybackRate) and flPlaybackRate or 1

	self:SendWeaponAnim(idealAct)
	self:SendViewModelMatchingSequence(idealSequence)

	local owner = self:GetOwner()
	if owner:IsValid() and owner:IsPlayer() then
		local vm = owner:GetViewModel(self:ViewModelIndex())
		if vm:IsValid() then
			vm:SendViewModelMatchingSequence(idealSequence)
			vm:SetPlaybackRate(flPlaybackRate)
		end
	end

	-- Set the next time the weapon will idle
	self:SetWeaponIdleTime(CurTime() + (self:SequenceDuration() * flPlaybackRate))
	return true
end

function SWEP:PerformIdleAnimation()
	self:SetWeaponAnim(ACT_VM_IDLE)
end

function SWEP:WeaponIdle()
	if self:GetWeaponIdleTime() > CurTime() then return end

	if self:Clip1() ~= 0 then
		self:PerformIdleAnimation()
	end
end

function SWEP:Holster()
	-- animation cancel for unfinished reload
	if self:GetInReload() then
		self:SetNextPrimaryFire(CurTime())
		self:SetNextSecondaryFire(CurTime())
	end

	self:SetShotsFired(0)
	self:SetBurstNum(0)
	self:SetZoomLevel(0)
	self:SetInReload(false)
	self:SetFinishReloadTime(0)

	if self:GetOwner():IsPlayer() then
		self:GetOwner():SetCanZoom(true)
		self:GetOwner():SetFOV(0, 0)
	end

	return true
end

function SWEP:AdjustMouseSensitivity()
	return self:GetOwner():GetFOV() / GetConVar"fov_desired":GetInt()
end

function SWEP:Initialize()
	self:SetHoldType(holdtypes[self.AnimSet])
	self:SetDeploySpeed(1)
end

function SWEP:Deploy()
	local owner = self:GetOwner()
	if not owner:IsValid() then return end

	self:SetHoldType(holdtypes[self.AnimSet])

	self:PerformDeployAnimation()

	if owner:IsPlayer() then
		local vm = owner:GetViewModel(self:ViewModelIndex())
		if vm:IsValid() then
			vm:SetPlaybackRate(self.m_WeaponDeploySpeed or 1)
			local flAnimFinish = CurTime() + (self:SequenceDuration() * (1 / (self.m_WeaponDeploySpeed or 1)))

			local oPrim = self:GetNextPrimaryFire()
			local oSec = self:GetNextSecondaryFire()

			self:SetWeaponIdleTime(flAnimFinish)
			self:SetNextPrimaryFire(oPrim < flAnimFinish and flAnimFinish or oPrim)
			self:SetNextSecondaryFire(oSec < flAnimFinish and flAnimFinish or oSec)
		end
	end

	return true
end

function SWEP:CanPrimaryAttack()
	local owner = self:GetOwner()
	if not owner:IsValid() then return false end
	if owner:WaterLevel() == 3 and not self.Primary.FireUnderWater then return false end
	if self:Clip1() ~= -1 and self:GetMaxAmmo1() > 0 and self:Clip1() <= 0 then return false end
	if self:GetInReload() then return false end
	if self:GetBurstNum() > 0 then return false end

	return true
end

function SWEP:CanSecondaryAttack()
	local owner = self:GetOwner()
	if not owner:IsValid() then return false end
	if owner:WaterLevel() == 3 and self.Secondary.Type ~= ATTACK_TYPE_ZOOM and not self.Secondary.FireUnderWater then return false end
	if self:Clip2() ~= -1 and self:GetMaxClip2() > 0 and self:Clip2() <= 0 then return false end
	if self:GetInReload() then return false end
	if self:GetBurstNum() > 0 then return false end

	return true
end

local our_dmg = bit.bor(DMG_BULLET, DMG_DIRECT)
function SWEP:ShootBullet(tab, iLagBullets)
	local owner = self:GetOwner()
	local s = ((self:GetInZoom() and self.UseScopedFireCone) and self.ScopedFireCone or self.Primary.Spread) / 100

	if owner:IsPlayer() then
		owner:ViewPunch(Angle(util.SharedRandom("pistolpax", .1, .3), util.SharedRandom("pistolpay", -.2, .2), 0))
	end

	local ammo = tab.Ammo
	if tab.UseAmmo == false then
		ammo = self.Primary.Ammo
	end

	local bul_data = game.GetAmmoData(game.GetAmmoID(ammo))
	local bul_dmg

	if owner:IsPlayer() then
		bul_dmg = bul_data.plydmg
	else
		bul_dmg = bul_data.npcdmg
	end
	local cvar_dmg = GetConVar(bul_dmg)

	local iBulletDamage = self.Damage
	if cvar_dmg then
		iBulletDamage = cvar_dmg:GetInt()
	end

	owner:MuzzleFlash()

	owner:FireBullets({
		Dir = (owner:GetAimVector():Angle() + (owner:IsPlayer() and owner:GetViewPunchAngles() or Angle())):Forward(),
		Src = owner:GetShootPos(),
		Spread = Vector(s, s, s),
		Attacker = owner,
		AmmoType = tab.Ammo,
		TracerName = tracers[self.TracerType],
		Tracer = (tab.Type == 3 or tab.Type == 4) and 3 or self.TracerFrequency,
		Damage = iBulletDamage,
		Num = iLagBullets or ((tab.Type == 3 or tab.Type == 4) and 7 or 1),
		Callback = function(atk, tr, dmg)
			local ent = tr.Entity

			if not ent:IsValid() then return end

			if ent:IsPlayer() or ent:IsNPC() then
				local calc_dmg = our_dmg
				local ammoID = game.GetAmmoID(tab.Ammo)

				if ammoID == 7 then -- buckshot
					calc_dmg = bit.bor(calc_dmg, DMG_BUCKSHOT)
				end

				dmg:SetDamageType(calc_dmg)
			else
				-- this fixes oc_nerv's flamethrow turret
				dmg:SetDamageType(DMG_BULLET)
			end
		end
	})

	self:SetShotsFired(self:GetShotsFired() + 1)
end

function SWEP:GetInZoom()
	return self:GetZoomLevel() > 0
end

local ActivityMap = {
	[0] = ACT_VM_PRIMARYATTACK,
	ACT_VM_RECOIL1,
	ACT_VM_RECOIL2,
	ACT_VM_RECOIL3
}
function SWEP:GetPrimaryAttackActivity()
	if self.NumberOfRecoilAnims > 0 then
		local speed = self.RecoilIncrementSpeed
		local shots = self:GetShotsFired()

		for i = 1, self.NumberOfRecoilAnims + 1 do
			return ActivityMap[math.Clamp(math.floor(shots / speed), 0, #ActivityMap)]
		end
	end

	return ACT_VM_PRIMARYATTACK
end

-- here is a comment from https://github.com/ValveSoftware/source-sdk-2013/blob/master/mp/src/game/shared/hl2mp/weapon_hl2mpbase_machinegun.cpp#L73-L74
-- ====================================================
-- To make the firing framerate independent, we may have to fire more than one bullet here on low-framerate systems,
-- especially if the weapon we're firing has a really fast rate of fire.
-- ====================================================
-- this makes the gun actually shoot the correct number of bullets when the server is lagging like fuck, or when you have low framerate servers :D

function SWEP:CalcLagBullets(is_primary_fire, fireInterval)
	local owner = self:GetOwner()


	local b_tab = is_primary_fire and self.Primary or self.Secondary

	local flNextFire = 0
	local iPelletCount = ((b_tab.Type == 3 or b_tab.Type == 4) and 7 or 1)
	local iNumBulletsToFire = 0

	if not owner:IsPlayer() then return iPelletCount, 1 end

	if is_primary_fire then
		flNextFire = self:GetNextPrimaryFire()
	else
		flNextFire = self:GetNextSecondaryFire()
	end

	if not owner:KeyDownLast(IN_ATTACK) then
		flNextFire = CurTime()
	end

	while flNextFire <= CurTime() do
		iNumBulletsToFire = iNumBulletsToFire + 1
		flNextFire = flNextFire + fireInterval
	end

	-- if uses clips for primary ammo
	if self:Clip1() ~= -1 and self:GetMaxAmmo1() > 0 and iNumBulletsToFire > self:Clip1() then
		iNumBulletsToFire = self:Clip1()
	end

	return iNumBulletsToFire * iPelletCount, iNumBulletsToFire
end

SWEP.ZoomLevels = {
	[0] = 0,
	40,
	16
}
ATTACK_TYPE_BULLET = 1
ATTACK_TYPE_BURSTFIRE = 2
ATTACK_TYPE_SHOTGUN = 3
ATTACK_TYPE_ZOOM = 7
ATTACK_TYPE_GRENADE = 8

function SWEP:Attack(tab)
	local owner = self:GetOwner()
	local primary = tab == self.Primary
	local _type = tab.Type
	local RemoveAmmo = primary and self.TakePrimaryAmmo or self.TakeSecondaryAmmo
	local SetNextFireTime = primary and self.SetNextPrimaryFire or self.SetNextSecondaryFire

	local iFireType = tab.Type

	if owner:IsNPC() and iFireType == ATTACK_TYPE_BURSTFIRE then
		iFireType = ATTACK_TYPE_BULLET
	end

	if iFireType == 0 then
		return
	elseif iFireType == ATTACK_TYPE_BULLET or iFireType == ATTACK_TYPE_SHOTGUN then
		local iNumBulletsToFire, iRemoveBullets = self:CalcLagBullets(primary, tab.Delay)

		self:ShootBullet(tab, iNumBulletsToFire)

		self:EmitSound(self.SoundData.single_shot)

		if iFireType == ATTACK_TYPE_SHOTGUN then
			self:SetNeedPump(true)
		end

		owner:SetAnimation(PLAYER_ATTACK1)
		self:SetWeaponAnim(self:GetPrimaryAttackActivity())

		if owner:IsPlayer() then
			RemoveAmmo(self, iRemoveBullets)
		end
	elseif iFireType == ATTACK_TYPE_BURSTFIRE then
		self:SetBurstNum(tab.BurstAmount)
		self.BurstTab = tab

		self:SetNextPrimaryFire(CurTime() + tab.Delay)

		self:PerformBurstFireAnimation()
	elseif not primary and iFireType == ATTACK_TYPE_ZOOM then
		self:SetZoomLevel(self:GetZoomLevel() + 1)

		-- unzoom
		if self:GetZoomLevel() > 2 then
			self:SetZoomLevel(0)
			owner:SetCanZoom(true)
		elseif owner:GetCanZoom() then
			owner:SetCanZoom(false)
		end

		owner:SetFOV(self.ZoomLevels[self:GetZoomLevel()], 0)
	elseif iFireType == ATTACK_TYPE_GRENADE then
		if owner:GetAmmoCount(self:GetSecondaryAmmoType()) <= 0 then return end

		self:EmitSound("Weapon_SMG1.Double")

		local vecSrc = self:GetOwner():GetShootPos()
		local vecThrow

		vecThrow = self:GetOwner():GetAimVector()
		vecThrow = vecThrow * 1000

		if SERVER then
			local nade = ents.Create("grenade_ar2")
			nade:SetPos(vecSrc)
			nade:Spawn()
			nade:SetAngles(Angle(0, 0, 0))

			nade:SetVelocity(vecThrow)

			nade:SetLocalAngularVelocity(AngleRand())
			nade:SetMoveType(bit.bor(MOVETYPE_FLYGRAVITY, MOVECOLLIDE_FLY_BOUNCE))
			nade:SetOwner(self:GetOwner())
		end

		self:PerformSecondaryAttackAnimation()

		self:GetOwner():SetAnimation(PLAYER_ATTACK1)

		self:TakeSecondaryAmmo(1)

		self:SetNextPrimaryFire(CurTime() + tab.Delay)
		self:SetNextSecondaryFire(CurTime() + tab.Delay)
	else
		print(string.format("%s unknown firetype%d %s", tostring(self), primary and 1 or 2, tostring(iFireType)))
		return
	end

	SetNextFireTime(self, CurTime() + tab.Delay)
end

function SWEP:PerformDeployAnimation()
	self:SetWeaponAnim(ACT_VM_DRAW)
end

function SWEP:PerformReloadAnimation()
	local act = self:GetReloadActivity()

	if act ~= -1 then
		self:SetWeaponAnim(act)
	end
end

function SWEP:PerformPrimaryAttackAnimation()
	local act = self:GetPrimaryAttackActivity()

	if act ~= -1 then
		self:SetWeaponAnim(act)
	end
end

function SWEP:PerformSecondaryAttackAnimation()
	local act = self:GetSecondaryAttackActivity()

	if act ~= -1 then
		self:SetWeaponAnim(act)
	end
end

function SWEP:PerformBurstFireAnimation()
	local act = self:GetBurstFireActivity()

	if act ~= -1 then
		self:SetWeaponAnim(act)
	end
end

function SWEP:GetSecondaryAttackActivity()
	return ACT_VM_SECONDARYATTACK
end

function SWEP:GetBurstFireActivity()
	return -1
end

function SWEP:Pump()
	self:SetNeedPump(false)

	self:EmitSound(self.SoundData.special1)

	local vm = self:GetOwner():GetViewModel()
	if vm:SelectWeightedSequence(ACT_SHOTGUN_PUMP) ~= -1 then
		self:SetWeaponAnim(ACT_SHOTGUN_PUMP)
	end

	self:SetNextPrimaryFire(CurTime() + self:SequenceDuration())
	self:SetNextSecondaryFire(CurTime() + self:SequenceDuration())
end

function SWEP:Think()
	local owner = self:GetOwner()
	if not owner:IsValid() then return end

	if not owner:KeyDown(IN_ATTACK) or self:Clip1() <= 0 then
		self:SetShotsFired(0)
	end

	if self:GetInReload() then
		if self.Primary.Type ~= 3 and self.Primary.Type ~= 4 then
			if self:GetFinishReloadTime() > CurTime() then
				self:InReloadThink()
			else
				local dif = self:GetMaxAmmo1() - self:Clip1()
				local amt = math.min(self:Clip1() + self:GetOwner():GetAmmoCount(self.Primary.Ammo), self:GetMaxAmmo1())

				self:GetOwner():RemoveAmmo(dif, self.Primary.Ammo)
				self:SetClip1(amt)

				self:SetInReload(false)
				self:OnFinishReload()
			end
		else
			if owner:KeyDown(IN_ATTACK) and self:Clip1() >= 1 --[[and not self:GetNeedPump()]] then
				self:SetNextPrimaryFire(CurTime())
				self:FinishReload()
				-- If I'm secondary firing and have two rounds, stop reloading and fire
			elseif self.Secondary.Type ~= 0 and owner:KeyDown(IN_ATTACK2) and self:Clip1() >= 2 --[[and not self:GetNeedPump()]] then
				self:SetNextSecondaryFire(CurTime())
				self:FinishReload()
			elseif self:GetNextPrimaryFire() <= CurTime() then
				-- If I'm out of ammo and reloading
				if owner:GetAmmoCount(self.Primary.Ammo) <= 0 then
					self:FinishReload()
					return
				end

				-- If clip not full, insert a shell
				if self:Clip1() < self:GetMaxAmmo1() then
					self:InsertShell()
					return
					-- Clip full, stop reloading
				else
					self:FinishReload()
					return
				end
			end
		end

		return
	end

	if self:GetNeedPump() and self:GetNextPrimaryFire() <= CurTime() then
		self:Pump()
	end

	if self.Primary.FastFire and self:GetNextPrimaryFire() > CurTime() and not owner:KeyDown(IN_ATTACK) then
		self:SetNextPrimaryFire(CurTime())
	end
	if self.Secondary.FastFire and self:GetNextSecondaryFire() > CurTime() and not owner:KeyDown(IN_ATTACK2) then
		self:SetNextSecondaryFire(CurTime())
	end


	if self:GetBurstNum() > 0 and
		self:GetBurstTime() <= CurTime()
	then
		if self:Clip1() < 1 then
			self:SetBurstNum(0)
			return
		end

		self:SetBurstNum(self:GetBurstNum() - 1)
		self:SetBurstTime(CurTime() + self.BurstTab.BetweenBurstTime)
		self:ShootBullet(self.BurstTab)
		self:TakePrimaryAmmo(1)
		owner:SetAnimation(PLAYER_ATTACK1)
		owner:MuzzleFlash()
		self:EmitSound(self.SoundData.single_shot)

		local iBurstActivity = self:GetBurstFireActivity()
		if iBurstActivity ~= -1 then
			self:PerformBurstAcitivty()
		else
			self:PerformSecondaryAttackAnimation()
		end
	end

	if self:Clip1() <= 0 then self:Reload() end

	self:WeaponIdle()
end

function SWEP:PrimaryAttack()
	if not self:CanPrimaryAttack() then
		self:EmitSound(self.SoundData.empty)
		self:SetNextPrimaryFire(CurTime() + .5)
		return
	end
	self:Attack(self.Primary)
end

function SWEP:CanReload()
	if self:GetInReload() then return false end
	if self:Clip1() >= self:GetMaxAmmo1() then return false end
	if self:GetNextPrimaryFire() > CurTime() and not (self.GetForceReload and self:GetForceReload()) then return false end
	local owner = self:GetOwner()
	if not IsValid(owner) then return false end
	if owner:GetAmmoCount(self.Primary.Ammo) < 1 then return false end
	if self:GetBurstNum() > 0 then return false end

	return true
end

function SWEP:Reload()
	if self:GetInReload() then return end
	local ok = self:CanReload()

	if ok then
		if self.Primary.Type ~= 3 and self.Primary.Type ~= 4 then
			self:SetInReload(true)
			self:PerformReloadAnimation()
			if self.SoundData.reload then
				self:EmitSound(self.SoundData.reload)
			end

			self:SetFinishReloadTime(CurTime() + self:SequenceDuration())
			self:SetNextPrimaryFire(CurTime() + self:SequenceDuration())
			self:SetNextSecondaryFire(CurTime() + self:SequenceDuration())

			self:GetOwner():DoReloadEvent()
		else
			self:StartReload()
		end

		self:OnStartReload()
	else
		self:OnReloadFail()
	end
end

function SWEP:StartReload()
	local owner = self:GetOwner()

	if owner:GetAmmoCount(self.Primary.Ammo) <= 0 then return false end

	if self:Clip1() >= self:GetMaxAmmo1() then return false end

	local j = math.min(1, owner:GetAmmoCount(self.Primary.Ammo))
	if j <= 0 then return false end

	self:SetWeaponAnim(ACT_SHOTGUN_RELOAD_START)
	owner:SetAnimation(PLAYER_RELOAD)

	self:SetNextPrimaryFire(CurTime() + self:SequenceDuration())

	self:SetInReload(true)
	return true
end

function SWEP:InsertShell()
	if not self:GetInReload() then
		ErrorNoHalt("ERROR: Shotgun InsertShell() called incorrectly!")
		return
	end

	local owner = self:GetOwner()

	if not owner then return false end
	if owner:GetAmmoCount(self.Primary.Ammo) <= 0 then return false end
	if self:Clip1() >= self:GetMaxAmmo1() then return false end

	local j = math.min(1, owner:GetAmmoCount(self.Primary.Ammo))
	if j <= 0 then return false end

	self:FillClip()
	self:EmitSound(self.SoundData.reload)
	self:SetWeaponAnim(ACT_VM_RELOAD)

	self:SetNextPrimaryFire(CurTime() + self:SequenceDuration())
	self:SetNextSecondaryFire(CurTime() + self:SequenceDuration())
end

function SWEP:FinishReload()
	self:SetInReload(false)

	self:SetWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)

	self:SetNextPrimaryFire(CurTime() + self:SequenceDuration())
	self:SetNextSecondaryFire(CurTime() + self:SequenceDuration())
end

function SWEP:FillClip()
	if self:GetOwner():GetAmmoCount(self.Primary.Ammo) > 0 and
		self:Clip1() < self:GetMaxAmmo1() then
		self:SetClip1(self:Clip1() + 1)
		self:GetOwner():RemoveAmmo(1, self.Primary.Ammo)
	end
end

function SWEP:OnStartReload()
	if self:GetInZoom() then
		self:SetZoomLevel(0)
		self:GetOwner():SetFOV(0, 0)
	end

	self:SetShotsFired(0)
end

function SWEP:InReloadThink() end

function SWEP:OnFinishReload() end

function SWEP:OnReloadFail() end

function SWEP:GetReloadActivity()
	return ACT_VM_RELOAD
end

function SWEP:SecondaryAttack()
	if not self:CanSecondaryAttack() then return end
	self:Attack(self.Secondary)
end

function SWEP:GetMaxAmmo1()
	return self:GetMaxClip1()
end

function SWEP:GetCapabilities()
	return bit.bor(CAP_WEAPON_RANGE_ATTACK1, CAP_INNATE_RANGE_ATTACK1)
end

if CLIENT then
	local hands_sub = {
		--custom_mp5k = 5,
		--custom_ak5 = 2,
		--custom_cz52 = 2,
		--custom_hmg1 = 2,
		--custom_oicw = 0,
		--custom_remi870 = 0,
		custom_xm1014 = 2,
		custom_spas12 = 2,
		weapon_uzi = 0,
		weapon_alyxgun = {0, 2},
		--weapon_sniperrifle = {1,5},
	}
	function SWEP:PreDrawViewModel(vm, wep, ply)
		local a = hands_sub[wep:GetClass()]
		if a then
			if istable(a) then
				for _, i in next, a do
					vm:SetSubMaterial(i, "null")
				end
			else
				vm:SetSubMaterial(a, "null")
			end

			--self.UseHands = true
		end
	end

	function SWEP:PostDrawViewModel(vm, wep, ply)
		local a = hands_sub[wep:GetClass()]
		if a then
			if istable(a) then
				for _, i in next, a do
					vm:SetSubMaterial(i, "")
				end
			else
				vm:SetSubMaterial(a, "")
			end
		end
	end

	local matScope = Material("sprites/sniper_scope")
	local uv1 = 1 / 512
	local uv2 = 1 - uv1

	function SWEP:DrawHUDBackground()
		if self:GetZoomLevel() == 0 then return end

		local iHeight = ScrH()
		local iWidth = ScrW()

		-- calculate the bounds in which we should draw the scope
		local y1 = iHeight / 6
		local x1 = (iWidth - iHeight) / 2 + y1
		local y2 = iHeight - y1
		local x2 = iWidth - x1

		local tVerts = {{}, {}, {}, {}}
		local tVert = tVerts[1]
		tVert.x = iWidth
		tVert.y = iHeight
		tVert.u = uv2
		tVert.v = uv1

		tVert = tVerts[2]
		tVert.x = 0
		tVert.y = iHeight
		tVert.u = uv1
		tVert.v = uv1

		tVert = tVerts[3]
		tVert.x = 0
		tVert.y = 0
		tVert.u = uv1
		tVert.v = uv2

		tVert = tVerts[4]
		tVert.x = iWidth
		tVert.y = 0
		tVert.u = uv2
		tVert.v = uv2

		if self.ScopeOverlayCol then
			surface.SetDrawColor(
				self.ScopeOverlayCol.r,
				self.ScopeOverlayCol.g,
				self.ScopeOverlayCol.b,
				32
			)
		else
			surface.SetDrawColor(255, 255, 255, 0)
		end

		surface.DrawPoly(tVerts)

		surface.SetDrawColor(0, 0, 0, 255)

		-- Draw the outline
		surface.SetMaterial(matScope)
		tVert.x = x1 - 7
		tVert.y = y2
		tVert.u = uv2
		tVert.v = uv2

		tVert = tVerts[1]
		tVert.x = x1 - 7
		tVert.y = y1 + 7
		tVert.u = uv2
		tVert.v = uv1

		tVert = tVerts[2]
		tVert.x = x2
		tVert.y = y1 + 7
		tVert.u = uv1
		tVert.v = uv1

		tVert = tVerts[3]
		tVert.x = x2
		tVert.y = y2
		tVert.u = uv1
		tVert.v = uv2

		surface.DrawPoly(tVerts)

		surface.DrawRect(0, y1 + 7, x1, iHeight)      -- Left
		surface.DrawRect(x2, y1 + 7, iWidth, iHeight) -- Right
		surface.DrawRect(0, 0, iWidth, y1 + 7)        -- Top
		surface.DrawRect(0, y2, iWidth, iHeight)      -- Bottom
	end

	function SWEP:DoDrawCrosshair()
		return self:GetZoomLevel() ~= 0
	end

	function SWEP:DrawFontIcon(wep, x, y, w, h, char1, char2, font1, font2, offset)
		y = y + (offset or 0)
		x = x + 10
		w = w - 20

		draw.DrawText(char1, font2, x + w / 2, y, Color(255, 220, 0), TEXT_ALIGN_CENTER)
		draw.DrawText(char2, font1, x + w / 2, y, Color(255, 220, 0), TEXT_ALIGN_CENTER)
	end

	function SWEP:DrawWeaponSelection(x, y, w, h, a)
		self:DrawFontIcon(self, x, y, w, h, self.Char or "NONE", self.SelectedChar or "", self.CharFont or "DermaDefault", self.SelectedCharFont or "DermaDefault", 18)
	end
else
	SWEP.NPCBurstMin = 1
	SWEP.NPCBurstMax = 1
	SWEP.NPCBurstDelay = 1

	SWEP.NPCRestMin = 0.3
	SWEP.NPCRestMax = 0.66

	function SWEP:GetNPCBulletSpread(prof)
		return 15
	end

	function SWEP:GetNPCBurstSettings()
		return self.NPCBurstMin, self.NPCBurstMax, self.NPCBurstDelay
	end

	function SWEP:GetNPCRestTimes()
		return self.NPCRestMin, self.NPCRestMax
	end
end
