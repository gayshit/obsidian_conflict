AddCSLuaFile()

SWEP.PrintName = "GRAPPLE HOOK"
SWEP.Spawnable = true
SWEP.ViewModel = "models/weapons/c_superphyscannon.mdl"
SWEP.WorldModel = "models/weapons/w_physics.mdl"
SWEP.Category = "Obsidian Conflict"
SWEP.UseHands = true

SWEP.HoldType = "physgun"

SWEP.Slot = 5
SWEP.SlotPos = 2

SWEP.ViewModelFOV = 54

local fov_desired = GetConVar("fov_desired")
function SWEP:AdjustMouseSensitivity()
	---@class Player
	local owner = self:GetOwner()
	return owner:GetFOV() / fov_desired:GetInt()
end

SWEP.SoundData = {
	reload = "Weapon_Pistol.Reload",
	reload_npc = "Weapon_Pistol.NPC_Reload",
	empty = "Weapon_MegaPhysCannon.DryFire",
	single_shot = "Weapon_Mortar.Single",
	single_shot_npc = "Weapon_Mortar.Single",
	special1 = "Weapon_PhysCannon.HoldSound",
	special2 = "Weapon_Pistol.Special2",
	burst = "Weapon_Pistol.Burst"
}

SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Ammo = "none"

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Ammo = "none"

SWEP.Char = "g"
SWEP.CharFont = "ObsidianWeaponIcons"
SWEP.SelectedChar = "g"
SWEP.SelectedCharFont = "ObsidianWeaponIconsSelected"

SWEP.FiresUnderwater = true

local BOLT_AIR_VELOCITY = 3500
local BOLT_WATER_VELOCITY = 150

function SWEP:DrawFontIcon(wep, x, y, w, h, char1, char2, font1, font2, offset)
	y = y + (offset or 0)
	x = x + 10
	w = w - 20

	draw.DrawText(char1, font2, x + w / 2, y, Color(255, 220, 0), TEXT_ALIGN_CENTER)
	draw.DrawText(char2, font1, x + w / 2, y, Color(255, 220, 0), TEXT_ALIGN_CENTER)
end

function SWEP:DrawWeaponSelection(x, y, w, h, a)
	self:DrawFontIcon(self, x, y, w, h, self.Char or "NONE", self.SelectedChar or "", self.CharFont or "DermaDefault", self.SelectedCharFont or "DermaDefault", 18)
end

function SWEP:SetupDataTables()
	self:NetworkVar("Entity", 0, "Hook")
	self:NetworkVar("Bool", 0, "HookState")
	self:NetworkVar("Vector", 0, "LastWeaponColor")

	if SERVER then
		self:SetHookState(false)
	end
end

function SWEP:Initialize()
	self:SetHoldType(self.HoldType)
	self:SetSkin(1)
	if SERVER then
		self:SetLastWeaponColor(VectorRand(0.0, 1.0))
	end
end

function SWEP:Equip(newOwner)
	if IsValid(newOwner) and newOwner:IsPlayer() then
		self:SetLastWeaponColor(newOwner:GetWeaponColor())
	end
end

function SWEP:GetWeaponColor()
	---@class Player
	local owner = self:GetOwner()
	local wepColor
	if IsValid(owner) and owner:IsPlayer() then
		wepColor = owner:GetWeaponColor()
	else
		wepColor = self:GetLastWeaponColor()
	end
	return wepColor
end

function SWEP:PrimaryAttack()
	if self:GetNextPrimaryFire() > CurTime() then return end
	if IsValid(self:GetHook()) then return end

	---@class Player
	local owner = self:GetOwner()
	if not IsValid(owner) then return end
	if not owner:IsPlayer() then return end

	self:EmitSound(self.SoundData.single_shot)

	if SERVER then
		self:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
		owner:SetAnimation(PLAYER_ATTACK1)

		self:SetNextPrimaryFire(CurTime() + 0.75)
		self:SetNextSecondaryFire(CurTime() + 0.75)
	end
	self:FireHook()
end

--[[if CLIENT then
	local MAT_WORLDMDL = Material("models/weapons/w_physics/w_physics_sheet2
	function SWEP:DrawWorldModel()
		local wepColor = self:GetWeaponColor():ToColor()
		MAT_WORLDMDL:SetVector("$selfillumtint", wepColor:ToVector())
		self:DrawModel()
	end
end--]]

function SWEP:SecondaryAttack()
	if self:GetNextSecondaryFire() > CurTime() then return end

	self:ToggleHook()
	self:EmitSound(self.SoundData.empty)

	self:SetNextSecondaryFire(CurTime() + 0.75)
end

function SWEP:Reload()
end

function SWEP:ToggleHook()
	---@class Player
	local owner = self:GetOwner()

	if self:GetHookState() then
		owner:PrintMessage(HUD_PRINTCENTER, "Pull mode")
	else
		owner:PrintMessage(HUD_PRINTCENTER, "Rappel mode")
	end

	self:SetHookState(not self:GetHookState())
end

function SWEP:Think()
	if CLIENT then return end
	---@class Player
	local owner = self:GetOwner()
	if not IsValid(owner) then return end

	if not owner:IsPlayer() then
		SafeRemoveEntity(self)
		return
	end

	if IsValid(self:GetHook()) then
		if not owner:KeyDown(IN_ATTACK) and not owner:KeyDown(IN_ATTACK2) then
			SafeRemoveEntity(self:GetHook())
			self:SetHook(NULL)
		end
	end
end

function SWEP:FireHook()
	---@class Player
	local owner = self:GetOwner()
	if not IsValid(owner) then return end
	if not owner:IsPlayer() then return end

	if SERVER then
		local vecAiming = owner:GetAimVector()
		local vecSrc = owner:GetShootPos()

		local angAiming = vecAiming:Angle()
		angAiming:Normalize()

		local hookEnt = ents.Create("grapple_hook")
		hookEnt:SetPos(vecSrc + (vecAiming * 30))
		hookEnt:SetAngles(angAiming)
		hookEnt:SetOwner(self)
		hookEnt:Spawn()

		hookEnt:SetPlayer(owner)

		self:SetHook(hookEnt)

		if owner:WaterLevel() == 3 then
			hookEnt:SetVelocity(vecAiming * BOLT_WATER_VELOCITY)
		else
			hookEnt:SetVelocity(vecAiming * BOLT_AIR_VELOCITY)
		end
	end

	owner:ViewPunch(Angle(-2, 0, 0))

	self:SendWeaponAnim(ACT_VM_PRIMARYATTACK)

	self:SetNextPrimaryFire(CurTime() + 0.75)
	self:SetNextSecondaryFire(CurTime() + 0.75)
end

function SWEP:OnRemove()
	if IsValid(self:GetHook()) and SERVER then
		SafeRemoveEntity(self:GetHook())
		self:SetHook(NULL)
	end
end

function SWEP:Holster()
	self:OnRemove()

	return true
end
