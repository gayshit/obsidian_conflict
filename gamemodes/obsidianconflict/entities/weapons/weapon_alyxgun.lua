SWEP.PrintName = "#HL2_ALYXGUN"
SWEP.Base = "oc_base"

DEFINE_BASECLASS(SWEP.Base)

SWEP.Spawnable = true

SWEP.ViewModel = "models/weapons/c_alyxgun.mdl"
SWEP.WorldModel = "models/weapons/w_alyxgun.mdl"
SWEP.UseHands = true
SWEP.ViewModelFOV = 54

SWEP.SoundData = {
	reload = "Weapon_Alyx_Gun.Reload",
	reload_npc = "Weapon_Alyx_Gun.NPC_Reload",
	empty = "Weapon_Alyx_Gun.Empty",
	single_shot = "Weapon_Alyx_Gun.Single",
	single_shot_npc = "Weapon_Alyx_Gun.NPC_Single",
	special1 = "Weapon_Alyx_Gun.Special1",
	special2 = "Weapon_Alyx_Gun.Special2",
	burst = "Weapon_Alyx_Gun.Burst"
}

SWEP.Char = "E"
SWEP.CharFont = "ObsidianWeaponIcons"
SWEP.SelectedChar = "E"
SWEP.SelectedCharFont = "ObsidianWeaponIconsSelected"

SWEP.Cone = Vector(.02, .02, 0)

SWEP.HoldType = "pistol"
SWEP.Slot = 1
SWEP.SlotPos = 3

SWEP.Damage = GetConVar("sk_plr_dmg_alyxgun"):GetInt()

SWEP.Primary.Ammo = "AlyxGun"
SWEP.Primary.DefaultClip = 150
SWEP.Primary.ClipSize = 30
SWEP.Primary.Automatic = true
SWEP.Primary.Spread = 2
SWEP.Primary.Delay = .1
SWEP.Primary.Type = ATTACK_TYPE_BULLET

SWEP.BurstFire = {}
SWEP.BurstFire.DefaultClip = 150
SWEP.BurstFire.ClipSize = 30
SWEP.BurstFire.Type = ATTACK_TYPE_BURSTFIRE
SWEP.BurstFire.Ammo = "AlyxGun"
SWEP.BurstFire.Spread = 2
SWEP.BurstFire.Automatic = true
SWEP.BurstFire.Delay = .725
SWEP.BurstFire.BurstAmount = 3
SWEP.BurstFire.BetweenBurstTime = .1

SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.ClipSize = -1
SWEP.Secondary.Ammo = "none"
SWEP.Secondary.Automatic = true
SWEP.Secondary.Delay = 0

if SERVER then
	SWEP.NPCBurstMin = 3
	SWEP.NPCBurstMax = 4
	SWEP.NPCBurstDelay = 0.1

	SWEP.NPCRestMin = 0.3
	SWEP.NPCRestMax = 0.6
end

local FIREMODE_BURST = 0
local FIREMODE_AUTO = 1

function SWEP:SetupDataTables()
	BaseClass.SetupDataTables(self)
	self:NetworkVar("Int", 3, "FireMode")
	self:NetworkVar("Float", 3, "FireModeSwitchBegin")
	self:NetworkVar("Float", 4, "FireModeSwitchEnd")

	self:SetFireMode(FIREMODE_BURST)
end

function SWEP:PeformSecondaryAttackActivity()
	self:SetWeaponAnim(ACT_VM_SECONDARYATTACK)
end

function SWEP:PerformPrimaryAttackActivity()
	self:SetWeaponAnim(ACT_VM_SECONDARYATTACK)
end

function SWEP:GetPrimaryAttackActivity()
	return ACT_VM_SECONDARYATTACK
end

function SWEP:GetSecondaryAttackActivity()
	return ACT_VM_PRIMARYATTACK
end

function SWEP:PerformDeployAnimation()
	if self:GetFireMode() == FIREMODE_AUTO then
		self:SetWeaponSequence("smg_draw")
	else
		BaseClass.PerformDeployAnimation(self)
	end
end

function SWEP:PerformReloadAnimation()
	if self:GetFireMode() == FIREMODE_AUTO then
		self:SetWeaponSequence("smg_reload")
	else
		BaseClass.PerformReloadAnimation(self)
	end
end

function SWEP:PrimaryAttack()
	if not self:CanPrimaryAttack() then
		self:EmitSound(self.SoundData.empty)
		self:SetNextPrimaryFire(CurTime() + .5)
		return
	end
	self:Attack(self:GetFireMode() == FIREMODE_BURST and self.BurstFire or self.Primary)
end

function SWEP:SecondaryAttack()
	local current = bit.band(self:GetFireMode() + 1, 1)
	self:SetFireMode(current)

	local FirstTimePredicted = IsFirstTimePredicted()

	if current == FIREMODE_AUTO then
		if FirstTimePredicted then
			self:EmitSound(self.SoundData.special1)
		end

		self:SetWeaponSequence("pistol2smg")
		self:SetHoldType("smg")
		self:SetPoseParameter("active", 1)
	elseif current == FIREMODE_BURST then
		if FirstTimePredicted then
			self:EmitSound(self.SoundData.special2)
		end

		self:SetWeaponSequence("smg2pistol")
		self:SetHoldType("pistol")
		self:SetPoseParameter("active", 0)
	end

	local curTime = CurTime()
	local endTime = curTime + self:SequenceDuration()
	self:SetFireModeSwitchBegin(curTime)
	self:SetFireModeSwitchEnd(endTime)
	self:SetNextSecondaryFire(endTime)
	self:SetNextPrimaryFire(endTime)
end

function SWEP:PerformIdleAnimation()
	if self:GetFireMode() == FIREMODE_AUTO then
		self:SetWeaponSequence("smg_idle01")
	else
		self:SetWeaponAnim(ACT_VM_IDLE)
	end
end

function SWEP:Think()
	BaseClass.Think(self)

	local switchEndTime = self:GetFireModeSwitchEnd()
	if switchEndTime > CurTime() then
		local d = switchEndTime - self:GetFireModeSwitchBegin()
		local f = math.Clamp((switchEndTime - CurTime()) / d, 0, 1)

		if self:GetFireMode() == FIREMODE_AUTO then
			self:SetPoseParameter("active", 1 - f)
			self:SetNWFloat("poseParam_active", 1 - f)
		else
			self:SetPoseParameter("active", f)
			self:SetNWFloat("poseParam_active", f)
		end
	end

	if self:GetFireModeSwitchBegin() > 0 and CurTime() > self:GetFireModeSwitchBegin() + self:SequenceDuration() then
		self:SetFireModeSwitchBegin(0)
	end
end

function SWEP:DrawWorldModel(flags)
	if self:GetOwner() ~= LocalPlayer() then
		self:SetPoseParameter("active", self:GetNWFloat("poseParam_active", 0))
		self:InvalidateBoneCache()
	end

	self:DrawModel(flags)
end

function SWEP:GetAimCone()
	return self.Cone
end
