-- :blushing:

SWEP.Base = "oc_base"
SWEP.PrintName = "#GMOD_MedKit"
SWEP.Category = "Obsidian Conflict"

SWEP.Slot = 0
SWEP.SlotPos = 4

SWEP.ViewModel = Model("models/weapons/c_medkit.mdl")
SWEP.WorldModel = Model("models/weapons/w_medkit.mdl")
SWEP.ViewModelFOV = 54
SWEP.UseHands = true

SWEP.Primary.ClipSize = 100
SWEP.Primary.DefaultClip = 100
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "none"

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

SWEP.HealAmount = 20 -- Maximum heal amount per use
SWEP.MaxAmmo = 100   -- Maxumum ammo

function SWEP:Initialize()
	self:SetHoldType("slam")
	if CLIENT then return end

	timer.Create("medkit_ammo" .. self:EntIndex(), 5, 0, function()
		if self:Clip1() < self.MaxAmmo then
			self:SetClip1(math.min(self:Clip1() + 5, self.MaxAmmo))
		end
	end)
end

local stru = {
	mins = Vector(-8, -8, -8),
	maxs = Vector(8, 8, 8),
	mask = MASK_SHOT_HULL,
}
function SWEP:GetTarget()
	---@class Player
	local owner = self:GetOwner()

	stru.start = owner:GetShootPos()
	stru.endpos = owner:GetShootPos() + (owner:GetAimVector() * 62)
	stru.filter = owner

	local tr = util.TraceHull(stru)

	local ent = tr.Entity

	if ent:IsValid() then return ent end
end

function SWEP:PrimaryAttack()
	---@class Player
	local owner = self:GetOwner()
	if not owner:IsValid() then return end

	self:SetNextPrimaryFire(CurTime() + .5)
	self:SetNextSecondaryFire(CurTime() + .5)

	if self:Clip1() <= 0 then
		self:PlayDenySound()

		return
	end

	owner:LagCompensation(true)
	local ent = self:GetTarget()
	owner:LagCompensation(false)

	if IsValid(ent) and (ent:IsNPC() or ent:IsPlayer()) then
		if not self.Sound then
			self.Sound = CreateSound(self, "WallHealth.LoopingContinueCharge")
		end

		local iHealthToAdd = math.min(ent:GetMaxHealth() - ent:Health(), 2, self:Clip1())
		if iHealthToAdd > 0 then
			if SERVER and hook.Run("PlayerCanHealEntity", owner, ent) ~= false then
				hook.Run("PlayerOnHealEntity", owner, ent)
				ent:SetHealth(ent:Health() + iHealthToAdd)
				self:TakePrimaryAmmo(iHealthToAdd)
			end

			if self.Sound and not self.Sound:IsPlaying() then
				self.Sound:Play()
			end
		else
			self:PlayDenySound()
		end
	else
		self:PlayDenySound()
	end
end

function SWEP:PlayDenySound()
	if self.Sound and self.Sound:IsPlaying() then
		self.Sound:Stop()
	end

	if IsFirstTimePredicted() then
		self:EmitSound("WallHealth.Deny")
	end
end

function SWEP:SecondaryAttack()
	if not IsFirstTimePredicted() then return end

	---@class Player
	local owner = self:GetOwner()

	self:SetNextPrimaryFire(CurTime() + 1)
	self:SetNextSecondaryFire(CurTime() + 1)

	if self:GetNextSecondaryFire() < CurTime() then
		self:PlayDenySound()
		return
	end

	if self:Clip1() >= 50 then
		self:TakePrimaryAmmo(50)
	else
		self:PlayDenySound()
		return
	end

	owner:DoAnimationEvent(ACT_GMOD_GESTURE_ITEM_DROP)
	self:SetWeaponAnim(ACT_VM_PRIMARYATTACK)

	self:EmitSound"HealthKit.Touch"

	if CLIENT then return end
	owner.m_tSpawnedMedkits = owner.m_tSpawnedMedkits or {}

	timer.Simple(0.6, function()
		if not self:IsValid() or self:GetOwner() ~= owner then return end

		local ent = Obsidian.CreateEntNoRespawn("item_healthkit", true)
		if not ent:IsValid() then return end

		local bone = owner:LookupBone("ValveBiped.Bip01_L_Hand")
		local info = bone and owner:GetBonePosition(bone)

		local pos = info or (owner:GetPos() + owner:GetUp() * 20 + owner:GetRight() * -20)

		ent:SetOwner(owner)
		ent:SetPos(pos)
		ent:Spawn()
		ent.NotForPlayer = owner
		ent:SetTrigger(false)

		local phys = ent:GetPhysicsObject()
		if not phys:IsValid() then
			ent:Remove()
			return
		end

		table.insert(owner.m_tSpawnedMedkits, 1, ent)

		local i = 1
		repeat
			local kit = owner.m_tSpawnedMedkits[i]
			if not kit:IsValid() or i > 2 then
				if kit:IsValid() then
					kit:Remove()
				end

				table.remove(owner.m_tSpawnedMedkits, i)
			else
				i = i + 1
			end
		until i > #owner.m_tSpawnedMedkits

		local vel = owner:GetAimVector()
		vel.z = 0
		vel = vel * 400 + owner:GetUp() * 100
		phys:SetVelocity(vel)

		timer.Simple(.4, function()
			if ent:IsValid() then
				ent:SetTrigger(true)
			end
		end)
	end)
end

function SWEP:Think()
	---@class Player
	local owner = self:GetOwner()
	if not owner:IsValid() then return end
	if not owner:IsPlayer() then
		SafeRemoveEntity(self)
		return
	end

	if self.Sound and not owner:KeyDown(IN_ATTACK) and self.Sound:IsPlaying() then
		self.Sound:Stop()
	end
end

function SWEP:OnRemove()
	timer.Stop("medkit_ammo" .. self:EntIndex())
	timer.Stop("weapon_idle" .. self:EntIndex())
end

function SWEP:Holster()
	timer.Stop("weapon_idle" .. self:EntIndex())

	return true
end

function SWEP:CustomAmmoDisplay()
	self.AmmoDisplay = self.AmmoDisplay or {}
	self.AmmoDisplay.Draw = true
	self.AmmoDisplay.PrimaryClip = self:Clip1()

	return self.AmmoDisplay
end
