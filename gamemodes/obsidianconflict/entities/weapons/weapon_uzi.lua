SWEP.PrintName = "#HL2_UZI"
SWEP.Base = "oc_base"

DEFINE_BASECLASS(SWEP.Base)

SWEP.Spawnable = true

SWEP.ViewModel = "models/weapons/v_uzi.mdl"
SWEP.WorldModel = "models/weapons/w_uzi_r.mdl"
SWEP.UseHands = true
SWEP.ViewModelFOV = 54

SWEP.Category = "Obsidian Conflict"

SWEP.Char = "B"
SWEP.CharFont = "ObsidianWeaponIcons"
SWEP.SelectedChar = "B"
SWEP.SelectedCharFont = "ObsidianWeaponIconsSelected"

SWEP.SoundData = {}

SWEP.Slot = 2

SWEP.Primary.ClipSize = 30
SWEP.Primary.DefaultClip = 30
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "smg1"
SWEP.Primary.Damage = 4

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Ammo = "none"

function SWEP:GetTracerOrigin()
	if self:IsCarriedByLocalPlayer() then
		---@class Player
		local owner = self:GetOwner()
		if not owner:ShouldDrawLocalPlayer() then
			local vm = owner:GetViewModel()
			if self:GetIsDual() and not self:GetRightFire() then
				local att = vm:GetAttachment(self:LookupAttachment"muzzleL")
				return att.Pos
			end
		else
			-- third person
		end
	end
end

function SWEP:SetupDataTables()
	BaseClass.SetupDataTables(self)

	self:NetworkVar("Bool", 2, "IsDual")
	self:NetworkVar("Bool", 3, "HasDual")
	self:NetworkVar("Int", 3, "RightAmmo")
	self:NetworkVar("Int", 4, "DeployingState")

	self:SetRightAmmo(self.Primary.DefaultClip)
	self:SetHasDual(false)
end

local DEPLOYSTATE_NONE = 0
local DEPLOYSTATE_FINISHED = 1
local DEPLOYSTATE_DEPLOY_SINGLE = 2
local DEPLOYSTATE_DEPLOY_DUAL = 3

function SWEP:GetIsDeploying()
	return self:GetDeployingState() > DEPLOYSTATE_FINISHED
end

function SWEP:Deploy()
	self:SetDeployingState(DEPLOYSTATE_DEPLOY_SINGLE)
	self:SetDeploySpeed(1)

	self:SetWeaponSequence("drawsingle")

	return true
end

function SWEP:GetRightFire()
	if self:GetRightAmmo() <= 0 then return false end
	return self:Clip1() % 2 == 0
end

function SWEP:PrimaryAttack()
	if not self:CanPrimaryAttack() then return end
	---@class Player
	local owner = self:GetOwner()

	local iBullets = self:CalcLagBullets(true, .075 * (self:GetIsDual() and .75 or 1))

	owner:FireBullets({
		Src = owner:GetShootPos(),
		Dir = (owner:GetAimVector():Angle() + owner:GetViewPunchAngles()):Forward(),
		Spread = Vector(.03, .03, 0),
		Num = iBullets,
		Damage = self.Primary.Damage,
		Callback = function(ent, tr, dmg)
			-- this fixes oc_nerv's flamethrow turret
			dmg:SetDamageType(DMG_BULLET)
		end
	})

	if owner:IsPlayer() then
		owner:ViewPunchReset(2)

		owner:ViewPunch(Angle(util.SharedRandom("pistolpax", -.1, -.3), util.SharedRandom("pistolpay", -.2, .2), 0) * .5)
	end

	self:EmitSound"Weapon_Uzi.Single"

	if self:GetIsDual() then
		if self:GetRightFire() then
			self:SetWeaponSequence("dualfireright")
			self:SetRightAmmo(self:GetRightAmmo() - 1)
		else
			self:SetWeaponSequence("dualfireleft")
		end
	else
		self:SetRightAmmo(self:GetRightAmmo() - 1)
		self:SetWeaponSequence("singlefire")
	end

	owner:SetAnimation(PLAYER_ATTACK1)
	owner:MuzzleFlash()

	self:SetShotsFired(self:GetShotsFired() + 1)

	self:TakePrimaryAmmo(1)
	self:SetNextPrimaryFire(CurTime() + (.075 * (self:GetIsDual() and .75 or 1)))
end

function SWEP:CanPrimaryAttack()
	if self:GetIsDeploying() then return false end
	return BaseClass.CanPrimaryAttack(self)
end

function SWEP:PerformIdleAnimation()
	if self:GetIsDual() then
		if self:GetDeployingState() == DEPLOYSTATE_DEPLOY_SINGLE then
			self:SetDeployingState(DEPLOYSTATE_DEPLOY_DUAL)
			self:SetWeaponSequence("drawdual")
		else
			self:SetDeployingState(DEPLOYSTATE_FINISHED)
			self:SetWeaponSequence("Uzi_IdleDual")
		end
	else
		if self:GetIsDeploying() then
			self:SetDeployingState(DEPLOYSTATE_FINISHED)
		end

		self:SetWeaponSequence("Uzi_IdleSingle")
	end
end

function SWEP:SecondaryAttack()
	if self:GetHasDual() then
		self:ToggleDual()
	end
end

function SWEP:PerformReloadAnimation()
	self:SetWeaponSequence(self:GetIsDual() and "reloaddual" or "reloadsingle")
end

function SWEP:OnStartReload()
	self:SetFinishReloadTime(CurTime() + (self:GetIsDual() and 5 or 2.5))
	self:SetNextPrimaryFire(CurTime() + (self:GetIsDual() and 5 or 2.5))
	self:SetNextSecondaryFire(CurTime() + (self:GetIsDual() and 5 or 2.5))
end

function SWEP:OnFinishReload()
	---@class Player
	local owner = self:GetOwner()
	self:SetRightAmmo(math.min(owner:GetAmmoCount(self.Primary.Ammo), 30))
end

function SWEP:GetMaxAmmo1()
	local max = self:GetMaxClip1()
	return self:GetIsDual() and max * 2 or max
end

function SWEP:ToggleDual()
	---@class Player
	local owner = self:GetOwner()

	self:SetIsDual(not self:GetIsDual())

	if self:GetIsDual() then
		local min = math.min(owner:GetAmmoCount(self.Primary.Ammo), 30)
		self:SetClip1(self:Clip1() + min)
		owner:RemoveAmmo(min, self.Primary.Ammo)
		self:SetWeaponSequence("drawdual")
		self:SetHoldType("duel")
	else
		local dif = self:Clip1() - self:GetRightAmmo()
		self:SetClip1(self:GetRightAmmo())
		if SERVER then
			owner:GiveAmmo(dif, self.Primary.Ammo, true)
		end
		self:SetWeaponSequence("drawsingle")
		self:SetHoldType("pistol")
	end

	self:SetNextPrimaryFire(CurTime() + .5)
	self:SetNextSecondaryFire(CurTime() + .5)
end

if SERVER then
	hook.Add("PlayerCanPickupWeapon", "weapon_uzi", function(ply, wep)
		local _wep = ply:GetWeapon"weapon_uzi"
		if _wep:IsValid() and not _wep:GetHasDual() and wep:GetClass() == "weapon_uzi" then
			_wep:SetHasDual(true)
			_wep:ToggleDual()
			hook.Run("WeaponEquip", wep, ply)
			return true
		end
	end)
else
	function SWEP:DrawWorldModel()
		self:DrawModel()

		---@class Player
		local owner = self:GetOwner()
		if owner:IsValid() and owner:IsPlayer() and self:GetHoldType() == "duel" then
			local bnep, bnea = owner:GetBonePosition(owner:LookupBone("ValveBiped.Bip01_L_Hand"))
			if not IsValid(self.m_hLeftModel) then
				local m = ClientsideModel("models/weapons/w_uzi_l.mdl")

				m:SetNoDraw(true)
				self.m_hLeftModel = m
			end

			bnep:Add(bnea:Right() * 3)
			bnep:Add(bnea:Up() * 3)
			bnep:Add(bnea:Forward() * 5)

			bnea:RotateAroundAxis(bnea:Right(), 0)
			bnea:RotateAroundAxis(bnea:Up(), 0)
			bnea:RotateAroundAxis(bnea:Forward(), -90)

			self.m_hLeftModel:SetPos(bnep)
			self.m_hLeftModel:SetAngles(bnea)

			self.m_hLeftModel:DrawModel()
			render.RenderFlashlights(function()
				if self.m_hLeftModel:IsValid() then
					self.m_hLeftModel:DrawModel()
				end
			end)
		end
	end
end
