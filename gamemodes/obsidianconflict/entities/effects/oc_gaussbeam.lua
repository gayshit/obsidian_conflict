EFFECT.Mat = CreateMaterial("oc_gaussbeam", "Sprite", util.KeyValuesToTable([[
"Sprite"
{
    "$basetexture" "sprites/laserbeam"
    "$vertexalpha" "1"
    "$translucent" "1"
    "$vertexcolor" "1"
    "$nocull" "1"
    "$spriterendermode" "5"
}
]]))

EFFECT.TracerTime = 0.1

function EFFECT:Init(data)
	self.StartPos = data:GetStart()
	self.EndPos = data:GetOrigin()
	self.Width = data:GetScale()

	self.WeaponEnt = data:GetEntity()
	self.Attachment = data:GetAttachment()

	self.Delta = self.EndPos - self.StartPos
	self.Dir = Vector(self.Delta)
	self.Dir:Normalize()

	self:SetRenderBoundsWS(self.StartPos, self.EndPos)

	-- Die when it reaches its target
	self.DieTime = CurTime() + self.TracerTime

	if CLIENT then
		util.Decal("RedGlowFade", self.StartPos, self.EndPos + (self.Dir * 1.1), {self, self.WeaponEnt:GetOwner()})
	end
end

function EFFECT:Think()
	if (CurTime() > self.DieTime) then
		-- Awesome End Sparks
		local effectdata = EffectData()
		effectdata:SetOrigin(self.EndPos + self.Dir * -5)
		effectdata:SetNormal(self.Dir * -3)
		effectdata:SetMagnitude(1)
		effectdata:SetScale(1)
		effectdata:SetRadius(6)
		util.Effect("Sparks", effectdata)

		return false
	end

	return true
end

function EFFECT:RenderGauss()
	local WeaponEnt = self.WeaponEnt
	if not IsValid(WeaponEnt) then return end

	local origin = self.Attachment ~= -1 and self:GetTracerShootPos(self.StartPos, WeaponEnt, self.Attachment) or self.StartPos
	if not origin then return end

	local owner = WeaponEnt:GetOwner()
	if not owner:IsValid() then return end

	render.SetMaterial(self.Mat)

	local vDir = (self.EndPos - origin)
	local flDist = vDir:Length()
	vDir:Normalize()

	local col = owner:GetWeaponColor():ToColor()
	render.DrawBeam(origin, self.EndPos, math.abs(0.5 - self.Width), 1, 1, col)

	self:GaussArc(origin, vDir, flDist)
	self:GaussArc(origin, vDir, flDist)
end

function EFFECT:GaussArc(origin, dir, dist)
	local amount = math.Round(dist / 100, 1)

	local positions = {}
	for i = 1, amount do
		local intensitivity = math.sin((i / amount) * 180 * (math.pi / 180))

		local arc_offset = dir * i * (dist / amount) + self.side:Right() * intensitivity * dist * 0.02
		local noise = Vector(math.random(-1, 1), math.random(-1, 1), math.random(-1, 1)) * intensitivity

		table.insert(positions, origin + arc_offset + noise)

		render.DrawBeam(positions[i - 1] or origin, positions[i], math.abs(3 - self.Width), 1, 1, Color(255, 255, 150 + math.random(0, 64), math.random(64, 255)))
	end
end

function EFFECT:Render()
	self.side = self.Dir:Angle()
	self.side:RotateAroundAxis(self.Dir, math.random(-180, 180))

	self:RenderGauss()
end
