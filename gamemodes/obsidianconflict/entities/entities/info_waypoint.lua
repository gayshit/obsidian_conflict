ENT.Type = "point"
ENT.Spawnable = false

local TAG = "info_waypoint"
if SERVER then
	util.AddNetworkString(TAG)
end
--[[
    FIXME
    edit on oc_payson, ent name: way_out
    enable for like 15s, then disable
]]

function ENT:Initialize()
	--self.Enabled = not self:HasSpawnFlags(SF_MSGMP_STARTDISABLED)

	--self:SendFullUpdate()

	hook.Add("PlayerFullyConnected", self, function(self, ply)
		self:SendFullUpdate(ply)
	end)
end

function ENT:SendFullUpdate(ply)
	net.Start(TAG)
	net.WriteUInt(0, 8)
	net.WriteUInt(self:EntIndex(), 16)
	net.WriteString(self.image or "")
	net.WriteString(self.text or "Waypoint")
	net.WriteEntity(self:GetParent())
	net.WriteVector(self:GetPos())
	net.WriteBool(self.EnabledGlobal)

	if IsValid(ply) and ply:IsPlayer() then
		net.Send(ply)
	else
		net.Broadcast()
	end
end

function ENT:AcceptInput(inp, actor, caller, data)
	inp = inp:lower()
	if inp == "enable" or inp == "disable" then
		self.EnabledGlobal = inp == "enable"
		self:SendFullUpdate()
		return true
	elseif (inp == "enableforactivator" or inp == "disableforactivator") and actor:IsPlayer() then
		net.Start(TAG)
		net.WriteUInt(1, 8)
		net.WriteUInt(self:EntIndex(), 16)
		net.WriteString(self.image)
		net.WriteString(self.text or "Waypoint")
		net.WriteEntity(self:GetParent())
		net.WriteVector(self:GetPos())
		net.WriteFloat(inp == "enableforactivator" and tonumber(data) or 0)
		---@diagnostic disable-next-line: param-type-mismatch
		net.Send(actor)
		return true
	elseif inp == "kill" then
		net.Start(TAG)
		net.WriteUInt(2, 8)
		net.WriteUInt(self:EntIndex(), 16)
		net.Broadcast()
		return true
	else
		print(self, "AcceptInput()", inp, actor, caller, data)
	end
end

function ENT:KeyValue(key, val)
	local k = key:lower()
	if k == "image" then
		self[k] = val
	elseif k == "text" then
		self[k] = val:gsub("%a+", GAMEMODE.__GsubSpellingMistakes):gsub("^%l", string.upper)
	end
end
