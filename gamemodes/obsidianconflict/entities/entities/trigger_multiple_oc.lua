ENT.Type = "brush"
ENT.Spawnable = false
ENT.Enabled = true

function ENT:Initialize()
	if self:HasSpawnFlags(SF_TRIGGER_ONLY_PLAYER_ALLY_NPCS) or self:HasSpawnFlags(SF_TRIGGER_ONLY_NPCS_IN_VEHICLES) then
		-- Automatically set this trigger to work with NPC's.
		self:SetKeyValue("spawnflags", bit.bor(self:GetSpawnFlags(), SF_TRIGGER_ALLOW_NPCS))
	end

	if self:HasSpawnFlags(SF_TRIGGER_ONLY_CLIENTS_IN_VEHICLES) then
		self:SetKeyValue("spawnflags", bit.bor(self:GetSpawnFlags(), SF_TRIGGER_ALLOW_CLIENTS))
	end

	if self:HasSpawnFlags(SF_TRIGGER_ONLY_CLIENTS_OUT_OF_VEHICLES) then
		self:SetKeyValue("spawnflags", bit.bor(self:GetSpawnFlags(), SF_TRIGGER_ALLOW_CLIENTS))
	end

	timer.Simple(0, function()
		if self:IsValid() then
			self:_Filter()
		end
	end)
end

-- internal
function ENT:_Filter()
	for ent in next, self.m_TouchingEntities do
		if not self:PassesTriggerFilter(ent) then
			self.m_TouchingEntities[ent] = nil
		end
	end
end

function ENT:AcceptInput(inp, actor, caller, data)
	if inp == "Enable" then
		self.Enabled = true
	elseif inp == "Disable" then
		self.Enabled = false
	elseif inp == "TouchTest" then
		if self.Enabled then
			self:_Filter()

			if not table.IsEmpty(self.m_TouchingEntities) then
				self:TriggerOutput("OnTouching", actor)
			else
				self:TriggerOutput("OnNotTouching", actor)
			end
		end

		return true
	end
end

function ENT:KeyValue(key, val)
	if string.Left(key, 2) == "On" then
		Obsidian.StoreOutputSafe(self, key, val)
	elseif key == "StartDisabled" then
		self.Enabled = not tobool(val)
	elseif key == "filtername" then
		self.filtername = val
	end
end

FILTER_AND = 0
FILTER_OR = 1
MAX_FILTERS = 5

local filter_passesfilter = {}
filter_passesfilter.filter_activator_name = function(this, caller, ent)
	local m_iFilterName = this:GetKeyValues().filtername
	if m_iFilterName == "!player" then
		return ent:IsPlayer()
	else
		return ent:GetName():find(m_iFilterName)
	end
end
filter_passesfilter.filter_multi = function(this, caller, ent)
	local kvs = this:GetKeyValues()
	local m_nFilterType = kvs.FilterType
	if m_nFilterType == FILTER_AND then
		for i = 0, MAX_FILTERS do
			if kvs["Filter0" .. i] then
				local filter = ents.FindByName(kvs["Filter0" .. i])[1]
				if not filter_passesfilter[filter:GetClass()](filter, caller, ent) then
					return false
				end
			end
		end

		return true
	else -- m_nFilterType == FILTER_OR
		for i = 0, MAX_FILTERS do
			if kvs["Filter0" .. i] then
				local filter = ents.FindByName(kvs["Filter0" .. i])[1]
				if not filter_passesfilter[filter:GetClass()](filter, caller, ent) then
					return true
				end
			end
		end

		return false
	end
end
filter_passesfilter.filter_activator_class = function(this, caller, ent)
	local m_iFilterClass = this:GetKeyValues().filterclass
	return ent:GetClass():find(m_iFilterClass) ~= nil
end

function ENT:PassesTriggerFilter(ent)
	if self:HasSpawnFlags(SF_TRIGGER_ALLOW_ALL) or
			(self:HasSpawnFlags(SF_TRIGGER_ALLOW_CLIENTS) and ent:IsPlayer()) or
			(self:HasSpawnFlags(SF_TRIGGER_ALLOW_NPCS) and ent:IsNPC()) or
			(self:HasSpawnFlags(SF_TRIGGER_ALLOW_PUSHABLES) and ent:GetClass() == "func_pushable") or
			(self:HasSpawnFlags(SF_TRIGGER_ALLOW_PHYSICS) and ent:GetMoveType() == MOVETYPE_VPHYSICS) or
			(self:HasSpawnFlags(SF_TRIG_TOUCH_DEBRIS) and (
				ent:GetCollisionGroup() == COLLISION_GROUP_DEBRIS or
				ent:GetCollisionGroup() == COLLISION_GROUP_DEBRIS_TRIGGER or
				ent:GetCollisionGroup() == COLLISION_GROUP_INTERACTIVE_DEBRIS))
	then
		if ent:IsNPC() then
			if self:HasSpawnFlags(SF_TRIGGER_ONLY_PLAYER_ALLY_NPCS) and ent:Classify() ~= CLASS_PLAYER_ALLY then
				return false
			end
			if self:HasSpawnFlags(SF_TRIGGER_ONLY_NPCS_IN_VEHICLES) and not ent:InVehicle() then
				return false
			end
		end

		if ent:IsPlayer() then
			if not ent:Alive() then return false end

			if self:HasSpawnFlags(SF_TRIGGER_ONLY_CLIENTS_IN_VEHICLES) then
				if not ent:InVehicle() then return false end
				if not IsValid(ent:GetVehicle()) then return false end
			end
			if self:HasSpawnFlags(SF_TRIGGER_ONLY_CLIENTS_OUT_OF_VEHICLES) and ent:InVehicle() then return false end
			if self:HasSpawnFlags(SF_TRIGGER_DISALLOW_BOTS) and ent:IsBot() then return false end
		end

		if self.filtername and self.filtername ~= "" then
			local filter = ents.FindByName(self.filtername)[1]

			if filter and filter:IsValid() then
				local filter_func = filter_passesfilter[filter:GetClass()]
				if filter_func then
					return filter_func(filter, self, ent)
				else
					print("[OC] valid filter but no filter func", self, self:GetName(), self.filtername, filter)
					return false
				end
			else
				return false
			end
		else
			return true
		end
	end

	return false
end

ENT.m_TouchingEntities = {}
ENT.IsOnce = false
function ENT:StartTouch(ent)
	local bAdded = false
	if not self.m_TouchingEntities[ent] then
		self.m_TouchingEntities[ent] = true
		bAdded = true
	end

	if not self.IsOnce and self.Enabled and self:PassesTriggerFilter(ent) then
		if not self.DoesCustomOutput then
			self:TriggerOutput("OnStartTouch", ent)
			self:TriggerOutput("OnTrigger", ent)
		end

		self:OnStartTouch(ent, bAdded and table.Count(self.m_TouchingEntities) == 1)
		--self:Fire("StartTouch", nil, 0, NULL, ent)

		if bAdded and table.Count(self.m_TouchingEntities) == 1 and not self.DoesCustomOutput then
			-- First entity to touch us that passes our filters
			self:TriggerOutput("OnStartTouchAll", ent)
		end
	end
end

function ENT:EndTouch(ent)
	if self.m_TouchingEntities[ent] then
		self.m_TouchingEntities[ent] = nil
		if not self.DoesCustomOutput then
			self:TriggerOutput("OnEndTouch", ent)
		end

		-- If there are no more entities touching this trigger, fire the lost all touches
		-- Loop through the touching entities backwards. Clean out old ones, and look for existing
		local bFoundOtherTouchee = false
		for e in next, self.m_TouchingEntities do
			if not IsValid(e) then
				self.m_TouchingEntities[e] = nil
			elseif e:IsPlayer() and not e:Alive() then
				self.m_TouchingEntities[e] = nil
			else
				bFoundOtherTouchee = true
			end
		end

		self:OnEndTouch(ent, not bFoundOtherTouchee)
		--self:Fire("EndTouch", nil, 0, NULL, ent)

		if not bFoundOtherTouchee and not self.DoesCustomOutput then
			self:TriggerOutput("OnEndTouchAll", ent)
		end
	end
end

function ENT:OnStartTouch(ent, was_empty)
	--
end

function ENT:OnEndTouch(ent, is_empty)
	--
end
