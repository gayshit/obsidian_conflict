AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_ammo"
ENT.Model = Model("models/items/boxmrounds.mdl")

DEFINE_BASECLASS(ENT.Base)

function ENT:SetupDataTables()
	self:NetworkVar("String", 0, "AmmoModel")
end

function ENT:KeyValue(key, val)
	local k = key:lower()
	if k == "ammoname" then
		self.AmmoType = tostring(val)
	elseif k == "amount" then
		self.AmmoAmount = tonumber(val)
	elseif k == "model" then
		self.Model = Model(tostring(val))
		self:SetAmmoModel(self.Model)
		self:SetModel(self.Model)
	end
end

function ENT:Initialize()
	if CLIENT then
		self.Model = self:GetAmmoModel()
	end

	BaseClass.Initialize(self)
end
