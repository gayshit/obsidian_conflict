AddCSLuaFile()

ENT.Type = "anim"
ENT.Model = Model("models/items/boxsrounds.mdl")

local ITEM_PICKUP_BOX_BLOAT = 24

function ENT:Initialize()
	self:SetModel(self.Model)

	self:SetMoveType(MOVETYPE_VPHYSICS)

	if SERVER then
		self:PhysicsInit(SOLID_VPHYSICS)
	end

	self:SetSolid(SOLID_BBOX)

	self:SetCollisionGroup(COLLISION_GROUP_WEAPON)
	--local b = 26
	--self:SetCollisionBounds(Vector(-b, -b, -b), Vector(b,b,b))

	if SERVER then
		self:SetTrigger(true)
		self:UseTriggerBounds(true, ITEM_PICKUP_BOX_BLOAT)
		self:SetUseType(SIMPLE_USE)
	end
end

if SERVER then
	hook.Add("GravGunOnPickedUp", "oc.base_item", function(ply, ent)
		if isfunction(ent.OnGravGunPickedUp) then
			ent:OnGravGunPickedUp(ply)
		end
	end)
	hook.Add("GravGunOnDropped", "oc.base_item", function(ply, ent)
		if isfunction(ent.OnGravGunDropped) then
			ent:OnGravGunDropped(ply)
		end
	end)

	function ENT:OnGravGunPickedUp(ply)
		self:UseTriggerBounds(true, ITEM_PICKUP_BOX_BLOAT * 2)
	end

	function ENT:OnGravGunDropped(ply)
		self:UseTriggerBounds(true, ITEM_PICKUP_BOX_BLOAT)
	end
end


function ENT:Use(activator, caller, useType, value)
	if activator:IsPlayer() and not self:IsPlayerHolding() then
		activator:PickupObject(self)
	end
end

function ENT:OnTouchPlayer(ent)
	-- REPLACE ME
end

function ENT:OnTouchNPC(ent)
	-- REPLACE ME
end

function ENT:Touch(ent)
	if SERVER and ent:IsValid() then
		if ent:IsPlayer() then
			self:OnTouchPlayer(ent)
		elseif ent:IsNPC() then
			self:OnTouchNPC(ent)
		end
	end
end

if SERVER then
	function ENT:Think()
		if not self.first_think then
			self:PhysWake()
			self.first_think = true

			-- Immediately unhook the Think, save cycles. The first_think thing is
			-- just there in case it still Thinks somehow in the future.
			self.Think = nil
		end
	end
end
