ENT.Type = "point"
ENT.Spawnable = false

local TAG = "point_message_multiplayer"

SF_MSGMP_STARTDISABLED = 0x01

function ENT:Initialize()
	self.Enabled = not self:HasSpawnFlags(SF_MSGMP_STARTDISABLED)

	self:SendFullUpdate()

	hook.Add("PlayerFullyConnected", self, function(self, ply)
		self:SendFullUpdate(ply)
	end)
end

function ENT:SendFullUpdate(ply)
	if not IsValid(ply) and #player.GetHumans() == 0 then
		return
	end

	net.Start(TAG)
	net.WriteUInt(self:EntIndex(), 16)
	net.WriteUInt(0, 4)
	net.WriteBool(self.Enabled)
	net.WriteFloat(self.radius)
	net.WriteString(self.message or "")
	net.WriteVector(self:GetParent():IsValid() and self:GetLocalPos() or self:GetPos())
	net.WriteColor(self.textcolor or color_white)
	net.WriteUInt(self:GetParent():EntIndex(), 16)

	if IsValid(ply) and ply:IsPlayer() then
		net.Send(ply)
	else
		net.Broadcast()
	end
end

function ENT:ChangeMessage(new)
	self.message = new:gsub("%a+", GAMEMODE.__GsubSpellingMistakes)

	self:SendFullUpdate()
end

function ENT:AcceptInput(inp, actor, caller, data)
	if inp == "ChangeMessage" then
		self:ChangeMessage(data)
	elseif inp == "Enable" or inp == "Disable" then
		self.Enabled = inp == "Enable"
		self:SendFullUpdate()
	elseif inp == "Kill" then
		net.Start(TAG)
		net.WriteUInt(self:EntIndex(), 16)
		net.WriteUInt(3, 4)
		net.Broadcast()
	end
end

function ENT:KeyValue(key, val)
	local k = key:lower()
	if k == "message" then
		self:ChangeMessage(val)
	elseif k == "radius" then
		self[k] = val
	elseif k == "textcolor" then
		self[k] = Color(val:match("(%d+) (%d+) (%d+)"))
	end
end

