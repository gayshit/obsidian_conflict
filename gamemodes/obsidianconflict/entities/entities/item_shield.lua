AddCSLuaFile()

ENT.Base = "base_item"
ENT.Model = Model("models/modules/w_powermodule.mdl")

function ENT:OnTouchPlayer(ent)
	if self._Touched then return end

	if not ent:GetHasShield() then
		self._Touched = true

		ent:SetHasShield(true)
		SafeRemoveEntityDelayed(self, 0)
	end
end
