AddCSLuaFile()

ENT.Type = "point"
ENT.Spawnable = false

DEFINE_BASECLASS("base_entity")

SF_PLAYEREQUIP_USEONLY = 0x0001
local MAX_EQUIP = 32

ENT.m_weaponNames = {}

function ENT:KeyValue(key, val)
	if key:StartsWith("item_") or key:StartsWith("weapon_") or key:StartsWith("custom_") then -- bad!!!!!
		for i = 1, MAX_EQUIP + 1 do
			if not self.m_weaponNames[i] then
				self.m_weaponNames[i] = GAMEMODE and GAMEMODE.LoadoutReplacementWeapons and GAMEMODE.LoadoutReplacementWeapons[key] or key
				return true
			end
		end
	end

	return false
end

function ENT:EquipPlayer(ply)
	if not IsValid(ply) or not ply:IsPlayer() then return end

	for i = 1, MAX_EQUIP + 1 do
		if not self.m_weaponNames[i] then break end

		for j = 1, #self.m_weaponNames do
			Obsidian.GivePlayerWeapon(ply, self.m_weaponNames[i])
		end
	end
end

function ENT:Touch(ent)
	if not IsValid(ent) then return end
	if bit.band(self:GetSpawnFlags(), SF_PLAYEREQUIP_USEONLY) > 0 then return end

	self:EquipPlayer(ent)
end

function ENT:Use(activator, caller, useType, value)
	self:EquipPlayer(activator)
end

function ENT:AcceptInput(inp, activator, caller, data)
	if inp == "EquipActivator" then
		self:EquipPlayer(activator)
	else
		return BaseClass.AcceptInput(self, inp, activator, caller, data)
	end
end

