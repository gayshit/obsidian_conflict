AddCSLuaFile()

ENT.Base = "base_item"
ENT.Model = Model("models/modules/w_cloak.mdl")

function ENT:OnTouchPlayer(ent)
	if self._Touched then return end

	if not ent:GetHasCloak() then
		self._Touched = true

		ent:SetHasCloak(true)
		SafeRemoveEntityDelayed(self, 0)
	end
end
