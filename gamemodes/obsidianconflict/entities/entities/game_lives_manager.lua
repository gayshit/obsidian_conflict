ENT.Type = "point"
ENT.Spawnable = false

--[[
    input GiveLivesToActivator(integer) : "Gives lives to the activator."
    input GiveLivesToActivatorTeam(integer) : "Gives lives to the activator and their team."
    input GiveLivesToEnemyTeam(integer) : "Gives lives to enemy team."
    input GiveLivesToBlueTeam(integer) : "Gives lives to the Blue team."
    input GiveLivesToRedTeam(integer) : "Gives lives to the Red team."
    input GiveLivesToAllPlayers(integer) : "Gives lives all players."
]]
ENT.InputFuncs = {
	GiveLivesToActivator = function(actor, caller, iLives)
		actor:AddLives(iLives)
	end,
	GiveLivesToActivatorTeam = function(actor, caller, iLives)
		for _, ply in ipairs(team.GetPlayers(actor:Team())) do
			ply:AddLives(iLives)
		end
	end,
	GiveLivesToEnemyTeam = function(actor, caller, iLives)
		local _team = TEAM_RED
		if actor:Team() == TEAM_RED then
			_team = TEAM_BLUE
		end

		for _, ply in ipairs(team.GetPlayers(_team)) do
			ply:AddLives(iLives)
		end
	end,
	GiveLivesToBlueTeam = function(actor, caller, iLives)
		for _, ply in ipairs(team.GetPlayers(TEAM_BLUE)) do
			ply:AddLives(iLives)
		end
	end,
	GiveLivesToRedTeam = function(actor, caller, iLives)
		for _, ply in ipairs(team.GetPlayers(TEAM_RED)) do
			ply:AddLives(iLives)
		end
	end,
	GiveLivesToAllPlayers = function(actor, caller, iLives)
		for _, ply in player.Iterator() do
			ply:AddLives(iLives)
		end
	end,
}
function ENT:AcceptInput(inp, actor, caller, data)
	if self.InputFuncs[inp] then
		local iLives = math.floor(tonumber(data))
		self.InputFuncs[inp](actor, caller, iLives)
		return true
	else
		print(self, "acceptinput()", inp, actor, caller, data)
	end
end

function ENT:KeyValue(key, val)
	local k = key:lower()

	print(self, "keyval()", k, val)
end
