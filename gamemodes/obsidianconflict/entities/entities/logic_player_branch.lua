ENT.Type = "point"
ENT.Spawnable = false

function ENT:AcceptInput(inp, actor, caller, data)
	if inp == "Test" then
		if (self.strictequalsto and player.GetCount() == self.initialvalue) or (not self.strictequalsto and player.GetCount() >= self.initialvalue) then
			self:TriggerOutput("OnTrue", actor)
		else
			self:TriggerOutput("OnFalse", actor)
		end
	elseif inp == "SetValue" then
		self.initialvalue = tonumber(data)
	end
end

function ENT:KeyValue(key, val)
	local k = key:lower()
	if string.Left(key, 2) == "On" then
		Obsidian.StoreOutputSafe(self, key, val)
	elseif k == "initialvalue" then
		self[k] = tonumber(val)
	elseif k == "strictequalsto" then
		self[k] = tobool(val)
	end
end
