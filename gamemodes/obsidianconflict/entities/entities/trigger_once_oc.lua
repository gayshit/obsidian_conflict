ENT.Type = "brush"
ENT.Base = "trigger_multiple_oc"
ENT.Spawnable = false
ENT.Enabled = true
ENT.IsOnce = true

--[[
function ENT:StartTouch(ent)
	print(self, ent)
	if self.Enabled and self:PassesTriggerFilter(ent) then
		self:TriggerOutput("OnStartTouch", ent)
		self:TriggerOutput("OnTrigger", ent)
		SafeRemoveEntityDelayed(self, .1)
	end
end
]]

function ENT:Touch(ent)
	if self.Enabled and self.m_TouchingEntities[ent] and self:PassesTriggerFilter(ent) then
		self:TriggerOutput("OnStartTouch", ent)
		self:TriggerOutput("OnTrigger", ent)

		self.Enabled = false
		SafeRemoveEntityDelayed(self, 0)
	end
end
