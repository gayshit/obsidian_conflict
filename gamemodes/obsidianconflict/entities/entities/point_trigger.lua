ENT.Type = "point"
ENT.Base = "trigger_multiple_oc"
ENT.Spawnable = false
DEFINE_BASECLASS(ENT.Base)
ENT.m_flRadius = 0
ENT.Enabled = true
ENT.IsOnce = false
ENT.__inRadius = {}

function ENT:Initialize()
	self:SetTrigger(true)
	BaseClass.Initialize(self)
end

function ENT:Think()
	if CLIENT then return end

	local inSphere = ents.FindInSphere(self:GetPos(), self.m_flRadius)
	for _, ent in pairs(inSphere) do
		if not self:PassesTriggerFilter(ent) then continue end
		if not IsValid(ent) then continue end
		if not self.__inRadius[ent] then
			self.__inRadius[ent] = true
			self:StartTouch(ent)
		end
		self:Touch(ent)
	end

	for ent in pairs(self.__inRadius) do
		if not IsValid(ent) then
			self.__inRadius[ent] = nil
			continue
		end
		if ent:GetPos():Distance(self:GetPos()) > self.m_flRadius / 2 then
			self.__inRadius[ent] = nil
			self:EndTouch(ent)
		end
	end
end

function ENT:KeyValue(key, val)
	--print(self, "KeyValue", key, val)
	if key == "TriggerRadius" then
		self.m_flRadius = tonumber(val)
	elseif key == "TriggerOnce" then
		self.IsOnce = not tobool(val)
	else
		BaseClass.KeyValue(self, key, val)
	end
end

function ENT:Touch(ent)
	--print(self, "Touch", ent)
	if self.Enabled and self.IsOnce and self.m_TouchingEntities[ent] and self:PassesTriggerFilter(ent) then
		self:TriggerOutput("OnStartTouch", ent)
		self:TriggerOutput("OnTrigger", ent)

		self.Enabled = false
		SafeRemoveEntityDelayed(self, 0)
	end
end

--[[function ENT:StartTouch(ent)
    print(self, "StartTouch", ent)
    BaseClass.StartTouch(self, ent)
end

function ENT:EndTouch(ent)
    print(self, "EndTouch", ent)
    BaseClass.EndTouch(self, ent)
end--]]

