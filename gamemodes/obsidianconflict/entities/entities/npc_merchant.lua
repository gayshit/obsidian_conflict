AddCSLuaFile()

ENT.Base = "base_ai"
ENT.Type = "ai"
ENT.PrintName = "merchant"
ENT.Spawnable = false
ENT.AutomaticFrameAdvance = true

local TAG = "npc_merchant"
if SERVER then
	net.Receive(TAG, function(_, ply)
		local what = net.ReadInt(2)
		local class = net.ReadString()
		local num = net.ReadUInt(8)
		local data = ply.oc_merchant.MerchantScript

		if what == 0 then
			if ply:Frags() >= data.Weapons[class] then
				local wep = ply:GetWeapon(class)

				-- special case for the uzi
				if class == "weapon_uzi" and wep:IsValid() and not wep:GetHasDual() then
					wep:SetHasDual(true)
					wep:SetIsDual(true)
					wep:SetClip1(wep:GetClip1() + 30)
				else
					if data.Weapons[class] and not wep:IsValid() then
						local e = Obsidian.GivePlayerWeapon(ply, class)
						if not e:IsValid() then
							print("[OC] failed to create merchant item", class)
							return
						end

						if e:GetClass() == "weapon_alyxgunr" then
							e.Primary.DefaultClip = e.Primary.ClipSize
						end
					else
						ply:ChatPrint("you already have it nerd, buy ammo")
						return
					end
				end

				ply:AddFrags(-data.Weapons[class])
				ply:ChatPrint("you lose " .. data.Weapons[class] .. " money lol :D")
			else
				ply:ChatPrint("u need more money nerd")
			end
		elseif what == 1 then
			if data.Items[class] then
				local amt = data.Items[class] * num
				if ply:Frags() >= amt then
					for i = 1, num do
						local e = Obsidian.CreateEntNoRespawn(class)
						e:SetPos(ply:GetPos())
						e:Spawn()
					end

					ply:ChatPrint("you lose " .. amt .. " money lol :D")
					ply:AddFrags(-amt)
				else
					ply:ChatPrint("u need more money nerd")
				end
			end
		end
	end)
else
	net.Receive(TAG, function()
		local data = net.ReadTable()

		local f = vgui.Create("DFrame")
		f:MakePopup()
		f:SetSize(420, 360)
		f:SetPos(85, 100)
		f:SetDraggable(false)
		f:ShowCloseButton(false)
		f:SetTitle(Format("Merchant :: %s", data.name or data.Name or "STINKY PETES WARES"))

		local but = vgui.Create("DButton", f)
		but:Dock(BOTTOM)
		but:SetText"OK"
		function but:DoClick()
			f:Remove()
		end

		local _ = vgui.Create("DSizeToContents", f)
		_:SetSizeX(false)
		_:Dock(LEFT)
		_:DockPadding(0, 10, 0, 0)
		_:SetWide(_:GetWide() + 20)

		local __ = vgui.Create("DSizeToContents", f)
		__:SetSizeX(false)
		__:Dock(FILL)
		__:DockPadding(20, 10, 0, 0)
		__:InvalidateLayout()

		local price_z = -100
		local function sell(whats, title, left, right, id, use_num_slider)
			local _text
			local price
			local item_text
			local items
			local buy
			local slider

			_text = vgui.Create("DLabel", left)
			_text:SetText"Price: "
			_text:Dock(TOP)

			price = vgui.Create("DLabel", right)
			price:SetText""
			price:Dock(TOP)
			price:SetZPos(price_z)

			item_text = vgui.Create("RichText", right)
			item_text:Dock(TOP)
			item_text:SetTall(116)
			function item_text:PerformLayout()
				self:SetBGColor(Color(72, 72, 72))
				self:SetFontInternal"HudHintTextLarge"
			end

			item_text:SetZPos(price_z + 1)
			price_z = price_z + 2

			items = vgui.Create("DComboBox", left)
			items:Dock(TOP)
			items:SetValue(title .. "s")
			function items:OnSelect(_, str, _data)
				local text = language.GetPhrase(string.format("#info_%s", str)):gsub("\\n", "\n")
				item_text:SetText(text)
				price:SetText(_data)
				if slider then
					slider:SetValue(1)
				end

				timer.Simple(0, function()
					item_text:GotoTextStart()
				end)
			end

			buy = vgui.Create("DButton", left)
			buy:Dock(TOP)
			buy:SetText("Buy " .. title)
			function buy:DoClick()
				local class = items:GetSelected()
				if class then
					net.Start(TAG)
					net.WriteInt(id, 2)
					net.WriteString(class)
					net.WriteUInt(slider and slider:GetValue() or 1, 8)
					net.SendToServer()
				end
			end

			if whats then
				for k, v in next, whats do
					items:AddChoice(k, v)
				end
			end

			if use_num_slider then
				slider = vgui.Create("DNumSlider", left)
				slider:Dock(TOP)
				slider:SetMin(1)
				slider:SetValue(1)
				slider:SetMax(10)
				slider:SetDecimals(0)
				slider:SetText"Amount: 1"

				slider.Label:Dock(TOP)

				function slider:ApplySchemeSettings()
					self.Label:ApplySchemeSettings()

					-- Copy the color of the label to the slider notches and the text entry
					local col = self.Label:GetTextStyleColor()
					if (self.Label:GetTextColor()) then col = self.Label:GetTextColor() end

					local col = table.Copy(col)
					col.a = 100 -- Fade it out a bit so it looks right
					self.Slider:SetNotchColor(col)
				end

				-- fuck you textarea fucking nuisance piece of fucking shit
				slider.TextArea:Remove()
				function slider:ValueChanged(val)
					val = math.Clamp(tonumber(val) or 0, self:GetMin(), self:GetMax())

					self.Slider:SetSlideX(self.Scratch:GetFraction(val))
					self:OnValueChanged(val)
				end

				function slider:OnValueChanged(val)
					local _, amt = items:GetSelected()
					self.Label:SetText("Amount: " .. math.floor(val))
					if amt then
						price:SetText(amt * math.floor(val))
					end
				end
			end
		end

		sell(data.Weapons, "Weapon", _, __, 0)

		for i = 1, 3 do
			-- pad left a bit
			local ___ = vgui.Create("DPanel", _)
			___.Paint = function() end
			___:Dock(TOP)
		end

		sell(data.Items, "Item", _, __, 1, true)
	end)
end

function ENT:DoMerchantScript()
	if self.MerchantScript then return end

	local path = Format("scripts/merchants/%s.txt", self:GetMerchantScript())

	if not file.Exists(path, "GAME") then
		print(Format("Merchant Error: Merchant txt file does not exist: %s.txt - Using Default", script))
		path = "scripts/merchants/merchant_test.txt"
	end

	if not file.Exists(path, "GAME") then
		-- this is the actual error in the mod.
		ErrorNoHalt("Merchant Error: HOLY MEGA FAIL BATMAN, NO DEFAULT MERCHANT SCRIPT EXISTS!")
		return
	end

	self.MerchantScript = util.KeyValuesToTable(
		file.Read(path, "GAME"),
		false,
		true
	)

	if not self:GetMerchantName() or self:GetMerchantName() == "" then
		self:SetMerchantName(self.MerchantScript.name or self.MerchantScript.Name)
	end

	-- reassign name so npcname shows on the window
	self.MerchantScript.name = self:GetMerchantName()
end

function ENT:SetupDataTables()
	self:NetworkVar("String", 0, "MerchantScript")
	self:NetworkVar("String", 1, "MerchantIconMaterial")
	self:NetworkVar("String", 2, "MerchantName")
	self:NetworkVar("Float", 0, "IconHeight")
	self:NetworkVar("Bool", 0, "ShowIcon")
end

function ENT:Initialize()
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetSolid(SOLID_BBOX)
	self:SetMoveType(MOVETYPE_NONE)

	if SERVER then
		self:SetNPCClass(CLASS_NONE)
		self:DoMerchantScript()
		self:SetHullSizeNormal()
		self:SetNPCState(NPC_STATE_SCRIPT)
		self:SetHullType(HULL_HUMAN)
		self:SetUseType(SIMPLE_USE)
		self:CapabilitiesAdd(CAP_ANIMATEDFACE)
		self:CapabilitiesAdd(CAP_TURN_HEAD)
	end
end

function ENT:Draw()
	self:DrawModel()

	if halo.RenderedEntity() == self then return end

	local ang = EyeAngles()
	ang:RotateAroundAxis(ang:Up(), -90)
	ang:RotateAroundAxis(ang:Forward(), 90)

	if self:GetShowIcon() then
		self.IconMat = self.IconMat or CreateMaterial(self:GetMerchantIconMaterial():gsub("%.vmt", ""):gsub("/", "_"), "UnlitGeneric", {
			["$basetexture"] = self:GetMerchantIconMaterial(),
			["$translucent"] = 1,
			["$alpha"] = 1
		})
		render.SetMaterial(self.IconMat)
		render.DrawSprite(self:GetPos() + Vector(0, 0, self:GetIconHeight()), 12, 12, Color(255, 255, 255, 128))
	end
end

function ENT:KeyValue(key, val)
	local k = key:lower()
	if k == "model" then
		self:SetModel(val)
	elseif k == "merchantscript" then
		self:SetMerchantScript(val)
	elseif k == "iconheight" then
		self:SetIconHeight(tonumber(val))
	elseif k == "showicon" then
		self:SetShowIcon(tobool(val))
	elseif k == "merchanticonmaterial" then
		self:SetMerchantIconMaterial(val)
	elseif k == "npcname" then
		self:SetMerchantName(val)
	end
end

function ENT:Use(actor, caller)
	actor.oc_merchant = self
	if istable(self.MerchantScript) and not table.IsEmpty(self.MerchantScript) then
		net.Start(TAG)
		net.WriteTable(self.MerchantScript)
		net.Send(actor)
	end
end
