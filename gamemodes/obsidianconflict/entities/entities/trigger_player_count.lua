ENT.Type = "brush"
ENT.Base = "trigger_multiple_oc"
ENT.Spawnable = false
DEFINE_BASECLASS(ENT.Base)

-- technically, players are the only ones to be able to touch this ent
-- but sometimes before the ent is fully initialized, ents will touch it before we have spawnflags
-- so they get added to the touching list
function ENT:OnStartTouch(ply, was_empty)
	if not ply:IsPlayer() then return end

	self:TriggerOutput("OnPlayerEntered", ply)

	local touching_plys = {}
	for e in next, self.m_TouchingEntities do
		if e:IsValid() and e:IsPlayer() then
			touching_plys[e] = true
		end
	end

	-- fuck bots
	-- also we just want majority since some people may be afk
	if table.Count(touching_plys) >= #player.GetHumans() / 2 then
		self:TriggerOutput("OnAllPlayersEntered")
	end
end

function ENT:OnEndTouch(ply, is_empty)
	if not ply:IsPlayer() then return end

	self:TriggerOutput("OnPlayerLeave", ply)
end
