ENT.Type = "anim"
ENT.Spawnable = false

function ENT:KeyValue(key, val)
	local k = key:lower()
	if k == "customweaponscript" then
		self._ClassName = string.lower(val)
	end
end

function ENT:Initialize()
	local wep = weapons.GetStored(self._ClassName)

	if not wep then
		print("weapon_scripted spawned, but custom weapon isn't defined!", self._ClassName)
		self:Remove()
		return
	end

	local ent = ents.Create(self._ClassName)
	if not IsValid(ent) then
		print("weapon_scripted spawned, but custom weapon isn't valid!", self._ClassName)
		self:Remove()
		return
	end
	ent:SetPos(self:GetPos())
	ent:SetAngles(self:GetAngles())
	ent:SetKeyValue("spawnflags", self:GetSpawnFlags())
	ent:Spawn()
	ent.oc_creationID = self:MapCreationID()
	ent.IsDesignerPlaced = true

	timer.Simple(0, function()
		ent:PhysWake()
	end)

	self:Remove()
end
