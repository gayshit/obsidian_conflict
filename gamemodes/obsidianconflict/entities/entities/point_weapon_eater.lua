ENT.Type = "point"
ENT.Spawnable = false

function ENT:KeyValue(key, val)
	local k = key:lower()
	if k == "eatradius" then
		self.EatRadius = tonumber(val)
	end
end

function ENT:Eat()
	if not self.EatRadius then return end

	local nearby = ents.FindInSphere(self:GetPos(), self.EatRadius)

	if table.IsEmpty(nearby) then return end

	for i, e in ipairs(nearby) do
		if e:IsValid() and e:IsWeapon() and not e:GetOwner():IsValid() then
			e:Remove()
		end
	end
end

function ENT:AcceptInput(inp, actor, caller, data)
	inp = inp:lower()
	if inp == "eat" then
		self:Eat()
	end
end
