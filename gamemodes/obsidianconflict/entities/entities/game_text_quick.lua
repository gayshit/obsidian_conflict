ENT.Type = "point"
ENT.Spawnable = false

local TAG = "game_text_quick"
local known_float = {
	x = true,
	y = true,
	holdtime = true,
	fxtime = true,
	fadeout = true,
	fadein = true,
	channel = true,
	effect = true,
}
function ENT:KeyValue(key, val)
	local k = key:lower()
	if known_float[k] then
		self[k] = tonumber(val)
	elseif k == "color" or k == "color2" then
		self[k] = Color(val:match("(%d+) (%d+) (%d+)"))
	elseif k == "message" then
		self[k] = val
	end
end

function ENT:SendMessage(ply, text)
	text = text or self.message or ""

	text = string.gsub(text, "%a+", GAMEMODE.__GsubSpellingMistakes)

	net.Start(TAG)
	net.WriteUInt(self.channel, 8)
	net.WriteString(text)
	net.WriteFloat(self.x)
	net.WriteFloat(self.y)
	net.WriteFloat(self.holdtime)
	net.WriteColor(self.color)
	net.WriteColor(self.color2)
	net.WriteInt(self.effect or 1, 4)
	net.WriteFloat(self.fadein)
	net.WriteFloat(self.fadeout)
	net.WriteFloat(self.fxtime)
	if ply and IsValid(ply) then
		net.Send(ply)
	else
		net.Broadcast()
	end

	return true
end

function ENT:AcceptInput(name, actor, caller, data)
	if name == "DisplayText" then
		return self:SendMessage(not self:HasSpawnFlags(1) and actor:IsPlayer() and actor or nil, data)
	elseif name == "Display" then
		return self:SendMessage(not self:HasSpawnFlags(1) and actor:IsPlayer() and actor or nil)
	elseif name == "AddOutput" then
		local _ = string.Split(data, " ")
		if _[1] == "message" then
			self.message = table.concat(_, " ", 2)
			return
		end

		print(self, "addout()", data)
		--self:StoreOutput("string name", string info)
	else
		print(self, "AcceptInput()", name, actor, caller, data)
	end
end
