AddCSLuaFile()

ENT.Type = "anim"
ENT.Model = Model("models/props_junk/rock001a.mdl")

DEFINE_BASECLASS("base_entity")

local DAMAGE_NO = 0

local MAX_TRACE_LENGTH = 1.732050807569 * (2 * 16384)

function ENT:SetupDataTables()
	self:NetworkVar("Entity", 0, "Player")
	self:NetworkVar("Float", 0, "SpringLength")
	self:NetworkVar("Bool", 0, "PlayerWasStanding")
	self:NetworkVar("String", 0, "ThinkFunc")

	if SERVER then
		self:SetSpringLength(0)
		self:SetPlayerWasStanding(false)
	end
end

function ENT:Initialize()
	self:SetModel(self.Model)

	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_BBOX)
	self:SetSolidFlags(FSOLID_NOT_STANDABLE)

	self:SetMoveType(MOVETYPE_FLYGRAVITY)
	self:SetMoveCollide(MOVECOLLIDE_FLY_CUSTOM)

	local b = 1
	self:SetCollisionBounds(Vector(-b, -b, -b), Vector(b, b, b))

	self:SetGravity(0.05)

	if SERVER then
		self:SetThinkFunc("fly")
		self:NextThink(CurTime() + 0.1)
		self:SetTrigger(true)
	end

	if CLIENT then
		self:SetRenderBounds(Vector(-1e7, -1e7, -1e7), Vector(1e7, 1e7, 1e7))
	end
end

if CLIENT then
	local beamMat = Material("sprites/physgbeamb")
	local spriteMat = CreateMaterial("oc_grapple_end", "Sprite", util.KeyValuesToTable([[
    "Sprite"
    {
        "$basetexture" "sprites/physgun_glow"

        "$spriteorientation" "vp_parallel"
        "$spriteorigin" "[ 0.50 0.50 ]"
        "$vertexalpha" 1
        "$vertexcolor" 1
        "$spriterendermode" 5
        "$alpha" "0.9"
        "$nocull" "1"
    }
    ]]))

	function ENT:Draw()
		if not IsValid(self) or not IsValid(self:GetPlayer()) or not IsValid(self:GetOwner()) then return end
		local wepColor = self:GetOwner():GetWeaponColor():ToColor()

		local vecShootOrigin = self:GetPlayer():GetShootPos()
		local vecDir = self:GetPlayer():GetAimVector()

		local vecEnd = vecShootOrigin + (vecDir * MAX_TRACE_LENGTH)

		local tr = util.TraceLine({
			start = vecShootOrigin,
			endpos = vecEnd,
			filter = {self, self:GetOwner(), self:GetPlayer()},
			mask = MASK_SHOT,
			collisiongroup = COLLISION_GROUP_NONE
		})

		if not tr.HitSky then
			render.SetMaterial(spriteMat)
			render.DrawSprite(self:GetPos(), 16, 16, wepColor)
		end

		local vm = self:GetPlayer():GetViewModel()
		if self:GetPlayer():EntIndex() == LocalPlayer():EntIndex() and not LocalPlayer():ShouldDrawLocalPlayer() and (IsValid(vm) and vm:LookupAttachment("muzzle") == 0) or (LocalPlayer():ShouldDrawLocalPlayer() and self:GetOwner():LookupAttachment("core") == 0) then return end

		local muzzle = (self:GetPlayer():EntIndex() == LocalPlayer():EntIndex() and not LocalPlayer():ShouldDrawLocalPlayer()) and vm:GetAttachment(vm:LookupAttachment("muzzle")) or self:GetOwner():GetAttachment(self:GetOwner():LookupAttachment("core"))
		render.SetMaterial(beamMat)
		render.DrawBeam(muzzle.Pos, self:GetPos(), 4, 0, 15.5, wepColor)
	end
end

function ENT:StartTouch(ent)
	if not ent:IsSolid() or bit.band(ent:GetSolidFlags(), FSOLID_VOLUME_CONTENTS) == 1 then return end

	if (ent ~= self:GetOwner() and ent ~= self:GetPlayer()) and ent:GetInternalVariable("m_takedamage") ~= DAMAGE_NO then
		SafeRemoveEntity(self)
	else
		local tr = self:GetTouchTrace()

		if ent:GetMoveType() == MOVETYPE_NONE and not tr.HitSky then
			self:GetOwner():EmitSound("Weapon_AR2.Reload_Push")

			self:SetMoveType(MOVETYPE_NONE)

			local vForward = self:GetAngles():Forward()
			vForward:Normalize()

			local data = EffectData()
			data:SetOrigin(tr.HitPos)
			data:SetNormal(vForward)
			util.Effect("Impact", data)

			self:PhysicsDestroy()
			self:PhysicsInit(SOLID_VPHYSICS)
			self:SetSolidFlags(FSOLID_NOT_STANDABLE)
			self:AddSolidFlags(FSOLID_NOT_SOLID)

			if not self:GetPlayer() or not IsValid(self:GetPlayer()) then return end
			local phys = self:GetPhysicsObject()

			phys:EnableMotion(false)
			phys:SetMass(50000)

			local origin = self:GetPlayer():GetPos()
			local rootOrigin = self:GetPos()
			self:SetSpringLength((origin - rootOrigin):Length())

			self:SetPlayerWasStanding(bit.band(self:GetPlayer():GetFlags(), FL_DUCKING) == 0)

			self:SetThinkFunc("hooked")
			self:NextThink(CurTime() + 0.1)
			return true
		else
			SafeRemoveEntity(self)
		end
	end
end

function ENT:HookedThink()
	if not IsValid(self:GetOwner()) then return end
	self:NextThink(CurTime() + 0.05)

	local ply = self:GetPlayer()
	local tempVec = self:GetPos() - ply:GetPos()
	tempVec:Normalize()

	ply:SetGravity(0)
	ply:SetGroundEntity(NULL)

	local curVel = ply:GetVelocity()

	if self:GetOwner():GetHookState() then
		ply:SetVelocity((curVel + tempVec - curVel) * 30)
	else
		ply:SetVelocity((curVel + tempVec - curVel) * 100)
	end

	return true
end

function ENT:FlyThink()
	if CLIENT then return end
	local newAngles = self:GetAbsVelocity():Angle()
	self:SetAngles(newAngles)

	self:NextThink(CurTime() + 0.1)
	return true
end

function ENT:Think()
	if self:GetThinkFunc() == "fly" then
		return self:FlyThink()
	elseif self:GetThinkFunc() == "hooked" then
		return self:HookedThink()
	end

	self:NextThink(CurTime())
	return true
end

