AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_item"
ENT.AmmoType = "Pistol"
ENT.AmmoAmount = 1
ENT.Model = Model("models/items/boxsrounds.mdl")

-- Pseudo-clone of SDK's UTIL_ItemCanBeTouchedByPlayer
-- aims to prevent picking stuff up through fences and stuff
function ENT:PlayerCanPickup(ply)
	if ply == self:GetOwner() then return false end

	local phys = self:GetPhysicsObject()
	local spos = phys:IsValid() and phys:GetPos() or self:OBBCenter()

	local tr = util.TraceLine({
		start = spos,
		endpos = ply:GetShootPos(),
		filter = {ply, self},
		mask = MASK_SOLID
	})

	-- can pickup if trace was not stopped
	if tr.Fraction == 1.0 then
		return hook.Run("PlayerCanPickupItem", ply, self)
	end
end

function ENT:OnTouchPlayer(ent)
	if self:PlayerCanPickup(ent) then
		timer.Simple(0, function()
			if IsValid(self) then
				ent:GiveAmmo(self.AmmoAmount, self.AmmoType)

				self:Remove()
			end
		end)
	end
end
