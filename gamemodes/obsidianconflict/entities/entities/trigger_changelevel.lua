ENT.Type = "brush"
ENT.Base = "trigger_multiple_oc"
ENT.Spawnable = false
ENT.Enabled = true
ENT.DoesCustomOutput = true

DEFINE_BASECLASS(ENT.Base)

--ENT.TargetMap = ""
--ENT.Landmark = ""

--ENT.Started = false

SF_CHANGELEVEL_NOTOUCH = 0x0002
SF_CHANGELEVEL_CHAPTER = 0x0004
SF_CHANGELEVEL_RESTART = 0x0008

local timeout = CreateConVar("sv_mapchange_timeout", 60, bit.bor(FCVAR_NOTIFY, FCVAR_REPLICATED), "Time before map change to let all people get to the map change")

function ENT:Initialize()
	BaseClass.Initialize(self)

	self:SetKeyValue("spawnflags", bit.bor(self:GetSpawnFlags(), SF_TRIGGER_ALLOW_CLIENTS))

	self:_Filter()
end

function ENT:AcceptInput(inp, actor, caller, data)
	if inp == "ChangeLevel" and not self.Started then
		if not GAMEMODE.LevelChangeTimer or not IsValid(GAMEMODE.LevelChangeTimer) then
			local lct = ents.Create("game_countdown_timer")
			lct:Spawn()
			lct:Activate()
			lct:Fire("SetTimerLabel", "Level Change")
			lct:Fire("StartTimer", tostring(timeout:GetInt()))

			GAMEMODE.LevelChangeTimer = lct
		end

		self.LevelChangeOwner = actor

		self.Started = true
		self.EndTime = CurTime() + timeout:GetInt()
	end

	BaseClass.AcceptInput(self, inp, actor, caller, data)
end

function ENT:KeyValue(key, val)
	BaseClass.KeyValue(self, key, val)

	if key == "map" then
		self.TargetMap = val
	elseif key == "landmark" then
		self.Landmark = val
	end
end

function ENT:Think()
	if self.Started and self.EndTime and CurTime() >= self.EndTime then
		self.Started = false
		if IsValid(GAMEMODE.LevelChangeTimer) then GAMEMODE.LevelChangeTimer:Fire("StopTimer") end
		self:DoLevelChange()
	end
end

ENT.LevelChangeOwner = NULL
function ENT:DoLevelChange()
	hook.Run("OnChangeLevelTrigger", self, self.LevelChangeOwner, self.TargetMap, self.Landmark)

	timer.Simple(0, function()
		RunConsoleCommand("changelevel", self.TargetMap)
	end)
end

function ENT:OnStartTouch(ent, was_empty)
	if self:HasSpawnFlags(SF_CHANGELEVEL_NOTOUCH) == true then
		return
	end

	print("StartTouch", ent, self:PassesTriggerFilter(ent))

	if self.Enabled then
		self:TriggerOutput("OnChangeLevel", ent)
		if ent:IsPlayer() then
			if not self.Started then
				if not GAMEMODE.LevelChangeTimer or not IsValid(GAMEMODE.LevelChangeTimer) then
					local lct = ents.Create("game_countdown_timer")
					lct:Spawn()
					lct:Activate()
					lct:Fire("SetTimerLabel", "Level Change")
					lct:Fire("StartTimer", tostring(timeout:GetInt()))

					GAMEMODE.LevelChangeTimer = lct
				end
				self.Started = true
				self.EndTime = CurTime() + timeout:GetInt()
			end

			self.LevelChangeOwner = ent
			ent:Freeze(true)

			if table.Count(self.m_TouchingEntities) == #player.GetHumans() then
				self.Started = false
				GAMEMODE.LevelChangeTimer:Fire("StopTimer")
				self:DoLevelChange()
				return
			end

			-- DEV
			-- IF I COMMIT THIS, I'M FUCKING DUMB
			--hook.Run("OnChangeLevelTrigger", self, self.LevelChangeOwner, self.TargetMap, self.Landmark)
		end
	end
end
