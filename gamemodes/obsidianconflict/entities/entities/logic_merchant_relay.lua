ENT.Type = "point"
ENT.Spawnable = false

ENT.Enabled = true
ENT.purhcasesound = ""
ENT.costof = 5
ENT.maxpointstake = 0 -- 0 means as much as needed
ENT.purchasename = "A New Car!"

function ENT:InputPurchase(actor, caller, data)
	if not self.Enabled then
		self:TriggerOutput("OnDisabled", actor)
		return
	end

	-- grrrr gmod
	if not actor:IsPlayer() then
		local es = ents.FindInSphere(self:GetPos(), 400)
		for _, ent in ipairs(es) do
			if not ent:IsPlayer() then continue end
			local use_ent = hook.Run("PlayerUseTrace", ent)
			use_ent = IsValid(use_ent) and use_ent or actor
			if use_ent == actor then
				actor = ent

				break
			end
		end
	end

	if actor:IsPlayer() then
		local pts = actor:Frags()
		local iSpent = 0

		if self.isshared then
			print("SHARED BUY")
			iSpent = math.min(pts, self.maxpointstake, self.costof)

			self.costof = self.costof - iSpent

			if iSpent > 0 then
				self:TriggerOutput("OnCashReduced", actor)
			end
		elseif pts >= self.costof then
			iSpent = self.costof
		end

		actor:AddFrags(-iSpent)

		if iSpent >= self.costof then
			self:TriggerOutput("OnPurchased", actor)

			actor:ChatPrint(string.format("Purchase of %s is complete!", self.purchasename))
			self:EmitSound(self.purchasesound)
		elseif iSpent == 0 then
			if pts == 0 then
				actor:ChatPrint("You have no points to spend!")
			else
				actor:ChatPrint(string.format("It will cost you %d points (%d more) to purchase the %s", self.costof, self.costof - actor:Frags(), self.purchasename))
			end

			self:TriggerOutput("OnNotEnoughCash", actor)
		end
	end
end

function ENT:InputEnable(actor, caller, data)
	self.Enabled = true
end

function ENT:InputDisable(actor, caller, data)
	self:TriggerOutput("OnDisabled", actor)
	self.Enabled = false
end

function ENT:InputSetPurchaseName(actor, caller, data)
	self.purchasename = string.gsub(data, "%a+", GAMEMODE.__GsubSpellingMistakes)
end

function ENT:InputSetPurchaseCost(actor, caller, data)
	if tonumber(data) then
		self.costof = tonumber(data)
	end
end

function ENT:AcceptInput(inp, actor, caller, data)
	local strInputFuncName = Format("Input%s", string.gsub(inp, "^%l", string.upper))
	if isfunction(self[strInputFuncName]) then
		local handled = self[strInputFuncName](self, actor, caller, data)
		return handled == nil and true or handled
	elseif inp == "AddOutput" then
		local _ = string.Split(data, " ")
		self:SetKeyValue(_[1], table.concat(_, "", 2):gsub(":", ","))
	else
		print(self, Format("unhandled AcceptInput %s", inp), actor, caller, data)
	end
end

function ENT:Initialize()
	if self.startdisabled then
		self.Enabled = false
	end

	if self.isshared then
		MsgC(Color(255, 0, 0), "[OC]")
		print("logic_merchant_relay shared purchase", game.GetMap(), self)
	end
end

local known = {
	purchasesound = true,
}
local BOOL = {
	announcecashneeded = true,
	isshared = true,
	startdisabled = true,

}
function ENT:KeyValue(key, val)
	local k = key:lower()

	if string.Left(key, 2) == "On" then
		Obsidian.StoreOutputSafe(self, key, val)
	elseif k == "costof" then
		self.costof = tonumber(val)
		self.originalcost = self.costof
	elseif known[k] then
		self[k] = val
	elseif BOOL[k] then
		self[k] = tobool(val)
	elseif k == "maxpointstake" then
		self[k] = tonumber(val)
	elseif k == "purchasename" then
		self[k] = val:gsub("%a+", GAMEMODE.__GsubSpellingMistakes)
	end
end
