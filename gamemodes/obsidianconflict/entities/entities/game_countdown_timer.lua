AddCSLuaFile()

ENT.Type = "point"
ENT.Spawnable = false

local TAG = "game_countdown_timer"
local MAX_EDICT_BITS = 13 -- https://github.com/Facepunch/garrysmod/blob/master/garrysmod/lua/includes/extensions/net.lua#L5

if SERVER then
	util.AddNetworkString(TAG)
end

function ENT:SetupDataTables()
	self:NetworkVar("Bool", 0, "Paused")
	self:NetworkVar("String", 0, "Label")
	self:NetworkVar("Float", 0, "TimeLeft")
	self:NetworkVar("Float", 1, "EndTime")
end

function ENT:Initialize()
	self:AddEFlags(EFL_FORCE_CHECK_TRANSMIT)
end

-- always transmit to all clients
function ENT:UpdateTransmitState()
	return TRANSMIT_ALWAYS
end

function ENT:InputSetTimerLabel(actor, caller, data)
	self:SetLabel(string.gsub(data, "%a+", GAMEMODE.__GsubSpellingMistakes))
end

function ENT:InputStartTimer(actor, caller, data)
	self:SetPaused(false)

	self:SetTimeLeft(tonumber(data))
	self:SetEndTime(CurTime() + self:GetTimeLeft())

	net.Start(TAG)
	net.WriteUInt(self:EntIndex(), MAX_EDICT_BITS)
	net.Broadcast()
	GAMEMODE.ActiveTimer = self
end

function ENT:InputStopTimer(actor, caller, data)
	self:SetPaused(true)

	local curtime = CurTime()
	self:SetTimeLeft(self:GetEndTime() - curtime)
	--self:SetEndTime(curtime + self:GetTimeLeft())

	net.Start(TAG)
	net.WriteUInt(0, MAX_EDICT_BITS)
	net.Broadcast()
	GAMEMODE.ActiveTimer = NULL
end

function ENT:InputPauseTimer(actor, caller, data)
	self:SetPaused(true)

	local curtime = CurTime()
	self:SetTimeLeft(self:GetEndTime() - curtime)
end

function ENT:InputResumeTimer(actor, caller, data)
	self:SetPaused(false)

	self:SetEndTime(CurTime() + self:GetTimeLeft())
end

function ENT:AcceptInput(inp, actor, caller, data)
	local strInputFuncName = Format("Input%s", string.gsub(inp, "^%l", string.upper))
	if isfunction(self[strInputFuncName]) then
		local handled = self[strInputFuncName](self, actor, caller, data)
		return handled == nil and true or handled
	elseif inp == "AddOutput" then
		local _ = string.Split(data, " ")
		self:SetKeyValue(_[1], table.concat(_, "", 2):gsub(":", ","))

		return true
	else
		print(self, Format("unhandled AcceptInput %s", inp), actor, caller, data)
	end
end

if SERVER then
	GAMEMODE.ActiveTimer = NULL
	hook.Add("PlayerFullyConnected", TAG, function(ply)
		if GAMEMODE.ActiveTimer:IsValid() then
			net.Start(TAG)
			net.WriteUInt(GAMEMODE.ActiveTimer:EntIndex(), MAX_EDICT_BITS)
			net.Send(ply)
		end
	end)

	function ENT:Think()
		if not self:GetPaused() then
			local timeleft = math.max(self:GetEndTime() - CurTime(), 0)
			self:SetTimeLeft(timeleft)
		end
	end
end
