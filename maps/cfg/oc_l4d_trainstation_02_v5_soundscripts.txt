"l4d_death"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.00"
	"pitch"			"100"

	"soundlevel"	"0"

	"wave"			"*#Left 4 Dead/Death.mp3"
}


"l4d_player_death"
{
	"channel"		"CHAN_STATIC"
	"volume"		"0.70"
	"pitch"			"100"

	"soundlevel"	"0"

	"wave"			"*#Left 4 Dead/LeftForDeathHit.mp3"
}


"l4d_start"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.00"
	"pitch"			"100"

	"soundlevel"	"0"

	"wave"			"*#TIMON_Z1535/Left 4 Dead/dead_city/NoMercyForYou.wav"
}


"l4d_background1"
{
	"channel"		"CHAN_STATIC"
	"volume"		"0.50"
	"pitch"			"100"

	"soundlevel"	"0"

	"wave"			"*#Left 4 Dead/L4D_Quarantine_02.mp3"
}


"l4d_background2"
{
	"channel"		"CHAN_STATIC"
	"volume"		"0.50"
	"pitch"			"100"

	"soundlevel"	"0"

	"wave"			"*#TIMON_Z1535/Left 4 Dead/dead_city/NM_Rabies_05.mp3"
}


"l4d_survival"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.00"
	"pitch"			"100"

	"soundlevel"	"0"

	"wave"			"*#Left 4 Dead/NotSurvive.mp3"
}


"l4d_tank"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.00"
	"pitch"			"100"

	"soundlevel"	"0"

	"wave"			"*#Left 4 Dead/Tank.wav"
}


"l4d_germ1"
{
	"channel"		"CHAN_STATIC"
	"volume"		"0.70"
	"pitch"			"100"

	"soundlevel"	"0"

	"wave"			"*#TIMON_Z1535/Left 4 Dead/dead_city/GermL1a.mp3"
}


"l4d_germ2"
{
	"channel"		"CHAN_STATIC"
	"volume"		"0.70"
	"pitch"			"100"

	"soundlevel"	"0"

	"wave"			"*#TIMON_Z1535/Left 4 Dead/dead_city/GermL2a.mp3"
}


"l4d_horde_rules"
{
	"channel"		"CHAN_STATIC"
	"volume"		"0.70"
	"pitch"			"100"

	"soundlevel"	"0"

	"wave"			"*#Left 4 Dead/MobRules.wav"
}


"l4d_horde"
{
	"channel"		"CHAN_STATIC"
	"volume"		"0.70"
	"pitch"			"100"

	"soundlevel"	"0"

	"wave"			"*#Left 4 Dead/Horde_01.wav"
}


"l4d_horde_end"
{
	"channel"		"CHAN_STATIC"
	"volume"		"0.70"
	"pitch"			"100"

	"soundlevel"	"0"

	"wave"			"*#Left 4 Dead/GatesOfHell.mp3"
}


"l4d_safe"
{
	"channel"		"CHAN_STATIC"
	"volume"		"1.00"
	"pitch"			"100"

	"soundlevel"	"0"

	"wave"			"*#Left 4 Dead/TheMonstersWithout.mp3"
}