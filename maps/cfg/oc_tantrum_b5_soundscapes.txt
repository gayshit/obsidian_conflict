"controllroom.soundscape"
{
	"dsp"	"9"

	"playlooping"
	{
		"volume"	"0.3"
		"pitch"		"100"
		"wave"		"ambient/atmosphere/indoor1.wav"		
	}

	"playrandom"
	{
		"time"		"8,10"
		"volume"	"1.2,1.0"
		"pitch"		"100"
		"rndwave"
		{
			"wave"	"npc/overwatch/cityvoice/f_protectionresponse_1_spkr.wav"
			"wave"	"npc/overwatch/cityvoice/fcitadel_10sectosingularity.wav"
			"wave"	"npc/overwatch/cityvoice/fcitadel_confiscationfailure.wav"
			"wave"	"npc/overwatch/cityvoice/fcitadel_deploy.wav"
			"wave"	"npc/overwatch/cityvoice/fprison_detectionsystemsout.wav"
			"wave"	"npc/overwatch/cityvoice/fprison_exogenbreach.wav"
		}
		
	}
}

"outdoor.soundscape"
{
	"dsp"	"1"

	"playlooping"
	{
		"volume"	"0.2"
		"pitch"		"100"
		"wave"	"ambient/wind/wasteland_wind.wav"		
	}

	"playrandom"
	{
		"time"		"2.5,4.5"
		"volume"	"0.2,0.4"
		"pitch"		"100"
		"position"	"random"
		"soundlevel"	"SNDLVL_140db"
		"rndwave"
		{
			"wave"	"ambient/levels/canals/critter4.wav"
			"wave"	"ambient/levels/canals/critter5.wav"
			"wave"	"ambient/levels/canals/critter7.wav"
			"wave"	"ambient/levels/canals/critter8.wav"
		}
		
	}
}

"1st_bunker.soundscape"
{
	"dsp"	"19"

	"playlooping"
	{
		"volume"	"0.3"
		"pitch"		"100"
		"wave"		"ambient/atmosphere/hole_amb3.wav"		
	}

	"playrandom"
	{
		"time"		"7,10"
		"volume"	"1.2,1.0"
		"pitch"		"80"
		"rndwave"
		{
			"wave"	"ambient/materials/metal_stress1.wav"
			"wave"	"ambient/materials/metal_stress2.wav"
			"wave"	"ambient/materials/metal_stress3.wav"
			"wave"	"ambient/materials/metal_stress4.wav"
			"wave"	"ambient/materials/metal_stress5.wav"
		}
		
	}
}

"ventilation.soundscape"
{
	"dsp"	"2"

	"playlooping"
	{
		"volume"	"0.2"
		"pitch"		"80"
		"wave"		"ambient/machines/city_ventpump_loop1.wav"		
	}

	"playrandom"
	{
		"time"		"5,10"
		"volume"	"0.1,0.3"
		"pitch"		"100"
		"rndwave"
		{
			"wave"	"ambient/materials/metal4.wav"
			"wave"	"ambient/materials/metal5.wav"
			"wave"	"ambient/materials/metal9.wav"
		}
		
	}
}

"large_hall.soundscape"
{
	"dsp"	"10"

	"playlooping"
	{
		"volume"	"0.4"
		"pitch"		"100"
		"wave"		"ambient/atmosphere/cargo_hold1.wav"
	}

	"playrandom"
	{
		"time"		"7,10"
		"volume"	"0.3,0.4"
		"pitch"		"100"
		"rndwave"
		{
			"wave"	"ambient/wind/wind_snippet1.wav"
			"wave"	"ambient/wind/wind_snippet2.wav"
			"wave"	"ambient/wind/wind_snippet3.wav"
			"wave"	"ambient/wind/wind_snippet4.wav"
			"wave"	"ambient/wind/wind_snippet5.wav"
		}
		
	}
}

"small_hall.soundscape"
{
	"dsp"	"8"

	"playlooping"
	{
		"volume"	"0.4"
		"pitch"		"100"
		"wave"		"ambient/atmosphere/corridor.wav"
	}

	"playrandom"
	{
		"time"		"2,4"
		"volume"	"0.5,0.7"
		"pitch"		"100"
		"rndwave"
		{
			"wave"	"ambient/creatures/flies1.wav"
			"wave"	"ambient/creatures/flies2.wav"
			"wave"	"ambient/creatures/flies3.wav"
			"wave"	"ambient/creatures/flies4.wav"
			"wave"	"ambient/creatures/flies5.wav"
		}
		
	}
}

"electrified_air.soundscape"
{
	"dsp"	"22"

	"playlooping"
	{
		"volume"	"0.5"
		"pitch"		"50"
		"wave"		"ambient/energy/electric_loop.wav"		
	}

	"playrandom"
	{
		"time"		"1,5"
		"volume"	"0.2,0.3"
		"pitch"		"100"
		"rndwave"
		{
			"wave"	"ambient/energy/spark1.wav"
			"wave"	"ambient/energy/spark2.wav"
			"wave"	"ambient/energy/spark3.wav"
			"wave"	"ambient/energy/spark4.wav"
			"wave"	"ambient/energy/spark5.wav"
			"wave"	"ambient/energy/spark6.wav"
		}
		
	}
}

"reactor.soundscape"
{
	"dsp"	"4"

	"playlooping"
	{
		"volume"	"0.5"
		"pitch"		"100"
		"wave"		"ambient/levels/labs/teleport_active_loop1.wav"		
	}
}

"medium_tunnel.soundscape"
{
	"dsp"	"6"

	"playlooping"
	{
		"volume"	"0.4"
		"pitch"		"100"
		"wave"		"ambient/atmosphere/tunnel1.wav"		
	}

	"playrandom"
	{
		"time"		"5,10"
		"volume"	"0.3,0.4"
		"pitch"		"100"
		"rndwave"
		{
			"wave"	"ambient/atmosphere/cave_hit1.wav"
			"wave"	"ambient/atmosphere/cave_hit2.wav"
			"wave"	"ambient/atmosphere/cave_hit3.wav"
			"wave"	"ambient/atmosphere/cave_hit4.wav"
			"wave"	"ambient/atmosphere/cave_hit5.wav"
			"wave"	"ambient/atmosphere/cave_hit6.wav"
		}
		
	}
}

"sewer.soundscape"
{
	"dsp"	"6"

	"playlooping"
	{
		"volume"	"0.6"
		"pitch"		"100"
		"wave"		"ambient/water/corridor_water.wav"		
	}
}

"elevator.soundscape"
{
	"dsp"	"4"

	"playlooping"
	{
		"volume"	"0.6"
		"pitch"		"100"
		"wave"		"ambient/atmosphere/elev_shaft1.wav"		
	}
}