//Created for use in Obsidian Conflict ONLY!
"oc_strike"
{
	//Player settings

	"mp_falldamage"	"1"
	"mp_flashlight" "1"
	"sv_infinite_aux_power" "0"
	"mp_bunnyhop" "2"
	"mp_playercollide" "1"
	"sv_playergibbage" "0"


	//Soundscape settings

	"dsp_enhance_stereo" "1"
	"dsp_volume" "2"
	

	//Weapons settings
	
	"sk_max_grenade" "1"
	"sk_battery" "100"


	//Gameplay settings

	//"sv_deathpenalty" "200"
	"sv_allownegativescore" "0"
	"mp_forcerespawn" "1"
	"sv_giblifetime" "0.5"
	"mp_friendlyfire" "0"
	"overview_locked" "0"	
}