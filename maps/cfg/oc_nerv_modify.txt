"oc_nerv"
{
	"SpawnItems"
	{
    	"weapon_healer" "1"
		"oc_nerv/custom_glock" "1" 
	}

	"CustomAmmo"
	{
		"nerv_556"
		{
			"dmgtype"		"1" // CRUSH = 0, BULLET = 1, SLASH = 2, BURN = 3, VEHICLE = 4, FALL = 5, BLAST = 6, CLUB = 7, SHOCK = 8, SONIC = 9, ENERGYBEAM = 10
			"tracer"		"4" // NONE = 0, LINE = 1, RAIL = 2, BEAM = 3, LINE AND WHIZ = 4
			"plrdmg"		"30"
			"npcdmg"		"30"
			"maxcarry"		"150"
			"grains"		"200" // 1 grain = 64.79891 milligrams
			"ftpersec"		"1225"
		}
		"nerv_9"
		{
			"dmgtype"		"1" // CRUSH = 0, BULLET = 1, SLASH = 2, BURN = 3, VEHICLE = 4, FALL = 5, BLAST = 6, CLUB = 7, SHOCK = 8, SONIC = 9, ENERGYBEAM = 10
			"tracer"		"4" // NONE = 0, LINE = 1, RAIL = 2, BEAM = 3, LINE AND WHIZ = 4
			"plrdmg"		"15"
			"npcdmg"		"15"
			"maxcarry"		"200"
			"grains"		"200" // 1 grain = 64.79891 milligrams
			"ftpersec"		"1225"
		}
		"Nerv_45"
		{
			"dmgtype"		"1"
			"tracer"		"4"
			"plrdmg"		"35"	//Damage done by players 	using this ammo.
			"npcdmg"		"35"	//Damage done by NPCs using this ammo.
			"maxcarry"		"150"
			"grains"		"200"	// 1 grain = 64.79891 milligrams
			"ftpersec"		"1225"
		}
		"Nerv_46"
		{
			"dmgtype"		"1"
			"tracer"		"4"
			"plrdmg"		"7"	//Damage done by players 	using this ammo.
			"npcdmg"		"7"	//Damage done by NPCs using this ammo.
			"maxcarry"		"120"
			"grains"		"200"	// 1 grain = 64.79891 milligrams
			"ftpersec"		"1225"
		}
		"Nerv_buckshot"
		{
			"dmgtype"		"1"
			"tracer"		"4"
			"plrdmg"		"15"	//Damage done by players 	using this ammo.
			"npcdmg"		"15"	//Damage done by NPCs using this ammo.
			"maxcarry"		"45"
			"grains"		"200"	// 1 grain = 64.79891 milligrams
			"ftpersec"		"1225"
		}
		"Nerv_357"
		{
			"dmgtype"		"1"
			"tracer"		"4"
			"plrdmg"		"120"	//Damage done by players 	using this ammo.
			"npcdmg"		"120"	//Damage done by NPCs using this ammo.
			"maxcarry"		"36"
			"grains"		"200"	// 1 grain = 64.79891 milligrams
			"ftpersec"		"1225"
		}
	}
}