// Created for use in Obsidian Conflict ONLY!
"cityfight"
{

		"mp_playercollide" "0"
		"mp_falldamage" "1"
		"mp_flashlight" "1"
		
		
		//Gunship&Helicopter
		"sk_gunship_health_increments" "7"
		
		"sk_helicopter_firingcone" "3"
		
		"sk_healthvial" "30"
		"sk_healthkit" "75"

		"sk_max_sniper_round" "10"
		"sk_max_357"	"21"		
		"sk_max_ar2"	"90"		
		"sk_max_ar2_altfire"	"5"
		"sk_max_buckshot"	"30"
		"sk_max_crossbow"	"25"
		"sk_max_grenade"	"7"
		"sk_max_pistol"		"200"
		"sk_max_rpg_round"	"5"
		"sk_max_smg1"		"150"
		"sk_max_smg1_grenade"	"10"
}