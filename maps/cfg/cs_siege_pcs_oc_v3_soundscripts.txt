//WEAPONS

//Beretta

// weapon_Beretta.txt
"Weapon_Beretta.Reload"
{
	"channel"		"CHAN_ITEM"
	"volume"		"0.7"
	"soundlevel"	"SNDLVL_NORM"

	"wave"		")weapons/Beretta/pistol_reload1.wav"
}

"Weapon_Beretta.NPC_Reload"
{
	"channel"		"CHAN_ITEM"
	"volume"		"0.7"
	"soundlevel"	"SNDLVL_NORM"

	"wave"		"weapons/smg1/smg1_reload.wav"
}

"Weapon_Beretta.Empty"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"0.7"
	"soundlevel"	"SNDLVL_NORM"

	"wave"		")weapons/pistol/pistol_empty.wav"
}

"Weapon_Beretta.Single"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"0.55"
	"soundlevel"	"SNDLVL_GUNFIRE"
	"pitch"			"98,102"

	"wave"		")weapons/Beretta/pistol_fire2.wav"
}

"Weapon_Beretta.NPC_Single"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"0.7"
	"soundlevel"	"SNDLVL_GUNFIRE"
	"pitch"			"90,120"

	"wave"		"^weapons/pistol/pistol_fire3.wav"
}

"Weapon_Beretta.Special1"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"0.7"
	"soundlevel"	"SNDLVL_NORM"

	"wave"		")weapons/smg1/switch_single.wav"
}

"Weapon_Beretta.Special2"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"0.7"
	"soundlevel"	")SNDLVL_NORM"

	"wave"			"weapons/smg1/switch_burst.wav"
}

//Benelli M3


"Weapon_beM3.Single"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"CompatibilityAttenuation"	"0.48"
	"pitch"		"PITCH_NORM"

	"wave"			")weapons/bem3/m3-1.wav"
}



"Weapon_beM3.Insertshell"
{
	"channel"		"CHAN_ITEM"
	"volume"		"1.0"
	"CompatibilityAttenuation"	"1.0"
	"pitch"		"PITCH_NORM"

	"wave"			"weapons/bem3/m3_insertshell.wav"
}

"Weapon_beM3.Pump"
{
	"channel"		"CHAN_ITEM"
	"volume"		"1.0"
	"CompatibilityAttenuation"	"1.0"
	"pitch"		"PITCH_NORM"

	"wave"			"weapons/bem3/m3_pump.wav"
}

//AA12

"Weapon_usas-12.Single"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"CompatibilityAttenuation"	"0.52"
	"pitch"		"PITCH_NORM"

	"wave"			")weapons/usas-12/xm1014-1.wav"
}