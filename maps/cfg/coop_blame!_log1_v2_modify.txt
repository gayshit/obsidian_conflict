coop_blame!_log1_v2
{
    Remove
    {
        ClassName
        {
            "game_score" {}
            "game_end" {}
        }
        TargetName
        {
            "makerebel" {}
            "client" {}
            "server" {}
            "endcount" {}
        }
    }
    Add
    {
        "math_counter"
        {
            "max" "2"
            "startvalue" "2"
            "targetname" "endcount"
            "OnHitMin" "mapchange,Command,changelevel coop_blame!_log2_v2,10,1"
            "OnHitMin" "es,PickRandom,,5,1"
            "origin" "-5577.35 5559 -22.1589"
        }
        "point_servercommand"
        {
            "targetname" "mapchange"
            "origin" "-5547.09 5559 -15.2944"
        }
    }
    Modify
    {
        ClassName
        {
            "worldspawn"
            {
                "mapversion" "5"
            }
        }
        Origin
        {
            "7.58046 -13982.1 -248"
            {
                "Reciprocal" "1"
            }
        }
    }
}
