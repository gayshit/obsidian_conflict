Obsidian Conflict

Map: ep2_oc_outhouse_1
Author: Xenire

Description:

You are a civilian being transported by the Combine to one of their facilities for exactly what they do to all civilians who are gathered up. In your darkest hour by what seems to be a stroke of luck in the thick of fog rebels ambush the convoy. You and some others manage to free yourselves but the fight is too intense and the Combine begin to overtake the rebels. In a desperate attempt to escape you run into the nearby forest along with the others who have been freed, but you notice that oddly the Combine are not following. The forest is infested and your situation has not improved.

Continue through the forest to try and find your salvation.

IMPORTANT:
Survival depends on moving cautiously, together, and sharing resources--do not let your fellow survivors down!
Remember to keep an eye out for things that seem out of place.