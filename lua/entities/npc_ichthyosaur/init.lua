include("shared.lua")
DEFINE_BASECLASS(ENT.Base)

local sk_ichthyosaur_melee_dmg = GetConVar"sk_ichthyosaur_melee_dmg"

ENT.m_strTargetName = ""
function ENT:KeyValue(key, val)
	if string.Left(key, 2) == "On" then
		self:StoreOutput(key, val)
	elseif key == "target" then
		self.m_strTargetName = val

		self:SetTarget(ents.FindByName(val)[1] or NULL)
	elseif key == "squadname" then
		self:SetSquad(val)
	else
		--print(self, "kv()", key, val)
	end
end

-- TODO: move a lot of this to baseclass

_G.AIE_NORMAL = 0
_G.AIE_EFFICIENT = 1

_G.ICH_AE_BITE = 11
_G.ICH_AE_BITE_START = 12

_G.MEMORY_CLEAR = 0
_G.MEMORY_PROVOKED = bit.lshift(1, 0)     -- right now only used for houndeyes.
_G.MEMORY_INCOVER = bit.lshift(1, 1)      -- npc knows it is in a covered position.
_G.MEMORY_SUSPICIOUS = bit.lshift(1, 2)   -- Ally is suspicious of the player, and will move to provoked more easily
_G.MEMORY_PATH_FAILED = bit.lshift(1, 5)  -- Failed to find a path
_G.MEMORY_FLINCHED = bit.lshift(1, 6)     -- Has already flinched
_G.MEMORY_TOURGUIDE = bit.lshift(1, 8)    -- I have been acting as a tourguide.
_G.MEMORY_LOCKED_HINT = bit.lshift(1, 10) --

_G.MEMORY_TURNING = bit.lshift(1, 13)     -- Turning, don't interrupt me.
_G.MEMORY_TURNHACK = bit.lshift(1, 14)    --

_G.MEMORY_HAD_ENEMY = bit.lshift(1, 15)   -- Had an enemy
_G.MEMORY_HAD_PLAYER = bit.lshift(1, 16)  -- Had player
_G.MEMORY_HAD_LOS = bit.lshift(1, 17)     -- Had LOS to enemy

_G.MEMORY_CUSTOM4 = bit.lshift(1, 28)     -- NPC-specific memory
_G.MEMORY_CUSTOM3 = bit.lshift(1, 29)     -- NPC-specific memory
_G.MEMORY_CUSTOM2 = bit.lshift(1, 30)     -- NPC-specific memory
_G.MEMORY_CUSTOM1 = bit.lshift(1, 31)     -- NPC-specific memory

-- main branch doesnt have _G.COND table at the time of writing
--[[if not _G.COND then
	_G.COND = {
		NONE = 0,
		IN_PVS = 1,
		IDLE_INTERRUPT = 2,
		LOW_PRIMARY_AMMO = 3,
		NO_PRIMARY_AMMO = 4,
		NO_SECONDARY_AMMO = 5,
		NO_WEAPON = 6,
		SEE_HATE = 7,
		SEE_FEAR = 8,
		SEE_DISLIKE = 9,
		SEE_ENEMY = 10,
		LOST_ENEMY = 11,
		ENEMY_WENT_NULL = 12,
		ENEMY_OCCLUDED = 13,
		TARGET_OCCLUDED = 14,
		HAVE_ENEMY_LOS = 15,
		HAVE_TARGET_LOS = 16,
		LIGHT_DAMAGE = 17,
		HEAVY_DAMAGE = 18,
		PHYSICS_DAMAGE = 19,
		REPEATED_DAMAGE = 20,
		CAN_RANGE_ATTACK1 = 21,
		CAN_RANGE_ATTACK2 = 22,
		CAN_MELEE_ATTACK1 = 23,
		CAN_MELEE_ATTACK2 = 24,
		PROVOKED = 25,
		NEW_ENEMY = 26,
		ENEMY_TOO_FAR = 27,
		ENEMY_FACING_ME = 28,
		BEHIND_ENEMY = 29,
		ENEMY_DEAD = 30,
		ENEMY_UNREACHABLE = 31,
		SEE_PLAYER = 32,
		LOST_PLAYER = 33,
		SEE_NEMESIS = 34,
		TASK_FAILED = 35,
		SCHEDULE_DONE = 36,
		SMELL = 37,
		TOO_CLOSE_TO_ATTACK = 38,
		TOO_FAR_TO_ATTACK = 39,
		NOT_FACING_ATTACK = 40,
		WEAPON_HAS_LOS = 41,
		WEAPON_BLOCKED_BY_FRIEND = 42,
		WEAPON_PLAYER_IN_SPREAD = 43,
		WEAPON_PLAYER_NEAR_TARGET = 44,
		WEAPON_SIGHT_OCCLUDED = 45,
		BETTER_WEAPON_AVAILABLE = 46,
		HEALTH_ITEM_AVAILABLE = 47,
		WAY_CLEAR = 49,
		GIVE_WAY = 48,
		HEAR_DANGER = 50,
		HEAR_THUMPER = 51,
		HEAR_BUGBAIT = 52,
		HEAR_COMBAT = 53,
		HEAR_WORLD = 54,
		HEAR_PLAYER = 55,
		HEAR_BULLET_IMPACT = 56,
		HEAR_PHYSICS_DANGER = 57,
		HEAR_MOVE_AWAY = 58,
		HEAR_SPOOKY = 59,
		NO_HEAR_DANGER = 60,
		FLOATING_OFF_GROUND = 61,
		MOBBED_BY_ENEMIES = 62,
		RECEIVED_ORDERS = 63,
		PLAYER_ADDED_TO_SQUAD = 64,
		PLAYER_REMOVED_FROM_SQUAD = 65,
		PLAYER_PUSHING = 66,
		NPC_FREEZE = 67,
		NPC_UNFREEZE = 68,
		TALKER_RESPOND_TO_QUESTION = 69,
		NO_CUSTOM_INTERRUPTS = 70,
	}
end]]

local GOAL_ENEMY = 0

local sched = ai_schedule.New("icky_patrol_walk")
sched:EngTask("TASK_SET_FAIL_SCHEDULE", SCHED_COMBAT_FACE)
sched:EngTask("TASK_SET_TOLERANCE_DISTANCE", 64)
sched:EngTask("TASK_SET_ROUTE_SEARCH_TIME", 4)
sched:AddTask("PathToRandomNode", 200)
sched:AddTask("SetIdealAnimation", ACT_WALK)
sched:EngTask("TASK_WALK_PATH", 0)
sched:EngTask("TASK_WAIT_FOR_MOVEMENT", 0)
_G.SCHED_ICH_PATROL_WALK = sched

sched = ai_schedule.New("icky_patrol_run")
sched:EngTask("TASK_SET_FAIL_SCHEDULE", SCHED_COMBAT_FACE)
sched:EngTask("TASK_SET_TOLERANCE_DISTANCE", 64)
sched:EngTask("TASK_SET_ROUTE_SEARCH_TIME", 4)
sched:EngTask("PathToRandomNode", 200)
sched:AddTask("SetIdealAnimation", ACT_RUN)
sched:EngTask("TASK_WALK_PATH", 0)
sched:EngTask("TASK_WAIT_FOR_MOVEMENT", 0)
_G.SCHED_ICH_PATROL_RUN = sched

sched = ai_schedule.New("icky_melee_attack1")
sched:EngTask("TASK_ANNOUNCE_ATTACK", 1)
sched:EngTask("TASK_MELEE_ATTACK1", 0)
sched:AddTask("SetIdealAnimation", ACT_MELEE_ATTACK1)
_G.SCHED_ICH_MELEE_ATTACK1 = sched

sched = ai_schedule.New("icky_chase_enemy")
--sched:EngTask("TASK_SET_FAIL_SCHEDULE", "SCHEDULE:74") -- 74 = SCHED_PATROL_WALK
sched:EngTask("TASK_SET_TOLERANCE_DISTANCE", 64)
sched:AddTask("SetGoal", GOAL_ENEMY)
sched:AddTask("GetPathToGoal", 0)
--sched:EngTask("TASK_SET_GOAL", "GOAL:ENEMY")
--sched:EngTask("TASK_GET_PATH_TO_GOAL", "PATH:TRAVEL")
sched:AddTask("SetIdealAnimation", ACT_RUN)
sched:EngTask("TASK_RUN_PATH", 0)
sched:EngTask("TASK_WAIT_FOR_MOVEMENT", 0)
_G.SCHED_ICH_CHASE_ENEMY = sched

do -- movement handling
	local ICH_WAYPOINT_DISTANCE = 64.0
	local ICH_SWIM_SPEED_WALK = 150
	local ICH_SWIM_SPEED_RUN = 500

	-- basically wrapper around CAI_Navigator::Move
	local ICH_MOVETYPE_SEEK = 0
	local ICH_MOVETYPE_ARRIVE = 1
	function ENT:OverrideMove(flTimeDelta)
		local MoveType = self:IsCurWaypointGoal() and ICH_MOVETYPE_ARRIVE or ICH_MOVETYPE_SEEK

		local moveGoal = self:GetCurWaypointPos()

		self:UpdateMaxYawSpeed()

		-- See if we can move directly to our goal
		local enemy = self:GetEnemy()
		local goalTarget = self:GetGoalTarget()
		if IsValid(enemy) and goalTarget == enemy then
			local goalPos = enemy:GetPos()

			local velocity
			if enemy:IsPlayer() then
				velocity = enemy.m_vecSmoothedVelocity
			else
				velocity = enemy:GetVelocity()
			end

			goalPos:Add(velocity * 0.5)

			local tr = util.TraceHull({
				start = self:GetPos(),
				endpos = goalPos,
				mins = self:OBBMins(),
				maxs = self:OBBMaxs(),

				mask = MASK_NPCSOLID,
				filter = {self, enemy},
				collisiongroup = COLLISION_GROUP_NONE
			})

			if tr.Fraction == 1 then
				moveGoal:Set(tr.HitPos)
			else
				moveGoal:Set(enemy:GetPos())
			end
		end

		-- Move
		debugoverlay.Box(moveGoal, self:OBBMins(), self:OBBMaxs(), 0.1, Color(0, 0, 255, 16), true)
		self:DoMovement(flTimeDelta, moveGoal, MoveType)

		-- Save the info from that run
		self.m_vecLastMoveTarget = moveGoal
		self.m_bHasMoveTarget = true

		return true
	end

	local ICH_MIN_TURN_SPEED = 4.0
	local ICH_MAX_TURN_SPEED = 30.0

	function ENT:UpdateMaxYawSpeed()
		local flMaxYawSpeed = 0

		local iActivity = self:GetActivity()
		if iActivity == ACT_MELEE_ATTACK1 or iActivity == ACT_ICH_THRASH then
			flMaxYawSpeed = 16
		else
			-- Ramp up the yaw speed as we increase our speed
			flMaxYawSpeed = ICH_MIN_TURN_SPEED + ((ICH_MAX_TURN_SPEED - ICH_MIN_TURN_SPEED) * (math.abs(self:GetVelocity():Length()) / ICH_SWIM_SPEED_RUN))
		end

		self:SetMaxYawSpeed(flMaxYawSpeed)
	end

	function ENT:GetGroundSpeed()
		-- if self.m_flHoldTime > CurTime() then
		--	return ICH_SWIM_SPEED_WALK / 2
		--end

		local iIdealActivity = self:GetActivity()
		if iIdealActivity == ACT_WALK then
			return ICH_SWIM_SPEED_WALK
		elseif iIdealActivity == ACT_ICH_THRASH then
			return ICH_SWIM_SPEED_WALK
		end

		return ICH_SWIM_SPEED_RUN
	end

	local LATERAL_NOISE_MAX = 2.0
	local LATERAL_NOISE_FREQ = 1.0

	local VERTICAL_NOISE_MAX = 2.0
	local VERTICAL_NOISE_FREQ = 1.0

	function ENT:AddSwimNoise(vel)
		local right, up = self:GetRight(), self:GetUp()

		local lNoise = LATERAL_NOISE_MAX * math.sin(CurTime() * LATERAL_NOISE_FREQ)
		local vNoise = VERTICAL_NOISE_MAX * math.sin(CurTime() * VERTICAL_NOISE_FREQ)

		vel:Add(right * lNoise)
		vel:Add(up * vNoise)
	end

	function ENT:DoMovement(flInterval, moveGoal, eMoveType)
		self:NPCForget(MEMORY_TURNING)

		local Steer, SteerAvoid, SteerRel = Vector(), Vector(), Vector()

		-- get our orientation vectors
		local forward, right, up = self:GetForward(), self:GetRight(), self:GetUp()

		local enemy = self:GetEnemy()

		if self:GetActivity() == ACT_MELEE_ATTACK1 and IsValid(enemy) then
			self:SteerSeek(Steer, enemy:GetPos())
		else
			-- if we are approaching our goal, use an arrival steering mechanism
			if eMoveType == ICH_MOVETYPE_ARRIVE then
				self:SteerArrive(Steer, moveGoal)
			else
				-- otherwise use a seek steering mechanism
				self:SteerSeek(Steer, moveGoal)
			end
		end

		-- see if we need to avoid any obstacles
		if self:SteerAvoidObstacles(SteerAvoid, self:GetVelocity(), forward, right, up) then
			-- take the avoidance vector
			Steer:Set(SteerAvoid)
		end

		-- clamp our ideal steering vector to within our physical limitations
		self:ClampSteer(Steer, SteerRel, forward, right, up)

		debugoverlay.Box(self:GetPos(), self:OBBMins(), self:OBBMaxs(), 0.1, Color(255, 0, 0, 16), true)
		debugoverlay.Line(self:GetPos(), self:GetPos() + Steer, 0.1, Color(0, 255, 0, 16), true)

		self:ApplyAbsVelocityImpulse(Steer * flInterval)

		local vecNewVelocity = self:GetVelocity()
		local flLength = vecNewVelocity:Length()

		-- clamp our final speed
		if flLength > self:GetGroundSpeed() then
			vecNewVelocity:Mul(self:GetGroundSpeed() / flLength)
			flLength = self:GetGroundSpeed()
		end

		local workVelocity = Vector(vecNewVelocity)

		self:AddSwimNoise(workVelocity)

		--self:SetLocalVelocity(workVelocity)

		-- pose the fish properly
		self:SetPoses(SteerRel, flLength)

		-- drag our victim before moving
		-- if m_pVictim != NULL then
		-- 	DragVictim( (workVelocity*flInterval).Length() )
		-- end

		self:SetAbsVelocity(workVelocity)
	end

	function ENT:SetPoses(moveRel, speed)
		local movePerc, moveBase

		-- Find out how fast we're moving in our animations boundaries
		if self:GetIdealActivity() == ACT_WALK then
			moveBase = 0.5
			movePerc = moveBase * (speed / ICH_SWIM_SPEED_WALK)
		else
			moveBase = 1.0
			movePerc = moveBase * (speed / ICH_SWIM_SPEED_RUN)
		end

		local tailPosition = Angle()
		local flSwimSpeed = movePerc

		-- Forward deviation
		if moveRel.x > 0 then
			flSwimSpeed = flSwimSpeed * (moveBase + ((moveRel.x / self.m_vecAccelerationMax.x) * moveBase))
		elseif moveRel.x < 0 then
			flSwimSpeed = flSwimSpeed * (moveBase - ((moveRel.x / self.m_vecAccelerationMin.x) * moveBase))
		end

		-- Vertical deviation
		if moveRel.z > 0 then
			tailPosition.p = -90.0 * (moveRel.z / self.m_vecAccelerationMax.z)
		elseif moveRel.z < 0 then
			tailPosition.p = 90.0 * (moveRel.z / self.m_vecAccelerationMin.z)
		else
			tailPosition.p = 0.0
		end

		-- Lateral deviation
		if moveRel.y > 0 then
			tailPosition.r = 25 * (moveRel.y / self.m_vecAccelerationMax.y)
			tailPosition.y = -1.0 * (moveRel.y / self.m_vecAccelerationMax.y)
		elseif moveRel.y < 0 then
			tailPosition.r = -25 * (moveRel.y / self.m_vecAccelerationMin.y)
			tailPosition.y = (moveRel.y / self.m_vecAccelerationMin.y)
		else
			tailPosition.r = 0.0
			tailPosition.y = 0.0
		end

		-- Clamp
		flSwimSpeed = math.Clamp(flSwimSpeed, 0.25, 1.0)
		tailPosition.y = math.Clamp(tailPosition.y, -90.0, 90.0)
		tailPosition.p = math.Clamp(tailPosition.p, -90.0, 90.0)

		-- Blend
		self:SetTailYaw((self:GetTailYaw() * 0.8) + (tailPosition.y * 0.2))
		self:SetTailPitch((self:GetTailPitch() * 0.8) + (tailPosition.p * 0.2))
		self:SetSwimSpeed((self:GetSwimSpeed() * 0.8) + (flSwimSpeed * 0.2))

		-- Pose the body
		--print(self:GetSwimSpeed())
		self:SetPoseParameter("speed", self:GetSwimSpeed())
		self:SetPoseParameter("sidetoside", self:GetTailYaw())
		self:SetPoseParameter("upanddown", self:GetTailPitch())

		--if self:GetActivity() == ACT_RUN or self:GetActivity() == ACT_WALK then
		--	self:ResetSequenceInfo()
		--end

		-- Face our current velocity
		local dir = (self:GetPos() + self:GetVelocity()) - self:GetPos()
		dir:Normalize()
		dir = dir:Angle()

		self:SetIdealYawAndUpdate(math.NormalizeAngle(dir.y), -2)

		local pitch = 0

		if speed > 0 then
			pitch = -math.deg(math.asin(self:GetVelocity().z / speed))
		end

		local angles = self:GetLocalAngles()
		angles:Normalize()
		angles.x = (angles.x * 0.8) + (pitch * 0.2)
		angles.z = (angles.z * 0.9) + (tailPosition.r * 0.1)

		angles:Normalize()
		self:SetLocalAngles(angles)
	end

	function ENT:SteerArrive(Steer, vecTarget)
		local Offset = vecTarget - self:GetPos()
		local fTargetDistance = Offset:Length()

		local fIdealSpeed = self:GetGroundSpeed() * (fTargetDistance / ICH_WAYPOINT_DISTANCE)
		local fClippedSpeed = math.min(fIdealSpeed, self:GetGroundSpeed())

		local DesiredVelocity = Vector()

		if fTargetDistance > ICH_WAYPOINT_DISTANCE then
			DesiredVelocity = (fClippedSpeed / fTargetDistance) * Offset
		end

		Steer:Set(DesiredVelocity - self:GetVelocity())
	end

	function ENT:SteerSeek(Steer, vecTarget)
		local offset = vecTarget - self:GetPos()
		offset:Normalize()

		local desiredVelocity = self:GetGroundSpeed() * offset
		Steer:Set(desiredVelocity - self:GetVelocity())
	end

	local ICH_HEIGHT_PREFERENCE = 16.0
	local ICH_DEPTH_PREFERENCE = 8.0
	local MAX_WATER_SURFACE_DISTANCE = 512

	function ENT:SteerAvoidObstacles(Steer, Velocity, Forward, Right, Up)
		local tr = {}

		local collided = false
		local dir = Vector(Velocity)
		local speed = dir:Length()
		dir:Normalize()
		local pos = self:GetPos()

		util.TraceHull({
			start = pos,
			endpos = pos + (dir * speed),
			mins = self:OBBMins(),
			maxs = self:OBBMaxs(),
			mask = MASK_NPCSOLID,

			filter = self,
			collisiongroup = COLLISION_GROUP_NONE,
			output = tr
		})

		-- If we're hitting our enemy, just continue on
		if IsValid(self:GetEnemy()) and tr.Entity == self:GetEnemy() then
			return false
		end

		if tr.Fraction < 1 then
			local blocker = tr.Entity

			if blocker:IsValid() and blocker:IsNPC() then
				--print("avoiding an npc")
				local HitOffset = tr.HitPos - pos

				local SteerUp = HitOffset:Cross(Velocity)
				Steer:Set(SteerUp:Cross(Velocity))
				Steer:Normalize()

				if tr.Fraction > 0 then
					Steer:Set((Steer * Velocity:Length()) / tr.Fraction)
					debugoverlay.Line(pos, pos + Steer, 0.1, Color(255, 0, 0, 16), true)
				else
					Steer:Set(Steer * 1000 * Velocity:Length())
					debugoverlay.Line(pos, pos + Steer, 0.1, Color(255, 0, 0, 16), true)
				end
			else
				if blocker:IsValid() and blocker == self:GetEnemy() then
					--print("avoided collision")
					return false
				end

				--print("avoiding the world")

				if tr.Fraction == 0 then
					return false
				end

				local steeringVector = tr.HitNormal
				Steer:Set(steeringVector * (Velocity:Length() / tr.Fraction))

				debugoverlay.Line(pos, pos + Steer, 0.1, Color(255, 0, 0, 16), true)
			end

			collided = true
		end

		-- Try to remain 8 feet above the ground.
		util.TraceLine({
			start = pos,
			endpos = pos + Vector(0, 0, -ICH_HEIGHT_PREFERENCE),
			mask = MASK_NPCSOLID_BRUSHONLY,

			filter = self,
			collisiongroup = COLLISION_GROUP_NONE,
			output = tr
		})

		if tr.Fraction < 1 then
			Steer:Add(Vector(0, 0, self.m_vecAccelerationMax.z / tr.Fraction))
			collided = true
		end

		-- Stay under the surface
		if not self.m_bIgnoreSurface then
			local waterLevel = (self:UTIL_WaterLevel(pos, pos.z, pos.z + ICH_DEPTH_PREFERENCE) - pos.z) / ICH_DEPTH_PREFERENCE

			if waterLevel < 1 then
				Steer:Sub(Vector(0, 0, self.m_vecAccelerationMax.z / waterLevel))
				collided = true
			end
		end

		return collided
	end

	function ENT:ApplyAbsVelocityImpulse(impulse)
		if not impulse:IsZero() then
			if self:GetMoveType() == MOVETYPE_VPHYSICS then
				local phys = self:GetPhysicsObject()
				if phys:IsValid() then
					phys:AddVelocity(impulse)
				end
			else
				self:SetVelocity(impulse)
				--local vecResult = self:GetAbsVelocity() + impulse
				--self:SetAbsVelocity(vecResult)
			end
		end
	end

	ENT.m_vecAccelerationMax = Vector(256, 1024, 512)
	ENT.m_vecAccelerationMin = Vector(-256, -1024, -512)

	function ENT:ClampSteer(SteerAbs, SteerRel, forward, right, up)
		local fForwardSteer = SteerAbs:Dot(forward)
		local fRightSteer = SteerAbs:Dot(right)
		local fUpSteer = SteerAbs:Dot(up)

		if fForwardSteer > 0 then
			fForwardSteer = math.min(fForwardSteer, self.m_vecAccelerationMax.x)
		else
			fForwardSteer = math.max(fForwardSteer, self.m_vecAccelerationMin.x)
		end

		if fRightSteer > 0 then
			fRightSteer = math.min(fRightSteer, self.m_vecAccelerationMax.y)
		else
			fRightSteer = math.max(fRightSteer, self.m_vecAccelerationMin.y)
		end

		if fUpSteer > 0 then
			fUpSteer = math.min(fUpSteer, self.m_vecAccelerationMax.z)
		else
			fUpSteer = math.max(fUpSteer, self.m_vecAccelerationMin.z)
		end

		SteerAbs:Set((fForwardSteer * forward) + (fRightSteer * right) + (fUpSteer * up))
		SteerRel:SetUnpacked(fForwardSteer, fRightSteer, fUpSteer)
	end
end

do -- custom task handlers
	function ENT:TaskStart_PathToRandomNode(data) end

	function ENT:Task_PathToRandomNode(data)
		if not IsValid(self:GetEnemy()) or not self:NavSetRandomGoal(data, (self:GetEnemy():GetPos() - self:GetPos()):GetNormalized()) then
			if not self:NavSetRandomGoal(data, self:GetAimVector()) then
				self:TaskFail("no reachable node")

				--self:NextTask(self.CurrentSchedule)
				return
			end
		end

		self:TaskComplete()
	end

	function ENT:TaskStart_SetGoal(data) end

	function ENT:Task_SetGoal(data)
		if data == GOAL_ENEMY and IsValid(self:GetEnemy()) then
			if not self:NavSetGoalTarget(self:GetEnemy()) then
				--print("SetGoal() fail: no reachable goal")
				self:TaskFail("no reachable goal")
				--self:SetEnemy(NULL, false)
				--self:NextTask(self.CurrentSchedule)
				return
			end

			self:TaskComplete()
		end
	end

	function ENT:TaskStart_GetPathToGoal(data) end

	function ENT:Task_GetPathToGoal(data)
		local enemy = self:GetEnemy()
		if IsValid(enemy) and not self:NavSetGoalTarget(enemy) then
			--print("GetPathToGoal() fail: no path found")
			self:TaskFail("no PathToTarget found")
			--self:NextTask(self.CurrentSchedule)
			return
		end

		self:TaskComplete()
	end

	ENT.m_iActivityOverride = -1
	function ENT:TaskStart_SetIdealAnimation(data)
		self.m_iActivityOverride = data
	end

	function ENT:Task_SetIdealAnimation(data)
		self:TaskComplete()
	end
end

do -- engine wrappers
	function ENT:GetNPCMemory()
		return self:GetInternalVariable("m_afMemory")
	end

	function ENT:SetNPCMemory(iMemory)
		self:SetSaveValue("m_afMemory", iMemory)
	end

	function ENT:NPCForget(iMemory)
		self:SetNPCMemory(bit.band(self:GetNPCMemory(), bit.bnot(iMemory)))
	end

	function ENT:NPCRemember(iMemory)
		self:SetNPCMemory(bit.bor(self:GetNPCMemory(), iMemory))
	end

	function ENT:HasNPCMemory(iMemory)
		return bit.band(self:GetNPCMemory(), iMemory) ~= 0
	end

	function ENT:HasAllNPCMemory(iMemory)
		return bit.band(self:GetNPCMemory(), iMemory) == iMemory
	end

	function ENT:GetEfficiency()
		return self:GetInternalVariable("m_Efficiency")
	end

	function ENT:SetEfficiency(val)
		self:SetSaveValue("m_Efficiency", val)
	end

	function ENT:SetEnemyOccluder(ent)
		self:SetSaveValue("m_hEnemyOccluder", ent)
	end

	function ENT:GetLifeState()
		return self:GetInternalVariable("m_lifeState")
	end

	function ENT:SetLifeState(val)
		self:SetSaveValue("m_lifeState", val)
	end

	function ENT:GetIdealNPCState()
		return self:GetInternalVariable("m_IdealNPCState")
	end

	function ENT:SetIdealNPCState(val)
		self:SetSaveValue("m_IdealNPCState", val)
	end
end

do -- conditions gathering
	function ENT:FInViewCone0(otherEnt)
		return self:FInViewCone(otherEnt:WorldSpaceCenter())
	end

	function ENT:FInViewCone(vecSpot)
		local los = vecSpot - self:EyePos()

		-- do this in 2D
		los.z = 0
		los:Normalize()

		local facingDir = self:GetAngles():Forward()
		facingDir.z = 0
		facingDir:Normalize()

		local flFOV
		if self.GetFOV then
			flFOV = self:GetFOV()
		elseif self.m_flFieldOfView then
			flFOV = self.m_flFieldOfView
		else
			flFOV = 90
		end

		return los:Dot(facingDir) > math.cos(math.rad(flFOV) / 2)
	end

	function ENT:FVisible(pEntity, traceMask, pBlocker)
		if pEntity:IsFlagSet(FL_NOTARGET) then
			return false, pBlocker
		end

		-- don't look through water
		if ((self:WaterLevel() ~= 3 and pEntity:WaterLevel() == 3)
					or (self:WaterLevel() >= 2 and pEntity:WaterLevel() == 0)) then
			return false, pBlocker
		end

		local vecLookerOrigin = self:EyePos() -- look through the caller's 'eyes'
		local vecTargetOrigin = pEntity:EyePos()

		local tr = {}
		util.TraceLine({start = vecLookerOrigin, endpos = vecTargetOrigin, mask = traceMask, filter = self, collisiongroup = COLLISION_GROUP_NONE, output = tr})

		if tr.Fraction ~= 1.0 then
			if IsValid(tr.Entity) then
				pBlocker = tr.Entity
			end

			return false, pBlocker -- Line of sight is not established
		end

		return true, pBlocker -- line of sight is valid.
	end

	function ENT:ForceGatherConditions()
		self.m_bForceConditionsGather = true
		self:SetEfficiency(AIE_NORMAL)
	end

	function ENT:QuerySeeEntity(pEntity)
		return true
	end

	function ENT:EnemyDistance(pEnemy)
		local enemyDelta = pEnemy:GetPos() - self:GetPos()

		local enemy_mins = pEnemy:LocalToWorld(pEnemy:OBBMins())
		local enemy_maxs = pEnemy:LocalToWorld(pEnemy:OBBMaxs())

		local self_mins = self:LocalToWorld(self:OBBMins())
		local self_maxs = self:LocalToWorld(self:OBBMaxs())

		if enemy_mins.z >= self_maxs.z then
			-- closest to head
			enemyDelta.z = enemy_mins.z - self_maxs.z
		elseif enemy_maxs.z <= self_mins.z then
			-- closest to feet
			enemyDelta.z = enemy_maxs.z - self_mins.z
		else
			-- in between, no delta
			enemyDelta.z = 0
		end

		return enemyDelta:Length()
	end

	function ENT:GatherConditions()
		--print("gather conds")
		--self:SetCondition(COND.CAN_MELEE_ATTACK1)

		--do return end

		if self:GetNPCState() ~= NPC_STATE_NONE and self:GetNPCState() ~= NPC_STATE_DEAD then
			local bForcedGather = self.m_bForceConditionsGather
			self.m_bForceConditionsGather = false

			self:CheckPVSCondition()

			-- Sample the environment. Do this unconditionally if there is a player in this
			-- npc's PVS. NPCs in COMBAT state are allowed to simulate when there is no player in
			-- the same PVS. This is so that any fights in progress will continue even if the player leaves the PVS.
			if self:GetEfficiency() == AIE_NORMAL and (bForcedGather or self:HasCondition(COND.IN_PVS) or self:HasSpawnFlags(SF_NPC_ALWAYSTHINK) or self:GetNPCState() == NPC_STATE_COMBAT) then
				if self:ShouldPlayIdleSound() then
					self:IdleSound()
				end

				-- perform sensing
				-- refresh memories
				--self:ChooseEnemy()
			else
				-- if not done, can have problems if leave PVS in same frame heard/saw things,
				-- since only PerformSensing clears conditions
				--self:ClearSenseConditions()
			end

			-- do these calculations if npc has an enemy.
			local enemy = self:GetEnemy()
			if IsValid(enemy) then
				self:NavSetGoalTarget(enemy)

				if self:GetEfficiency() == AIE_NORMAL then
					self:GatherEnemyConditions(enemy)
				else
					self:SetEnemy(NULL, false)
				end
			end

			if IsValid(self:GetTarget()) then
				self:CheckTarget(self:GetTarget())
			end

			--self:CheckAmmo()

			--self:CheckFlinches()

			--self:CheckSquad()
		else
			self:ClearCondition(COND.IN_PVS)
		end
	end

	-- https://github.com/VSES/SourceEngine2007/blob/43a5c90a5ada1e69ca044595383be67f40b33c61/src_main/game/server/ai_basenpc.cpp#L5533
	function ENT:GatherEnemyConditions(pEnemy)
		self:ClearCondition(COND.ENEMY_FACING_ME)
		self:ClearCondition(COND.BEHIND_ENEMY)
		self:ClearCondition(COND.HAVE_ENEMY_LOS)
		self:ClearCondition(COND.ENEMY_OCCLUDED)

		local pBlocker = NULL
		self:SetEnemyOccluder(NULL)

		local bIsVisible
		bIsVisible, pBlocker = self:FVisible(pEnemy, MASK_OPAQUE, pBlocker)

		if not bIsVisible then
			-- No LOS to enemy
			self:SetEnemyOccluder(pBlocker)
			self:SetCondition(COND.ENEMY_OCCLUDED)

			if self:HasNPCMemory(MEMORY_HAD_LOS) then
				-- Send output event
				if pEnemy:IsPlayer() then
					self:Fire("OnLostPlayerLOS", nil, 0, self, self)
				end
				self:Fire("OnLostEnemyLOS", nil, 0, self, self)
			end

			self:NPCForget(MEMORY_HAD_LOS)
			self:SetEnemy(NULL, false)
		else
			-- Have LOS but may not be in view cone
			self:SetCondition(COND.HAVE_ENEMY_LOS)

			if self:FInViewCone0(pEnemy) and self:QuerySeeEntity(pEnemy) then
				-- Have LOS and in view cone
				self:SetCondition(COND.SEE_ENEMY)
			end

			if not self:HasNPCMemory(MEMORY_HAD_LOS) then
				if pEnemy:IsPlayer() then
					self:Fire("OnFoundPlayer", pEnemy, 0, self, self)
				end
				self:Fire("OnFoundEnemy", pEnemy, 0, self, self)
			end

			self:NPCRemember(MEMORY_HAD_LOS)
		end

		-- !pEnemy->IsAlive()
		if not pEnemy:Alive() or pEnemy:GetInternalVariable("m_lifeState") ~= 0 then
			self:SetCondition(COND.ENEMY_DEAD)
			self:ClearCondition(COND.SEE_ENEMY)
			self:ClearCondition(COND.ENEMY_OCCLUDED)
			return
		else
			self:ClearCondition(COND.ENEMY_DEAD)
		end

		local flDistToEnemy = self:EnemyDistance(pEnemy)
		if self:HasCondition(COND.SEE_ENEMY) then
			-- Trail the enemy a bit if he's moving
			local vel = pEnemy:IsPlayer() and pEnemy.m_vecSmoothedVelocity or pEnemy:GetVelocity()
			if not vel:IsZero() then
				local vTrailPos = pEnemy:GetPos() - vel * math.Rand(-0.05, 0)
				self:UpdateEnemyMemory(pEnemy, vTrailPos)
			else
				self:UpdateEnemyMemory(pEnemy, pEnemy:GetPos())
			end

			-- If it's not an NPC, assume it can't see me
			if not pEnemy:IsNPC() and self:FInViewCone0(pEnemy, self:GetPos()) then
				self:SetCondition(COND.ENEMY_FACING_ME)
				self:ClearCondition(COND.BEHIND_ENEMY)
			else
				self:ClearCondition(COND.ENEMY_FACING_ME)
				self:SetCondition(COND.BEHIND_ENEMY)
			end
		elseif (not self:HasCondition(COND.ENEMY_OCCLUDED)) and (not self:HasCondition(COND.SEE_ENEMY)) and flDistToEnemy <= 256 then
			-- if the enemy is not occluded, and unseen, that means it is behind or beside the npc.
			-- if the enemy is near enough the npc, we go ahead and let the npc know where the
			-- enemy is. Send the enemy in as the informer so this knowledge will be regarded as
			-- secondhand so that the NPC doesn't
			self:UpdateEnemyMemory(pEnemy, pEnemy:GetPos())
		end

		if flDistToEnemy >= self.m_flDistTooFar then
			-- enemy is very far away from npc
			self:SetCondition(COND.ENEMY_TOO_FAR)
		else
			self:ClearCondition(COND.ENEMY_TOO_FAR)
		end

		if self:FCanCheckAttacks() then
			self:GatherAttackConditions(pEnemy, flDistToEnemy)
		else
			self:ClearAttackConditions()
		end

		-- If my enemy has moved significantly, or if the enemy has changed update my path
		self:UpdateEnemyPos()

		-- If my target entity has moved significantly, update my path
		-- This is an odd place to put this, but where else should it go?
		self:UpdateTargetPos()

		--[[
			// ----------------------------------------------------------------------------
			// Check if enemy is reachable via the node graph unless I'm not on a network
			// ----------------------------------------------------------------------------
			if (GetNavigator()->IsOnNetwork())
			{
				// Note that unreachablity times out
				if (IsUnreachable(GetEnemy()))
				{
					SetCondition(COND_ENEMY_UNREACHABLE);
				}
			}
		]]

		--[[
			//-----------------------------------------------------------------------
			// If I haven't seen the enemy in a while he may have eluded me
			//-----------------------------------------------------------------------
			if (gpGlobals->curtime - GetEnemyLastTimeSeen() > 8)
			{
				//-----------------------------------------------------------------------
				// I'm at last known position at enemy isn't in sight then has eluded me
				// ----------------------------------------------------------------------
				Vector flEnemyLKP = GetEnemyLKP();
				if (((flEnemyLKP - GetAbsOrigin()).Length2D() < 48) &&
					!HasCondition(COND_SEE_ENEMY))
				{
					MarkEnemyAsEluded();
				}
				//-------------------------------------------------------------------
				// If enemy isn't reachable, I can see last known position and enemy
				// isn't there, then he has eluded me
				// ------------------------------------------------------------------
				if (!HasCondition(COND_SEE_ENEMY) && HasCondition(COND_ENEMY_UNREACHABLE))
				{
					if ( !FVisible( flEnemyLKP ) )
					{
						MarkEnemyAsEluded();
					}
				}
			}
		]]
	end

	function ENT:InnateWeaponLOSCondition(ownerPos, targetPos, bSetConditions)
		-- Check for occlusion

		-- Base class version assumes innate weapon
		local barrelPos = ownerPos + self:GetViewOffset()
		local tr = util.TraceLine({
			start = barrelPos,
			endpos = targetPos,
			mask = MASK_SHOT,
			filter = self,
			collisiongroup = COLLISION_GROUP_NONE
		})

		if tr.Fraction == 1 then
			return true
		end

		local hitEntity = tr.Entity
		local curEnemy = self:GetEnemy()

		-- Translate a hit vehicle into its passenger if found
		if curEnemy:IsValid() and curEnemy:IsPlayer() and curEnemy:InVehicle() then
			-- Ok, player in vehicle, check if vehicle is target we're looking at, fire if it is
			-- Also, check to see if the owner of the entity is the vehicle, in which case it's valid too.
			-- This catches vehicles that use bone followers.
			local veh = curEnemy:GetVehicle()
			if hitEntity == veh or hitEntity:GetOwner() == veh then
				return true
			end
		end

		if hitEntity == curEnemy then
			return true
		elseif hitEntity:IsValid() and (hitEntity:IsNPC() or hitEntity:IsPlayer() or hitEntity:IsNextBot()) then
			if self:Disposition(hitEntity) == D_HT then
				return true
			elseif bSetConditions then
				self:SetCondition(COND.WEAPON_BLOCKED_BY_FRIEND)
			end
		elseif bSetConditions then
			self:SetCondition(COND.WEAPON_SIGHT_OCCLUDED)
			self:SetEnemyOccluder(hitEntity)
		end

		return false
	end

	function ENT:CurrentWeaponLOSCondition(targetPos, bSetConditions)
		return self:WeaponLOSCondition(self:GetPos(), targetPos, bSetConditions)
	end

	-- https://github.com/VSES/SourceEngine2007/blob/master/se2007/game/server/ai_basenpc.cpp#L5272
	function ENT:WeaponLOSCondition(ownerPos, targetPos, bSetConditions)
		local bHaveLOS

		local wep = self:GetActiveWeapon()
		local capabilities = self:CapabilitiesGet()
		if wep:IsValid() and bit.band(capabilities, CAP_WEAPON_RANGE_ATTACK1) ~= 0 then
			--
		elseif bit.band(capabilities, CAP_INNATE_RANGE_ATTACK1) ~= 0 then
			bHaveLOS = self:InnateWeaponLOSCondition(ownerPos, targetPos, bSetConditions)
		else
			if bSetConditions then
				self:SetCondition(COND.NO_WEAPON)
			end
			bHaveLOS = false
		end

		-- Check for friendly fire with the player

		return bHaveLOS
	end

	function ENT:GatherAttackConditions(pTarget, flDist)
		local vecLOS = (pTarget:GetPos() - self:GetPos())
		vecLOS.z = 0
		vecLOS:Normalize()

		local vBodyDir = self:GetAngles():Forward()
		vBodyDir.z = 0

		local flDot = vecLOS:Dot(vBodyDir)

		local capability, condition = 0, 0
		local targetPos
		local bWeaponHasLOS

		capability = self:CapabilitiesGet()

		self:ClearAttackConditions()
		targetPos = pTarget:EyePos()
		bWeaponHasLOS = self:CurrentWeaponLOSCondition(targetPos, true)

		if not bWeaponHasLOS then
			self:ClearAttackConditions()
			targetPos = pTarget:BodyTarget(self:GetPos())
			bWeaponHasLOS = self:CurrentWeaponLOSCondition(targetPos, true)

			if bWeaponHasLOS then
				self:SetCondition(COND.WEAPON_HAS_LOS)
			end
		else
			self:SetCondition(COND.WEAPON_HAS_LOS)
		end

		local bWeaponIsReady = false -- (self:GetActiveWeapon():IsValid() and not self:IsWeaponStateChanging())
		--if bit.band(capability, CAP_WEAPON_RANGE_ATTACK1) ~= 0 and bWeaponIsReady then
		--	--
		--elseif bit.band(capability, CAP_INNATE_RANGE_ATTACK1) ~= 0 then
		--	--
		--end
		-- range attack2

		if bit.band(capability, CAP_WEAPON_MELEE_ATTACK1) ~= 0 and bWeaponIsReady then
			--self:SetCondition(self:GetActiveWeapon():WeaponMeleeAttack1Condition(flDot, flDist))
		elseif bit.band(capability, CAP_INNATE_MELEE_ATTACK1) ~= 0 then
			self:SetCondition(self:MeleeAttack1Conditions(flDot, flDist))
		end

		if bit.band(capability, CAP_WEAPON_MELEE_ATTACK2) ~= 0 and bWeaponIsReady then
			--self:SetCondition(self:GetActiveWeapon():WeaponMeleeAttack2Condition(flDot, flDist))
		elseif bit.band(capability, CAP_INNATE_MELEE_ATTACK2) ~= 0 then
			self:SetCondition(self:MeleeAttack2Conditions(flDot, flDist))
		end

		if self:HasCondition(COND.CAN_RANGE_ATTACK2) or
				self:HasCondition(COND.CAN_RANGE_ATTACK1) or
				self:HasCondition(COND.CAN_MELEE_ATTACK2) or
				self:HasCondition(COND.CAN_MELEE_ATTACK1)
		then
			self:ClearCondition(COND.TOO_CLOSE_TO_ATTACK)
			self:ClearCondition(COND.TOO_FAR_TO_ATTACK)
			self:ClearCondition(COND.WEAPON_BLOCKED_BY_FRIEND)
		end
	end

	function ENT:ClearAttackConditions()
		-- Clear all attack conditions
		self:ClearCondition(COND.CAN_RANGE_ATTACK1)
		self:ClearCondition(COND.CAN_RANGE_ATTACK2)
		self:ClearCondition(COND.CAN_MELEE_ATTACK1)
		self:ClearCondition(COND.CAN_MELEE_ATTACK2)
		self:ClearCondition(COND.WEAPON_HAS_LOS)
		self:ClearCondition(COND.WEAPON_BLOCKED_BY_FRIEND)
		self:ClearCondition(COND.WEAPON_PLAYER_IN_SPREAD) -- Player in shooting direction
		self:ClearCondition(COND.WEAPON_PLAYER_NEAR_TARGET) -- Player near shooting position
		self:ClearCondition(COND.WEAPON_SIGHT_OCCLUDED)
	end

	function ENT:MeleeAttack1Conditions(flDot, flDist)
		local enemy = self:GetEnemy()
		if not IsValid(enemy) then return 0 end

		local predictedDir = (enemy:GetPos() + (enemy:IsPlayer() and enemy.m_vecSmoothedVelocity or enemy:GetVelocity())) - self:GetPos()
		local flPredictedDist = predictedDir:Length()
		predictedDir:Normalize()

		local vBodyDir = self:GetAngles():Forward()

		local flPredictedDot = predictedDir:Dot(vBodyDir)

		if flPredictedDot < 0.8 then
			return COND.NOT_FACING_ATTACK
		end

		if flPredictedDist > self:GetVelocity():Length() * 0.5 and flDist > 128 then
			return COND.TOO_FAR_TO_ATTACK
		end

		return COND.CAN_MELEE_ATTACK1
	end

	function ENT:CheckPVSCondition()
		local filter = RecipientFilter()
		filter:AddPVS(self:GetPos())

		local bInPVS = filter:GetCount() > 0

		if bInPVS then
			self:SetCondition(COND.IN_PVS)
		else
			self:ClearCondition(COND.IN_PVS)
		end

		return bInPVS
	end

	function ENT:FCanCheckAttacks()
		-- Not allowed to check attacks while climbing or jumping
		-- Otherwise schedule is interrupted while on ladder/etc
		-- which is NOT a legal place to attack from
		if self:GetNavType() == NAV_CLIMB or self:GetNavType() == NAV_JUMP then
			return false
		end

		if self:HasCondition(COND.SEE_ENEMY) and not self:HasCondition(COND.ENEMY_TOO_FAR) then
			return true
		end

		return false
	end
end

function ENT:HandleAnimEvent(event, time, cycle, eventtype, options)
	if event == ICH_AE_BITE then
		self:Bite()
		return true
	elseif event == ICH_AE_BITE_START then
		self:EmitSound("NPC_Ichthyosaur.AttackGrowl")
		return true
	end
end

local SMOOTHING_FACTOR = 0.9

hook.Add("PlayerPostThink", "oc.smoothed_velocity", function(ply)
	local plyTable = ply:GetTable()

	if not plyTable.m_vecSmoothedVelocity then
		plyTable.m_vecSmoothedVelocity = Vector()
	end

	plyTable.m_vecSmoothedVelocity = (plyTable.m_vecSmoothedVelocity * SMOOTHING_FACTOR) + (ply:GetAbsVelocity() * (1 - SMOOTHING_FACTOR))
end)

ENT.m_SensesTimer = util.Timer()
function ENT:PerformSenses()
	if self.m_SensesTimer:Elapsed() then
		self.m_SensesTimer:Start(0.5)

		local tEnts = ents.FindInCone(self:EyePos(), self:GetAngles():Forward(), self:GetMaxLookDistance(), math.cos(math.rad(self.m_flFieldOfViewDegrees)))
		local i = 1
		repeat
			local otherEnt = tEnts[i]

			if otherEnt == self or                          -- don't target self
					not otherEnt:IsValid() or
					not (otherEnt:IsPlayer() or otherEnt:IsNPC()) or -- only target players and npcs
					self:Disposition(otherEnt) ~= D_HT or       -- only characters we hate
					not self:FVisible(otherEnt)                 -- only characters we can see
			then
				table.remove(tEnts, i)
			else
				self:UpdateEnemyMemory(otherEnt, otherEnt:GetPos())

				i = i + 1
			end
		until i > #tEnts

		if #tEnts > 0 then
			self:SetCondition(COND.SEE_ENEMY)
		end
	end
end

-- https://github.com/ValveSoftware/source-sdk-2013/blob/0d8dceea4310fde5706b3ce1c70609d72a38efdf/sp/src/game/server/util.cpp#L1526-L1554
function ENT:UTIL_WaterLevel(vecPosition, flMinZ, flMaxZ)
	local midUp = Vector(vecPosition)
	midUp.z = flMinZ

	if bit.band(util.PointContents(midUp), MASK_WATER) == 0 then
		return flMinZ
	end

	midUp.z = flMaxZ
	if bit.band(util.PointContents(midUp), MASK_WATER) ~= 0 then
		return flMaxZ
	end

	local diff = flMaxZ - flMinZ
	while diff > 1.0 do
		midUp.z = flMinZ + diff / 2.0
		if bit.band(util.PointContents(midUp), MASK_WATER) ~= 0 then
			flMinZ = midUp.z
		else
			flMaxZ = midUp.z
		end
		diff = flMaxZ - flMinZ
	end

	return midUp.z
end

function ENT:Think()
	self:PerformSenses()

	--Randomly emit bubbles
	if math.random(0, 10) == 0 then
		local pos = self:GetPos()
		local mins, maxs = pos + self:OBBMins(), pos + self:OBBMaxs()
		local mid = (mins + maxs) * 0.5

		local flHeight = self:UTIL_WaterLevel(mid, mid.z, mid.z + 1024) - 10
		flHeight = flHeight - mins.z

		effects.Bubbles(self:GetPos() + self:OBBMins() * 0.5, self:GetPos() + self:OBBMaxs() * 0.5, 10, flHeight, 8, 0)
	end

	return true
end

ENT.m_tScheduleInterrupts = {
	["icky_chase_enemy"] = {
		[COND.NEW_ENEMY] = false,
		[COND.ENEMY_DEAD] = true,
		[COND.ENEMY_UNREACHABLE] = true,
		[COND.CAN_MELEE_ATTACK1] = true,
		[COND.TOO_CLOSE_TO_ATTACK] = true,
		[COND.LOST_ENEMY] = true,
		[COND.TASK_FAILED] = false,
	},
	["icky_patrol_run"] = {
		[COND.CAN_MELEE_ATTACK1] = true,
		[COND.GIVE_WAY] = true,
		[COND.NEW_ENEMY] = false,
		[COND.LIGHT_DAMAGE] = true,
		[COND.HEAVY_DAMAGE] = true,
	},
	["icky_patrol_walk"] = {
		[COND.CAN_MELEE_ATTACK1] = true,
		--[COND.GIVE_WAY] = true,
		--[COND.NEW_ENEMY] = false,
		--[COND.LIGHT_DAMAGE] = true,
		--[COND.HEAVY_DAMAGE] = true,
	},
	["icky_melee_attack1"] = {
		[COND.NEW_ENEMY] = false,
		[COND.ENEMY_DEAD] = false,
		[COND.ENEMY_OCCLUDED] = false,
	}
}
function ENT:RunAI(strExp)
	-- If we're running an Engine Side behaviour
	-- then return true and let it get on with it.
	if self:IsRunningBehavior() then
		return true
	end

	-- If we're doing an engine schedule then return true
	-- This makes it do the normal AI stuff.
	if self:DoingEngineSchedule() then
		return true
	end

	self:GatherConditions()

	-- If we're currently running a schedule then run it.
	if self.CurrentSchedule then
		--print("able to atk? PRE", self.CurrentSchedule.DebugName, self:HasCondition(COND.CAN_MELEE_ATTACK1))

		if istable(self.CurrentSchedule) then
			local tInterrupts = self.m_tScheduleInterrupts[self.CurrentSchedule.DebugName]
			if tInterrupts then
				for cond, on in pairs(tInterrupts) do
					if not on then continue end

					if self:HasCondition(cond) then
						print(self, Format("Schedule \"%s\" interrupted by %s (%d)", self.CurrentSchedule.DebugName, self:ConditionName(cond), cond))
						--if isnumber(on) then
						--	self:SetSchedule(on)
						--end
						self:ScheduleFinished()
						--self:StartEngineSchedule(SCHED_NONE)
						break
					end
				end
			end
		end

		if self.CurrentSchedule then
			--PrintTable(self:GetTable())
			self:DoSchedule(self.CurrentSchedule)
			--print(Format("Running Schedule \"%s\", Task (%d/%d) %s", self.CurrentSchedule.DebugName, self.CurrentTaskID, self.CurrentSchedule.TaskCount, self.CurrentTask.TaskName or self.CurrentTask.FunctionName))
		end
	end

	--print("able to atk? POST", self:HasCondition(COND.CAN_MELEE_ATTACK1))

	-- If we have no schedule (schedule is finished etc)
	-- Then get the derived NPC to select what we should be doing
	if not self.CurrentSchedule then
		self:SelectSchedule()
	end

	-- if the npc didn't use these conditions during the above call to MaintainSchedule()
	-- we throw them out cause we don't want them sitting around through the lifespan of a schedule
	-- that doesn't use them.
	--self:ClearCondition(COND.LIGHT_DAMAGE)
	--self:ClearCondition(COND.HEAVY_DAMAGE)

	-- Do animation system
	self:CalcActivity(self:GetVelocity())
	self:MaintainActivity()
end

function ENT:SelectSchedule()
	local state = self:GetNPCState()

	local enemy = self:GetEnemy()
	--if IsValid(enemy) then
	--	if not self:FVisible(enemy) then
	--		self:SetEnemy(NULL)
	--	end
	--end

	print(self, "SelectSchedule", state, NPC_STATE_COMBAT, enemy or NULL)
	if state == NPC_STATE_COMBAT then
		if self.m_flHoldTime > CurTime() then
			print("selected drown")
			--self:StartSchedule(SCHED_ICH_DROWN_VICTIM)
			return
		elseif self.m_flNextBiteTime > CurTime() then -- On cooldown for biting, reposition & line up to bite again
			print("selected patrol run (bite??)")
			self:SetSchedule(SCHED_PATROL_RUN)
			--self:StartSchedule(SCHED_ICH_PATROL_RUN)
			return
		elseif self:HasCondition(COND.CAN_MELEE_ATTACK1) then -- CHOMP
			print("selected melee attack1")
			self:SetSchedule(SCHED_MELEE_ATTACK1)
			--self:NavSetGoalPos(goalPos)
			--self:StartSchedule(SCHED_ICH_MELEE_ATTACK1)
			return
		end

		if IsValid(enemy) then
			--if self:GetNPCState() ~= NPC_STATE_COMBAT then
			--	self:SetNPCState(NPC_STATE_COMBAT)
			--end

			print("chase!!")
			self:StartSchedule(SCHED_ICH_CHASE_ENEMY)
		else
			print("patrol walk")
			self:StartSchedule(SCHED_ICH_PATROL_WALK)
		end
		return
	end

	return BaseClass.SelectSchedule(self)
end

do -- attack handling
	local ICH_SCREENFADE_RED = Color(64, 0, 0, 255)
	function ENT:Bite()
		-- Don't allow another bite too soon
		if self.m_flNextBiteTime > CurTime() then return end

		local pHurt = NULL
		local enemy = self:GetEnemy()

		-- FIXME: E3 HACK - Always damage bullseyes if we're scripted to hit them
		if IsValid(enemy) and (enemy:IsNPC() and enemy:Classify() == CLASS_BULLSEYE) then
			pHurt = enemy
		else
			pHurt = self:CheckTraceHullAttack0(108, Vector(-32, -32, -32), Vector(32, 32, 32), 0, DMG_CLUB)
		end

		-- Hit something
		if pHurt:IsValid() then
			local info = DamageInfo()
			info:SetInflictor(self)
			info:SetAttacker(self)
			info:SetDamage(sk_ichthyosaur_melee_dmg:GetInt())
			info:SetDamageType(DMG_CLUB)

			if pHurt:IsPlayer() then
				info:SetDamage(sk_ichthyosaur_melee_dmg:GetInt() * 3)

				self:CalculateMeleeDamageForce(info, self:GetVelocity(), pHurt:GetPos())
				pHurt:TakeDamageInfo(info)

				pHurt:ScreenFade(SCREENFADE.IN, ICH_SCREENFADE_RED, 0.5, 0)

				-- Disorient the player
				local angles = pHurt:EyeAngles()
				angles.x = angles.x + math.Rand(60, 25)
				angles.y = angles.y + math.Rand(60, 25)
				angles.z = 0
				pHurt:SetEyeAngles(angles)
			else
				self:CalculateMeleeDamageForce(info, self:GetVelocity(), pHurt:GetPos())
				pHurt:TakeDamageInfo(info)
			end

			self.m_flNextBiteTime = CurTime() + math.Rand(2, 4)

			-- Bubbles!
			local pos = pHurt:GetPos()
			local mins, maxs = Vector(-32, -32, -32), Vector(32, 32, 32)
			local mid = pos + ((mins + maxs) * 0.5)

			local flHeight = self:UTIL_WaterLevel(mid, mid.z, mid.z + 1024)
			flHeight = flHeight - mins.z

			effects.Bubbles(pos + mins * 0.5, pos + maxs * 0.5, math.random(16, 32), flHeight, 8, 0)

			self:EmitSound("NPC_Ichthyosaur.Bite")

			if self:GetActivity() == ACT_MELEE_ATTACK1 then
				self:SetActivity(self:GetSequenceActivity(self:LookupSequence("attackend")))
			end

			return
		end

		-- Play the miss animation and sound
		if self:GetActivity() == ACT_MELEE_ATTACK1 then
			self:SetActivity(self:GetSequenceActivity(self:LookupSequence("attackmiss")))
		end

		self:EmitSound("NPC_Ichthyosaur.BiteMiss")
	end

	local phys_pushscale = GetConVar"phys_pushscale"

	-- Fill out a takedamageinfo with a damage force for a melee impact
	function ENT:CalculateMeleeDamageForce(info, vecMeleeDir, vecForceOrigin, flScale)
		if flScale == nil then
			flScale = 1
		end

		info:SetDamagePosition(vecForceOrigin)

		-- Calculate an impulse large enough to push a 75kg man 4 in/sec per point of damage
		local flForceScale = 50 * (75 * 4)
		local vecForce = Vector(vecMeleeDir)
		vecForce:Normalize()
		vecForce:Mul(flForceScale)
		vecForce:Mul(phys_pushscale:GetFloat())
		vecForce:Mul(flScale)
		info:SetDamageForce(vecForce)
	end

	function ENT:CheckTraceHullAttack0(flDist, mins, maxs, iDamage, iDmgType)
		-- If only a length is given assume we want to trace in our facing direction
		local forward = Vector()
		forward:Set(self:GetAngles():Forward())
		local vStart = self:GetPos()

		-- The ideal place to start the trace is in the center of the attacker's bounding box.
		-- however, we need to make sure there's enough clearance. Some of the smaller monsters aren't
		-- as big as the hull we try to trace with. (SJB)
		local flVerticalOffset = self:GetCollisionBounds().z * 0.5

		if flVerticalOffset < maxs.z then
			-- There isn't enough room to trace this hull, it's going to drag the ground.
			-- so make the vertical offset just enough to clear the ground.
			flVerticalOffset = maxs.z + 1
		end

		vStart.z = vStart.z + flVerticalOffset
		local vEnd = vStart + (forward * flDist)

		return self:CheckTraceHullAttack(vStart, vEnd, mins, maxs, iDamage, iDmgType)
	end

	function ENT:CheckTraceHullAttack(vStart, vEnd, mins, maxs, iDamage, iDmgType)
		local tr = {}
		util.TraceHull({
			output = tr,

			start = vStart,
			endpos = vEnd,
			mins = mins,
			maxs = maxs,
			filter = self,
			mask = MASK_SHOT_HULL,
			collisiongroup = COLLISION_GROUP_PROJECTILE
		})

		if not tr.Entity:IsValid() then
			-- See if perhaps I'm trying to claw/bash someone who is standing on my head.
			local vecTopCenter = self:GetPos()
			local vecEnd = Vector()
			local vecMins, vecMaxs = vecTopCenter + self:OBBMins(), vecTopCenter + self:OBBMaxs()

			vecTopCenter.z = vecMaxs.z + 1
			vecEnd:Set(vecTopCenter)
			vecEnd.z = vecEnd.z + 2

			util.TraceHull({
				output = tr,

				start = vecTopCenter,
				endpos = vEnd,
				mins = mins,
				maxs = maxs,
				filter = self,
				mask = MASK_SHOT_HULL,
				collisiongroup = COLLISION_GROUP_PROJECTILE
			})
		end

		----[[
		-- Handy debuging tool to visualize HullAttack trace
		local length = (vEnd - vStart):Length()
		local direction = (vEnd - vStart)
		direction:Normalize()

		local hullMaxs = Vector(maxs)
		hullMaxs.x = length + hullMaxs.x
		debugoverlay.SweptBox(vStart, vEnd, mins, hullMaxs, direction:Angle(), 1, Color(100, 255, 255, 20))
		debugoverlay.SweptBox(vStart, vEnd, mins, maxs, direction:Angle(), 1, Color(255, 0, 0, 20))
		--]]

		local hEntity = tr.Entity
		if hEntity:IsValid() then
			if hEntity:GetInternalVariable("m_takedamage") == 0 then
				return NULL
			end

			if self:Disposition(hEntity) == D_HT then
				if iDamage > 0 then
					local info = DamageInfo()
					CalculateMeleeDamageForce(info, (vEnd - vStart), vStart)
					hEntity:TakeDamageInfo(info)
				end
			end

			return hEntity
		end

		return NULL
	end
end

local DIST_LOOK = 2048
function ENT:CheckTarget(pTarget)
	self:ClearCondition(COND.HAVE_TARGET_LOS)
	self:ClearCondition(COND.TARGET_OCCLUDED)

	if pTarget:GetPos():DistToSqr(self:GetPos()) >= DIST_LOOK * DIST_LOOK or not self:FVisible(pTarget) then
		self:SetCondition(COND.TARGET_OCCLUDED)
	else
		self:SetCondition(COND.HAVE_TARGET_LOS)
	end

	self:UpdateTargetPos()
end
