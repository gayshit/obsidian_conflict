AddCSLuaFile()
local sk_ichthyosaur_health = GetConVar"sk_ichthyosaur_health"

ENT.Type = "ai"
ENT.Base = "base_ai"
ENT.PrintName = "Ichthyosaur"
ENT.Spawnable = true
ENT.AdminOnly = true
ENT.AutomaticFrameAdvance = true
ENT.m_iClass = CLASS_ANTLION

ENT.m_flFieldOfView = -0.707 -- 270 degrees
ENT.m_flFieldOfViewDegrees = 270
ENT.m_flDistTooFar = 1024

DEFINE_BASECLASS(ENT.Base)

ENT.Sounds = {
	Die = "NPC_Icky.Die",
	Pain = "NPC_Icky.Pain"
}

do -- register sounds
	sound.Add({
		name = "NPC_Icky.Die",
		channel = CHAN_VOICE,
		volume = 1,
		sound = {
			"npc/ichthyosaur/ichy_die1.wav",
			"npc/ichthyosaur/ichy_die2.wav",
			"npc/ichthyosaur/ichy_die3.wav",
			"npc/ichthyosaur/ichy_die4.wav"
		}
	})
	sound.Add({
		name = "NPC_Icky.Pain",
		channel = CHAN_VOICE,
		volume = 1,
		sound = {
			"npc/ichthyosaur/ichy_pain1.wav",
			"npc/ichthyosaur/ichy_pain2.wav",
			"npc/ichthyosaur/ichy_pain3.wav",
			"npc/ichthyosaur/ichy_pain4.wav"
		}
	})
	sound.Add({
		name = "NPC_Icky.Idle",
		channel = CHAN_VOICE,
		volume = 1,
		sound = {
			"npc/ichthyosaur/ichy_idle1.wav",
			"npc/ichthyosaur/ichy_idle2.wav",
			"npc/ichthyosaur/ichy_idle3.wav",
			"npc/ichthyosaur/ichy_idle4.wav"
		}
	})
	sound.Add({
		name = "NPC_Icky.Alert",
		channel = CHAN_VOICE,
		volume = 1,
		sound = {
			"npc/ichthyosaur/ichy_alert1.wav",
			"npc/ichthyosaur/ichy_alert2.wav",
			"npc/ichthyosaur/ichy_alert3.wav"
		}
	})
end

function ENT:AcceptInput(inp, actor, caller, data)
	if inp == "AddOutput" then
		local _ = string.Split(data, " ")
		self:SetKeyValue(_[1], table.concat(_, "", 2):gsub(":", ","))
	else
		--print(self, "acceptinput()", inp, actor, caller, data)
	end
end

function ENT:Initialize()
	self:SetModel("models/ickyosaur.mdl")

	if SERVER then
		self:SetHullType(HULL_LARGE_CENTERED)
		self:SetHullSizeNormal()

		self:SetNavType(NAV_FLY)
		self:SetNPCState(NPC_STATE_NONE)
		self:SetBloodColor(BLOOD_COLOR_GREEN)

		self:SetHealth(sk_ichthyosaur_health:GetFloat())
		self:SetMaxHealth(self:Health())

		--self:SetMaxLookDistance(1024) -- 2048 default

		print("npc_ichthyosaur init; target name:", self.m_strTargetName, self:GetTarget())
		if #self.m_strTargetName > 0 then
			local target = self:GetTarget() or NULL
			if target:IsValid() then
				self:NavSetGoalTarget(target)

				local goalTarget = self:GetGoalTarget()
				if goalTarget:IsValid() then
					self:StartTargetHandling(goalTarget)
				end
			end
		end
	end

	self:SetSurroundingBoundsType(BOUNDS_HITBOXES)

	self:SetSolid(SOLID_BBOX)
	self:AddSolidFlags(FSOLID_NOT_STANDABLE)
	self:SetMoveType(MOVETYPE_STEP)
	self:AddFlags(FL_FLY)

	if SERVER then
		self:CapabilitiesClear()
		self:CapabilitiesAdd(CAP_MOVE_FLY)
		self:CapabilitiesAdd(CAP_INNATE_MELEE_ATTACK1)
		self:CapabilitiesAdd(CAP_ANIMATEDFACE)
	end
end

function ENT:StartTargetHandling(targetEnt)
	--local bIsFlying = self:GetMoveType() == MOVETYPE_FLY or self:GetMoveType() == MOVETYPE_FLYGRAVITY
	self:SetNPCState(NPC_STATE_IDLE)
	self:SetSchedule(SCHED_IDLE_WALK)
end

function ENT:SetupDataTables()
	self:NetworkVar("Float", 0, "TailYaw")
	self:NetworkVar("Float", 1, "TailPitch")
	self:NetworkVar("Float", 2, "SwimSpeed")
end

local MIN_PHYSICS_FLINCH_DAMAGE = 25 -- FIXME: guess

ENT.m_flLastDamageTime = 0
ENT.m_flSumDamage = 0
function ENT:OnTakeDamage(dmgInfo)
	if self:Health() <= 0 then return false end

	self:SetHealth(self:Health() - dmgInfo:GetDamage())

	self:DoDamageSound()

	local attacker = dmgInfo:GetAttacker()
	if self.m_flLastDamageTime ~= CurTime() then
		self:TriggerOutput("OnDamaged", attacker)

		if attacker:IsPlayer() then
			self:TriggerOutput("OnDamagedByPlayer", attacker)
			-- fire OnDamagedByPlayerSquad
		else
			-- check for npc on player's squad
		end
	end

	if dmgInfo:IsDamageType(DMG_CRUSH) and not dmgInfo:IsDamageType(DMG_PHYSGUN) and dmgInfo:GetDamage() >= MIN_PHYSICS_FLINCH_DAMAGE then
		self:SetCondition(COND.PHYSICS_DAMAGE)
	end

	if self:Health() <= self:GetMaxHealth() / 2 then
		self:TriggerOutput("OnHalfHealth", attacker)
	end

	if attacker:IsValid() and (attacker:IsPlayer() or attacker:IsNPC()) then
		-- update enemy last known pos
		-- https://github.com/VSES/SourceEngine2007/blob/master/se2007/game/server/ai_basenpc.cpp#L807

		-- add pain to the conditions
		if self:IsLightDamage(dmgInfo) then
			self:SetCondition(COND.LIGHT_DAMAGE)
		end
		if self:IsHeavyDamage(dmgInfo) then
			self:SetCondition(COND.HEAVY_DAMAGE)
		end

		self:ForceGatherConditions()

		if (CurTime() - self.m_flLastDamageTime) < 1 then
			self.m_flSumDamage = self.m_flSumDamage + dmgInfo:GetDamage()
		else
			self.m_flSumDamage = dmgInfo:GetDamage()
		end

		self.m_flLastDamageTime = CurTime()

		self:MarkTookDamageFromEnemy(dmgInfo:GetAttacker())

		if self.m_flSumDamage > self:GetMaxHealth() * 0.3 then
			self:SetCondition(COND.REPEATED_DAMAGE)
		end
	end

	if self:Health() <= 0 then
		self:DoDie(dmgInfo)
	end

	return false
end

function ENT:IsLightDamage(info)
	return info:GetDamage() > 0
end

function ENT:IsHeavyDamage(info)
	return info:GetDamage() > 20
end

local LIFE_ALIVE = 0
local LIFE_DYING = 1
local LIFE_DEAD = 2
function ENT:DoDie(dmgInfo)
	if self.m_bAlreadyDead then return end

	self:SetLifeState(LIFE_DYING)
	self.m_bAlreadyDead = true
	self:EmitSound(self.Sounds.Die)

	hook.Run("OnNPCKilled", self, dmgInfo:GetAttacker(), dmgInfo:GetInflictor())
	self:TriggerOutput("OnDeath", dmgInfo:GetAttacker())

	self.CurrentSchedule = nil
	self:SetSchedule(SCHED_DIE)
end

function ENT:GetRelationship(ent)
	if ent:IsPlayer() then
		return D_HT
	else
		return D_NU
	end
end

function ENT:DoDamageSound()
	self:EmitSound(self.Sounds.Pain)
end

ENT.m_flHoldTime = 0
ENT.m_flNextBiteTime = 0

function ENT:OnTaskFailed(code, reason)
	--print("OnTaskFailed", code, reason)
end

function ENT:CalcActivity(vel)
	if self.m_iActivityOverride ~= -1 then
		--print("activity override currently \"works\" but doesnt allow melee")
		--return self:SetIdealActivity(self.m_iActivityOverride)
	end

	--return self:SetIdealActivity(ACT_WALK)
end

-- https://github.com/VSES/SourceEngine2007/blob/master/se2007/game/server/ai_basenpc.cpp#L10183
function ENT:ChooseEnemy()
	local hInitialEnemy = self:GetEnemy()
	local hChosenEnemy = hInitialEnemy

	local fHadEnemy = self:HasNPCMemory(bit.bor(MEMORY_HAD_ENEMY, MEMORY_HAD_PLAYER))
	local fEnemyWasPlayer = self:HasNPCMemory(MEMORY_HAD_PLAYER)
	local fEnemyWentNull = fHadEnemy and not IsValid(hInitialEnemy)
	local fEnemyEluded = fEnemyWentNull or self:HasEnemyEluded(hInitialEnemy)

	local fHaveCondNewEnemy
	local fHaveCondLostEnemy

	local sched = self:GetCurrentSchedule()

	--print("choose enemy", hInitialEnemy, fHadEnemy, fEnemyWasPlayer, sched)
end

function ENT:ShouldPlayIdleSound()
	local iState = self:GetNPCState()
	if (iState == NPC_STATE_IDLE or iState == NPC_STATE_ALERT) and math.random(0, 10) == 0 and not self:HasSpawnFlags(SF_NPC_GAG) then
		return true
	end

	return false
end

function ENT:IdleSound()
	print("grrr idle sound")
end

-- https://github.com/VSES/SourceEngine2007/blob/43a5c90a5ada1e69ca044595383be67f40b33c61/src_main/game/server/ai_basenpc.cpp#L5804
function ENT:UpdateTargetPos()
	--print("update target pos")
end

-- https://github.com/VSES/SourceEngine2007/blob/43a5c90a5ada1e69ca044595383be67f40b33c61/src_main/game/server/ai_basenpc.cpp#L5763
function ENT:UpdateEnemyPos()
	--print("update enemy pos")
end
