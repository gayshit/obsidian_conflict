AddCSLuaFile()

ENT.Type = "ai"
ENT.Base = "base_ai"
ENT.PrintName = "Mortar Synth"
ENT.Spawnable = true
ENT.AdminOnly = true
ENT.AutomaticFrameAdvance = true
ENT.m_iClass = CLASS_SCANNER

function ENT:Initialize()
	self:SetModel("models/mortarsynth.mdl")

	if SERVER then
		self:SetHullType(HULL_LARGE_CENTERED)
		self:SetHullSizeNormal()

		self:SetNavType(NAV_FLY)
		self:SetNPCState(NPC_STATE_NONE)
		self:SetBloodColor(BLOOD_COLOR_GREEN)

		self:SetHealth(100)
		self:SetMaxHealth(self:Health())

		--self:SetMaxLookDistance(1024) -- 2048 default
	end
end
