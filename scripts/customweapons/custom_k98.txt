"WeaponData"
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"			"Mauser Karabiner 98 Kurz"
	"viewmodel"			"models/weapons/v_k98.mdl"
	"playermodel"			"models/weapons/w_k98.mdl"
	"anim_prefix"			"ar2"
	"bucket"			"2"
	"bucket_position"		"17"

	"clip_size"			"5"
	"clip2_size"			"0"

	"default_clip"			"5"
	"default_clip2"			"0"

	"primary_ammo"			"k98_ammo"
	"secondary_ammo"		"None"

	"weight"			"5"
	"item_flags"			"0"

	"BuiltRightHanded" 		"1"
	"AllowFlipping" 		"0"

	"csviewmodel"			"0"

	"ironsightoffset"
	{
		"x"   "-11"
		"y"   "-6.675"
		"z"   "3.8"
   	}

	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	"SoundData"
	{
		"empty"			"Default.ClipEmpty_RifleDOD"
		"single_shot"		"Weapon_Kar.Shoot"

		// NPC SECTION
		"single_shot_npc"	"Weapon_Kar.Shoot"
		"reload_npc"		"Weapon_K98.WorldReload"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	"TextureData"
	{
		"weapon"
		{
				"file"		"sprites/hud/dod_weapons01"
				"x"			"324"
				"y"			"320"
				"width"		"188"
				"height"	"40"
		}
		"weapon_s"
		{	
				"file"		"sprites/hud/dod_weapons01"
				"x"			"324"
				"y"			"320"
				"width"		"188"
				"height"	"40"
		}
		"ammo"
		{
				"file"		"sprites/hud/clips_bullets"
				"x"			"95"
				"y"			"0"
				"width"		"65"
				"height"	"36"
		}
		"crosshair"
		{
				"file"		"vgui/crosshairs/crosshair1"
				"x"			"0"
				"y"			"0"
				"width"		"64"
				"height"	"64"
		}
		"autoaim"
		{
				"file"		"vgui/crosshairs/crosshair1"
				"x"			"0"
				"y"			"0"
				"width"		"64"
				"height"	"64"
		}
	}
	"Advanced" // only for Weapon_Scripted
	{

	// **Primary Attack**

		// 0 = none, 1 = Basic Bullet, 2 = Burst, 3 = shotgun, 4 = autoshotgun, 5 = laser, 6 = warp
		"FireType1"	"1"


		// Rate of Weapons Fire ( Not for laser or warp )
		"FireRate1"	"1.6"

		// Allow a refire as fast as the player can click, Basic Bullet Only.
		"FastFire1"	"0"

		// Allow Fire Underwater?
		"FireUnderWater1"	"0"

		// For Bullet accuracy
		"FireCone1"		"1" // Starting Value ( 0-20 )
		"FireConeLerp1"		"1" // Bool
		"FireConeLerpto1"	"3" // Value to lerp accuracy too ( 0-20 )

	// **Secondary Attack**

		// 0 = none, 1 = Basic Bullet, 2 = Burst, 3 = shotgun, 4 = autoshotgun, 5 = laser, 6 = warp, 8 = scope
		"FireType2"	"0"

	// **Global Weapon Settings**

		// Number of Recoil Animations, weapon models with ACT_VM_RECOIL animations.
		// ( Only if a weapon has recoil animations, Not for laser or warp )
		"NumberOfRecoilAnims"	"0"

		// Shots fired till next recoil animation. ( Only if a weapon has recoil animations )
		"RecoilIncrementSpeed"	"1"

		// 1 = Pistol, 2 = AR2, 3 = crossbow, 4 = physgun, 5 = shotgun, 6 = smg1
		// This is the player animation set that will be used on all players you view ingame.
		"PlayerAnimationSet"	"2"

		//Tracer type ( Bullet weapons only )
		// 0 = none, 1 = normal, 2 = strider, 3 = ar2, 4 = helicopter, 5 = Gunship, 6 = Gauss, 7 = Airboat
		"TracerType"	"1"

		//Tracer Frequency
		"TracerFrequency"	"2"

		// Bullet Impact Effect, 0 = none, 1 = normal, 2 = AR2, 3 = jeep, 4 = Gauss, 5 = airboat, 6 = helicopter
		"ImpactEffect"	"1"

		// Sniper Scope Settings
		"UseScopedFireCone"	"0"

	}
}