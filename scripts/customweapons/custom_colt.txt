"WeaponData"
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"			"1911 Colt"
	"viewmodel"			"models/weapons/v_colt.mdl"
	"playermodel"			"models/weapons/w_colt.mdl"
	"anim_prefix"			"pistol"
	"bucket"			"1"
	"bucket_position"		"16"

	"clip_size"			"7"
	"clip2_size"			"0"

	"default_clip"			"7"
	"default_clip2"			"0"

	"primary_ammo"			"colt_ammo"
	"secondary_ammo"		"None"

	"weight"			"5"
	"item_flags"			"0"

	"BuiltRightHanded" 		"1"
	"AllowFlipping" 		"0"

	"csviewmodel"			"0"

	"ironsightoffset"
	{
		"x"   "-8"
		"y"   "-3.85"
		"z"   "3.7"
		"xori"	"-0.2"
   	}

	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	"SoundData"
	{
		"empty"			"Default.ClipEmpty_RifleDOD"
		"single_shot"		"Weapon_Colt.Shoot"

		// NPC SECTION
		"single_shot_npc"	"Weapon_Colt.Shoot"
		"reload_npc"		"Weapon_Colt.WorldReload"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	"TextureData"
	{
		"weapon"
		{
				"file"		"sprites/hud/dod_weapons01"
				"x"			"193"
				"y"			"0"
				"width"		"90"
				"height"	"60"
		}
		"weapon_s"
		{	
				"file"		"sprites/hud/dod_weapons01"
				"x"			"193"
				"y"			"0"
				"width"		"90"
				"height"	"60"
		}
		"ammo"
		{
				"file"		"sprites/hud/clips_bullets"
				"x"			"242"
				"y"			"390"
				"width"		"73"
				"height"	"122"
		}
		"crosshair"
		{
				"file"		"vgui/crosshairs/crosshair1"
				"x"			"0"
				"y"			"0"
				"width"		"64"
				"height"	"64"
		}
		"autoaim"
		{
				"file"		"vgui/crosshairs/crosshair1"
				"x"			"0"
				"y"			"0"
				"width"		"64"
				"height"	"64"
		}
	}
	"Advanced" // only for Weapon_Scripted
	{

	// **Primary Attack**

		// 0 = none, 1 = Basic Bullet, 2 = Burst, 3 = shotgun, 4 = autoshotgun, 5 = laser, 6 = warp
		"FireType1"	"1"


		// Rate of Weapons Fire ( Not for laser or warp )
		"FireRate1"	"0.4"

		// Allow a refire as fast as the player can click, Basic Bullet Only.
		"FastFire1"	"1"

		// Allow Fire Underwater?
		"FireUnderWater1"	"0"

		// For Bullet accuracy
		"FireCone1"		"4" // Starting Value ( 0-20 )
		"FireConeLerp1"		"1" // Bool
		"FireConeLerpto1"	"6" // Value to lerp accuracy too ( 0-20 )

	// **Secondary Attack**

		// 0 = none, 1 = Basic Bullet, 2 = Burst, 3 = shotgun, 4 = autoshotgun, 5 = laser, 6 = warp, 8 = scope
		"FireType2"	"0"

	// **Global Weapon Settings**

		// Number of Recoil Animations, weapon models with ACT_VM_RECOIL animations.
		// ( Only if a weapon has recoil animations, Not for laser or warp )
		"NumberOfRecoilAnims"	"0"

		// Shots fired till next recoil animation. ( Only if a weapon has recoil animations )
		"RecoilIncrementSpeed"	"1"

		// 1 = Pistol, 2 = AR2, 3 = crossbow, 4 = physgun, 5 = shotgun, 6 = smg1
		// This is the player animation set that will be used on all players you view ingame.
		"PlayerAnimationSet"	"1"

		//Tracer type ( Bullet weapons only )
		// 0 = none, 1 = normal, 2 = strider, 3 = ar2, 4 = helicopter, 5 = Gunship, 6 = Gauss, 7 = Airboat
		"TracerType"	"1"

		//Tracer Frequency
		"TracerFrequency"	"2"

		// Bullet Impact Effect, 0 = none, 1 = normal, 2 = AR2, 3 = jeep, 4 = Gauss, 5 = airboat, 6 = helicopter
		"ImpactEffect"	"1"

		// Sniper Scope Settings
		"UseScopedFireCone"	"0"

	}
}