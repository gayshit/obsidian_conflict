"WeaponData"
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"			"M1 Carbine"
	"viewmodel"			"models/weapons/oc_nacht/v_m1carbine.mdl"
	"playermodel"			"models/weapons/oc_nacht/w_m1carb.mdl"
	"anim_prefix"			"ar2"
	"bucket"			"2"
	"bucket_position"		"19"

	"clip_size"			"15"
	"clip2_size"			"-1"

	"default_clip"			"150"
	"default_clip2"			"-1"

	"primary_ammo"			"m1carbine_ammo"
	"secondary_ammo"		"None"

	"weight"			"0"
	"item_flags"			"0"

	"BuiltRightHanded"		"1"
	"AllowFlipping"			"0"

	"csviewmodel"			"1"

   	"ironsightoffset"
	{
		"x"   "-7"
		"y"   "-6.84"
		"z"   "3.3"
   	}


	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	"SoundData"
	{
		
		"empty"			"Default.ClipEmpty_RifleDOD"
		"single_shot"		"Weapon_Carbine.Shoot"
		
		"single_shot_npc"	"Weapon_Carbine.Shoot"
		"reload_npc"		"Weapon_Pistol.NPC_Reload"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	"TextureData"
	{
		"weapon"
		{
				"file"		"sprites/hud/dod_weapons01"
				"x"			"0"
				"y"			"200"
				"width"		"192"
				"height"	"44"
		}
		"weapon_s"
		{	
				"file"		"sprites/hud/dod_weapons01"
				"x"			"0"
				"y"			"200"
				"width"		"192"
				"height"	"44"
		}
		"ammo"
		{
				"file"		"sprites/hud/clips_bullets"
				"x"			"300"
				"y"			"0"
				"width"		"70"
				"height"	"155"
		}
		"crosshair"
		{
				"file"		"vgui/crosshairs/crosshair1"
				"x"			"0"
				"y"			"0"
				"width"		"64"
				"height"	"64"
		}
		"autoaim"
		{
				"file"		"vgui/crosshairs/crosshair1"
				"x"			"0"
				"y"			"0"
				"width"		"64"
				"height"	"64"
		}
	}
	"Advanced" // only for Weapon_Scripted
	{

	// **Primary Attack**

		// 0 = none, 1 = Basic Bullet, 2 = Burst, 3 = shotgun, 4 = autoshotgun, 5 = laser, 6 = warp
		"FireType1"	"1"


		// Rate of Weapons Fire ( Not for laser or warp )
		"FireRate1"	"0.4"

		// Allow a refire as fast as the player can click, Single Only.
		"FastFire1"	"1"

		// Allow Fire Underwater?
		"FireUnderWater1"	"1"

		// For Bullet accuracy
		"FireCone1"		"2" // Starting Value ( 0-20 )
		"FireConeLerp1"		"0" // Bool

	// **Secondary Attack**

		// 0 = none, 1 = Basic Bullet, 2 = Burst, 3 = shotgun, 4 = autoshotgun, 5 = laser, 6 = warp, 8 = scope
		"FireType2"	"0"

	// **Global Weapon Settings**

		// Number of Recoil Animations, weapon models with ACT_VM_RECOIL animations.
		// ( Only if a weapon has recoil animations, Not for laser or warp )
		"NumberOfRecoilAnims"	"0"

		// Shots fired till next recoil animation. ( Only if a weapon has recoil animations )
		"RecoilIncrementSpeed"	"1"

		// 1 = Pistol, 2 = AR2, 3 = crossbow, 4 = physgun, 5 = shotgun, 6 = smg1
		// This is the player animation set that will be used on all players you view ingame.
		"PlayerAnimationSet"	"2"

	}
}