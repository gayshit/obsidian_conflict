"WeaponData"
{
	"printname"			"Scout"
	"viewmodel"			"models/weapons/v_snip_scout.mdl"
	"playermodel"			"models/weapons/w_snip_scout.mdl"
	"anim_prefix"			"ar2"
	"bucket"			"3"
	"bucket_position"		"5"
	"clip_size"			"10"
	"clip2_size"			"0"
	"default_clip"			"10"
	"default_clip2"			"0"
	"primary_ammo"			"SniperRound"
	"secondary_ammo"		"None"
	"weight"			"3"
	"item_flags"			"0"
	"BuiltRightHanded" 		"0"
	"AllowFlipping" 		"1"
	"csviewmodel"			"1"

	"SoundData"
	{
		"reload_npc"		"Weapon_SniperRifle.NPC_Reload"
		"empty"			"Default.ClipEmpty_Rifle"
		"single_shot"		"Weapon_Scout.Single"
		"single_shot_npc"	"Weapon_Scout.Single"
		"special1"		"Default.Zoom"
		"special2"		"Default.Zoom"
	}

	"TextureData"
	{
		"weapon"
		{
				"font"		"CSWeaponIcons"
				"character"	"n"
		}
		"weapon_s"
		{	
				"font"		"CSWeaponIconsSelected"
				"character"	"n"
		}
		"ammo"
		{
				"font"		"ObsidianWeaponIcons"
				"character"	"s"
		}
		"crosshair"
		{
				"font"		"Crosshairs"
				"character"	"Q"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"		"0"
				"y"		"48"
				"width"		"24"
				"height"	"24"
		}
	}

	"Advanced"
	{
		"FireType1"	"1"
		"FireRate1"	"1.0"
		"FastFire1"	"0"
		"FireUnderWater1"	"0"

		"FireCone1"		"3"
		"FireConeLerp1"		"1"
		"FireConeLerpto1"	"4"

		"FireType2"	"7"
		"FireRate2"	"1.0"
		"FastFire2"	"0"
		"FireUnderWater2"	"0"
		"SecondaryAmmoUsed"	"0"

		"NumberOfRecoilAnims"	"0"
		"RecoilIncrementSpeed"	"1"
		"PlayerAnimationSet"	"2"

		"UseScopedFireCone"	"1"
		"ScopedFireCone"	"0"
		"ScopedColorR"		"0"
		"ScopedColorG"		"0"
		"ScopedColorB"		"0"
	}
}