// Python

WeaponData
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"			"Colt Python .357"
	"viewmodel"			"models/weapons/v_bluepython.mdl"
	"playermodel"		"models/weapons/w_blupython.mdl"
	"anim_prefix"		"python"
	"bucket"			"1"
	"bucket_position"	"15"

	"clip_size"			"6"
	"default_clip"		"30"
	"primary_ammo"		"357"
	"secondary_ammo"	"None"

	"weight"		"6"
	"item_flags"		"0"
	"ITEM_FLAG_NOAUTOSWITCHEMPTY"	"1"

	 "BuiltRightHanded"		"1"
	"AllowFlipping"			"1"

	"csviewmodel"			"0"

	"ironsightoffset"
	{
	"x"   "-6"
	"y"   "-3.2"
	"z"   "2"
	}

	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	SoundData
	{
		"reload_npc"		"Weapon_Pistol.NPC_Reload"
		"empty"			"Weapon_Pistol.Empty"
		"single_shot"		"Weapon_357.Single"
		"single_shot_npc"	"Weapon_357.Single"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"font"		"WeaponIcons"
				"character"	"e"
		}
		"weapon_s"
		{	
				"font"		"WeaponIconsSelected"
				"character"	"e"
		}
		"ammo"
		{
				"font"		"WeaponIcons"
				"character"	"q"
		}
		"crosshair"
		{
				"font"		"Crosshairs"
				"character"	"Q"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"			"0"
				"y"			"48"
				"width"		"24"
				"height"	"24"
		}
	}
"Advanced" // only for Weapon_Scripted
	{

	// **Primary Attack**

		// 0 = none, 1 = Basic Bullet, 2 = Burst, 3 = shotgun, 4 = autoshotgun, 5 = laser, 6 = warp
		"FireType1"	"1"


		// Rate of Weapons Fire ( Not for laser or warp )
		"FireRate1"	"0.6"

		// Allow a refire as fast as the player can click, Single Only.
		"FastFire1"	"0"

		// Allow Fire Underwater?
		"FireUnderWater1"	"0"

		// For Bullet accuracy
		"FireCone1"		"1" // Starting Value ( 0-20 )
		"FireConeLerp1"		"0" // Bool
		"FireConeLerpto1"	"1" // Value to lerp accuracy too ( 0-20 )

	// **Secondary Attack**

		// 0 = none, 1 = Basic Bullet, 2 = Burst, 3 = shotgun, 4 = autoshotgun, 5 = laser, 6 = warp, 8 = scope
		"FireType2"	"0"

		// Rate of Weapons Fire ( Not for laser or warp )
		"FireRate2"	"0.8"

		// Allow a refire as fast as the player can click, Single Only.
		"FastFire2"	"0"

		// How many bullets in a burst. ( For Burst Weapon Only! )
		"BurstAmount2"	"3"
		"BetweenBurstTime2"	"0.05"

		// Allow Fire Underwater?
		"FireUnderWater2"	"1"

		// For Bullet accuracy
		"FireCone1"		"2" // Starting Value ( 0-20 )
		"FireConeLerp1"		"1" // Bool
		"FireConeLerpto1"	"3" // Value to lerp accuracy too ( 0-20 )

	// **Global Weapon Settings**

		// Number of Recoil Animations, weapon models with ACT_VM_RECOIL animations.
		// ( Only if a weapon has recoil animations, Not for laser or warp )
		"NumberOfRecoilAnims"	"0"

		// Shots fired till next recoil animation. ( Only if a weapon has recoil animations )
		"RecoilIncrementSpeed"	"1"

		// 1 = Pistol, 2 = AR2, 3 = crossbow, 4 = physgun, 5 = shotgun, 6 = smg1
		// This is the player animation set that will be used on all players you view ingame.
		"PlayerAnimationSet"	"1"


	}
}