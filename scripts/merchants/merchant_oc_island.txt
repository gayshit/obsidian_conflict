"merchant_oc_island"
{
	"Name"	"Bariga"

	"Weapons"
	{
		"weapon_crossbow" "50"
		"weapon_pistol" "10"
		"weapon_shotgun" "80"
	}
	"Items"
	{
		"item_ammo_pistol" "2"
		"item_box_buckshot" "4"
		"item_ammo_crossbow" "5"
	}
}