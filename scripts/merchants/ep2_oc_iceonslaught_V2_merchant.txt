"ep2_oc_iceonslaught_V2_merchant"
{
	"Name"	"Billy"

	"Weapons"
	{
		"weapon_crossbow" "25"
		"weapon_slam" "3"
		"weapon_ar2" "25"
		"weapon_frag" "3"
		"weapon_357" "10"
		"weapon_gauss" "30"
		"weapon_manhack" "5"
	}
	"Items"
	{
		"item_ammo_357" "2"
		"item_ammo_ar2_large" "2"
		"item_box_alyxrounds" "2"
		"item_ammo_tau" "3"
		"item_ammo_smg1_grenade" "3"
		"item_ammo_crossbow" "3"
		"item_ammo_ar2_altfire" "4"
		"item_cloak" "20"
		"item_shield" "30"
	}
}